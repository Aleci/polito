clear all; close all; clc; addpath("Library")
s=tf('s');
om=logspace(-4,3,1000);
% Data
ωp=0.02;
ωs=40;
G=12.5/(s * (1+s/.8) * (1+s/2.5));
Ga=0.095;

% Paper calculations for this
Kd=1;
S0=0.15;   % this come from S2, the stronger constraint on S*(0)
SLF=25e-3; % this come from S4, the stronger constraint on S(ω<ωp)
THF=5e-3;  % this come from S5, the stronger constraint on T(ω>ωs)

% Overshoot
zitaMin=convOvershoot(0.1);
Tp=getTp(zitaMin);
Sp=getSp(zitaMin);

% Speed requirements
ωc1=convRiseTime(3,zitaMin);
ωc2=convSettlingTime(12,0.05,zitaMin);
ωc=max(ωc1,ωc2);

% Lower order approximation of the constraints
w=makeweight(db2mag(-80),[ωp SLF], 1, 0,2); w=tf(w); % LF tight
w2=makeweight(.9,ωc,Sp,0,1); w2=tf(w2);
Ws1=minreal(w*w2*(s+1e-6)*(s+2.5e-3)/(s^2 + 0.001789*s + 1.6e-06),1e-3); % remove a zero and add a derivatior
% Higher order approximation of the constraints
w=makeweight(db2mag(-60),[ωp SLF], 1, 0,3); w=zpk(tf(w)) % LF tight
w2=makeweight(.9,ωc,Sp,0,1); w2=zpk(tf(w2))        % HF tight
zer=zero(w); Ws1=minreal(w*w2*(s+1e-5)/(s-zer(1)),1e-3); % remove a zero and add a derivatior
% Higher order approximation of the relaxed constraint for RP of S3 only (-55 instead of -60)
Ws11=minreal(Ws1/(s+0.006841)*(s+0.011),1e-3); % delay a zero to relax the constrant on S0
%S0=0.218; Ws1=Ws11; % debug for the relaxed weight

mag=squeeze(bode(Ws1,om));
figure(1), semilogx(om,mag2db(mag)), title("Weight on S"), hold on
mag=squeeze(bode(s*S0,om)); semilogx(om,mag2db(mag)); 
stairs([om(1),ωp,ωc,om(end)],[mag2db(SLF),0,mag2db(Sp),mag2db(Sp)]), hold off;

w=makeweight(Tp,[ωs THF],THF/100,1e-6,2); w=tf(w); w=d2c(w,'matched');
[n,d]=tfdata(w); Wt1=tf(n{1}(3),d{1}); % remove zeros
mag=squeeze(bode(Wt1,om));
figure(2), semilogx(om,mag2db(mag)), title("Weights on T"), hold on
stairs([om(1),ωs,om(end)],[mag2db(Tp),mag2db(THF),mag2db(THF)])

% Perform a gridding of the system set
K=linspace(9,16,5);
p1=linspace(0.55,1.05,5);
p2=linspace(1.9,3.1,5);
% Find the maximum frequency response of the system set
maxMag=-ones(size(om'));
for ii=1:5
  for jj=1:5
    for kk=1:5
      sys=K(ii)/(s * (1+s/p1(jj)) * (1+s/p2(kk)));
      mag=squeeze(bode(sys,om));
      %semilogx(om,mag2db(mag),'r');
      maxMag=max(maxMag,mag);
    end
  end
end
% Actual maximum response
%semilogx(om,mag2db(maxMag),'k');

% Nominal response
mag=squeeze(bode(G,om));

% Multiplicative error 
err=maxMag./mag - 1;
%% Fit the multiplicative error with a system 
magPack=vpck(err,om);
w=magfit(magPack,[0.001,1,1,3]);
[Au,Bu,Cu,Du]=unpck(w);
Wu=zpk(ss(Au,Bu,Cu,Du));
% Plot fitted response
mag=squeeze(bode(1/Wu,om)); semilogx(om,mag2db(mag),'m');

% find the maximum non conservative error of the fitting
%mag=squeeze(bode(Wu,om));
maxEfit=max(maxMag-mag) % good weight if it is small and negative

% debug the actual fit between Wu and err
figure, semilogx(om,mag2db(err),'k*'), hold on, semilogx(om,mag2db(mag),'r');

W1=1/Ws1;
pol=2.7;
w21=Tp/(1+s/pol)^2;
mag=squeeze(bode(w21,om));
semilogx(om,mag2db(mag),'g')
W2=1/Tp; % Attach 2 derivative actions in pol

%% Optimize the hinfinity controller
sys=Ga*G; % the loop without the controller
[sA,sB,sC,sD]=linmod("unstrSim1");
P=ltisys(sA,sB,sC,sD);
P=sderiv(P,2,[1/pol 1]);         % Attach the non causal dynamics of W2 to output z2
P=sderiv(P,2,[1/pol 1]);         % Attach the non causal dynamics of W2 to output z2
[~,Cmod]=hinflmi(P,...       % Generalized plant model    
                 [1 1],...   % n controlled outputs, n controlled inputs
                 0,...       % find best controller for the given tolerance (min gamma)
                 1e-2,...    % tolerance
                 [0 0 0 1]); % [enhanceDamping, , ,suppressOutput]
[Ac,Bc,Cc,Dc]=ltiss(Cmod);
Copt=minreal(zpk(ss(Ac,Bc,Cc,Dc)),1e-3);

%% Modify controller to satisfy performances
% Custom loopshaping
n=[0,0,1.197767148921637,4.889654482068693,5.556338569902091, ...
    2.102884757438709,0.145050183321155,0.004718982672705]*1e7;
d=[0.000000100000000,0.000007657178317,0.010388817878492,0.278162265736638, ...
   1.512795248329021,1.197944571906810,0.031067033097565,0.000779557184684]*1e7;
C=tf(n,d);

% Checks
L=minreal(C*Ga*G,1e-3);
S=minreal(1/(1+L),1e-3);
T=minreal(1-S,1e-3);
t=linspace(0,1,100000);

% Check nominal frequency requirements       % OK
mag=squeeze(bode(T,om));
semilogx(om,mag2db(mag),'r'), hold off
legend("Wt^{-1}","NP-S8,S5","Wu^{-1}","W2^{-1}","T")
%
nichols1(L,Tp,Sp,[ωp ωc ωs])

%% Check nominal steady state requirement        % OK, S2 satisfied
fdte=minreal(1/s*(Kd-T),1e-3);
figure,step(fdte,1000)
title("Steady state following error"), yline(0.15), yline(-0.15)
followingErr=dcgain(fdte)

%% Check nominal transient requirements
figure, step(T), title("Transient")     % OK, S6-S7-S8 satisfied
xline(3), xline(12), yline(.95), yline(1.05), yline(1.1)
stepinfo(T)

%% Check nominal actuation disturbance rejection    % OK, S3 satisfied
fdtda=5.5e-3*minreal(G*S,1e-3);
figure, step(fdtda,1000)
title("Actuation disturbance"),yline(1.5e-2), yline(-1.5e-2)
actuationErr=dcgain(fdtda)

%% Check nominal performance for plant disturbance rejection
figure, lsim(S,2e-2*sin(ωp*t*1000),t*1000) % OK, S4 satisfied
title("Plant disturbance"), yline(5e-4), yline(-5e-4);

%% Check nominal performance for sensor disturbance rejection
figure, lsim(T,0.1*sin(ωs*t*10),t*10) % OK, Sp4 satisfied
title("Sensor disturbance"), yline(5e-4), yline(-5e-4);

%% Check robust stability
magStab=squeeze(bode(Wu*T,om));
figure,semilogx(om,mag2db(magStab)) , title("Robust Stability")    % OK, RS

%% Plot S for visual check
mag=squeeze(bode(S,om));
figure(1), hold on, semilogx(om,mag2db(mag),"g")         
legend("W1^{-1}","NP-S2,S3","NP-S4,S6,S7,S8","S","location","southeast")

%% Robust performance for plant disturbance rejection
mag=squeeze(bode(S*1/Ws1,om));      % FAIL, no result on RP for S4
figure, semilogx(om,mag2db(abs(mag)+abs(magStab))), title("Robust plant rejection")
%% Robust performance for actuation disturbance rejection
mag=squeeze(bode(S*1/Ws11,om));     % FAIL, no result on RP for S3
figure, semilogx(om,mag2db(abs(mag)+abs(magStab))), title("Robust actuation rejection")

% Compute elementary unstructured uncertainty models data
Kn=mean(K);   Wk=K(end)-Kn;
p1n=mean(p1); Wp1=p1(end)-p1n;
p2n=mean(p2); Wp2=p2(end)-p2n;

% Check robust performance for S4
WmuS=1/Ws1;
deltaset=[-1 0;-1 0;-1 0;1 0]; %[port1; port2; .... portn]
% portx=[-1, 0] if the input/output x is related to a real delta  (Wx=gain)
% portx=[ 1, 0] if the input/output x is related to a complex delta (Wx=transfer function)

[A,B,C,D]=linmod('strRPonS_Sim1');
N=pck(A,B,C,D);
Nf=frsp(N,om);
mubnds=mu(Nf,deltaset);
vplot('liv,m',mubnds);
legend('\mu upper bound','\mu lower bound')
xlabel('\ω [rad/s]');ylabel('magnitude');
