close all; clear ; clc
omegaSamp=logspace(-3,3,10000);
s=tf('s');

Kd=1;                                 % OK                   
r=1/s^2;                 rhor=1.5e-1; % OK
da0=5.5e-3;              rhoa=1.5e-2; % OK
dp0=2e-2;   omegap=0.02; rhop=5e-4;
ds0=0.1;    omegas=40;   rhos=5e-4;
tr=3;
tsetl5=12;
ov=0.1; % in percentage
%
% Problem data
%
G=12.5/(s+0.8)/(s+2.5); % Nominal system
Ga=.095;

fprintf("========== Requirements ==========\n");

% Closed loop gain
Gf=1

% The system requires at least one pole in the origin to follow the ramp
v=1, p=0; h=1;

% S(0)<=rhor;

% Since we have an integrator before the actuator step disturbance, we can totally reject it

% Plant disturbance
SatOmegapMinor=rhop/dp0
% S(omega<omegap)<=rhop/dp0;

% Sensor disturbance
TatOmegasMinor=rhos/ds0
% T(omega>omegas)<=rhos/ds0;

% Max overshoot
zita=abs(log(ov))/sqrt(pi^2+log(ov)^2);
Sp=2*zita*sqrt(2+4*zita^2+2*sqrt(1+8*zita^2))/(sqrt(1+8*zita^2)+4*zita^2-1)
Tp=1/(2*zita*sqrt(1-zita^2))
% T(omega)<=Tp;
% S(omega)<=Sp;

% Rise time
val=sqrt(sqrt(1-4*zita^4)-2*zita^2);
omegaCut1 = (pi-acos(zita))/(tr*sqrt(1-zita^2))*val; % min omegaCut for L

% Settling time
omegaCut2 = -log(.05)/(tsetl5*zita)*val; % min omegaCut for L

omegaCut = max(omegaCut1,omegaCut2) % select the more stringent constraint
% S(omegaCut)==0;  % the omegaCut of S is a lower bound for the omegaCut of L

%% Create constraint of second order on S
ws=makeweight(SatOmegapMinor*0.0001,[omegap SatOmegapMinor],Sp,0.000001,2); 
ws=d2c(ws,"matched");
[n,d]=tfdata(ws);
epsilon=omegap*.01; % stable placeholder for a zero in zero
num=conv([1 omegap*.1],[1 epsilon]); % replace the zero in zero with the placeholder
ws=Sp*tf(num,d)
oms=logspace(-3,2,1000);
magCheck=squeeze(bode(ws,oms));
figure(1),semilogx(oms,mag2db(magCheck)) , xline(omegap), yline(mag2db(SatOmegapMinor));
ws=1/ws;

%% Create a weight for an ustructured multiplicative uncertainty

% Approximate the model set with a gridding of the pui
% and collect the maximum frequency response
maxMag=zeros(size(omegaSamp))';
mset=[];
figure
for K=linspace(9,16,5)
  for p1=linspace(.55,1.05,5)
    for p2=linspace(1.9,3.1,5)
      sys=K/(s+p1)/(s+p2);
      mag=squeeze(bode(sys,omegaSamp));
      semilogx(omegaSamp,mag2db(mag),'k'),hold on;
      maxMag=max(maxMag, mag);
      mset=[mset sys];
    end
  end
end
% plot maximum frequency response
semilogx(omegaSamp,mag2db(maxMag),'b')
% select a transfer function to fit the maximum frequency response
magPack=vpck(maxMag,omegaSamp);
wu=magfit(magPack,[1,0.1,1,3]);
[Au,Bu,Cu,Du]=unpck(wu);
wu=zpk(ss(Au,Bu,Cu,Du));
[n,d]=tfdata(wu);
wu=dcgain(wu)*tf(d{1}(3),d{1}) % remove zeros

% plot the fitted tf response
wumag=squeeze(bode(wu,omegaSamp));
semilogx(omegaSamp,mag2db(wumag),'r')
% find the maximum non conservative error of the fitting
maxEfit=max(maxMag-wumag)
% final weight
wu=1/wu;

%% Create constraint of second order on T 2 deg
wt=tf(makeweight(Tp,[omegas TatOmegasMinor],0.001,0.0001,2));
wt=d2c(wt,"matched");
[~,dT]=tfdata(wt);
rootden=roots(dT{1});
rootden=real(rootden(1));
rootden=-2.7; % adjust the pole position to loose the constraint
wtComplete=Tp*(rootden/(s+rootden))^2; % zeros not needed, and replace second order poly with two first order ones
magWt=squeeze(bode(wtComplete,omegaSamp));
semilogx(omegaSamp,mag2db(magWt),'g');
xline(omegas); yline(mag2db(Tp)); yline(mag2db(TatOmegasMinor));
wt=Tp; % remove the poles ( only poles will be not causal if reversed)
wt=1/wt;

% Wt is always more strict than Wu

%% Optimize the hinfinity controller
sys=Ga*G; % the loop without the controller
[sA,sB,sC,sD]=linmod("unstructuredSim1");
P=ltisys(sA,sB,sC,sD);
P=sderiv(P,2,[1/rootden 1]);         % Attach the non causal dynamics of wt to output z2
P=sderiv(P,2,[1/rootden 1]);         % Attach the non causal dynamics of wt to output z2
[~,Cmod]=hinflmi(P,...       % Generalized plant model    
                 [1 1],...   % n controlled outputs, n controlled inputs
                 0,...       % find best controller for the given tolerance (min gamma)
                 1e-2,...    % tolerance
                 [0 0 0 1]); % [enhanceDamping, , ,suppressOutput]
[Ac,Bc,Cc,Dc]=ltiss(Cmod);
Cmod=zpk(ss(Ac,Bc,Cc,Dc));

% Restore the integrator in the controller
pol=pole(Cmod);
C=minreal(Cmod/s*(min(abs(pol))+s))

%% Real loop, sensitivity and complementary sensitivity functions
L=C*Ga*G;
S=1/(1+L);
T=L*S;

% Approximated bounds satisfaction
magT=squeeze(bode(T,omegaSamp));
semilogx(omegaSamp,mag2db(magT),'m'), hold off;
title("Weighting functions on T")
%
magS=squeeze(bode(S,omegaSamp));
figure(1), hold on, semilogx(omegaSamp,mag2db(magS),'r'), hold off
title("Weighting function on S")
%
nichols1(L,Tp,Sp,[omegaCut omegap omegas]) % OK

%% Time domain check for rise time and settling time
figure, step(L/(1+L)) %OK   Only time domain check are correct checks for performances
title("Step reference transient"), yline(1.05), yline(.95), xline(3), xline(12)

%% Time domain check for plant disturbance reduction
t=linspace(0,1,10000)*700;
figure, lsim(S,dp0*sin(omegap*t),t), title("Plant disturbance")
yline(rhop), yline(-rhop)

%% Time domain check for sensor disturbance reduction
t=linspace(0,1,10000)*10;
figure, lsim(T,ds0*sin(omegas*t),t), title("Sensor disturbance")
yline(rhos), yline(-rhos)

%% Robust performance satisfaction for polynomial actuation disturbance
step(G/(1+L)),title("Actuation disturbance rejection") % ALWAYS SATISFIED WITH 1 POLE IN 0

%% Robust stability satisfaction (unstructured case)
[MAG_STAB,PHASE] = bode(wu*T,omegaSamp); MAG_STAB=squeeze(MAG_STAB);
figure, semilogx(omegaSamp,MAG_STAB), title("Robust stability (unstructured case)"); % SATISFIED

%% Robust stability satisfaction (structured case)

%% Robust performance satisfaction for sinusoidal plant disturbance (unstructured case)
[MAG,PHASE] = bode(ws*S,omegaSamp); MAG=squeeze(MAG);
figure, semilogx(omegaSamp,MAG+MAG_STAB); % NOT SATISFIED IN THIS TEST
legend('||W_s(s)S(s)|+|W_u(s)T(s)||'); title("Robust performance")

%% Robust performance satisfaction for sinusoidal plant disturbance (structured case)
Wmu=wtComplete;
Gc=C*Ga;
deltaset=[-4 0];
[An,Bn,Cn,Dn]=linmod('structuredSim1');
N=pck(An,Bn,Cn,Dn);
rspns=frsp(N,omegaSamp);
mub=mu(rspns,deltaset);
vplot('liv,m',mub);
