% Convert the time domain specification in terms of cutting frequency
function omegacMin=convSettlingTime(Ts,alpha,zita)
  omegacMin=-log(alpha)/(Ts*zita)*sqrt(sqrt(1+4*zita^4)-2*zita^2);
end
