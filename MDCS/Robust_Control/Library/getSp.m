% Compute the Sensitivity peak from the given zita.
function Sp=getSp(zita)
  num=2*zita*sqrt(2+4*zita^2+2*sqrt(1+8*zita^2));
  den=sqrt(1+8*zita^2)+4*zita^2-1;
  Sp=num/den;
end
