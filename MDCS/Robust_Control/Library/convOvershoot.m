% Convert the time domain specification in terms of required damping
% Note: In order to apply the computed zita is necessary to express it in terms of Sensitivity and Resonance peaks
function zitaMin=convOvershoot(Shat)
  zitaMin=abs(log(Shat))/sqrt((log(Shat)^2+pi^2));
end
