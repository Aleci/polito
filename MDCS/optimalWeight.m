clear all, close all, clc
pkg load optim %octave directive


N=50;
n=3; % deg 2
t=[1:50];
req=[zeros(10,1); ones(40,1)];
phi=[t'.^2,t',ones(50,1)];


A=[phi, zeros(N,N); -phi, -eye(N); zeros(N,n) -eye(N)];
B=[req; -req; zeros(N,1)];
c=[zeros(1,n), ones(1,10), 50*ones(1,10), ones(1,N-20)]';

opt=linprog(c,A,B);
w=opt(1:n);

plot(t,req,t,phi*w),legend("req","opt")
