% Plot nichols chart of G with closed loop constant magnitude loci Tp and Sp.
% The vector of omegas of particular interest will be explicitly rendered in magenta.
% A handy choice of omegaPar can be [omegaCmin, omegaCmax, omegaDisturbSinusoidalPlant, omegaDisturbSinusoidalSensor]
% NOTE: In case of negative phase/gain margin the omegaParticular is not correctly plotted
function nichols1(G,Tp,Sp,omegaPar)
  [m,p]=bode(G,omegaPar);
  figure
  myngridst(Tp,Sp)
  hold on
  [mag,pha,omega]=nichols(G);
  nichols(G,"k")
  l=length(omega);
  for k=1:floor(l/10):l
    text(pha(k),mag2db(mag(k)),num2str(omega(k)))
  end
  for k=1:length(omegaPar)
   set (text(p(k),mag2db(m(k)),sprintf("%.2f:(%.2f,%.2f)",omegaPar(k),p(k),mag2db(m(k)))), "color", "magenta")
  end
  hold off
end
