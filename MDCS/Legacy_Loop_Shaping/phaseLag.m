% Generate a phase lag network
% p can be viewed as the ratio between the omega at which the effect should manifest and the relative omega selected from the tables
% m is the desired zero/pole spacind selected from the tables
function sys=phaseLag(p,m,s)
  sys=(1+s/(m*p)) / (1+s/p);
end
