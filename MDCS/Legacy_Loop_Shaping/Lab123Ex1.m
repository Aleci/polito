clear all; close all; clc
pkg load symbolic
pkg load control
syms s
% Data
Ga=.095;
Gp=25/(s*(s^2+3.3*s+2));

% Ramp error <= 1.5e-1
Eramp=1.5e-1;
syms K positive; % steady state controller just proportional because the nonzero error
Gf=1;% => Kc=1 => (S1)
G=simplify(K*Ga*Gp); %Plant with unitary feedback
tfe=simplify( 1/(1+G) ); %Error transfer function 
steadyStateError=subs(s*tfe*1/s^2,s,0); %Final value theorem
RampCond=solve(steadyStateError<=Eramp,K);

% Actuation disturbance <= 1.5e-2
maxDa=1.5e-2;
da=5.5e-3; %max step disturbance
tfda=simplify( Gp/(1+Gp*K*Ga) ); %Da transfer function
steadyStateEffectDa=subs(s*tfda*da/s,s,0); %Final value theorem
DaCond=solve(steadyStateEffectDa<=maxDa,K);

% Select static gain of the steady state controller
K=320/57;
