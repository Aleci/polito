% Compute the Resonance peak from the given zita.
% Note: the real closed loop resonance peak is Tp/Hg where Hg is the feedback gain.
function Tp=getTp(zita)
  Tp=1/(2*zita*sqrt(1-zita^2));
end
