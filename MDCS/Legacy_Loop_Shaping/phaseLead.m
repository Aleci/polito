% Generate a phase lead network
% z can be viewed as the ratio between the omega at which the effect should manifest and the relative omega selected from the tables
% m is the desired zero/pole spacing selected from the tables
function sys=phaseLead(z,m,s)
  sys=(1+s/z) / (1+s/(m*z));
end
