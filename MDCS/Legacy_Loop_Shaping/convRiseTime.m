% Convert the time domain specification in terms of cutting frequency
function omegacMin=convRiseTime(Tr,zita)
  omegacMin= (pi-acos(zita))/(Tr*sqrt(1-zita^2)) * sqrt(sqrt(1+4*zita^4)-2*zita^2);
end
