pkg load symbolic
pkg load control
clear all; close all; clc

% Data
syms s
Ga=-.09;
Gp=40/(s^2+3*s+4.5);

% Ramp error <= 1.5e-1
Eramp=3.5e-1;
syms K negative; % steady state controller just proportional because of the nonzero error
Gf=1;%Kc=1
G=simplify(K/s*Ga*Gp); %Plant with unitary feedback
tfe=simplify( 1/(1+G) ); %Error transfer function 
steadyStateError=subs(s*tfe*1/s^2,s,0); %Final value theorem
RampCond=solve(steadyStateError<=Eramp,K);

% Actuation disturbance <= 1.75e-2
% completely rejected by the controller integrator
da=8.5e-3;
DaCond="none"

% Plant disturbance <= 1e-3
maxDp=1e-3;
dp=3e-3; %max step disturbance
tfda=simplify( 1/(1+Gp*K/s*Ga) ); %Dp transfer function
steadyStateEffectDp=subs(s*tfda*dp/s^2,s,0); %Final value theorem
DpCond=solve(steadyStateEffectDp<=maxDp,K);

K=-25/7

% Sensor disturbance <= 2e-4
maxDs=2e-4;
omegaS=50;
ds=1e-2;
syms absLatOmegas
absLcond=absLatOmegas<=20*log(maxDs/ds);

% Overshoot
zita=convOvershoot(.08);
Tp=getTp(zita);
Sp=getSp(zita);

% RiseTime
syms omegaC
RiseCond=omegaC>=convRiseTime(2.5,zita);

% Settling time
SetlCond=omegaC>=convSettlingTime(10,.05,zita);

%Print results
clc
RampCond
DaCond
DpCond
absLcond
omegaS
RiseCond
SetlCond

clear s G
omegaCMin=0.81;

s=tf('s');
Gp=40/(s^2+3*s+4.5);
%C=K/s*phaseLag(0.8/100,5,s);%*phaseLead(0.4/2,3,s);
C=K/s*phaseLag(1/100,3,s)*phaseLead(3,16,s)*phaseLead(2,2,s)
L=minreal(C*Ga*Gp)
nichols1(L,Tp,Sp,[omegaCMin,omegaS,2])

t=[0:0.1:10]'; % 10 seconds of simulation
reference=t; % unitary ramp input
y=lsim(C*Ga*Gp/(1+L),reference,t);
figure,plot(t,y-reference),title("no disturbance error")
ys=step(C*Ga*Gp/(1+L),t);
figure,plot(t,ys,t,1.05*ones(size(ys)),t,.95*ones(size(ys))),title("step response")
yda=lsim(Gp/(1+L),da*ones(size(t)),t);				 
figure,plot(t,yda),title("step actuation disturbance output")
ydp=lsim(1/(1+L),dp*t,t);			  
figure,plot(t,ydp),title("ramp drift plant disturbance output")
yds=lsim(C*Ga*Gp/(1+L),ds*sin(omegaS*t),t);			  
figure,plot(t,yds),title("sinusoidal sensor disturbance response")
