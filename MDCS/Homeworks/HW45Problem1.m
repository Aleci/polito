clear all; close all; clc
syms s

%% Data
% system data
Gp=25/(s*(s^2+3.3*s+2));
Gs=1;
Ga=.095;
Gr=1;
Gd=1;
da=5.5e-3;
dda=1; % step
dp=0.02;
omegap=0.02;
ds=0.1;
omegas=40;

% requirements
er=0.15; % ramp error
ea=0.015; % actuation error
ep=5e-4; % plant error
es=5e-4; % sensor error
Tr=3; % rise time 0-100
Ts=12; % settling time 5%
alpha=0.05; % settling time percentage
ov=0.1; % overshoot 10%

%% (S2)
kd=1; R0=1;
% the plant has already a pole. The controller doesn't need a pole.
S0starRamp=er/(kd*R0);


%% (S3)
Gp0=subs( s* Gp  ,s,0);
S0starAct=double(ea/(Gp0*da));

%% (S4)
SLF=mag2db(ep/dp);

%% (S5)
THF=mag2db(es/ds);

%% (S8)
% same of HW123
zita=0.5912;
Tp=1.0487;
Sp=1.3611;

%% (S6)
% same of HW123
omegaCutR=0.6573;
omegaNatR=omegaCutR/0.72;
% alternative path
omegaNatR=1/(Tr*sqrt(1-zita^2))*(pi-acos(zita));

%% (S7)
% same of HW123
omegaNatS=-log(alpha)/(Ts*zita); % from slide 42 block 4

%% More stringent requirement selection
omegaNat=omegaNatR;
omegaCut=omegaCutR;
S0star=S0starRamp;

%% Selection of the performance weighting function for S
clear s;
s=tf('s');
omega=logspace(-2,2,1000); % figure frequency axis

% Draw S0star line
mag=bode(S0star*s,omega(1:end/2)); mag=squeeze(mag);
semilogx(omega(1:end/2),mag2db(mag),'m'),hold on

% Draw cutting frequency and plant noise requirement position
semilogx([omegap omegaCutR],[SLF 0],"*")

% Draw Sp line
yline(mag2db(Sp),'r')

% Draw professor's 2nd order prototype
S2prot=s*(s+2*zita*omegaNat)/(s^2+2*zita*omegaNat*s+omegaNat^2);
mag=bode(S2prot,omega); mag=squeeze(mag);
semilogx(omega,mag2db(mag))

% Best fit the edge with Sp
W=makeweight(db2mag(SLF),omegaCut,Sp,0,2); W=tf(W);
% Change the autocomputed zeros with s and a pole such that the constraint on S*(0) is satisfied
[z,p,k]=zpkdata(W); W=zpk([0 -0.04],p,k); Ws_1=W; % This is WS^-1
% Draw weighting function
mag=bode(W,omega); mag=squeeze(mag); semilogx(omega,mag2db(mag),'c');
title("Weighting function on S")
legend("S*(0)s","SLF and omegaCut","Sp","2nd order prototype","Ws^{-1}",'location','southeast')

%% Selection of the performance weighting function for T
hold off,figure

% Draw sensor noise requirement position
semilogx(omegas,THF,"*"), hold on;

% Draw Tp line
yline(mag2db(Tp),'r');

% Compute weighting function
W=makeweight(Tp,[omegas,db2mag(THF)],0,0,2); W=tf(W)

% Draw weighting function
mag=bode(W,omega); mag=squeeze(mag); semilogx(omega,mag2db(mag),'c');
title("Weighting function on T"), legend("THF","Tp","Wt^{-1}")



