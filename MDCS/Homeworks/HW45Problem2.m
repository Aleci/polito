clear all; close all; clc
s=tf('s');

% problem data
Gp=40/(s^2+3*s+4.5);
Gs=1;
Ga=-0.09;
Gr=1;
Gd=1;
da=8.5e-3; % step
dp=3e-3; % ramp
ds=2e-2;
omegas=50;

% requirements
kd=1;
er=0.35;
ea=1.75e-2;
ep=1e-3; R0=1;
es=2e-4;
Tr=2.5;
Ts=10;
alpha=0.05;
ov=0.08;

%% Requirement translation

% (S1) same of HW123
Gf=1/kd;

% (S2)
S0starRamp=er/(kd*R0); % controller type 1

% (S3)
% Always satisfied

% (S4)
S0starPlant=ep/dp;

% (S5)
THF=es/ds;

% (S8) from tables
zita=0.625;
Tp=1.025;
Sp=1.325;

% (S6) from tables
omegacR=2.01/Tr;

% (S7) from tables
omegacS=3.4/Ts;

% Constraint selection
S0star=min(S0starPlant,S0starRamp);
omegac=max(omegacS,omegacR);

%% Performance weighting function creation for S
om=logspace(-2,2,1000);

% Plot S0star constraint
mag=bode(s*S0star,om); mag=squeeze(mag);
semilogx(om(1:end/2),mag2db(mag(1:end/2)),'m'), hold on

% Plot desired minimum cutting frequency
semilogx(omegac,0,'k*');

% Plot Sp
yline(mag2db(Sp),'r');

% Create a weight
W=tf(makeweight(0,omegac,Sp,0,2)); % best fit denominator
[z,p,k]=zpkdata(W); W=zpk([0 -0.135],p,k); % modify numerator to match S*(0)
Ws_1=W

% plot weight
mag=bode(W,om);mag=squeeze(mag);
semilogx(om,mag2db(mag),'c');

% Draw professor's 2nd order prototype for check
omegaNat=1.2;
S2prot=s*(s+2*zita*omegaNat)/(s^2+2*zita*omegaNat*s+omegaNat^2);
mag=bode(S2prot,om); mag=squeeze(mag);
semilogx(om,mag2db(mag),'b')

legend("S*(0)*s","omegacut","Sp","Ws^{-1}","2nd order prototype",'location','southeast')
title("Performance weighting function on S"), hold off

%% Performance weighting function creation for T
figure

% Plot THF vertex
semilogx(omegas,mag2db(THF),'k*'); hold on;

% create weight
W=tf(makeweight(Tp,[omegas THF],0,0,2));
Wt_1=W

% plot weight
mag=bode(W,om);mag=squeeze(mag);
semilogx(om,mag2db(mag),'c');

% Beautify plot
stairs([om(1),omegas,om(end)],[mag2db(Tp),mag2db(THF),mag2db(THF)],'r')

legend("THF","Constraints","Wt^{-1}")
title("Performance weighting function on T"), hold off



