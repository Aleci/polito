clear all; close all; clc

syms s 
syms K real

p=1;
Gp=-30/(s*(s^2+3*s+2));
Ga=0.006;
da=2.5e-3;
dp=8.5e-3;
ds=5e-2; omegas=40;

kd=1;
er=2.5e-1;
ea=1e-2;
ep=1.5e-3;
es=5e-4;

%(S1)
Gf=1/kd; %R0/(Gs*Kd)

%(S2)
Gc=-K;
loop=Gc*Ga*Gp;
cloop=simplify(  loop/(1+loop)  );
tferr=simplify(  kd-cloop     );
fvtRamp=simplify( s*tferr*1/s^2 );
myans=solve(fvtRamp==er,s,0);
Kramp=double(myans.K)

%(S3)
tferrAct=simplify( Gp/(1+loop)  );
fvtAct=simplify( s*tferrAct*da/s );
myans=solve(fvtAct==ea,s,0);
Kact=abs(double(myans.K))

%(S4)
tferrPlant=simplify(  1/(1+loop)  );
fvtPlant=simplify( s*tferrPlant*dp/s^2 );
myans2=solve(fvtPlant==ep,s,0);
Kplant=double(myans2.K)

%(S5)
% L < es/ds = THF | omega>omegas
THF=es/ds
THFdb=mag2db(es/ds)
