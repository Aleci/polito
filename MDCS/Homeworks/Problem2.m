close all; clear all; clc
s=tf('s');
om=logspace(-3,3,1000);
% data
K=[20;60]; Z=[0.65; 0.75]; On=[1.7;2.5]; % original problem
%K=[16;64];  Z=[0.52; 0.88]; On=[1.36;2.84]; % modified problem
Kn=mean(K); Zn=mean(Z); Onn=mean(On);
deltaK=K(2)-Kn, deltaZ=Z(2)-Zn, deltaOn=On(2)-Onn,
Gpn=Kn/(4.5*(1+2*Zn/Onn*s+s^2/Onn^2));
Ga=-0.09;
da=8.5e-3;
dp=3e-3;
ds=1e-2;
omegas=50;

% Maximum PUI enlargement preserving stability
muOldPUI=0.39; % this is the maximum mu relative to RS in the original problem
factor=1/muOldPUI;
intK=K(2)-K(1); intZ=Z(2)-Z(1); intOn=On(2)-On(1);
intK3=intK*factor; intZ3=intZ*factor; intOn3=intOn*factor;
deltaK=intK3/2, deltaZ=intZ3/2, deltaOn=intOn3/2 % enlarged PUI
K=[Kn-deltaK Kn+deltaK]; Z=[Zn-deltaZ Zn+deltaZ]; On=[Onn-deltaOn Onn+deltaOn];
K(1)=0.1; % WARNING: usable PUI cannot change sign between upper and lower bound!
% Gpn=K(2)/(4.5*(1+2*Z(2)/On(2)*s+s^2/On(2)^2));

% requirements
er=3.5e-1;
ea=1.75e-2;
ep=1e-3;
es=2e-4;
ov=0.08;
ts=10; alpha=0.05;
tr=2.5;

%% ramp 
nu=1; p=0;
S0starRamp=er;

%% actuation rejected
% plant
S0starPlant=ep/dp

%% S0 constraint creation
S0star=min(S0starPlant,S0starRamp);
Ws01=S0star*s^(nu+p);
Ws0=1/Ws01;

%% sensor
THF=es/ds

%% overshoot
zita=0.62;
Tp=1.005;
Sp=1.35;

%% rise time
omegac=2/tr  % stricter constraint

%% settling time
%omegac=3.5/ts

%% Ws
figure(1)
bodemag(Ws01,om(1:end/4),'k'); title("Ws1"); hold on
% w=makeweight(0,omegac,Sp,0,2);
% [zer,pol,gain]=zpkdata(w);
% Ws1=zpk([0 -0.14],pol,gain);
% save("Wsp2.mat","Ws1")
load("Wsp2.mat");
Ws=1/Ws1;
bodemag(Ws1,om), yline(mag2db(Sp))

%% Wt
figure(2)
% Wt=makeweight(Tp,[omegas,THF],0,0,2);
% save("Wtp2.mat","Wt1")
load("Wtp2.mat");
Wt=1/Wt1;
bodemag(Wt1),xline(omegas),yline(mag2db(THF)),yline(mag2db(Tp)),title('Wt1')


%% model gridding
% maxmag=-ones(size(om));
% n=5; figure
% mag=bode(Gpn,om); nomMag=squeeze(mag);
% semilogx(om,nomMag,'g*'),hold on
% for ii=linspace(On(1),On(2),n) % Om
%     for jj=linspace(Z(1),Z(2),n) % zita
%         for kk=linspace(K(1),K(2),n) % K
%             sys=kk/(4.5*(1+2*jj/ii*s+s^2/ii^2));
%             mag=bode(sys,om); mag=squeeze(mag);
%             semilogx(om,mag,'k')
%             maxmag=max(maxmag,mag);
%         end
%     end
% end
% semilogx(om,maxmag,'r')
% 
% %% Wu
% deltamag=maxmag ./ nomMag -1;
% magPack=vpck(deltamag,om);
% dim=[1e-3,1,1,8];
% W=magfit(magPack,dim);
% [au,bu,cu,du]=unpck(W);
% Wu=zpk(ss(au,bu,cu,du));
% % save("Wup2.mat","Wu");
load("Wup2.mat");
Wu1=1/Wu;

%% Compare Wu1 e Wt1
figure(2), hold on
bodemag(Wu1,om)
return
%% W1
lambda=omegac/1000;
W1=minreal(Ws*s^(nu+p)/(s+lambda)^(nu+p),1e-3);
figure(1),hold on , bodemag(1/W1,om)

%% W2
W21=Tp/(1+s/7)^2;
figure(2),hold on, bodemag(W21,om)
W2=1/W21;
W2mod=1/Tp;

%% generalized plant
sys=Ga*Gpn;
% P=augw(sys,W1,[],W2mod);
% P=ltisys(P.A,P.B,P.C,P.D);
% P=sderiv(P,2,[1/7 1]);
% P=sderiv(P,2,[1/7 1]);
% 
%% optimizer
% [~,Cmod]=hinflmi(P,[1 1],0,1e-2,[0 0 0 1]);
% [Ac,Bc,Cc,Dc]=ltiss(Cmod);
% Cmod=minreal(  zpk(ss(Ac,Bc,Cc,Dc))  ,1e-3)
% save("Cp2.mat","Cmod");
load("Cp2.mat")
C=minreal(     Cmod*(s+lambda)/s     ,1e-2);

%% Test step
L=minreal(  C*sys ,1e-3);
G=minreal( feedback(L,1) ,1e-3);
figure,step(G), hold on, xline(ts),xline(tr),yline(1+alpha),yline(1-alpha)

%% Nichols
figure,nichols1(L,Tp,Sp,[omegac,omegas])
return
%% S  nominal performance OK
S=minreal(  1/(1+L)  ,1e-3);
%figure(1),hold on, bodemag(S,om,'g*')

%% T  nominal performance OK
T=minreal( 1-S   ,1e-3);
%figure(2),hold on, bodemag(T,om,'g*')

%% ramp OK
% t=linspace(0,100,1000);
% y=lsim(G,t,t);
% er
% y(end)-t(end)

%% act OK
% GAC=minreal(  feedback(Gpn,C*Ga)  ,1e-3);
% y=lsim(GAC,da*ones(size(t)),t);
% ea
% y(end)

%% Plant ok
% y=lsim(S,dp*t,t);
% ep
% y(end)

%% Sensor
% t=linspace(0,5,1000); % reduce aliasing
% figure, lsim(T,ds*sin(t*omegas),t),hold on, yline(es,'r'),yline(-es,'r')

%% Robust stability OK
fs=logspace(0,1,1000);
figure, bodemag(T*Wu,fs) % maxmu=0.81

%% Robust performance for (S2) (S3) (S4) CAN'T SAY OK
% fun=minreal(T*Wu+S*Ws,1e-3);
% figure, bodemag(fun)

%% Robust performance for (S2) (S3) (S4) using mu analysis OK
% WmuS=Ws0; figure
% om=logspace(-9,-8,1000); % check only on S(0)
% deltaset=[-2 0; -1 0; -1 0; 1 1];
% [an,bn,cn,dn]=linmod("structuredWS_P2");
% N=pck(an,bn,cn,dn);
% Nf=frsp(N,om);
% mub=mu(Nf,deltaset);
% vplot('liv,lm',mub)

%% Robust performance for (S5) using mu analysis OK
% WmuT=THF; figure
% om=logspace(log10(omegas),3,1000); % check only on S(0)
% deltaset=[-2 0; -1 0; -1 0; 1 1];
% [an,bn,cn,dn]=linmod("structuredWT_P2");
% N=pck(an,bn,cn,dn);
% Nf=frsp(N,om);
% mub=mu(Nf,deltaset);
% vplot('liv,lm',mub)

%% Robust Stability for modified uncertainty intervals
% fail unstructured
om=logspace(-3,3,1000); % check only on S(0)
deltaset=[-2 0; -1 0; -1 0];
[an,bn,cn,dn]=linmod("structuredRS_P2");
N=pck(an,bn,cn,dn);
Nf=frsp(N,om);
mub=mu(Nf,deltaset);
figure, vplot('liv,lm',mub)
% Ok using 

