%% Problem data
clear all; close all; clc
syms s K ref

% Plant data
Gp=100/(s^2+5.5*s+4.5);
Gs=1;
Ga=0.014;
Gr=1;
Gd=1;
da=1.5e-3;
dp=0.16;
omegap=0.03;
ds=0.2;
omegas=60;

% requirements data
er=0.15; % ramp
ea=4.5e-3; % step
ep=2e-3;
es=8e-4;
Tr=2;
Ts=8;
alpha=0.05;
ov=0.12;

%% (S1)
Gf=1;

%% (S2)
G=K/s * Ga * Gp;
W=simplify(  G/(1+G)  );
tferr=simplify(  (ref-ref*W)/ref   );
fvt_err=subs(   s*tferr*1/s^2  ,s,0);
K=double(solve(fvt_err==er)); % min value

%% (S3) 
% always satisfied

%% (S4)
% L @ 0.03 > 38dB
% mag2db(dp/ep)

%% (S5)
% L @ 60 < -48dB
%mag2db(es/ds)

%% (S6)
omegac=1.945/Tr;

%% (S7)
%omegacS=4/Ts;

%% (S8)
zita=0.56;
Tp=1.075;
Sp=1.4;

%% Initial loop shape
clear s Gp
s=tf('s');
%
C=K/s;
Gp=100/(s^2+5.5*s+4.5);
L=C*Ga*Gp;

% Move 8.5 to the right using a phase lead
m=16; z=5.5/2.8; % point at 5.5 and modify only its phase
lead1=(1+s/z)/(1+s/(m*z));

% ok, it's better to move 5.5 instead because now I can try to move down the 
% cutting frequency to the correct spot with a lag for loosing db.
% the rise and settling speed are very good so I can use a lag.
m=5; p=omegac/100; % point at the omegacut and modify only its magnitude
lag1=(1+s/(m*p))/(1+s/p);

% great, that's nice. now let's take some more degrees in phase at the
% at around .6 (middlepoint of the segment into the green ball)
m=2; z=0.6/1.15; % point at 0.6 and modify only its phase
lead2=(1+s/z)/(1+s/(m*z));

% beautiful response! that's a good controller!
% we are close to the requirement with L @ omegap but it's still satisfied
nets=lead1*lag1*lead2;
C=C*nets;
L2=L*nets;

subplot(1,2,1)
nichols1(L2,Tp,Sp,[omegap,omegac,omegas]);

% controller testing

% Transient requirements (S6) (S7) (S8) OK!
W=feedback(C*Ga*Gp,1);
subplot(1,2,2),step(W)
xline(Tr,'g'),xline(Ts),yline(1+alpha),yline(1-alpha),yline(1+ov,'r');

% Reasonable transient command activity
%step(minreal(C/(1+L2))) % This is not a professor's requirement

%% Steady state reference tracking 
t=0:.001:10000;
[y,t]=lsim(W,t,t);
rampErr=y(end)-t(end) % (S2) OK!
er

%% Steady state disturbance rejection
Wa=feedback(Gp,C*Ga);
[y,t]=lsim(Wa,da*ones(size(t)),t);
distErr=y(end) % (S3) OK!
ea

%% Plant noise attenuation
Wp=feedback(1,L2);
pSig=dp*sin(omegap*t);
[y,t]=lsim(Wp,pSig,t);
plantNoise=max(abs(y(end-4000:end)))  % (S4) OK!
ep

%% Sensor noise attenuation
Ws=minreal(L2/(1+L2),1e-3);
sSig=ds*sin(omegas*t);
[y,t]=lsim(Ws,sSig,t);
sensorNoise=max(abs(y(end-100:end)))  % (S5) OK! 
es  % PAY ATTENTION TO THE ALIASING!!!












