clear all; close all; clc
syms s

%% Data
% system data
Gp=25/(s*(s^2+3.3*s+2));
Gs=1;
Ga=.095;
Gr=1;
Gd=1;
da=5.5e-3;
dda=1; % step
dp=0.02;
omegap=0.02;
ds=0.1;
omegas=40;

% requirements
er=0.15; % ramp error
ea=0.015; % actuation error
ep=5e-4; % plant error
es=5e-4; % sensor error
Tr=3; % rise time 0-100
Ts=12; % settling time 5%
alpha=0.05; % settling time percentage
ov=0.1; % overshoot 10%

%% (S1)
% Closed_loop_gain = 1/feedback_gain => 
Gf=1;


%% (S2)
syms ref K
% 1 integrator on direct path required for bounded error on ramp, plant already has one.
G_ss= simplify(K*Ga*Gp/(1+K*Ga*Gp)); % compute G for steady state
tfsse= simplify( (ref-G_ss*ref)/ref ); %transfer function steady state error (ydes-y)
fvt_err= subs(s*tfsse*1/s^2,s,0); % lim s->0 s*tf*ramp (final value theorem)
KrampMin= double(solve(fvt_err==er,K)); % K min for S2 satisfaction

%% (S3)
% 0 integrator on the controller required for bounded error on step actuation disturb
tfssda=simplify(Gp/(1+K*Ga*Gp)); % transfer function steady state actuation disturb
fvt_act= subs(s*tfssda*da/s,s,0); % lim s->0 s*tf*step_disturb
KactMin= double(solve(fvt_act==ea,K));

%% (S4)
% applying formula on page 1 of notes: L|omegap >= dp0/spec
% => L at 0.02 >= 40 = 32db

%% (S5)
% applying formula on page 1 of notes: L|omegas <= spec/ds0
% => L at 40 <= 0.005 = -46db

%% (S8)
% applying formulas on page 131 block 3:
zita=abs(log(ov))/sqrt((log(ov)^2+pi^2)) % min value of damping to satisfy overshoot spec
% converting zita into frequency domain
Tp=1/(2*zita*sqrt(1-zita^2));
Sp=( 2*zita*sqrt(2+4*zita^2+2*sqrt(1+8*zita^2)) )/( sqrt(1+8*zita^2)+4*zita^2-1 );
% all these formulas hold for 2nd order systems

%% (S6)
% applying formula on page 131 block 3:
omegacMinR= (pi-acos(zita))/(Tr*sqrt(1-zita^2)) * sqrt(sqrt(1+4*zita^4)-2*zita^2);
% this formula holds for 2nd order systems

%% (S7)
% applying formula on page 131 block 3:
omegacMinS=-log(alpha)/(Ts*zita)*sqrt(sqrt(1+4*zita^4)-2*zita^2);
% this formula holds for 2nd order systems

%% Select more stringent constraints
K=max(KactMin,KrampMin)
omegaCut=max(omegacMinS,omegacMinR)


%% Initial loop shape
clear s;
s=tf('s');
Gp=25/(s*(s^2+3.3*s+2));
L=K*Ga*Gp;
%figure,nichols1(L,Tp,Sp,[0.02 omegaCut 40]);

%% loop shaping networks

% try to move 2.62 to the left to stabilize the system
m=3;z=2.62/1.8;
lead=(1+s/z)/(1+s/(m*z));
lead1=lead;

% try to move 1 to the left to increase the phase margin
m=3;z=1/1.7;
lead=(1+s/z)/(1+s/(m*z));
lead2=lead;

% damn, the system is unstable! Try to move the desired cutting frequency
% (taking down also the whole graph) near the cutting point
m=8;p=0.66/100;
lag=(1+s/(m*p))/(1+s/p);
lag1=lag;

% To be honest I first added an other lead network to match the rise time
% requirement, then I reduced the m of all networks
% Finally I noticed that the last network was worsening the performance
% so I removed it.

% m=1.25;z=0.02/100;
% lead=(1+s/z)/(1+s/(m*z));
% lead3=lead;

% this is a really nice response!
nets=lead1*lead2*lag1; % *lead3

%figure,nichols1(nets,Tp,Sp,[0.02 omegaCut 40])

L2=L*nets;

figure,subplot(1,2,1)
nichols1(L2,Tp,Sp,[0.02 omegaCut 40]);
Gc=K*nets;

% controller testing
t=0:0.01:100000;
loop=Gc*Ga*Gp;
W=minreal(loop/(1+loop),1e-3);

% step transient without disturbs
subplot(1,2,2),step(W), hold on, yline(1-alpha), yline(1+alpha), xline(Ts), xline(Tr), yline(1+ov,"r")
% risetime 0-100 OK
% settling time OK
% overshoot OK

% steady state tracking error
[y,t]=lsim(W,t,t);
rampError=y(end)-t(end) % OK
er

% steady state actuation disturb rejection
Wa=Gp/(1+loop);
[y,t]=lsim(Wa,da*ones(size(t)),t);
actuationError=y(end) % OK
ea

%% steady state plant disturb rejection
distp=dp*sin(omegap*t);
S=1/(1+loop);
[y,t]=lsim(S,distp,t);
%figure,plot(t,y),yline(ep,"r"),yline(-ep,"r")
plantError=max(abs(y(end-10000:end))) % OK
ep

%% steady state sensor disturb rejection
dists=ds*sin(omegas*t);
T=loop/(1+loop);
[y,t]=lsim(T,dists,t);
%figure,plot(t,y),yline(es,"r"),yline(-es,"r")
sensorError=max(abs(y(end-100:end))) % OK
es