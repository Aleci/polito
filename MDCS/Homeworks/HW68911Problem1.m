clear all; close all; clc
syms s

%% Data
% system data
K=[9 16];
p1=[0.55 1.05];
p2=[1.9 3.1];
Kn=mean(K); dK=K(2)-Kn;
p1n=mean(p1); dp1=p1(2)-p1n;
p2n=mean(p2); dp2=p2(2)-p2n;
Gpn=simplify(  Kn/(s*(1+s/p1n)*(1+s/p2n))  ); % same nominal plant of HW45Problem1


Gs=1;
Ga=.095;
Gr=1;
Gd=1;
da=5.5e-3;
dda=1; % step
dp=0.02;
omegap=0.02;
ds=0.1;
omegas=40;

%% All the requirements can be copied from the HW45Problem1 because are the same
% requirements
er=0.15; % ramp error
ea=0.015; % actuation error
ep=5e-4; % plant error
es=5e-4; % sensor error
Tr=3; % rise time 0-100
Ts=12; % settling time 5%
alpha=0.05; % settling time percentage
ov=0.1; % overshoot 10%

%% (S2)
kd=1; R0=1;
% the plant has already a pole. The controller doesn't need a pole.
S0starRamp=er/(kd*R0);


%% (S3)
Gpn0=subs( s* Gpn  ,s,0);
S0starAct=double(ea/(Gpn0*da));

%% (S4)
SLF=ep/dp;

%% (S5)
THF=es/ds;

%% (S8)
% same of HW123
zita=0.5912;
Tp=1.0487;
Sp=1.3611;

%% (S6)
% same of HW123
omegaCutR=0.6573;
omegaNatR=omegaCutR/0.72;
% alternative path
omegaNatR=1/(Tr*sqrt(1-zita^2))*(pi-acos(zita));

%% (S7)
% same of HW123
omegaNatS=-log(alpha)/(Ts*zita); % from slide 42 block 4

%% More stringent requirement selection
omegaNat=omegaNatR;
omegaCut=omegaCutR;
S0star=S0starRamp;

%% Selection of the performance weighting function for S
clear s Gpn;
s=tf('s');
Gpn=Kn/(s*(1+s/p1n)*(1+s/p2n));
om=logspace(-3,2,1000); % figure frequency axis

% Draw S0star line
figure(1);
mag=bode(S0star*s,om(1:end/2)); mag=squeeze(mag);
semilogx(om(1:end/2),mag2db(mag),'m'),hold on

% Draw cutting frequency and plant noise requirement position
semilogx([omegap omegaCutR],[mag2db(SLF) 0],"*")

% Draw Sp line
yline(mag2db(Sp),'r')

% Draw professor's 2nd order prototype
S2prot=s*(s+2*zita*omegaNat)/(s^2+2*zita*omegaNat*s+omegaNat^2);
mag=bode(S2prot,om); mag=squeeze(mag);
semilogx(om,mag2db(mag))

% Best fit the edge with Sp
%W=makeweight(SLF,omegaCut,Sp,0,2); W=tf(W);  % ------ Ws tight
W=makeweight(SLF,omegaCut/1.5,Sp,0,2); W=tf(W); % ===== Ws loose
% Change the autocomputed zeros with s and a pole such that the constraint on S*(0) is satisfied
%[z,p,k]=zpkdata(W); W=zpk([0 -0.04],p,k); Ws_1=W; % This is WS^-1 ------ Ws tight
[z,p,k]=zpkdata(W); W=zpk([0 -0.02],p,k); Ws_1=W; % This is WS^-1 ====== Ws loose
Ws_1prof= (1.36*s*(s+0.02))/(s^2 + 0.60236*s + 0.18147); %almost the same of my Ws loose
% Draw weighting function
mag=bode(Ws_1,om); mag=squeeze(mag); semilogx(om,mag2db(mag),'c');
mag=bode(Ws_1prof,om); mag=squeeze(mag); semilogx(om,mag2db(mag),'b');
title("Weighting function on S")
legend("S*(0)s","SLF and omegaCut","Sp","2nd order prototype","Ws^{-1}","Ws^{-1}_{prof}",'location','southeast')
%Ws_1=Ws_1prof;

%% Selection of the performance weighting function for T
hold off,figure(2);

% Draw sensor noise requirement position
semilogx(omegas,mag2db(THF),"*"), hold on;

% Draw Tp line
yline(mag2db(Tp),'r');

% Compute weighting function
%W=makeweight(Tp,[omegas,THF],THF/1.1,0,3); Wt_1proper=tf(W);  % proper tight
Wt_1=Tp/(1+s/2.8)^2;  % not proper loose
Wt_1prof=8.22/(s^2 + 3.6821*s + 7.84);

% Draw weighting function
mag=bode(Wt_1,om); magWt_1=squeeze(mag); semilogx(om,mag2db(magWt_1),'c');
mag=bode(Wt_1prof,om); magWt_1=squeeze(mag); semilogx(om,mag2db(magWt_1),'b');
title("Weighting function on T"), legend("THF","Tp","Wt^{-1}","Wt^{-1}_{prof}")
%Wt_1=Wt_1prof;

%% Model gridding
N=5;
family=cell(N*N*N);
nomMag=bode(Gpn,om); nomMag=squeeze(nomMag); maxMag=nomMag;
figure(3),semilogx(om,mag2db(maxMag),'r'), hold on % plot nominal
count=1;
for kk=linspace(K(1),K(2),N)
    for ii=linspace(p1(1),p1(2),N)
        for jj=linspace(p2(1),p2(2),N)
            G=kk/(s*(1+s/ii)*(1+s/jj));
            family{count}=G; count=count+1;
            mag=bode(G,om); mag=squeeze(mag);
            maxMag=max(mag,maxMag);
            semilogx(om,mag2db(mag),'k');
        end
    end
end
semilogx(om,mag2db(nomMag),'g'); % plot again the nominal plant to see better
semilogx(om,mag2db(maxMag),'r'); % plot again the nominal plant to see better
title("Model family")

%% Sampled uncertainty calculation and fitting
deltaMag=maxMag./nomMag -1;

magPack=vpck(deltaMag,om);
dim=[0.001,1,1,2];
w=magfit(magPack,dim);  %dim is an optional parameter to reduce the weight order
[Au,Bu,Cu,Du]=unpck(w);
Wu=zpk(ss(Au,Bu,Cu,Du));

figure(4);
semilogx(om,mag2db(deltaMag),'b*'); hold on
mag=bode(Wu,om); mag=squeeze(mag);
semilogx(om,mag2db(mag),'c'); hold off
title("Multiplicative uncertainty fitting");
legend("deltaMag","Fit")

%% Wt_1 Wu_1 comparison
figure(5)
semilogx(om,mag2db(magWt_1),'c'); hold on;
mag=bode(1/Wu,om); mag=squeeze(mag);
semilogx(om,mag2db(mag),'b'); hold off
title("Weights on T"), legend("Wt^{-1}","Wu^{-1}")

%% Controller synthesis
 lambda=0.0001;
 W1=1/Ws_1; 
 W1=minreal(  W1* s/(s+lambda)  ,1e-3); % replace integrators with poles in lambda

% modify wt
[z,p,k]=zpkdata(Wt_1);
Wt_1mod=Tp;
pol=p{1}; pol=real(pol(1));
derActions=(1+s/pol)^2;

W2=1/Wt_1mod;
Wtprime=Wt_1mod/derActions;
%W2=1/Wt_1; % proper tight

mag=squeeze(bode(Wtprime,om)); figure(2),hold on
semilogx(om,mag2db(mag),'m'), hold off

% Generate generalized plant model
sys=Ga*Gpn; % the loop without the controller
%[sA,sB,sC,sD]=linmod("unstrSim1");
%P=ltisys(sA,sB,sC,sD); P3=ss(sA,sB,sC,sD);
P3=augw(sys,W1,[],W2); P=P3;
P=ltisys(P3.A,P3.B,P3.C,P3.D);
P=sderiv(P,2,[1/pol 1]);         % Attach the non causal dynamics of W2 to output z2
P=sderiv(P,2,[1/pol 1]);         % Attach the non causal dynamics of W2 to output z2

% Optimize the hinfinity controller
[~,Cmod]=hinflmi(P,...       % Generalized plant model                              % OPTIMIZER 1
                   [1 1],...   % n controlled outputs, n controlled inputs
                   0,...       % find best controller for the given tolerance (min gamma)
                   1e-2,...    % tolerance
                   [0 0 0 1]); % [enhanceDamping, , ,suppressOutput]
%
%opts = hinfsynOptions('Method','LMI','Display','on');                              % OPTIMIZER 2
%[Cmod2,CL,gamma] = hinfsyn(P3,1,1,opts);                                           % OPTIMIZER 2


%C=zpk(tf(Cmod2));                                                                  % OPTIMIZER 2
[Ac,Bc,Cc,Dc]=ltiss(Cmod);                                                          % OPTIMIZER 1
Copt=minreal(zpk(ss(Ac,Bc,Cc,Dc)),1e-3);                                               % OPTIMIZER 1

%% Controllers provided by the professor's solution
% Cprof=2.29e7*((s+3.2528e-6)*(s+0.1249)*(s+.8)*(s+2.5))/...
%       ((s+0.0099985)*(s+0.02)*(s+5.8308)*(s+656.3503)*(s+10495.7824));
% CprofSimp=2.18e3*((s+0.1249)*(s+.8)*(s+2.5))/...
%           ((s+0.02)*(s+5.8308)*(s+656.3503));
%Copt=CprofSimp;
%% Controller order reduction
C=zpk(Copt);
%C=controllerOrderReduction(C,1e-3,4e3)

% Gain cannot be increased to reduce overshoot without violating the requirement on sensor noise
%damp(C);
%C=zpk(z,p,k)/5.6234e03/2
%% Hinf controllers out of the box
% Computed with Wt with num/den of order 3 and Ws tight. Optimizer: hinflmi gamma=0 damp=0. Simulink
%          3.8757e09 (s+179.1) (s+2.5) (s+0.8) (s+0.2724) (s+3.451e-06) (s^2 + 164.4s + 3.984e04)
%  C = -----------------------------------------------------------------------------------------------
%      (s+3.78e05) (s+717.4) (s+0.03996) (s+0.0009469) (s^2 + 15.5s + 116.6) (s^2 + 999.7s + 9.654e05)

% Computed with improper Wt of order 2 and W2 loose; lambda=.001. Optimizer: hinflmi gamma=0.3 damp=0. augw
%        4.218e06 (s+9.591e-06) (s+0.1856) (s+0.8) (s+2.5)
%  C = ------------------------------------------------------
%      (s+1421) (s+986.2) (s+6.358) (s+0.01993) (s+0.0009055)

% Computed with improper Wt of order 2 and W2 loose; lambda=.0001. Optimizer: hinflmi gamma=0 damp=0. augw
% Simplified at LF=1e-3 and HF=4e3 with controllerOrderReduction
%       4118.7 (s+2.5) (s+0.8) (s+0.1853)
%  C =  ---------------------------------
%         (s+1374) (s+6.33) (s+0.01995)

%% Test
L=C*Ga*Gpn;
S=minreal(1/(1+L),1e-3);
T=minreal(1-S,1e-3);
W=feedback(L,1);
Wa=minreal(S*Gpn,1e-3);
t=linspace(0,100,100000);

%% Actual nichols plot
figure(6), nichols1(L,Tp,Sp,[omegap omegaCut omegas])

%% Check nominal frequency requirements       
mag=squeeze(bode(T,om)); figure(2),hold on
semilogx(om,mag2db(mag),'g*'), hold off
mag=squeeze(bode(S,om)); figure(1),hold on
semilogx(om,mag2db(mag),'g*'), hold off

%% Check steady state response
% ramp response
y=lsim(W,t,t);
er
y(end)-t(end)

% actuation response
y=lsim(Wa,da*ones(size(t)),t);
ea
y(end)

%% Plant noise attenuation
pSig=dp*sin(omegap*t);
[y,t]=lsim(S,pSig,t);
plantNoise=max(abs(y(end-1000:end)))  % (S4) OK!
ep

%% Sensor noise attenuation
sSig=ds*sin(omegas*t);
[y,t]=lsim(T,sSig,t);
sensorNoise=max(abs(y(end-1000:end)))  % (S5) OK! 
es  % PAY ATTENTION TO THE ALIASING!!!

%% Check nominal step response requirements
figure(7), step(W)
xline(Tr,'g'),xline(Ts),yline(1+alpha),yline(1-alpha),yline(1+ov,'r');

%% Unstructured uncertaity for Robust Stability and Robust Performance on (S2),(S3),(S4) but not on (S5) 
figure(8),
Ws=(1/Ws_1);
nomPerfFun=minreal(   S*Ws  ,1e-3);
robStabFun=minreal(  T*Wu   ,1e-3);
robPerfFun=minreal(  nomPerfFun+robStabFun   ,1e-3);
bodemag(robStabFun,nomPerfFun,robPerfFun,om),legend("Robust Stability","Nominal Performance","Robust Performance on (S2),(S3),(S4) only")
% Robust Stability OK, Nominal Performance OK, Robust Performance not known

%% Structured Uncertainty for Robust Stability
figure
om=logspace(-3,3,500);
deltaset=[-1 0; -1 0; -1 0];
[An,Bn,Cn,Dn]=linmod("StrUn_P1");
N=pck(An,Bn,Cn,Dn);
Nf=frsp(N,om);
mubounds=mu(Nf,deltaset);
vplot('liv,lm',mubounds);
legend("Upper bound","Lower bound")
title("Robust Stability Test"); % OK! (as expected)

%% Structured Uncertaity for Robust Performance on (S2),(S3),(S4)
Ws=1/(S0star*s);
om=logspace(-6,-4,200);
figure
deltaset=[-1 0; -1 0; -1 0; 1 1];
[An,Bn,Cn,Dn]=linmod("StrUnWs_P1");
N=pck(An,Bn,Cn,Dn);
Nf=frsp(N,om);
mubounds=mu(Nf,deltaset);
vplot('liv,lm',mubounds);
legend("Upper bound","Lower bound")
title("Robust Performance Test for (S2),(S3),(S4)"); 

%% Structured Uncertaity for Robust Performance on (S5)
om=logspace(log10(omegas),6,500);
figure
deltaset=[-1 0; -1 0; -1 0; 1 0];
%Wt=(1/Wt_1proper);
Wt=1/es;
[An,Bn,Cn,Dn]=linmod("StrUnWt_P1");
N=pck(An,Bn,Cn,Dn);
Nf=frsp(N,om);
mubounds=mu(Nf,deltaset);
vplot('liv,lm',mubounds);
legend("Upper bound","Lower bound")
title("Robust Performance Test for (S5)"); 
