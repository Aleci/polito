clear all; close all; clc

syms s K

Gr=1;
Gp=100/(s*(s^2+5.5*s+4.5));
Gs=0.5;
Ga=0.112;
da=1.5e-3; %ramp
dp=16e-2; omegap=0.03;
ds=2e-1; omeagas=60;

Kd=8;
er=1.5e-1;
ea=2.14;
ep=5.1e-3;
es=1.6e-3;

p=1;
nu=1; %(due to S3)

Gc=K/s;

%(S1) nu+p>=1 ok
Gf=Gr/(Kd*Gs)

%(S2) always rejected due to nu=1

%(S3) 
tfderrAct=simplify(  Gp/(1+Gc*Ga*Gp*Gs*Gf)  );
fvterrAct=simplify(  s* tfderrAct * da/s^2  );
myans=solve(fvterrAct==ea,s,0);
KAct=double(myans.K)

%(S4)
% dp*S<ep => 1/S=L>dp/ep
SLF=dp/ep;
SLFdb=mag2db(SLF)

%(S5)
% ds*TfErr<es => ds*T/Gs<es => L<es*Gs/ds
THF=es*Gs/ds;
THFdb=mag2db(THF)

clear s K
s=tf('s');
Gc=KAct/s;
Gp=100/(s*(s^2+5.5*s+4.5));

L=minreal(   Gc*Ga*Gp*Gs*Gf   ,1e-3);
nichols(L)

