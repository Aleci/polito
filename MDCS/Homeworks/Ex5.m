clear all; close all; clc
syms s K

Gs=2;
Ga=0.38;
Gp=25/(s*(s^2+3.3*s+2));

da=5.5e-3; 
dp=2e-2; omegap=0.02;
ds=1e-1; omegas=40;

kd=4;
er=1.5e-1;
ea=5.8;
ep=3.6e-4;
es=1.25e-4;

nu=1;
p=1;

%(S1)
Gf=1/(Gs*kd);

%(S2) always satisfied since nu+p=2;
Gc=K/s;
loop=simplify( Gc*Ga*Gp*Gs*Gf );

%(S3)
tferrAct=simplify(  Gp/(1+loop)  );
fvtAct=simplify(  s*tferrAct*da/s^2 );
myans=solve(fvtAct==ea,s,0);
K=double(myans.K)

%(S4)
% 1/L<ep/dp => L>dp/ep=SLF | omega<omegap
SLF=dp/ep;
SLFdb=mag2db(SLF)

%(S5)
% L<es/ds | omega>omegas
THF=es/ds;
THFdb=mag2db(THF)