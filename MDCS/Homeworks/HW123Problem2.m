%% Problem data
clear all; close all; clc
syms s K ref

% Plant data
Gp=40/(s^2+3*s+4.5);
Gs=1;
Ga=-.09;
Gr=1;
Gd=1;
da=8.5e-3; % step
dp=3e-3; % ramp
ds=1e-2; % sin
omegas=50;

% Specification data
er=3.5e-1;
ea=1.75e-2;
ep= 1e-3;
es=2e-4;
Tr=2.5; % 0-100;
Ts=10;
alpha=0.05;
ov=0.08;

%% (S1)
Gf=1;

%% (S2)
Css=-K/s;
L=simplify(Css*Ga*Gp); % controller required to be type 1
Gss=simplify(L/(1+L));
fvt_err=subs(s* simplify((ref-ref*Gss)/ref) *1/s^2, s, 0);
Kramp=double(solve(fvt_err==er)); % min gain

%% (S3)
% always satisfied with a type 1 controller

%% (S4)
Wp=simplify(1/(1+L));
fvt_distp=subs(s* Wp *dp/s^2, s,0);
Kplant=double(solve(fvt_distp==ep)); % min gain

%% (S5)
% L|omegas <= es/ds = 0.02 = -34dB

%% (S8)
% from the tables
zita=0.62;
Tp=1.025;
Sp=1.33;

%% (S6)
% from the tables
omegacR=2/Tr;

%% (S7)
% from the tables
omegacS=3.5/Ts;

%% specification selection
K=max(Kplant,Kramp)
omegaCut=max(omegacS,omegacR)

%% initial loop shape
clear s L Css;
s=tf('s');
Gp=40/(s^2+3*s+4.5);
Css=-K/s; % REMEMBER THE MINUS HERE!!!
L=Css*Ga*Gp;
%nichols1(L2,Tp,Sp,[omegaCut, 0.02])

%% Loop shaping
% System marginally stable, try to move the whole graph in the stable part 
m=4; z=4/2.7; % at the beginning m was 16
lead1=(1+s/z)/(1+s/(m*z));

% System stable but too much undamped, try to move the whole graph below
m=3; p=3.17/100; % at the beginning m was 4
lag1=(1+s/(m*p))/(1+s/p);

% Not elegant, but still a satisfactory response (after m reductions in the nets)
nets=lead1*lag1;
L2=L*nets;

figure, subplot(1,2,1)
nichols1(L2,Tp,Sp,[omegaCut, 0.02])
C=Css*nets;

W=minreal(L2/(1+L2),1e-3);
subplot(1,2,2),step(W), hold on
yline(1+ov,'r'),yline(1+alpha),yline(1-alpha),xline(Tr,'r'),xline(Ts)

%% Test controller
t=0:.01:10000;

% Steady state error on ramp input
[y,t]=lsim(W,t,t);
rampErr=y(end)-t(end) % OK
er  

% Steady state error on step actuation disturb
Wa=Gp/(1+L2);
[y,t]=lsim(Wa,da*ones(size(t)),t);
actuatorErr=y(end) % OK
ea

% Steady state error on ramp plant disturb
Wp=1/(1+L2);
[y,t]=lsim(Wp,dp*t,t);
plantErr=y(end) % OK
ep

% Steady state sensor noise attenuation
Ws=L2/(1+L2);
dsSig=ds*sin(omegas*t);
[y,t]=lsim(Ws,dsSig,t);
%plot(t(end-1000:end),y(end-1000:end))
sensorErr=max(abs(y(end-1000:end)))  % OK
es









