clear all; close all; clc
s=tf('s');
om=logspace(-3,3,1000);

K=[.8 1.2];
Kn=mean(K);

% Nominal system
Gpn=0.075*Kn/(s*(s^2+5*s+6));
Ga=1.5;
Gc=1000*(1+s/2)^2*(1+s/1.5)^2/(1+s/15)^2/(1+s/0.5)^2;
Ws_1=0.1*s*(1+s/.1905)/(1+1.414*s/2+s^2/4); Ws=1/Ws_1;
Wu=0.2;

myGcRIC=(1.644e08*s^4 + 1.438e09*s^3 + 4.069e09*s^2 + 3.7e09*s + 4623)/ ...
        (s^5 + 294*s^4 + 4.025e04*s^3 + 3.111e06*s^2 + 3.377e06*s + 3374);
myGcLMI=(3.528e08*s^4 + 3.011e09*s^3 + 8.35e09*s^2 + 7.48e09*s + 3478)/ ...
        (s^5 + 3945*s^4 + 2.438e05*s^3 + 7.502e06*s^2 + 7.97e06*s + 7746);

%Gc=balred(myGcRIC,4); % This controller order reduction works!!

% Nominal complementary functions
L=minreal(  Gc*Ga*Gpn   ,1e-3);
S=minreal(  1/(1+L)    ,1e-3);
T=minreal(  1-S        ,1e-3);

% Random realization of the system
Kr=0.8+rand()/2;
Gp=0.075*Kr/(s*(s^2+5*s+6));

% Transient requirements
Tr=0.5;
ov=.38;

%% Plot S
omegacut=1.795/Tr;
figure(1);
mag=bode(S,om); mag=squeeze(mag);
semilogx(om,mag2db(mag),'k'); hold on
mag=bode(Ws_1,om); mag=squeeze(mag);
semilogx(omegacut,0,'*b')
semilogx(om,mag2db(mag),'r'); hold off
title("Constraint on S for NP")

%% Plot T
figure(2);
mag=bode(T,om); mag=squeeze(mag);
semilogx(om,mag2db(mag),'k'); hold on
yline(mag2db(1/Wu),'r'); title("Constraints on T for RS")

%% Plot L
zita=0.295;
Tp=1.76;
Sp=2.04;

% L on Nichols plot (Constraint cannot be graph here because are relative
% to frequency/magnitude only)
figure(3)
nichols1(L,Tp,Sp,[1])

% L on Bode plot
figure(4)
mag=bode(L,om); mag=squeeze(mag);
semilogx(om,mag2db(mag),"k"); hold on;

% LOWER BOUND AT LOW FREQUENCY
% if |Wu|<1 (usually satisfied at low frequencies)
% then RP <= |L| > |Ws|/(1-|Wu|)
lf=logspace(-3,-1,1000);
lbound=minreal(    Ws/(1-Wu)    ,1e-3);
mag=bode(lbound,lf); mag=squeeze(mag);
semilogx(lf,mag2db(mag),'r');

% UPPER BOUND AT HIGH FREQUENCY
% if |Ws|<1 (usually satisfied at high frequency)
hf=logspace(1,3,1000);
ubound=minreal(    (1-Ws)/Wu    ,1e-3);
mag=bode(ubound,hf); mag=squeeze(mag);
semilogx(hf,mag2db(mag),'g');
legend("Loop function","Lower bound","Upper bound")
title("Constraints on L for RP")

% Constraints on S and T for RP of (S2),(S3),(S4)
figure(5)
RPfun=minreal(   Ws*S + Wu*T   ,1e-3);
mag=bode(RPfun,om); mag=squeeze(mag);
semilogx(om,mag2db(mag),'g'); hold on, yline(0,'r'),
title("Constraint on S and T for RP of (S2),(S3),(S4)")

%% Time domain performance
L=minreal(  Gc*Ga*Gp   ,1e-3);
S=minreal(  1/(1+L)    ,1e-3);
T=minreal(  1-S        ,1e-3);
W=minreal(  S*L       ,1e-3);
Wa=minreal(  S*Gp       ,1e-3);
t=linspace(0,100,1000);

%% Steady state

% ramp response
y=lsim(W,t,t);
er=0.2
y(end)-t(end)

% actuation response
da=7e-3;
y=lsim(Wa,da*ones(size(t)),t);
ea=1e-5
y(end)

% plant response
dp=7e-3;
y=lsim(S,dp*t,t);
ep=1.5e-3
y(end)


%% Transient
figure
step(W),yline(1+ov,'r'),xline(Tr,'g')

% Note: Transient requirements rely on approximated bounds so in general is
% not possible to guarantee RP for them.
% In this case using Kr=0.8 we notice that rise time is not satisfied


%% THE HOMEWORK ENDS HERE
return


%% Compute a tighter Ws_1
myWs_1=tf(makeweight(0,omegacut,Sp,0,2));
[z,p,k]=zpkdata(myWs_1); myWs_1=zpk([0 -1.1], p, k);
myWs=minreal(   1/myWs_1  ,1e-3);
mag=bode(myWs_1,om); mag=squeeze(mag);
figure(1),hold on, semilogx(om,mag2db(mag),'b')

%% Compute a new controller
W1=minreal(   myWs*s/(s+0.001)   ,1e-3); 
W2=Wu;

P=augw(Ga*Gpn,W1,[],W2);
opts=hinfsynOptions('Method','LMI')
[K,CL,G]=hinfsyn(P,1,1,10,opts)
C=tf(K)
