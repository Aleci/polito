% Remove all the poles/zeros that do not belongs to the interval (LF,HF)
% To mantain DC gain and HF gain slow poles are simplified using the appropriate notatio
function Clow=controllerOrderReduction(C,LFn,HFn,LFd,HFd)
    if nargin<5
        LFd=LFn;
        HFd=HFn;
    end
    s=tf('s');
    [z,p,k]=zpkdata(C); z=z{1}; p=p{1};
    simpDen=1; % Multiplication factor used to simplify the denominator
    for ii=[1:length(p)]
        if(abs(p(ii))<LFd)
            simpDen=simpDen*(s-p(ii));
        end
        if(abs(p(ii))>HFd)
            simpDen=simpDen*(1-s/p(ii));
        end
    end
    simpNum=1; % Multiplication factor used to simplify the numerator
    for ii=[1:length(z)];
        if(abs(z(ii))<LFn)
            simpNum=simpNum*(s-z(ii));
        end
        if(abs(z(ii))>HFn)
            simpNum=simpNum*(1-s/z(ii));
        end
    end
    Clow=zpk(minreal(  C*simpDen/simpNum   ,1e-3));
end