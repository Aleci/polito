%pkg load symbolic; pkg load signal; pkg load control      % octave library inclusions
    
%======================
%% Internal stability
%======================

%% Example internally marginally stable (FULL ANALYSIS) (real root)
clear all; close all; clc

% 1) Compute eigenvalues and check that have non positive real part
A=[0 0 0; 0 0 1; 0 0 -5]
eigsA=eig(A)
% 2) If all the eigenvalues are strictly negative the system is internally asymptotically stable
% 3) if an eigenvalue with null real part is present with algebraic multiplicity = 1 
%    the system is internally marginally stable without further analysis
% 4) Otherwise the geometric multiplicity must be computed                                  (extended version)
syms('s');
Aprime=simplify( inv(s*eye(3)-A) ) 
minPoly=Aprime(2,3)
% 5) If within Aprime there are no polynomial elements having poles with null real part
%    and algebraic multiplicity > 1 then the system is internally marginally stable
%
% 4-5bis) In other words if the minimal (common multiple) polynomial of Aprime has roots 
%         with null real part with algebraic multiplicity=1 the system is 
%         internally marginally stable                                                      (shortened version)
rootsMinPoly=roots(minpoly(A)) % the system is internally marginally stable
% (slide17-pag5-Stability.pdf)

%% Example internally marginally stable (DIRECT) (complex roots)
A=[0 1 0;-1 0 0; 0 0 -1]
eigsA=eig(A) % we have two COMPLEX poles in zero (AlgMult=1)
% The system is internally marginally stable without further analisys

%% Example internally marginally stable (DIRECT) (complex+real roots)
A=[0 1 0;-1 0 0; 0 0 -1]
eigsA=eig(A) % we have two COMPLEX and one REAL poles in zero (AlgMult=1)
% The system is internally marginally stable without further analisys
% (slide20-pag5-Stability.pdf)


%% Example internally unstable (FULL ANALYSIS) (real roots)
A=blkdiag([0 -8; 0 0],[-4 -8;0 0])
eigsA=eig(A) % we have three REAL poles in zero (AlgMult=3)
rootsMinPoly=roots(minpoly(A)) % we have two REAL poles in zero (GeoMult=2)
% The system is internally unstable



%==================
%% BIBO stability
%==================

%% Examples
clear all
s=tf('s');

% Check that the transfer function after the zero/poles cancellations 
% has all the poles with real part strictly negative
H1=(s-5)/( (s+4)*(s+300) )
p1=pole(H1) % SISO, BIBO stable
z1=zero(H1)
G1=dcgain(H1)
%
H2=5/s % SISO, not BIBO stable
p2=pole(H2)
z2=zero(H2)
G2=dcgain(H2)
%
H3=[ H1 ; H2 ] % SIMO, not BIBO stable
%
H=-3 * (s-1)/( (s-1)*(s+1))
pH=pole(minreal(H)) % SISO, BIBO stable

%===============================================
%% CONCLUSION
clear all
s=tf('s');

% Given system in state space
A=[1 1; 0 -1];
B=[2; 3];
C=[0 1];
D=0;
%
eigA=eig(A) % Internally unstable!

% Transform it in Laplace domain
[Hnum, Hden] = ss2tf(A,B,C,D) ;
H=minreal(tf(Hnum,Hden),1e3); % zero poles cancellation here
polesH=pole(H) % BIBO stable !

% Internal stability and BIBO stability are different properties since the
% poles in Laplace domain are a subset of the natural modes of the system