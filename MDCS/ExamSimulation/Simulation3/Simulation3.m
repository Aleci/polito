clear all; close all; clc
s=tf('s');

K=[80 120]; p1=[0.8 1.2]; p2=[2 7];
%K=[64 136]; p1=[0.64 1.36]; p2=[1.6 7.4]; % modified PUI

Kn=mean(K); p1n=mean(p1); p2n=mean(p2);
deltaK=K(2)-Kn; deltap1=p1(2)-p1n; deltap2=p2(2)-p2n;


Gpn=Kn/4.5/(1+s/p1n)/(1+s/p2n);
Ga=0.014;
da=1.5e-3;
dp=16e-2;
ds=2e-1;
omegap=0.03;
omegas=60;

% requirements
kd=1;
er=1.5e-1;
ea=4.5-3;
ep=2e-3;
es=8e-4;

% derived requirements
S0star=er;
SLF=ep/dp;
THF=es/ds;
omegac=0.95;
zita=0.49;
Tp=1.15;
Sp=1.45;

%% Ws
Ws01=S0star*s;
Ws0=1/Ws01;

polo=omegac/1.5;
zero=.039;
Ws1=Sp/(polo^2+1.414*s*polo+s^2)*s*(s+zero);
Ws=1/Ws1;
figure(1),bodemag(Ws01,Ws1), yline(mag2db(Sp)),hold on, semilogx([omegap omegac],[mag2db(SLF) 0],'*')

%% Wt
polo=3.5;
Wt1=Tp/(1+1.414*s/polo+s^2/polo^2)
figure(2),bodemag(Wt1),yline(mag2db(Tp)),hold on,  semilogx([omegas],[mag2db(THF)],'*')

%% Wu
om=logspace(-3,3,500);
n=5;
figure(3)
mag=bode(Gpn,om); magNom=squeeze(mag);
semilogx(om,magNom,"g*"),hold on
maxMag=magNom;
for ii=linspace(p1(1),p1(2),n)
    for jj=linspace(p2(1),p2(2),n)
        for kk=linspace(K(1),K(2),n)
            system=kk/4.5/(1+s/ii)/(1+s/jj);
            mag=bode(system,om); mag=squeeze(mag);
            maxMag=max(mag,maxMag);
            semilogx(om,mag,'k');
        end
    end
end
semilogx(om,maxMag,'r')
close(3) 

deltamag=maxMag./magNom -1;

pack=vpck(deltamag,om);
wu=magfit(pack,[1e-3,1,1,3]);
[au,bu,cu,du]=unpck(wu);
Wu=zpk(ss(au,bu,cu,du));
figure,semilogx(om,mag2db(deltamag),'k'), hold on, bodemag(Wu,'b')
%load("Wu.mat")
figure(2), hold on, bodemag(1/Wu)

%% W1
lambda=omegac/1000;
W1=Ws*s/(s+lambda); W1=minreal(W1,1e-3);
figure(1), hold on, bodemag(1/W1)

%% W2
W21=Tp/(1+s/3.5)^2;
figure(2),hold on, bodemag(W21)
W2mod=1/W21;
W2=1/Tp;

%% P
sys=Ga*Gpn;
P=augw(sys,W1,[],W2);
P=ltisys(P.A,P.B,P.C,P.D);
P=sderiv(P,2,[1/3.5 1]);
P=sderiv(P,2,[1/3.5 1]);

% optim
[ddd,Cmod]=hinflmi(P,[1 1],0,1e-2,[0 0 0 1]);
[ac,bc,cc,dc]=ltiss(Cmod);
Cmod=zpk(ss(ac,bc,cc,dc))
%load("Cmod.mat");
%% Model reduction
C=minreal(Cmod*(s+lambda)/s,1e-3);

%% functions
L=minreal(C*sys,1e-3);
S=minreal(feedback(1,L),1e-3);
T=minreal(1-S,1e-3);
G=minreal(feedback(L,1),1e-3);
% PLOTS OK
figure,nichols(L),hold on,myngridst(Tp,Sp)
figure(1), bodemag(S,'*')
figure(2), bodemag(T,'*')

%% step OK
figure, step(G),yline(1-0.05),yline(1+0.05),xline(2),xline(8),yline(1+0.12,'r')

%% ramp OK
t=linspace(0,10,100)
y=lsim(G,t,t);
er
y(end)-t(end)

%% act OK
t=linspace(0,10,100)
Gac=minreal(  feedback(Gpn, C*Ga) ,1e-3);
y=lsim(Gac,da*ones(size(t)),t);
ea
y(end)

%% plant OK
t=linspace(0,1000,2000)
figure,lsim(S,dp*sin(t*omegap),t),yline(ep),yline(-ep)

%% sensor OK
t=linspace(0,10,1000)
figure,lsim(T,ds*sin(t*omegas),t),yline(es),yline(-es)

%% Robust stability OK
figure,bodemag(Wu*T)

%% Robust performance (S2) OK
figure, bodemag(Wu*T+Ws0*S,logspace(-8,-6,100))

%% Robust performance (S4) OK
figure, bodemag(Wu*T+1/SLF*S,logspace(-3,log10(omegap)))

%% mu analysis on (S5) NOT RP!
Wtmu=1/THF;
om=logspace(log10(omegas),6,100);
deltaset=[-1 0;-1 0; -1 0; 1 1];
[an,bn,cn,dn]=linmod("RP_T");
N=pck(an,bn,cn,dn);
Nf=frsp(N,om);
mubounds=mu(Nf,deltaset);
figure,vplot('liv,lm',mubounds)

%% YES ALSO WITH THE MODIFIED PUI RS IS ACHIEVED




















