% Template of the matlab script file for collecting some of the 
% results of your written exam (Modern Design of Control Systems)
% 
s=tf('s');
%
% Below type your Surname (family name) and your ID number 
%
nome=['Cavalli-301494'];
%
% Below type the chosen nominal value of the uncertain variable k
%
k_n= -15;
%
% Below type the chosen nominal value of the uncertain variable p1
%
p1_n=   1;
%
% Below type the chosen nominal value of the uncertain variable p2
%
p2_n=   2 ;
%
% In the following lines, type the requested weighting functions, denoting 
% 's' as the independent complex variable. A unitary default value, 
% e.g. WS=tf(1,1), is typed to indicate that you do not have a result for
% that item. Below type the weighting function WS
%
WS=(s^2 + 0.5171*s + 0.1338)/(1.45*s^2 + 0.0232*s);
%
% Below type the weighting function WT
%
WT=(3.75*s^2 + 19.88*s + 52.73)/59.33;
%
% Below type the weighting function Wu
%
Wu=(0.92001*(s+0.7307)*(s+1.901))/((s+2.548)*(s+1.506));
 
%
% Below type the weighting function W1
%
W1=Ws;
%
% Below type the weighting function W2
%
W2=(s^2 + 7.54*s + 14.21)/15.99;
%
% Below type the weighting function W1mod
%
W1mod=0.68966*(s^2 + 0.5171*s + 0.1338)/((s+0.016)*(s+0.0005486));
%
% Below type the weighting function W2mod
%
W2mod=0.8889;
%
% Below type other possible weighting functions. 
%
WSmu=tf(1,1);
%
WTmu=100;
%
Wunew=tf(1,1);
%
% Below type the transfer function Gcmod of the designed controller as 
% obtained from the optimizer
%
Gcmod= (-1.0675e07*(s+1.759e-07)*(s+0.1525)*(s+1)*(s+2)) /...
    ((s+8.579)*(s+0.016)*(s+0.0005142)*(s^2 + 578.2*s + 1.676e05));
%
% Below type the transfer function Gc of the designed controller in its 
% final form
%
Gc=(-1.0675e07*(s+0.1525)*(s+1)*(s+2))/...
    ((s+8.579)*(s+0.016)*(s^2 + 578.2*s + 1.676e05));
%
% In the lines below, write obtained nominal performance in the time domain
% "0" (zero) is the default value if you do not have a result
% Below type the obtained rise time
%
r_tr=1.75            ;
%
% Below type the obtained settling time
%
r_ts=6.38         ;
%
% Below type the obtained percentage overshoot
%
r_sovr=7.38           ;
%
% Below type the maximum value of the absolute output error in the presence
% of da
%
r_yda=  0.0211         ;
%
% Below type the maximum value of the absolute output error in the presence
% of dp
%
r_ydp=   0.0055         ;
%
% Below type the amplitude of the steady-state output error in the presence
% of dp
%
r_ydp_ss=6.6736e-04            ;
%
% Below type the amplitude of the steady-state output error in the presence
% of ds
%
r_yds_ss=3.5e-4             ;
%
% Below type the maximum value of the Lower Bound of mu for Robust
% Performance:
%
muRPLBmax=1.135;
%
%
% Below type the maximum value of the Upper Bound of mu for Robust
% Performance:
%
muRPUBmax=1.135;
%
%
% Below type the maximum value of the Lower Bound of mu for Robust
% Stability:
%
muRSLBmax=0.47;
%
%
% Below type the maximum value of the Upper Bound of mu for Robust
% Stability:
%
muRSUBmax=0.5132;
%
% Do not modify the lines below
%
save Results_MDCS.mat  k_n p1_n p2_n WS WT Wu W1 W2 W1mod W2mod WSmu WTmu Wunew ...
     Gcmod Gc r_tr r_ts r_sovr r_yda r_ydp r_yds_ss r_ydp_ss ...
     muRPLBmax muRSLBmax muRSLBmax muRSUBmax
%
% Fill in and run this script file, in order to get the related .mat file
% 
% This script file and the related .mat file must be placed in
% sub-directory MCDS\Results
%
%