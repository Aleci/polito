clear all; close all; clc
s=tf('s');

K=[-20 -10]; p1=[0.8 1.2]; p2=[1.6;2.4];
%K=[-24 -6];  p1=[0.64 1.36]; p2=[1.28 2.72]; % modified PUI
Kn=mean(K); p1n=mean(p1); p2n=mean(p2);
deltaK=K(2)-Kn; deltap1=p1(2)-p1n; deltap2=p2(2)-p2n;

Gpn=Kn/(1+s/p1n)/(1+s/p2n)/s;
Ga=0.006;
da=2.5e-3;
dp=8.5e-3;
ds=5e-2;
omegas=40;

er=2.5e-1;
ea=1e-2;
ep=1.5e-3;
es=5e-4;
tr=3.5;
ts=14;
alpha=0.05;
ov=0.15;

S0star=0.1765;
THF=0.01;
omegac=0.5486;
Tp=1.125;
Sp=1.45;

%% Ws
Ws01=S0star*s; Ws0=1/Ws01;
zer=0.016;
pol=omegac/1.5;
Ws1=Sp*s*(s+zer)/(pol^2+s*1.414*pol+s^2); Ws=1/Ws1;
figure(1),bodemag(Ws01,Ws1),yline(mag2db(Sp),'r')
hold on, semilogx(omegac,0,'*');

%% Wt
pol=3.75;
Wt1=Tp/(1+1.414*s/pol+s^2/pol^2); Wt=1/Wt1;
figure(2), bodemag(Wt1), yline(mag2db(Tp)), 
hold on, semilogx(omegas,mag2db(THF),'*')

%% Wu
% n=5;
% om=logspace(-3,3,1000);
% mag=bode(Gpn,om);
% nomMag=squeeze(mag);
% maxMag=nomMag;
% figure, semilogx(om,nomMag,'g*'), hold on
% for ii=linspace(p1(1),p1(2),n)
%     for jj=linspace(p2(1),p2(2),n)
%         for kk=linspace(K(1),K(2),n)
%             plant=kk/(1+s/ii)/(1+s/jj)/s;
%             mag=bode(plant,om);mag=squeeze(mag);
%             semilogx(om,mag,'k');
%             maxMag=max(mag,maxMag);
%         end
%     end
% end
% semilogx(om,maxMag,'r')
% 
% deltamag=maxMag ./ nomMag -1;
% pack=vpck(deltamag,om);
% Wu=magfit(pack,[1e-3,1,1,3]);
% [au,bu,cu,du]=unpck(Wu);
% Wu=zpk(ss(au,bu,cu,du));
% 
% figure, semilogx(om,mag2db(deltamag)), hold on, bodemag(Wu)
load("Wu.mat")

%% W1
lambda=omegac/1000;
W1=minreal(  Ws*s/(s+lambda)  ,1e-3);
figure(1),hold on, bodemag(1/W1)

%% W2
pol=3.77;
W21mod=Tp/(1+s/pol)^2;  W2mod=1/W21mod;
figure(2),hold on, bodemag(W21mod)
W2=1/Tp

%% P
% P=augw(Ga*Gpn,W1,[],W2);
% P=ltisys(P.A,P.B,P.C,P.D);
% P=sderiv(P,2,[1/pol 1]);
% P=sderiv(P,2,[1/pol 1]);
% 
%% optim
% [aaaa,Cmod]=hinflmi(P,[1 1],0,1e-2,[0 0 0 1])
% [ac,bc,cc,dc]=ltiss(Cmod);
% Cmod=zpk(ss(ac,bc,cc,dc));
% no pole in zero
load("Cmod.mat")
%% Order reduction
C=minreal(  Cmod/(s+1.759e-07)*(s+0.0005142)   ,1e-3)

%% Functions
L=minreal(  C*Ga*Gpn  ,1e-3);
S=minreal(  feedback(1,L)   ,1e-3);
T=minreal(  1-S             ,1e-3);
G=minreal(  feedback(L,1)  ,1e-3);
Gac=minreal( feedback(Gpn,C*Ga)  ,1e-3);

%% Step  OK
figure, step(G), yline(1+ov,'r'), yline(1+alpha,'k'), yline(1-alpha,'k')
xline(tr,'g'), xline(ts,'k')

%% NP OK
figure(1),hold on, bodemag(S,'g*')
figure(2), hold on, bodemag(T,'g*')

%% (S2) OK
t=linspace(0,100,1000);
y=lsim(G,t,t);
er
y(end)-t(end)

%% (S3) OK
t=linspace(0,100,1000);
y=lsim(Gac,da*ones(size(t)),t);
maxerr=max(abs(y))
ea
y(end)

%% (S4) OK
t=linspace(0,100,1000);
y=lsim(S,dp*t,t);
maxerr=max(abs(y))
ep
y(end)

%% (S5) OK
t=linspace(0,10,1000);
figure,lsim(T,ds*sin(omegas*t),t); yline(es), yline(-es)

%% Robust Stability OK
fun=minreal(  Wu*T+Ws*S   ,1e-3);
figure, bodemag(fun)

%% Robust Performance (S2) (S3) (S4) OK
om=logspace(-6,-4,500);
fun=minreal( Wu*T+Ws0*S   ,1e-3);
figure, bodemag(fun,om);

%% Robust Performance (S5) NON OK
om=logspace(log10(omegas),4,100)
Wtmu=1/THF;
deltaset=[-1 0; -1 0; -1 0; 1 1];
[an,bn,cn,dn]=linmod("RP_T");
N=pck(an,bn,cn,dn);
Nf=frsp(N,om);
mub=mu(Nf,deltaset);
figure, vplot('liv,lm',mub);

%% Robust Stability different PUI OK
om=logspace(-4,4,1000)
deltaset=[-1 0; -1 0; -1 0];
[an,bn,cn,dn]=linmod("RS");
N=pck(an,bn,cn,dn);
Nf=frsp(N,om);
mub=mu(Nf,deltaset);
figure, vplot('liv,lm',mub);



