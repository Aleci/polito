clear all; close all ; clc
s=tf('s');
om=logspace(-3,3,1000);

Gs=3;
F=1/9;
Ga=-.27;

K=[20,60]; Z=[.65,.75]; omn=[1.7,2.5];
Kn=mean(K); Zn=mean(Z); omnn=mean(omn);
deltaK=K(2)-Kn; deltaZ=Z(2)-Zn; deltaOmn=omn(2)-omnn;

Gpn=Kn/(4.5*s*(1+s*Zn*2/omnn+s^2/omnn^2));
%Gpn=K(2)/(4.5*s*(1+s*Z(2)*2/omn(2)+s^2/omn(2)^2));

da=8.5e-3;
dp=3e-3;
ds=1e-2;

kd=3;
er=3.5e-1;
ea=1.75e-2;
ep=0.375;
es=3.3e-5;
tr=2.35;
ts=8;
alpha=0.05;
ov=0.09;
omegas=50;

S0star=ep/dp/2;
THF=es/ds*Gs;
Tp=1.05;
Sp=1.35;
omegac=1.98/tr;

%% Ws
pol=omegac/1.5;
Ws1=Sp*s*s/(pol^2+1.414*pol*s+s^2); Ws=1/Ws1;
Ws01=S0star*s^2; Ws0=1/Ws01;
figure(1),bodemag(Ws1,Ws01,om),yline(mag2db(Sp)), hold on,semilogx(omegac,0,'*')

%% Wt
pol=4.8;
Wt1=Tp/(1+1.414*s/pol+(s/pol)^2); Wt=1/Wt1;
figure(2),bodemag(Wt1,om), hold on, semilogx(omegas,mag2db(THF),'*'); yline(mag2db(Tp))

%% Wu
% n=5;
% mag=bode(Gpn,om); magnom=squeeze(mag);
% figure, semilogx(om,magnom,'g*'), hold on
% maxmag=magnom;
% for ii=linspace(K(1),K(2),n)
%     for jj=linspace(Z(1),Z(2),n)
%         for kk=linspace(omn(1),omn(2),n)    
%             sys=ii/(4.5*s*(1+s*jj*2/kk+s^2/kk^2));
%             mag=bode(sys,om); mag=squeeze(mag);
%             semilogx(om,mag,'k')
%             maxmag=max(mag,maxmag);
%         end
%     end
% end
% semilogx(om,maxmag,'r')
% 
% deltamag=maxmag ./ magnom -1;
% pack=vpck(deltamag,om);
% wu=magfit(pack,[1e-3,1,1,3]);
% [a,b,c,d]=unpck(wu);
% Wu=zpk(ss(a,b,c,d));
% %%
% figure, semilogx(om,mag2db(deltamag),'k'), hold on, bodemag(Wu,om,'r')
load("Wu.mat")
figure(2),bodemag(1/Wu,om)

%% W1 
lambda=omegac/1000;
W1=minreal(  Ws*s^2/(s+lambda)^2  ,1e-3);
figure(1),bodemag(1/W1,om)

%% W2               %% WU !!!!!!!!!!!!!!!!!!
pol=4.85;
W21mod=Tp/(s/pol+1)^2; W2mod=1/W21mod;
figure(2),bodemag(W21mod,om)
W2=1/Tp;

%% model
% [a,b,c,d]=linmod("myaugw");
% P=ltisys(a,b,c,d);
% P=sderiv(P,2,[1/pol 1]);
% P=sderiv(P,2,[1/pol 1]);
% [aaaa,Cmod]=hinflmi(P,[1 1],0,1e-2,[0 0 0 1]);
% [a,b,c,d]=ltiss(Cmod);
% Cmod=zpk(ss(a,b,c,d));
load("Cmod.mat")

%% model reduction
C=minreal( 1.1*Cmod*(s+lambda)/s/(s+7.586e-06)*(s+7.078e-05) ,1e-3)


%% functions
L=minreal( C*Ga*Gpn*Gs*F ,1e-3);
T=minreal( L/(1+L) ,1e-3);
S=minreal( 1-T     ,1e-3);
Gac=minreal( Gpn/(1+L),  1e-3);
G=minreal( feedback(C*Ga*Gpn,Gs*F)  ,1e-3);
Tnoise=T/Gs;
figure(1), bodemag(S,om,'g*')
figure(2), bodemag(T,om,'g*')
figure, nichols(L), hold on, myngridst(Tp,Sp)

%% step ok (S1), (S6), (S7), (S8)
figure, step(G), yline(kd+kd*ov,'r'), yline(kd-alpha*kd), yline(kd+alpha*kd), xline(tr), xline(ts)

%% RS OK
fun=minreal(  Wu*T  ,1e-3);
figure,bodemag(fun,logspace(-6,6,1000))

%% (S2) ok
t=linspace(0,10,100);
y=lsim(G,t,t);
er
y(end)-kd*t(end)

%% (S3) OK
t=linspace(0,100,100);
y=lsim(Gac,da*ones(size(t)),t);
ea
y(end)
ydamax=max(abs(y))

%% (S4) OK
t=linspace(0,100,100000);
y=lsim(S,dp*0.5*t.^2,t);
ep
y(end)
ydpmax=max(abs(y))

%% (S5) OK
t=linspace(0,10,1000);
figure,lsim(Tnoise,ds*sin(t*omegas),t);
yline(es),yline(-es)

%% RP (S5) % NON OK
Wmut=1/THF;
om=logspace(log10(omegas),4,500);
[a,b,c,d]=linmod("RP5");
deltaset=[-1 0; -2 0; -1 0; 1 1];
N=pck(a,b,c,d);
Nf=frsp(N,om);
mub=mu(Nf,deltaset);
figure,vplot('liv,lm',mub)

%% RS modified PUI OK
K=[16,64]; Z=[.52,.88]; omn=[1.36,2.84];
Kn=mean(K); Zn=mean(Z); omnn=mean(omn);
deltaK=K(2)-Kn; deltaZ=Z(2)-Zn; deltaOmn=omn(2)-omnn;

om=logspace(-5,5,1000);
[a,b,c,d]=linmod("RS");
deltaset=[-1 0; -2 0; -1 0];
N=pck(a,b,c,d);
Nf=frsp(N,om);
mub=mu(Nf,deltaset);
figure,vplot('liv,lm',mub)





















