% Template of the matlab script file for collecting some of the 
% results of your written exam (Modern Design of Control Systems)
% 
s=tf('s');
%
% Below type your Surname (family name) and your ID number 
%
nome=['Cavalli-301494'];
%
% Below type the chosen nominal value of the uncertain variable k
%
k_n= 40;
%
% Below type the chosen nominal value of the uncertain variable p1
%
p1_n=  0.7;
%
% Below type the chosen nominal value of the uncertain variable p2
%
p2_n=    2.1;
%
% In the following lines, type the requested weighting functions, denoting 
% 's' as the independent complex variable. A unitary default value, 
% e.g. WS=tf(1,1), is typed to indicate that you do not have a result for
% that item. Below type the weighting function WS
%
WS= (s^2 + 0.7942*s + 0.3155)/(1.35*s^2)
%
% Below type the weighting function WT
%
WT=(4.8*s^2 + 32.58*s + 110.6)/116.1
%
% Below type the weighting function Wu
%
Wu=1.1265*(s+0.2165)*(s^2 + 2.472*s + 3.007)/ ...
   ((s+0.2211) *(s^2 + 3.2*s + 6.653))
%
% Below type the weighting function W1
%
W1=(0.7407*s^2 + 0.5883*s + 0.2337)/ ...
   (s^2 + 0.001685*s + 7.099e-07)
%
% Below type the weighting function W2
%
W2=0.9524
%
% Below type the weighting function W1mod
%
W1mod=WS
%
% Below type the weighting function W2mod
%
W2mod=24.7/(s^2 + 9.7*s + 23.52)
%
% Below type other possible weighting functions. 
%
WSmu=tf(1,1);
%
WTmu=101.0101
%
Wunew=tf(1,1);
%
% Below type the transfer function Gcmod of the designed controller as 
% obtained from the optimizer
%
Gcmod=-3.1032e07*(s+0.2303)*(s+7.586e-06)*(s^2 + 2.94*s + 4.41) / ...
     ((s+2622)*(s+2076)*(s+11)*(s+0.0009847)*(s+7.078e-05))
%
% Below type the transfer function Gc of the designed controller in its 
% final form
%
Gc=-3.4135e07*(s+0.2303)*(s+0.0008426)*(s^2 + 2.94*s + 4.41) / ...
    (s*(s+2622)*(s+2076)*(s+11)*(s+0.0009847))
%
% In the lines below, write obtained nominal performance in the time domain
% "0" (zero) is the default value if you do not have a result
% Below type the obtained rise time
%
r_tr=1.1;
%
% Below type the obtained settling time
%
r_ts=4.67;
%
% Below type the obtained percentage overshoot
%
r_sovr=8.5333;
%
% Below type the maximum value of the absolute output error in the presence
% of da
%
r_yda= 0.0319;
%
% Below type the maximum value of the absolute output error in the presence
% of dp
%
r_ydp=0.066;
%
% Below type the amplitude of the steady-state output error in the presence
% of dp
%
r_ydp_ss=0.066;
%
% Below type the amplitude of the steady-state output error in the presence
% of ds
%
r_yds_ss=2.77e-5;
%
% Below type the maximum value of the Lower Bound of mu for Robust
% Performance:
%
muRPLBmax=84.17;
%
%
% Below type the maximum value of the Upper Bound of mu for Robust
% Performance:
%
muRPUBmax=84.17;
%
%
% Below type the maximum value of the Lower Bound of mu for Robust
% Stability:
%
muRSLBmax=0
%
%
% Below type the maximum value of the Upper Bound of mu for Robust
% Stability:
%
muRSUBmax=0.592;
%
% Do not modify the lines below
%
save Results_MDCS.mat  k_n p1_n p2_n WS WT Wu W1 W2 W1mod W2mod WSmu WTmu Wunew ...
     Gcmod Gc r_tr r_ts r_sovr r_yda r_ydp r_yds_ss r_ydp_ss ...
     muRPLBmax muRSLBmax muRSLBmax muRSUBmax
%
% Fill in and run this script file, in order to get the related .mat file
% 
% This script file and the related .mat file must be placed in
% sub-directory MCDS\Results
%
%
