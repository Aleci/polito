clear all; close all; clc
addpath("Library")
% load data
addpath("Lab4Data")
% load precomputed pui's
addpath("Lab4Data/PUI")
% Ale's set of data
noise,rect%,wave
%gaussInp,
% Carlo's set of data
C_unifInp%,C_rect
% Tipo's set of data
%tipos_data=load("T_gaussOff.mat");
%Excitation=tipos_data.dataR2;

epsilonMax=max(NoiseData(:,1));
etaMax=max(NoiseData(:,2));

% Use the exciting input to identify the system
Nskip=2000;
N=1000; % This is the random input, the longer the better
relax=2;
uhat=Excitation(Nskip+1:Nskip+1+N,1);
yhat=Excitation(Nskip+1:Nskip+1+N,2); 

% Select system orders and delays
Na=2;Nb=3;Nk=0;
% Find Parameter Uncertainty Intervals
compute=false;
if(compute)
  [theta pui]=eiv(yhat,uhat,Na,Nb,Nk,epsilonMax,etaMax,relax);
  save(sprintf("US%dN%dR%da%db%dk%d.m",Nskip,N,relax,Na,Nb,Nk),"pui","-ascii")
else
  pui=load(sprintf("US%dN%dR%da%db%dk%d.m",Nskip,N,relax,Na,Nb,Nk),"-ascii");
  theta=centralEstimate(pui);
end

yo=Excitation(N+Nskip:end,2);
uo=Excitation(N+Nskip:end,1);
ngrid=5;
sysOpt=bestFitGriddedEstimate(yo,uo,1000,pui,ngrid,Na,Nb,Nk);

uv=RectData(:,1);
yv=RectData(:,2);
% simulate the system
sys=makeSys(theta,Na,Nb,Nk);
ycesimv=lsim(sys,uv);
yoptsimv=lsim(sysOpt,uv);
% plot result
t=[1:length(uv(41940:end))];
h=plot(t,yv(41940:end),t,ycesimv(41940:end),t,yoptsimv(41940:end));
set(gca,"FontSize",14)
set(h,"LineWidth",2)
legend("Measurement","Central Estimate","Best Fit Estimate")
title(sprintf("%s: %s, %s: %d, %s: %d, %s: %d, %s: %d, %s: %d, %s: %d, %s: %d",
              "Training settings","Uniform distribution","Nskip",Nskip,"Nsamples",N,
              "Relaxation Order",relax,"Na",Na,"Nb",Nb,"Nk",Nk,"Grid Edge",ngrid))
