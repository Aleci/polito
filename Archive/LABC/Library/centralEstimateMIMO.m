% computes the central estimate for each transfer function of a MIMO system
function ce=centralEstimateMIMO(PUI)
  for jj=1:size(PUI,3)
    for ii=1:size(PUI,4)
      ce(:,1,jj,ii)=centralEstimate(PUI(:,:,jj,ii));
    end
  end
end
