% Return the snr in db (voltage) of each signal of the dataset
function [y_snr, u_snr]=dataset_snr(yhatV,uhatV,etaMaxV,epsilonMaxV)
  for jj=1:size(yhatV,2)
    y_snr(1,jj)=mag2db(rms(yhatV(:,jj)/etaMaxV(jj)));
  end
  for jj=1:size(uhatV,2)
    u_snr(1,jj)=mag2db(rms(uhatV(:,jj)/epsilonMaxV(jj)));
  end
end
