% Compute the Parameter Uncertainty Intervals of a MISO system
% yhat and uhatV are the noisy data, uhatV is the vector of the inputs
% The max input and output noise is bounded by epsilonMaxV (again a vector) and etaMax respectively
% The degrees of the denominator and numerator polynomials of each transfer function are given by Na and Nb respectively
% The order of relaxation is given by relaxOrder (the greather the better, but the slower)
% At least a relaxation order of 2 is required to have reasonable results
function [ce,PUI]=eivMISO(yhat,uhatV,Na,Nb,Nk,epsilonMaxV,etaMax,relaxOrder)
  K=size(uhatV,2); % K inputs
  N=length(yhat);  % N samples per input
  % Partial regressor construction for each input
  for ii=1:K
    indx=max(Na+1, Nb+Nk);
    yu=yhat(indx:N);
    phi{ii}=[];
    for jj=Nb:-1:1
      phi{ii}=[phi{ii}, uhatV(jj:N-indx+jj,ii)];
    end
  end

  % Convention per internal transfer function
  % Xj=[theta, yupsilon, epsilon]
  dimTheta=Na+Nb;% total contribution
  dimYupsilon=Na+1;   % single equation contribution (+1 per new equation)
  dimEpsilon=Nb;      % single equation contribution (+1 per new equation)
  Neq=N-indx+1;       % we can create only Neq rows in the partial regressor
  dimEpsilons=N-max(1,Nk)-Na+max(Nb,Nk); % Skip first (Nk or 1) samples
                                         % Skip last samples if Na>Nb
  dimX=dimTheta+N+dimEpsilons;
  dimXtot=dimX*K+N; % dimX per transfer function plus etas of output error

   %^- A variable per thetaj and a variable of noise per sampling time
%   In case of Nk input delay the related noise variables are neglected
%   In case of Nb < Na some variables shouldn't be considered

  % Basic block matrixes
  I=eye(dimTheta); Ie=eye(dimEpsilon); Fy=flip(eye(dimYupsilon-1)); Fe=flip(eye(dimEpsilon));
  On=zeros(dimEpsilon,dimYupsilon-1); Oy=zeros(dimTheta,dimYupsilon-1); Oe=zeros(dimEpsilon);
  o=zeros(1,dimTheta); oy=zeros(1,dimYupsilon-1); oe=zeros(1,dimEpsilon); O=zeros(dimTheta);

  a=ones(Na,1); b=ones(Nb,1);
  % High level block matrixes
  T=[I;o;[On,Ie]]; Y=[[Fy;On;oy],[o';1];On,oe']; E=[On';Fe;oe;Oe]; S=[o'; 0; oe'];
  C=[o;o;o]; x=zeros(1,Na); X=zeros(3,Na); ZEROS=zeros(dimTheta+1+dimEpsilon, dimX); 
  ZERO=zeros(1,dimEpsilons); ZER0=zeros(3,dimEpsilons); 
  noTerms=dimTheta+1+dimEpsilon; Z=zeros(noTerms,N);

% Single Equation and Single Transfer Function Structure
% [  ^  ,Fy, ^ ,On';  % Bilinear part: theta(1:Na)*yupsilon
%    I  ,On, o',Fe ;  % Bilinear part: theta(Na+2:Nb)*epsilon
%    o  ,oy, 1 ,oe ;  % Spare yupsilon(k)
%  On,Ie,On,oe',Oe ]  % Linear part: row(phiu)*theta (basically u*theta with correct timing)
%
% [T,  Y  , E ]  % Higher order block matrix representation
% 
% Multi Equation and Single Transfer Function Structure
% Example: 3 Constraints 
% [T, | Y, S, S,| E, S, S]; % Support 1
% [T, | S, Y, S,| S, E, S]; % Support 2
% [T, | S, S, Y,| S, S, E]; % Support 3
%
% [T,  YUPSILON , EPSILON]; = Qk % Partial assembly notation (for kth tf)
%
% Multi Equation and Multi Transfer Function Structure
% Example: 2 transfer functions (Support bond = 1 Support per row)
% [Q1  ][^]     % Support bond for intermediate output of tf1 
% [  Q2][Z]     % Support bond for intermediate output of tf2
% [SUM ][R]     % Summing bond for real output
%
% Summing bonds example for 3 constraints 2 transfer function
%            TF 1                    TF 2
% [T, | Y, S, S,| E, S, S][         ZEROS        ][ ]; %-\             /- Support 1
% [T, | S, Y, S,| S, E, S][         ZEROS        ][ ]; %  - Bond TF 1 - - Support 2
% [T, | S, S, Y,| S, S, E][         ZEROS        ][ ]; %-/             \- Support 3
% [         ZEROS        ][T, | Y, S, S,| E, S, S][ ]; %-\             /- Support 1
% [         ZEROS        ][T, | S, Y, S,| S, E, S][ ]; %  - Bond TF 2 - - Support 2
% [         ZEROS        ][T, | S, S, Y,| S, S, E][ ]; %-/             \- Support 3
% [C, | A       |  ZER0  ][C, |         |  ZER0  ][B]; %-\            /- Support 1
% [o, |         |  ZERO  ][o, | 1       |  ZERO  ][B]; %  |          |  
% [C, |    A    |  ZER0  ][C, |         |  ZER0  ][B]; %  |          | - Support 2
% [o, |         |  ZERO  ][o, |    1    |  ZERO  ][B]; %  - Bond Sum - 
% [C, |       A |  ZER0  ][C, |         |  ZER0  ][B]; %  |          | 
% [o, |         |  ZERO  ][o, |       1 |  ZERO  ][B]; %-/            \- Support 3
% Where A (for Support j) is defined as
% A (used for the first transfer function)
% [ 1 ]  % Yj selector (1 variable in the support j of degree 1)
% [ 0 ]  % The output error etak (the one will be at the end of the support row)
% [ 0 ]  % The total sum should be equal to yk (1 variable of degree 0)
% x or X are zero matrixes that null the Y variables in the first equation different from Yk
% that variables will never be considered in the sum because they are always past samples
% For convenience the bond sum is generated using sub matrixes dependent from an identity matrix
% [C,| X,    aa,    |  ZER0  ][C, | X,  a00,   |  ZER0  ][ ae ]; % - Support sJ
% [o,| x,    a0,    |  ZERO  ][o, | x,   a1,   |  ZERO  ][ a0 ]; %/

  % Equality constraints
  for TFjPtr=0:Neq:Neq*K-1 % for each transfer function generate Neq supports
    for cJ=1:Neq % starting from the pointer of the current tf populate the structure
      c{TFjPtr+cJ}.typeCone=-1; c{TFjPtr+cJ}.dimVar=dimXtot; c{TFjPtr+cJ}.degree=2;
      c{TFjPtr+cJ}.noTerms=noTerms; % fixed confs
      YUPSILON=[]; EPSILON=[];
  % Generate multi equation single tf structure
      for sJ=1:Neq
        if(sJ==cJ)
          YUPSILON=[YUPSILON,Y];
          EPSILON=[EPSILON,E];
        else
          YUPSILON=[YUPSILON,S];
          EPSILON=[EPSILON,S];
        end
      end
  % Generate padding: use only the variables related to the current tf
      support=[];
      for pJ=0:K-1
        if(pJ == TFjPtr/Neq) % if is the current tf
          support=[support, T,YUPSILON,EPSILON];
        else % otherwise add padding
          support=[support, ZEROS];
        end
      end 
      c{TFjPtr+cJ}.supports=[support, Z]; % append output error variables
      % Y(k) +theta1*Y(k-1) ... +thetaNa*Y(k-Na) -
      % -thetaNa+1*u(k+Nk) ... -thetaNa+Nb*u(k+Nk+Nb) +
      % +thetaNa+1*epsilon(k+Nk) ... thetaNa+Nb*u(k+Nk+Nb) = 0
      c{TFjPtr+cJ}.coef=[        a;                 % theta*Y variables
                                 b;                 % theta*epsilon variables
                                 1;                 % Spare Y(k)
                      -phi{TFjPtr/Neq +1}(cJ,:)'];  % -row(phi)*theta
    end
  end

  % Append summing constraints 
  basePtr=K*Neq; % we already have generated Neq constraint per each tf (K in total)
  % For each sampling instant generate a constraint
  % Helping matrixes
  AA=eye(N); a0=zeros(1,N);
  for sJ=1:N
    % configure the structure for the constraint
    c{basePtr+sJ}.typeCone=-1; c{basePtr+sJ}.dimVar=dimXtot; c{basePtr+sJ}.degree=1;
    c{basePtr+sJ}.noTerms=K+1+1; % fixed confs
    %
    support=[];

    % Temporary matrixes
    a1=AA(sJ,:); aa=[a1; a0; a0]; ae=[a0; a1; a0]; a00=[a0;a0;a0];

    % Populate the support
    for ii=1:K % for each row
      rowi=[];
      for jj=1:K % generate the row
        %
        if ii==1 % in the row of the first tf (with the known term yk)
          if jj==1 % in the column of the first tf
            rowi=[rowi, C, aa, ZER0];
          else % in the other columns
            rowi=[rowi, C, a00, ZER0];
          end
        %
        else % in the other rows without the known term yk
          if jj==ii % in the right spot place the 1 
            rowi=[rowi, o, a1, ZERO];
          else % otherwise keep zero
            rowi=[rowi, o, a0, ZERO];
          end
        end
      end
      %
      if ii==1 % add noise variable in the first tf
        rowi=[rowi, ae];
      else     % otherwise just trailing zeros
        rowi=[rowi, a0];
      end
      support=[support; rowi];
    end

    % Y1(k) + Y2(k) ... YK(k)- yhat(k) + eta(k) = 0
    c{basePtr+sJ}.supports=support;
    c{basePtr+sJ}.coef=[     1;          % The first Y variable of TF1
                             1;           % The output error eta
                         -yhat(sJ);       % The real output data
                         ones(K-1,1);];  % The other Y variables of other TF
  end


  % Variables' boundings
  myInf=1e10;
  %
  ub=[];
  for ii=1:K
    ub=[ub; myInf*[a;b]; myInf*ones(N,1); epsilonMaxV(ii)*ones(dimEpsilons,1)]; 
  end
  ub=[ub; etaMax*ones(N,1)];
  lb=-1*ub;

  % Optimization parameters
  params.relaxOrder=relaxOrder;
  params.POPsolver='active-set'; %'interior-point';
  for ii=1:K % for each tf
    for j=1:Na+Nb % find pui
      % Objective function settings
      obj.typeCone=1; obj.dimVar=dimXtot; obj.degree=1; obj.noTerms=dimXtot; % fixed confs
      obj.supports=eye(dimXtot); mask=zeros(dimXtot,1); % start from empty objective function
                                                        %
      mask(dimX*(ii-1)+j)=1; % select theta_j
      obj.coef=mask;
      % Minimize theta_j
      Status=(dimTheta*(ii-1)+j-.5)/(K*dimTheta)*100 
      [a,b,POP]=sparsePOP(obj,c,lb,ub,params);
      if(isempty(POP.xVectL))
        PUI(j,1,ii)=mask'*POP.xVect;
        warning(sprintf("Refined solution not available for min theta %d",j))
      else
        PUI(j,1,ii)=mask'*POP.xVectL;
      end
      % Maximize theta_j
      Status=(dimTheta*(ii-1)+j)/(K*dimTheta)*100 
      obj.coef=-1*obj.coef;
      [a,b,POP]=sparsePOP(obj,c,lb,ub,params);
      if(isempty(POP.xVectL))
        PUI(j,2,ii)=mask'*POP.xVect;
        warning(sprintf("Refined solution not available for max theta %d",j))
      else
        PUI(j,2,ii)=mask'*POP.xVectL;
      end
    end
  end
  ce=centralEstimateMIMO(PUI);
end
