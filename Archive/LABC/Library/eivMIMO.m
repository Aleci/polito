% Most general case: Identify from a number of input uhat (noise corrupted with maximum
% uncertainty amplitude epsilonMax) and from an other number of output yhat
% (noise corrupted with maximum uncertainty amplitude etaMax)
% Consider that in general this identification procedure requires an high snr in the training
% dataset. Also remember that the problem complexity increases exponentially with the relax
% order, so probably 1 will be enough if the available data are sufficient.
function [ce,PUI]=eivMIMO(yhatV,uhatV,Na,Nb,Nk,epsilonMaxV,etaMaxV,relaxOrder)
  M=size(yhatV,2);
  [y_snr, u_snr]=dataset_snr(yhatV,uhatV,etaMaxV,epsilonMaxV)
  pad="=================================================";
  for jj=1:M
    fprintf("\n\n%s\n   %s %d of %d\n%s\n\n\n",pad, ...
            "Identification of MISO system number",jj,M,pad)
    [ce(:,1,:,jj) PUI(:,:,:,jj)] = ...
      eivMISO(yhatV(:,jj),uhatV,Na,Nb,Nk,epsilonMaxV,etaMaxV(jj),relaxOrder);
  end
end
