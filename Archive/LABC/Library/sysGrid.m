% Create a grid of n intermediate value for each interval
% Then generates all possible system coming from the combination of those intermediate values 
function sys_family=sysGrid(PUI,n,Na,Nb,Nk)
    % Create N samples per PUI
    for jj=1:size(PUI,1)
      a(jj,:)=linspace(PUI(jj,1),PUI(jj,2),n);
    end
    % Create all possible combinations of the sampled PUI
    elements={a(1,:)};
    for jj=2:size(a,1)
      elements={elements{1,:},a(jj,:)};
    end
    combinations = cell(1, numel(elements)); %set up the varargout result
    [combinations{:}] = ndgrid(elements{:});
    combinations = cellfun(@(x) x(:), combinations,'uniformoutput',false);
    result = [combinations{:}]; % NumberOfCombinations by N matrix. Each row is unique
    % Generates all possible systems
    for jj=1:size(result,1)
      sys_family{jj}=makeSys(result(jj,:),Na,Nb,Nk);
    end
end
