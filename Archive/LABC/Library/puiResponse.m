% This function generates all the possible responses of the system family
% obtained by gridding the PUI with edge n and plots them in a graph.
function puiResponse(y,u,PUI,n,Na,Nb,Nk)
  sys_family=sysGrid(PUI,n,Na,Nb,Nk);
  t=[1:length(y)];
  figure, hold on
  h=plot(t,y,'r'),set(h,"LineWidth",5),title("PUI validation")
  for jj=1:length(sys_family)
    ysim=lsim(sys_family{jj},u);
    plot(t,ysim,'k');
  end
  hold off
end
