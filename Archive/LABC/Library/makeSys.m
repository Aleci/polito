% Constructs the transfer function starting from the arx parametric description
function sys=makeSys(theta,Na,Nb,Nk,Ts)
  if nargin==4, Ts=1; end   % sets the default sampling time
  % Nb not used actually since can be inferred from Na and length theta
  sys=tf([theta(Na+1:end), zeros(1,Nk)],[1, theta(1:Na)],Ts);
end
