% Return the central estimate for each interval
function ce=centralEstimate(PUI)
  for jj=1:size(PUI,1)
    ce(jj)=sum(PUI(jj,:))/2;
  end
end
