% This function generates all the possible responses of the system family
% obtained by gridding the PUI with edge n and plots them in a graph.
function puiResponseMIMO(y,u,PUI,n,Na,Nb,Nk,Nskip)
  K=size(u,2); % number of inputs
  M=size(y,2); % number of outputs
  t=[1:size(y,1)]'; % time vector
  % Compute system family for each transfer function involved
  for ii=1:M % for each output
    for jj=1:K % for each tf of forming that output
      sys_family{jj,ii}=sysGrid(PUI(:,:,jj,ii),n,Na,Nb,Nk);
      for kk=1:length(sys_family{jj,ii}) % for each possible realization in the gridding
        ypar(:,kk,jj)=lsim(sys_family{jj,ii}{kk},u(:,jj)); % compute the partial output simulation
      end
    end
    % Compute the sum of the partial output for each realization kk and for each output ii
    for kk=1:length(sys_family{jj,ii})
      sumY=zeros(size(t));
      for jj=1:K
        sumY=sumY+ypar(:,kk,jj);
      end
      ysim(:,kk,ii)=sumY;
    end
  end
  % Generate a plot for each output
  for ii=1:M
    figure
    h=plot(t(Nskip:end),y(Nskip:end,ii),'r');
    set(h,"LineWidth",2); hold on
    for kk=1:size(ysim,2)
      if(abs(ysim(end,kk,ii))>abs(1000*y(end,ii))) % if the output diverge
        warning("One realization of output %d is diverging",ii) % warn
      else
        plot(t(Nskip:end),ysim(Nskip:end,kk,ii),'k'); % otherwise plot
      end
    end
    title(sprintf("%s %d (Ngrid: %d, Na: %d, Nb: %d, Nk: %d)", ...
                  "PUI validation of output",ii,n,Na,Nb,Nk))
    hold off
  end
end
