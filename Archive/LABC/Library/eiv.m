% Compute the Parameter Uncertainty Intervals
% yhat and uhat are the noisy data
% The max input and output noise is bounded by epsilonMax and etaMax respectively
% The degrees of the denominator and numerator polynomials are given by Oa and Ob respectively
% The order of relaxation is given by relaxOrder (the greather the better, but the slower)
% At least a relaxation order of 2 is required to have reasonable results
function [ce,PUI]=eiv(yhat,uhat,Na,Nb,Nk,epsilonMax,etaMax,relaxOrder)
  N=length(yhat);
  [y_snr, u_snr]=dataset_snr(yhat,uhat,etaMax,epsilonMax)
  % Regressor construction
  indx=max(Na+1, Nb+Nk);
  yu=yhat(indx:N);
  phi=[];
  for j=1:Na 
    phi=[phi, -yhat(indx-j:N-j)];
  end
  for j=Nb:-1:1
    phi=[phi, uhat(j:N-indx+j)];
  end

  % Convention
  % X=[theta, eta, epsilon]
  dimTheta=Na+Nb;% total contribution
  dimEta=Na+1;   % single equation contribution (+1 per new equation)
  dimEpsilon=Nb; % single equation contribution (+1 per new equation)
  dimYk=1;       % single equation contribution (+1 per new equation)
  Neq=N-indx+1;  % we can create only Neq rows in the linear regressor
  % each output sampling but the lastone tleads to a noise variable
  dimEpsilons=N-max(1,Nk)-Na+max(Nb,Nk); % Skip first (Nk or 1) samples
                                         % Skip last samples if Na>Nb
  dimX=dimTheta+N+dimEpsilons;
  %^- A variable per thetaj and a variable of noise per sampling time
  %   In case of Nk input delay the related noise variables are neglected
  %   In case of Nb < Na some variables shouldn't be considered

  % Basic block matrixes
  I=eye(dimTheta); Ih=flip(eye(dimEta-1)); Ie=flip(eye(dimEpsilon));
  On=zeros(dimEpsilon,dimEta-1); Oh=zeros(dimTheta,dimEta-1); Oe=zeros(dimTheta,dimEpsilon);
  o=zeros(1,dimTheta); oh=zeros(1,dimEta-1); oe=zeros(1,dimEpsilon);
  a=ones(Na,1); b=ones(Nb,1);
  % High level block matrixes
  T=[I;o;o;I]; H=[[Ih;On;oh;oh],[o';1;0];Oh,o']; E=[On';Ie;oe;oe;Oe]; S=[o'; 0; 0; o'];

  % Single Equation Structure
  % [I,Ih,o',On';  % Bilinear part: theta(1:Na)*eta
  %  ^,On,^ ,Ie ;  % Bilinear part: theta(Na+2:Nb)*epsilon
  %  o,oh,1 ,oe ;  % Spare eta(k)
  %  o,oh,0 ,oe ;  % Known term y(k)
  %  I,Oh,o',Oe ]  % Linear part: row(phi)*theta
  %
  % [T,  H  , E ]  % Higher order block matrix representation
  % 
  % Multi Equation Structure
  % Example: 3 Constraints 
  % [T, | H, S, S,| E, S, S]; % Support 1
  % [T, | S, H, S,| S, E, S]; % Support 2
  % [T, | S, S, H,| S, S, E]; % Support 3
  %
  % [T,     ETA   , EPSILON]; % Final assembly notation

  % Equality constraints
  for cJ=1:Neq
    c{cJ}.typeCone=-1; c{cJ}.dimVar=dimX; c{cJ}.degree=2;
    c{cJ}.noTerms=dimTheta+dimEta+dimEpsilon+dimYk; % fixed confs
    ETA=[]; EPSILON=[];
    for sJ=1:Neq
      if(sJ==cJ)
        ETA=[ETA,H];
        EPSILON=[EPSILON,E];
      else
        ETA=[ETA,S];
        EPSILON=[EPSILON,S];
      end
    end
    c{cJ}.supports=[T,ETA,EPSILON];
    c{cJ}.coef=[    -a;           % -theta*eta variables
                     b;           % theta*epsilon variables
                    -1;           % Spare -eta(k)
                   yu(cJ);        % Spare y(k)
                -phi(cJ,:)'];     % -row(phi)*theta
  end

  % Variables' boundings
  myInf=1e10;
  % Boundings construction method similar to the support one
  % This contains the variables present in the first equation
  ub=[myInf*[a;b];etaMax*ones(N,1);epsilonMax*ones(dimEpsilons,1)]; 
  lb=-1*ub;
  %
  % Optimization parameters
  params.relaxOrder=relaxOrder;
  params.POPsolver='interior-point';
  for j=1:Na+Nb
    % Objective function settings
    obj.typeCone=1; obj.dimVar=dimX; obj.degree=1; obj.noTerms=dimX; % fixed confs
    obj.supports=eye(dimX); mask=zeros(dimX,1); % start from empty objective function
                                                %
    mask(j)=1; % select theta_j
    obj.coef=mask;
    % Minimize theta_j
    Status=(j-.5)/(Na+Nb)*100
    [a,b,POP]=sparsePOP(obj,c,lb,ub,params);
    if(isempty(POP.xVectL))
      PUI(j,1)=mask'*POP.xVect;
      warning("Refined solution not available for min theta %d",j)
    else
      PUI(j,1)=mask'*POP.xVectL;
    end
    % Maximize theta_j
    Status=j/(Na+Nb)*100
    obj.coef=-1*obj.coef;
    [a,b,POP]=sparsePOP(obj,c,lb,ub,params);
    if(isempty(POP.xVectL))
      PUI(j,2)=mask'*POP.xVect;
      warning("Refined solution not available for max theta %d",j)
    else
      PUI(j,2)=mask'*POP.xVectL;
    end
  end
  ce=centralEstimate(PUI);
end
