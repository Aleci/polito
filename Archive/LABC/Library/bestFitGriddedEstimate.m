% This function will perform a gridding of the intervals and will evaluate the
% best fit index for each system belonging to the gridded family
% Then it will return the system that maximize the index
% y=training output, u=training input,
% Nskip=number of samples skept from the best fit index computation (transient)
% PUI=parameters intervals, n=number of intermediate value for each interval
% Na,Nb,Nk system order description
function [bfSys bfVals indx]=bestFitGriddedEstimate(y,u,Nskip,PUI,n,Na,Nb,Nk)
  sys_family=sysGrid(PUI,n,Na,Nb,Nk);

  % Compute best fit after a transient
  N=length(y);
  yavg=sum(y(Nskip+1:end))/(N-Nskip); % mean real output
  yvar=sum((y(Nskip+1:end)-yavg).^2)/(N-Nskip);     % mean variance of the real output

  % Compute the best fit value for each system
  maxBestFit=0;
  for jj=1:length(sys_family);
    ysim=lsim(sys_family{jj},u);
    MSE=sum((y(Nskip+1:end)-ysim(Nskip+1:end)).^2)/(N-Nskip); % mean squared error
    bfVals(jj)=1-sqrt( MSE/yvar );
  end

  % Maximize the best fit
  [bestFit,indx]=max(bfVals);
  bfSys=sys_family{indx};
end
