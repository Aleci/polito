clear all; close all; clc
addpath("Library")
% load data
addpath("Lab6Data")
% load precomputed pui's
addpath("Lab6Data/PUI")

% Use the exciting input to identify the system
Nskip=2000;
N=100; % This is the random input, the longer the better

% Additional configurations
relax=1;
nGrid=3;
compute=false;

% Select system orders and delays
Na=2;Nb=3;Nk=0;

% Select noise boundings
noise=load("noise.mat");
epsilonMax1=max(noise.noise_u1);
epsilonMax2=max(noise.noise_u2);
epsilonMaxV=[epsilonMax1 epsilonMax2];
etaMax=max(noise.noise_out);

% Trim training dataset
excitation=load("gaus_u1u2_s1000_off2_std1.mat");
uhatV(:,1)=excitation.gaus_u1(Nskip+1:Nskip+1+N);
uhatV(:,2)=excitation.gaus_u2(Nskip+1:Nskip+1+N);
yhat=excitation.gaus_out(Nskip+1:Nskip+1+N); 

% Compute data quality
[y_snr, u_snr]=dataset_snr(yhat,uhatV,etaMax,epsilonMaxV)

% Find Parameter Uncertainty Intervals
if(compute)
  [theta pui]=eivMISO(yhat,uhatV,Na,Nb,Nk,epsilonMaxV,etaMax,relax);
  save(sprintf("GS%dN%dR%da%db%dk%d.mat",Nskip,N,relax,Na,Nb,Nk),"pui")
else
  load(sprintf("GS%dN%dR%da%db%dk%d.mat",Nskip,N,relax,Na,Nb,Nk));
  theta=centralEstimateMIMO(pui);
end

% Trim validation dataset
validation=load("square_u1u2_s1000_off2_freq5.mat");
uval=[validation.square_u1 validation.square_u2];
yval=validation.suqare_out;
puiResponseMIMO(yval,uval,pui,nGrid,Na,Nb,Nk,4700)
