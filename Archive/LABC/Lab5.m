clear all; close all; clc; addpath("Library")
% Initial settings
G1=tf([2, 1.8, 0.68],[1, 1.4, 0.76, 0.184],1);
G2=tf([3, 3.4, 1.28],[1, 1.4, 0.76, 0.184],1);
N=50;
etaMax=0.01; % the data are noisy if the snr is lower than 20db
epsilonMax=0.001; % the data are clean if the snr is higher than 60db
% Data generation
u1=rand(N,1); %random in
u2=rand(N,1); %random in
y1=lsim(G1,u1); %noiseless out
y2=lsim(G2,u2); %noiseless out
y=y1+y2;
eta=2*etaMax*rand(N,1)-etaMax; %output noise
epsilon1=2*epsilonMax*rand(N,1)-epsilonMax; %input noise
epsilon2=2*epsilonMax*rand(N,1)-epsilonMax; %input noise
yhat=y+eta;
uhat=[u1+epsilon1, u2+epsilon2];
epsilonMaxV=[epsilonMax, epsilonMax];
y_snr=mag2db(rms(yhat/etaMax))
u_snr=[mag2db(rms(uhat(1)/epsilonMaxV(1))) mag2db(rms(uhat(2)/epsilonMaxV(2)))]

Na=3; Nb=3; Nk=0;
relaxOrder=1;
[ce,PUI]=eivMISO(yhat,uhat,Na,Nb,Nk,epsilonMaxV,etaMax,relaxOrder)
