clear all; close all; clc
addpath("Library")
N=10;
etaMax=0.1;
epsilonMax=0.01; % output error structure if this is zero
eta=2*etaMax*rand(N,1)-etaMax; %output noise
epsilon=2*epsilonMax*rand(N,1)-epsilonMax; %input noise

% System 1 Na > Nb > Nk (Generic tf, delay present) OK
%G=tf([0, 32.24, 21.41],[1, -0.7647, 0.3012],1);
%Na=2; Nb=2; Nk=1;

% System 2 Na > Nb < Nk (Maximum delay tf) OK
%G=tf([0, 0, 21.41],[1, 0.2, -0.7647, 0.3012],1);
%Na=3; Nb=1; Nb=2;

% System 3 Na = Nb ; Nk=0 (Strictly proper) OK  ATTENZIONE QUI SBAGLIATO
%G=tf([ 28, 21.41],[1, -0.7647, 0.3012],1);
%Na=2; Nb=2; Nk=0;

% System 4 Na = Nk+1 ; Nk=0 (Non strictly proper tf) ATTENZIONE QUI! SBAGLIATO 
G=tf([0, 32.24, 21.41],[1, -0.7647, 0.3012],1);
Na=2; Nb=3; Nk=0;

% Data generation
u=rand(N,1); %random in
y=lsim(G,u); %noiseless out
yhat=y+eta;
uhat=u+epsilon;
[ce, pui]=eiv(yhat,uhat,Na,Nb,Nk,epsilonMax,etaMax,2);
G
pui
