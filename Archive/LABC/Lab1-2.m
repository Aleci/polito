clear all; close all; clc
pkg load control
s=tf('s');
% DATA
N=10;
G=100/(s^2+1.2*s+1);
Gd=c2d(G,1,'zoh')
u=rand(N,1);

yr=lsim(Gd,u); 
  [num,den]=tfdata(Gd,"v");
  var=5;
  e=sqrt(var)*randn(N,1); % e ~ N(0,5)
  thetaTrue=[den(2:end)';num'];
% EX0 No noise     err=2.5e-13  errSM=3.6e-14

% EX1 Equation Error   err=0.56 errSM=0.039 N=1000
errTF=tf(1,den,1);
u=u;
y=yr+lsim(errTF,e);

% EX2 Output Error    err=6.7 N=10
%y=yr+e;

% Manual LS L2-norm (ARX)
Na=2;Nb=2;Nk=0;

indx=max(Na+1, Nb+Nk);
yu=y(indx:N);
phi=[];
for j=1:Na 
  phi=[phi, -y(indx-j:N-j)];
end
for j=Nb:-1:1
  phi=[phi, u(j:N-indx+j)];
end
thetaLS=phi\yu
% ARX idsys=arx(iddata(y,u,1),'na',2,'nb',2,'nk',0)

%update noise assumption
emaxVal=sqrt(var)*erfinv(.999);
e=rand(N,1)*2*emaxVal-emaxVal;

% Ex 1 Equation Error
y=yr+lsim(errTF,e);

% update phi with the new noise
indx=max(Na+1, Nb+Nk);
yu=y(indx:N);
phi=[];
for j=1:Na 
  phi=[phi, -y(indx-j:N-j)];
end
for j=Nb:-1:1
  phi=[phi, u(j:N-indx+j)];
end

pkg load optim

A=[phi -ones(length(phi),1); -phi -ones(length(phi),1)];
b=[yu; -yu];
c=[zeros(size(thetaLS)); 1];
x=linprog(c,A,b);
thetaLSinf=x(1:size(thetaLS)) % cancel gamma and get theta

A=pinv(phi);
modA=A .* sign(A);
emaxVect=emaxVal*ones(size(yu));
thetaSM_EUI(:,1)= A*yu - modA*emaxVect; %min
thetaSM_EUI(:,2)= 2*thetaLS - thetaSM_EUI(:,1); %max

% min c'*x s.t. A*x<=b
A=[phi; -phi];
b=[yu+emaxVect; -yu+emaxVect];
%
c=[1,0,0,0]'; % min thetaSM1
thetaSM_PUI(1,1)=[1,0,0,0]*linprog(c,A,b);
c=[-1,0,0,0]'; % max thetaSM1
thetaSM_PUI(2,1)=[1,0,0,0]*linprog(c,A,b);
%
c=[0,1,0,0]'; % min thetaSM2
thetaSM_PUI(1,2)=[0,1,0,0]*linprog(c,A,b);
c=[0,-1,0,0]'; % max thetaSM2
thetaSM_PUI(2,2)=[0,1,0,0]*linprog(c,A,b);
%
c=[0,0,1,0]'; % min thetaSM3
thetaSM_PUI(1,3)=[0,0,1,0]*linprog(c,A,b);
c=[0,0,-1,0]'; % max thetaSM3
thetaSM_PUI(2,3)=[0,0,1,0]*linprog(c,A,b);
%
c=[0,0,0,1]'; % min thetaSM4
thetaSM_PUI(1,4)=[0,0,0,1]*linprog(c,A,b);
c=[0,0,0,-1]'; % max thetaSM4
thetaSM_PUI(2,4)=[0,0,0,1]*linprog(c,A,b);

thetaSM(1)=sum(thetaSM_PUI(:,1))/2; % central estimate
thetaSM(2)=sum(thetaSM_PUI(:,2))/2; 
thetaSM(3)=sum(thetaSM_PUI(:,3))/2; 
thetaSM(4)=sum(thetaSM_PUI(:,4))/2; 
thetaSM=thetaSM'

% Print results
errLS=sum(abs(thetaTrue-thetaLS))
errLSinf=sum(abs(thetaTrue-thetaLSinf))
errSM=sum(abs(thetaTrue-thetaSM))
