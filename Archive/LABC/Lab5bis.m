clear all; close all; clc; addpath("Library")
% Initial settings
G1=tf([2, 1.8, 0.68],[1, 1.4, 0.76, 0.184],1);
G2=tf([3, 3.4, 1.28],[1, 1.4, 0.76, 0.184],1);
N=50;
etaMax=0.001; % the data are noisy if the snr is lower than 20db
epsilonMax=0.0001; % the data are clean if the snr is higher than 60db
% Data generation
u=rand(N,1); %random in
y1=lsim(G1,u); %noiseless out
y2=lsim(G2,u); %noiseless out
eta=2*etaMax*rand(N,1)-etaMax; %output noise
epsilon=2*epsilonMax*rand(N,1)-epsilonMax; %input noise
yhatV=[y1+eta, y2+eta]
uhatV=[u+epsilon];
epsilonMaxV=epsilonMax;
etaMaxV=[etaMax, etaMax];
[y_snr, u_snr]=dataset_snr(yhatV,uhatV,etaMaxV,epsilonMaxV)

Na=3; Nb=3; Nk=0;
relaxOrder=1;
[ce,PUI]=eivMIMO(yhatV,uhatV,Na,Nb,Nk,epsilonMaxV,etaMaxV,relaxOrder)
