clear all; close all; clc
% Initial settings
%z=tf("z",1); q=1/z; Gp=minreal((32.34*q + 21.41*q^2)/(1-0.7647*q+0.3012*q^2))
G=tf([0, 32.24, 21.41],[1, -0.7647, 0.3012],1);
N=7;
etaMax=0.1;
epsilonMax=0.01; % output error structure if this is zero
% Data generation
u=rand(N,1); %random in
y=lsim(G,u); %noiseless out
eta=2*etaMax*rand(N,1)-etaMax; %output noise
epsilon=2*epsilonMax*rand(N,1)-epsilonMax; %input noise
yhat=y+eta;
uhat=u+epsilon;

% Select system order and delays
Na=2;Nb=2;Nk=1;

% Regressor construction
indx=max(Na+1, Nb+Nk);
yu=yhat(indx:N);
phi=[];
for j=1:Na 
  phi=[phi, -yhat(indx-j:N-j)];
end
for j=Nb:-1:1
  phi=[phi, uhat(j:N-indx+j)];
end

% Convention
% X=[theta, eta, epsilon]
dimTheta=Na+Nb;% total contribution
dimEta=Na+1;   % single equation contribution (+1 per new equation)
dimEpsilon=Nb; % single equation contribution (+1 per new equation)
dimYk=1;       % single equation contribution (+1 per new equation)
Neq=N-indx+1;  % we can create only Neq rows in the linear regressor
% each output sampling but the lastone tleads to a noise variable
dimX=dimTheta+N+(N-Nk);
%^- A variable per thetaj and a variable of noise per equation plus a spare eta
%   In case of Nk input delay the related noise variables are neglected

% Basic block matrixes
I=eye(dimTheta); Ih=flip(eye(dimEta-1)); Ie=flip(eye(dimEpsilon));
On=zeros(dimEpsilon,dimEta-1); Oh=zeros(dimTheta,dimEta-1); Oe=zeros(dimTheta,dimEpsilon);
o=zeros(1,dimTheta); oh=zeros(1,dimEta-1); oe=zeros(1,dimEpsilon);
a=ones(Na,1); b=ones(Nb,1);
% High level block matrixes
T=[I;o;o;I]; H=[[Ih;On;oh;oh],[o';1;0];Oh,o']; E=[On';Ie;oe;oe;Oe]; S=[o'; 0; 0; o'];

% Single Equation Structure
% [I,Ih,o',On';  % Bilinear part: theta(1:Na)*eta
%  ^,On,^ ,Ie ;  % Bilinear part: theta(Na+2:Nb)*epsilon
%  o,oh,1 ,oe ;  % Spare theta(Na+1)*eta(k)
%  o,oh,0 ,oe ;  % Known term y(k)
%  I,Oh,o',Oe ]  % Linear part: row(phi)*theta
%
% [T,  H  , E ]  % Higher order block matrix representation
% 
% Multi Equation Structure
% Example: 3 Constraints 
% [T, | H, S, S,| E, S, S]; % Support 1
% [T, | S, H, S,| S, E, S]; % Support 2
% [T, | S, S, H,| S, S, E]; % Support 3
%
% [T,     ETA   , EPSILON]; % Final assembly notation

% Equality constraints
for cJ=1:Neq
  c{cJ}.typeCone=-1; c{cJ}.dimVar=dimX; c{cJ}.degree=2;
  c{cJ}.noTerms=dimTheta+dimEta+dimEpsilon+dimYk; % fixed confs
  ETA=[]; EPSILON=[];
  for sJ=1:Neq
    if(sJ==cJ)
      ETA=[ETA,H];
      EPSILON=[EPSILON,E];
    else
      ETA=[ETA,S];
      EPSILON=[EPSILON,S];
    end
  end
  c{cJ}.supports=[T,ETA,EPSILON];
  c{cJ}.coef=[    -a;           % -theta*eta variables
                   b;           % theta*epsilon variables
                  -1;           % Spare -theta*eta(k)
                 yu(cJ);        % Spare y(k)
              -phi(cJ,:)'];     % -row(phi)*theta
end

% Variables' boundings
myInf=1e10;
% Boundings construction method similar to the support one
% This contains the variables present in the first equation
ub=[myInf*[a;b];etaMax*ones(N,1);epsilonMax*ones(N-Nk,1)]; 
lb=-1*ub;
%
% Optimization parameters
params.relaxOrder=2;
params.POPsolver='interior-point';

for j=1:Na+Nb
% Objective function settings
  obj.typeCone=1; obj.dimVar=dimX; obj.degree=1; obj.noTerms=dimX; % fixed confs
  obj.supports=eye(dimX); mask=zeros(dimX,1); % start from empty objective function
  %
  mask(j)=1; % select theta_j
  obj.coef=mask;
  % Minimize theta_j
  Status=j-.5
  [a,b,POP]=sparsePOP(obj,c,lb,ub,params);
  PUI(j,1)=mask'*POP.xVect;
  % Maximize theta_j
  Status=j
  obj.coef=-1*obj.coef;
  [a,b,POP]=sparsePOP(obj,c,lb,ub,params);
  PUI(j,2)=mask'*POP.xVect;
end
G
PUI
