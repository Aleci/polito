clear all; close all; clc
addpath("Library")
addpath("Lab9Pui")
s=tf('s');

% Consider n samples
N=200;
SamplesPerTransient=55; 
etaMax=0.0001;

% Continuous time unknown model
Gs=4500/(s^2+16*s+4500);

% Desired continuous time close loop model
Ms=(51.54*s+861.8)/(s^2+56.97*s+861.8);

% Discretize models
Ts=stepinfo(Gs).SettlingTime/SamplesPerTransient;
Gd=c2d(Gs,Ts,'zoh');
Md=c2d(Ms,Ts,'zoh');

% Acquire measurements
u=3*rand(N,1)-1; %random in distributed with mean different from 0
y=lsim(Gd,u); %noiseless out
eta=2*etaMax*rand(N,1)-etaMax; %output noise
yhat=y+eta;

% Generate the synthetic signal
sd=lsim(Md/(1-Md),u);

% Estimate the controller
Na=3;Nb=4;Nk=0; relax=1; 
compute=false;
if(compute)
  [theta pui]=eiv(sd,yhat,Na,Nb,Nk,etaMax,0,relax);
  save(sprintf("CONTR-N%dR%da%db%dk%d.m",N,relax,Na,Nb,Nk),"pui","-ascii")
else
  pui=load(sprintf("CONTR-N%dR%da%db%dk%d.m",N,relax,Na,Nb,Nk),"-ascii");
  theta=centralEstimate(pui);
end

% Compute central estimate controller
Kd=makeSys(theta,Na,Nb,Nk,Ts);
Ks=d2c(Kd,'tustin');

% Simulate the preformances in continuos time
figure
loop=feedback(Ks*Gs,1);
stepplot(loop,Ms);

% Simulate the performances in discrete time
figure
loop=feedback(Kd*Gd,1);
stepplot(loop,Md);
