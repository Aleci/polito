classdef Lib
    methods (Static)
% ---------------------------------- LIBRARY BEGIN ---------------------------------------------
      
% CUT THE MEASUREMENTS TO OBTAIN A SINGLE CYCLE OF SINE WAVE PER SIGNAL
% ARGS: data, frequency of the signals (-1 if unknown)
function std_data=standardizeData(datatbl,freq)
  if(freq==-1)
    freq=Lib.estimateFrequency(datatbl);
  end
  [t,sine]=Lib.getSine(freq,Lib.getTsample(datatbl)); % generate sine wave mask
  len=length(sine);
  % set each signal in phase with the mask and cut the first cycle
  Var1=datatbl.Var1; 
  Var2=Lib.setInPhase(sine,datatbl.Var2);
  Var3=Lib.setInPhase(sine,datatbl.Var3);
  std_data=table(Var1(1:len),Var2(1:len),Var3(1:len));
end

% CREATE A COUPLE TIME, SIGNAL WITH A SINGLE SINE WAVE PERIOD
function [t,sine]=getSine(freqWave,Tsampling)
    Tcycle=1/freqWave;
    t=0:Tsampling:Tcycle; 
    sine=sin(2*pi*freqWave*t);
end

% TRIM THE DATA
% ARGS: data, lower bound index, upper bound index
function cutted=dataTrim(datatbl,lbi,ubi)
    cutted=table(datatbl.Var1(lbi:ubi),datatbl.Var2(lbi:ubi),datatbl.Var3(lbi:ubi));
end

% GET THE FREQUENCY OF THE MEASUREMENTS
function freq=estimateFrequency(datatbl)
  Tsample=Lib.getTsample(datatbl);
  autocorr=xcorr(datatbl.Var2,datatbl.Var2); % compute autocorrelation
  [~,locs]=findpeaks(autocorr); % find peaks indexes in the autocorrelation
  period=mean(diff(locs)*Tsample); % find the average distance between peaks and multiply for sampling time
  freq=1/period;
end

% RETURN THE SAMPLING TIME OF THE DATA
function T=getTsample(datatbl)
  T=datatbl.Var1(2);
end

function estimate=estimateGainNormalizedData(datatbl,freq)
  std_data=Lib.standardizeData(datatbl,freq);
  estimate=Lib.estimateGainLS(std_data);
end

% GET THE BEST ESTIMATE OF THE INPUT/OUTPUT GAIN IN THE LEAST SQUARE SENSE
% WARNING: Missing estimate confidence interval
function theta=estimateGainLS(datatbl)
  y=Lib.phaseShift(datatbl,0);
  fi=datatbl.Var2;
  theta=pinv(fi)*y;
end

% GET THE THIRD SIGNAL OF THE DATA STRUCTURE IN PHASE WITH THE SECOND ONE
% ARGS: data structure, figure number, suppress plot if zero
function Var3Prime=phaseShift(datatbl,n)
    Var3Prime=Lib.setInPhase(datatbl.Var2,datatbl.Var3);
    if (n ~= 0)
        figure(n)
        plot(datatbl.Var1,datatbl.Var2,'k'), hold on
        plot(datatbl.Var1,Var3Prime,'r')
    end
end

% SHIFT THE SECOND SIGNAL TO GET IT IN PHASE WITH THE FIRST ONE
% ARGS: reference signal, to be shifted signal
function sig3=setInPhase(sig1,sig2)
    % compute crosscorretaltion in both direction
    [c1,lags1] = xcorr(sig2,sig1);      
    [c2,lags2] = xcorr(sig1,sig2); 
    % compute the delay steps between the cosscorrelation peak and the first sample
    s1 = lags1(c1==max(c1)); 
    s2 = lags2(c2==max(c2));
    % shift the sampled data of s steps
    sig3a = circshift(sig2,s1);
    sig3b = circshift(sig2,s2);
    % choose the crosscorrelation without the jump in the length of the first signal
    sig3aC=sig3a(1:length(sig1));
    sig3bC=sig3b(1:length(sig1));% trim the signal to the length of the first signal
    excA=abs(min(diff(sig3aC))) + abs(max(diff(sig3aC))); % compute max excursion
    excB=abs(min(diff(sig3bC))) + abs(max(diff(sig3bC)));
    if(excA<excB) % the minimum excursion one should not have the jump
        sig3=sig3a;
    else
        sig3=sig3b;
    end
end

% LOAD DIFFERENTIAL DATA FROM CSV, PERFORM SUBTRACTION VAR3-VAR4 AND STORE IT IN A STANDARD TABLE
% This function has the same interface as plotCsv and cast the data into the standard 3-signal format
function datatbl=plotCsvSub(file, n, i,j, picTitle, legend1, legend2, unitX, unitY);
    raw_datatbl=Lib.loadData(file);
    sub=raw_datatbl.Var3-raw_datatbl.Var4;
    datatbl=table(raw_datatbl.Var1,raw_datatbl.Var2,sub);
    Lib.plotData(datatbl, n, i,j, picTitle, legend1, legend2, unitX, unitY);
end

% LOAD DATA FROM CSV, PLOT THEM DIRECLY AND RETURN THE CORRESPONDING DATA STRUCTURE
% ARGS: data file, figure and subplot numbers, title, legend configs, label axis configs
function datatbl=plotCsv(file, n, i,j, picTitle, legend1, legend2, unitX, unitY)
    datatbl=Lib.loadData(file);
    Lib.plotData(datatbl, n, i,j, picTitle, legend1, legend2, unitX, unitY);
end

% LOAD A DATA TABLE WITH TIME SIGNAL IN SECONDS
function datatbl=loadData(file)
  datatbl=readtable(file);
  fid = fopen(file, 'r');
  cellFile = textscan(fid, '%s'); 
  fclose(fid);
  rawmetadata = cellFile{1}{2};
  metadata=split(rawmetadata,',');
  Tsampling=str2double(metadata{end});
  datatbl.Var1=datatbl.Var1 .* Tsampling;
end

% PLOT A DATA TABLE
% ARGS: data, figure and subplot numbers, title, legend configs, label axis configs
function plotData(datatbl, n, i,j, picTitle, legend1, legend2, unitX, unitY)
    figure(n), subplot(i,1,j)
    plot(datatbl.Var1,datatbl.Var2,'k')
    hold on, xlabel(unitX), ylabel(unitY)
    plot(datatbl.Var1,datatbl.Var3,'r')
    title(picTitle), legend(legend1,legend2)
end

%---------------------- LIBRARY END --------------------------------------------------------------------------

end
end
