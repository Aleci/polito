close all; clear all; clc
cd ~/Git/PoliTO/ESM_Labs/Lab4/
addpath("..")

% Load and trim data
aleRaw=Lib.loadData("Data/R39Kohm1khz.csv");
ale=table(aleRaw.Var1(96:893)-aleRaw.Var1(95),aleRaw.Var2(96:893));

plot(ale.Var1,ale.Var2,'k'), hold on
plot(ale.Var1,movmean(ale.Var2,3),'r')

q=mean(ale.Var2(1:15));
u=linspace(q,mean(ale.Var2(783:end)),length(ale.Var2))';
m=u \ ale.Var2;
plot(ale.Var1,m*u,'g')
