clear all; clc
%% Device constraints
% (OPAMP MAXCURRENT) Io e {-14mA,14mA} [positive current limit, p.4]
maxSup=14;  %(OPAMP SUPPLY) Vout e {-14V,14V} [features outline, p.1]
mibc=.4e-9; %OPAMP MAX INPUT BIAS CURRENT=.4nA [absolute maximum ratings, p.2]
% (COUNTER SUPPLY) Vout e {5V,20V} [features outline, p.1]
% (COUNTER MAXCURRENT@15V) Io e {0,26mA} [figure 2, p.8]
mor=300;    %COUNTER MAX OUTPUT RESISTANCE=300 Ohm [professor's hint]

%% Design
% Set input parameters
Vdd=5;
c_snr=20e3; %Current Signal Noise Ratio

% Current boundings
Io_min=mibc * c_snr
Io_max=Io_min * 16
% Network resistors parameters
R=Vdd / Io_max
r_snr=R/mor  % Resistor "Signal Noise Ratio"
% Gain resistor
Rgain=maxSup/Io_max

%% Checks
% Choose commercial values
Rcom=39e3;
RcomGain=100e3;

% Check boundings
Io_max=Vdd/Rcom
Vo_max=-Io_max*RcomGain

% Max error
error_by_current=RcomGain*mibc % Voltage error caused by parasitic currents
R2=Rcom*2;
i_tot_eq=Vdd/(R2*(R2+mor))/(2*R2+mor); % Imax with the parasitic impedance
error_1_branch_cur=Io_max/2*R2 - i_tot_eq; % difference between theorical current and real one
error_by_resistance=RcomGain*error_1_branch_cur %Scale the error with the gain factor