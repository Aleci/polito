clear all; close all; clc
cd /home/ale/Git/PoliTO/ESM_Labs/Lab4/
load("Data/variables.mat")

%% Plot with Simu for Counter [not used]
y=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
x=[0,0.001,0.002,0.003,0.004,0.005,0.006,0.007,0.008,0.009,0.01,0.011,0.012,0.013,0.014,0.015];
plot(time_Simu,Volt_Simu,x,y,'o')
xlabel('Time [s]')
ylabel('Voltage [V]')
grid on
legend('Simulation','real behaviour','Location','southeast')
%% Plot with Simu values for current switches [not used]
a=linspace(0,0.001,100);
b=ones(1,100);
z=linspace(0.001,0.001,100);
x=[a,a+z,2*z+a,3*z+a,4*z+a,5*z+a,6*z+a,7*z+a,8*z+a,9*z+a,10*z+a,11*z+a,12*z+a,13*z+a,14*z+a,15*z+a];
y=[0*b,0.332445*b,0.332445*2*b,0.332445*3*b,0.332445*4*b,0.332445*5*b,0.332445*6*b,0.332445*7*b,0.332445*8*b,0.332445*9*b,0.332445*10*b,0.332445*11*b,0.332445*12*b,0.332445*13*b,0.332445*14*b,0.332445*15*b];
y2=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
x2=[0.0005,0.0015,0.0025,0.0035,0.0045,0.0055,0.0065,0.0075,0.0085,0.0095,0.0105,0.0115,0.0125,0.0135,0.0145,0.0155];
plot(x,y,x2,y2,'o')
xlabel('Time [s]')
ylabel('Voltage [V]')
legend('Simulation','real behaviour','Location','southeast')
%% Plot with Simu values for current switches
a=linspace(0,0.001,100);
b=ones(1,100);
z=linspace(0.001,0.001,100);
x=[a,a+z,2*z+a,3*z+a,4*z+a,5*z+a,6*z+a,7*z+a,8*z+a,9*z+a,10*z+a,11*z+a,12*z+a,13*z+a,14*z+a,15*z+a];
y=[0*b,0.332445*b,0.332445*2*b,0.332445*3*b,0.332445*4*b,0.332445*5*b,0.332445*6*b,0.332445*7*b,0.332445*8*b,0.332445*9*b,0.332445*10*b,0.332445*11*b,0.332445*12*b,0.332445*13*b,0.332445*14*b,0.332445*15*b];
y2=[0.00119,0.3377,0.6752,1.011,1.347,1.693,2.021,2.357,2.696,3.033,3.37,3.706,4.042,4.378,4.716,5.051];
x2=[0.0005,0.0015,0.0025,0.0035,0.0045,0.0055,0.0065,0.0075,0.0085,0.0095,0.0105,0.0115,0.0125,0.0135,0.0145,0.0155];
plot(x,y,x2,y2,'o')
plot(x2,y2,'r o',a,0*b,'b',a+z,0.332445*b,'b',2*z+a,0.332445*2*b,'b',3*z+a,0.332445*3*b,'b',4*z+a,0.332445*4*b,'b',5*z+a,0.332445*5*b,'b',6*z+a,0.332445*6*b,'b',7*z+a,0.332445*7*b,'b',8*z+a,0.332445*8*b,'b',9*z+a,0.332445*9*b,'b',10*z+a,0.332445*10*b,'b',11*z+a,0.332445*11*b,'b',12*z+a,0.332445*12*b,'b',13*z+a,0.332445*13*b,'b',14*z+a,0.332445*14*b,'b',15*z+a,0.332445*15*b,'b')
xlabel('Time [s]')
ylabel('Voltage [V]')
legend('real behaviour','Simulation','Location','southeast')
%% Filtered data vs desired behaviour
figure
plot(X*(2*10^(-5)),movmean(circshift(counter_V,-275),5),time_Simu,Volt_Simu)
hold on
xlabel('Time [s]')
ylabel('Voltage [V]')
legend('Lab (moving average 5)','Simulation','Location','southeast')
axis([0 0.015 0 5])
%% Raw data vs characteristic lines
%clean data
t=X*(2*10^(-5));
v=circshift(counter_V,-275);
vtrim=v(1:776); ttrim=t(1:776);
%create desired characteristic line
vStar=linspace(0,5,776)';
%linear regression of the data
m=vStar\vtrim
q=mean(vtrim(1:26))
figure
plot(ttrim,vtrim,'k'), hold on,
plot(ttrim,vStar,'g'),
plot(ttrim,vStar*m+q,'r')
xlabel('Time [s]'),ylabel('Voltage [V]')
legend('Lab (Raw)','Ideal line','LS line','Location','southeast')

