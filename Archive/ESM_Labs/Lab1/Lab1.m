close all; clear all; clc
cd ~/Git/PoliTO/ESM_Labs/Lab1/
addpath("..")
		
%% Inverter
% I-O Characteristics (Raw data plots)
ioinv2V=Lib.plotCsv("Data/inverter2V.csv",1,6,1,"Inverter 2V","Vin","Vout","s","V");
ioinv4V=Lib.plotCsv("Data/inverter4V.csv",1,6,2,"Inverter 4V","Vin","Vout","s","V");
ioinv6V=Lib.plotCsv("Data/inverter6V.csv",1,6,3,"Inverter 6V","Vin","Vout","s","V");
%ioinv8V=Lib.plotCsv("Data/inverter8V.csv",1,6,4,"Inverter 8V","Vin","Vout","s","V");    % BAD DATA
%ioinv10V=Lib.plotCsv("Data/inverter10V.csv",1,6,5,"Inverter 10V","Vin","Vout","s","V"); % BAD DATA
%ioinv15V=Lib.plotCsv("Data/inverter15V.csv",1,6,6,"Inverter 15V","Vin","Vout","s","V"); % BAD DATA

% Estimate I/O characteristic over a voltage range
ioInvGain(1)=Lib.estimateGainNormalizedData(ioinv2V,2e3);
ioInvGain(2)=Lib.estimateGainNormalizedData(ioinv4V,2e3);
ioInvGain(3)=Lib.estimateGainNormalizedData(ioinv6V,2e3);
%ioInvGain(4)=Lib.estimateGainNormalizedData(ioinv8V,2e3);
%ioInvGain(5)=Lib.estimateGainNormalizedData(ioinv10V,2e3);
%ioInvGain(6)=Lib.estimateGainNormalizedData(ioinv15V,2e3);
ioInvGain=ioInvGain'
figure(50),plot(1:length(ioInvGain),ioInvGain,'b'),title("Inverter gain over voltage range");

% Bandwidth
inverter_10=Lib.plotCsv("Data/inverter_10.csv",2,8,1,"Inverter 10hz","Vin","Vout","s","V");
inverter100=Lib.plotCsv("Data/inverter100.csv",2,8,2,"Inverter 100hz","Vin","Vout","s","V");
inv1k=Lib.plotCsv("Data/inverter1k.csv",2,8,3,"Inverter 1Khz","Vin","Vout","s","V");
inv10k=Lib.plotCsv("Data/inverter10k.csv",20,1,1,"Inverter 10Khz","Vin","Vout","s","V");
inv100k=Lib.plotCsv("Data/inverter100k.csv",2,8,5,"Inverter 100Khz","Vin","Vout","s","V");
inv500k=Lib.plotCsv("Data/inverter500k.csv",2,8,6,"Inverter 500Khz","Vin","Vout","s","V");
inv700k=Lib.plotCsv("Data/inverter700k.csv",2,8,7,"Inverter 700Khz","Vin","Vout","s","V");
inv1M=Lib.plotCsv("Data/inverter1M.csv",2,8,8,"Inverter 1Mhz","Vin","Vout","s","V");

% Estimate I/O characteristic over a frequency range
bandInvGain(1)=Lib.estimateGainNormalizedData(inverter_10,10);
bandInvt(1)=10;
bandInvGain(2)=Lib.estimateGainNormalizedData(inverter100,100);
bandInvt(2)=100;
%bandInvGain(3)=Lib.estimateGainNormalizedData(inv1k,1e3); % BAD DATA
bandInvGain(3)=ioInvGain(1); % Replaced with a different dataset
bandInvt(3)=2000;
bandInvGain(4)=Lib.estimateGainNormalizedData(inv10k,1e4);
bandInvt(4)=1e4;
bandInvGain(5)=Lib.estimateGainNormalizedData(inv100k,1e5);
bandInvt(5)=1e5;
bandInvGain(6)=Lib.estimateGainNormalizedData(inv500k,5e5);
bandInvt(6)=5e5;
bandInvGain(7)=Lib.estimateGainNormalizedData(inv700k,7e5);
bandInvt(7)=7e5;
bandInvGain(8)=Lib.estimateGainNormalizedData(inv1M,1e6);
bandInvt(8)=1e6;
bandInvGain=bandInvGain'
figure(51),loglog(bandInvt,bandInvGain,'r-o'),title("Bandwidth Inverter Amplifier"),yline(.95),xlabel("Frequency [Hz]"),ylabel("|Gain|")

% Max current behaviour
invMaxCur=Lib.plotCsv("Data/inverterMaxCurrent.csv",3,1,1,"Inverter Current Saturation","Vin","Vout","Time [s]","Voltage [V]");

%% Voltage follower

% Offset measurements
offset=readtable("Data/offset1.csv")
figure(30),plot(offset.Var1,offset.Var2,"k*"),title("Voltage Follower Offset"),legend("Vout"),xlabel("k^{th} sample"),ylabel("Voltage [V]");
offsetbetter=readtable("Data/offset_better.csv")
%figure(31),plot(offsetbetter.Var1,offsetbetter.Var2,"k*"),title("GND follower's offset"),legend("Vout"),xlabel("k^{th} sample"),ylabel("V");

% I/O Characteristics in a voltage range (raw data)
ioFol2=Lib.plotCsv("Data/voltage_fol2V.csv",5,1,1,"Voltage Follower 2V","Vin","Vout","s","V");
ioFol4=Lib.plotCsv("Data/voltage_fol4V.csv",5,1,1,"Voltage Follower 4V","Vin","Vout","s","V");
ioFol6=Lib.plotCsv("Data/voltage_fol6V.csv",5,1,1,"Voltage Follower 6V","Vin","Vout","s","V");
ioFol8=Lib.plotCsv("Data/voltage_fol8V.csv",5,1,1,"Voltage Follower 8V","Vin","Vout","s","V");
ioFol10=Lib.plotCsv("Data/voltage_fol10V.csv",5,1,1,"Voltage Follower 10V","Vin","Vout","s","V");
ioFol15=Lib.plotCsv("Data/voltage_fol15V.csv",5,1,1,"Voltage Follower 2,4,6,8,10,15V I/O Raw","Vin","Vout","Time [s]","Voltage [V]");

% Gain estimation in a voltage range
ioFolGain(1)=Lib.estimateGainNormalizedData(ioFol2,2e3);
ioFolV(1)=2;
ioFolGain(2)=Lib.estimateGainNormalizedData(ioFol4,2e3);
ioFolV(2)=4;
ioFolGain(3)=Lib.estimateGainNormalizedData(ioFol6,2e3);
ioFolV(3)=6;
ioFolGain(4)=Lib.estimateGainNormalizedData(ioFol8,2e3);
ioFolV(4)=8;
ioFolGain(5)=Lib.estimateGainNormalizedData(ioFol10,2e3);
ioFolV(5)=10;
%bioFolGain(6)=Lib.estimateGainNormalizedData(ioFol15,2e3); % SATURATED DATA
% ioFolV(6)=15;
ioFolGain=ioFolGain'

figure(52),plot(1:length(ioFolGain),ioFolGain,'r-o'),title("Voltage Follower Gain"), xlabel("Voltage Input"), ylabel("|Gain|")

% I/O Characteristics in a frequency range (raw data)
bandFol10=Lib.plotCsv("Data/voltage_fol10.csv",6,7,1,"Voltage Follower 10 Hz","Vin","Vout","s","V");
bandFol100=Lib.plotCsv("Data/voltage_fol100.csv",6,7,2,"Voltage Follower 100 Hz","Vin","Vout","s","V");
bandFol1k=Lib.plotCsv("Data/voltage_fol1k.csv",6,7,3,"Voltage Follower 1kHz","Vin","Vout","s","V");
bandFol10k=Lib.plotCsv("Data/voltage_fol10k.csv",6,7,4,"Voltage Follower 10kHz","Vin","Vout","s","V");
bandFol100k=Lib.plotCsv("Data/voltage_fol100k.csv",6,7,5,"Voltage Follower 100kHz","Vin","Vout","s","V");
bandFol500k=Lib.plotCsv("Data/voltage_fol500k.csv",6,7,6,"Voltage Follower 500kHz","Vin","Vout","s","V");
bandFol1M=Lib.plotCsv("Data/voltage_fol1M.csv",66,1,1,"Voltage Follower 2V@1MHz","Vin","Vout","Time [s]","Voltage [V]");

% I/O Characteristics in a frequency range (estimated gains)
bandFolGain(1)=Lib.estimateGainNormalizedData(bandFol10,10);
bandFolt(1)=10;
bandFolGain(2)=Lib.estimateGainNormalizedData(bandFol100,1e2);
bandFolt(2)=100;
bandFolGain(3)=Lib.estimateGainNormalizedData(bandFol1k,1e3);
bandFolt(3)=1000;
bandFolGain(4)=Lib.estimateGainNormalizedData(bandFol10k,1e4);
bandFolt(4)=1e4;
bandFolGain(5)=Lib.estimateGainNormalizedData(bandFol100k,1e5);
bandFolt(5)=1e5;
bandFolGain(6)=Lib.estimateGainNormalizedData(bandFol500k,5e5);
bandFolt(6)=5e5;
bandFolGain(7)=Lib.estimateGainNormalizedData(bandFol1M,1e6);
bandFolt(7)=1e6;
bandFolGain=bandFolGain'

figure(53),loglog(bandFolt,bandFolGain,'r-o'),title("Voltage Follower Bandwidth"), xlabel("Frequency [Hz]"), ylabel("|Gain|");

% Max current behaviour
folMaxCurr=Lib.plotCsv("Data/voltage_foll_currentMax.csv",9,2,1,"Voltage Follower Max current","Vin","Vout","s","V");
folMaxCurrBest=Lib.plotCsv("Data/voltage_foll_current_max_best.csv",9,2,2,"Voltage Follower Max current","Vin","Vout","s","V");

% Spare data
spare1=Lib.plotCsv("Data/voltage_follower1.csv",10,2,1,"Voltage Follower VS Voltage Divider","Vin","Vout","s","V");
spare2=Lib.plotCsv("Data/voltage_follower_2.csv",10,2,2,"Voltage Follower VS Voltage Divider","Vin","Vout","s","V");

%% Analog summing stage

% I/O Characteristics in a voltage range (raw data)
ioSum2=Lib.plotCsv("Data/sum_2V.csv",7,6,1,"Summing stage 2V","Vin","Vout","s","V");
ioSum4=Lib.plotCsv("Data/sum_4V.csv",7,6,2,"Summing stage 4V","Vin","Vout","s","V");
ioSum6=Lib.plotCsv("Data/sum_6V.csv",7,6,3,"Summing stage 6V","Vin","Vout","s","V");
ioSum8=Lib.plotCsv("Data/sum_8V.csv",7,6,4,"Summing stage 8V","Vin","Vout","s","V");
ioSum10=Lib.plotCsv("Data/sum_10V.csv",7,6,5,"Summing stage 10V","Vin","Vout","s","V");
ioSum15=Lib.plotCsv("Data/sum_15V.csv",7,6,6,"Summing stage 15V","Vin","Vout","s","V");

% I/O Characteristics in a voltage range 
% (PAY ATTENTION TO DC OFFSET AND SATURATION, POSSIBLY WRONG GAINS)
sumGain(1)=Lib.estimateGainNormalizedData(ioSum2,2e3);
sumGain(2)=Lib.estimateGainNormalizedData(ioSum4,2e3);
sumGain(3)=Lib.estimateGainNormalizedData(ioSum6,2e3);
sumGain(4)=Lib.estimateGainNormalizedData(ioSum8,2e3);
sumGain(5)=Lib.estimateGainNormalizedData(ioSum10,2e3);
sumGain(6)=Lib.estimateGainNormalizedData(ioSum15,2e3);
sumGain=sumGain'
figure(54),plot(1:length(sumGain),sumGain,'b'),title("summing stage gain over voltage range");
