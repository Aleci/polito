clear all; close all; clc; format short
%% Data
fc=33e3
Q=0.7845,  f=fc*.5286, G=1

%% Component selection
% Set
C5=10e-9
% Get
omega=2*pi*f;
C4=4*C5*Q^2*(G+1)
R2=1/(2*Q*omega*C5)
R3=R2/(G+1)
R1=R2/G

%% Checks
checkF=1/(2*pi*sqrt(C4*C5*R2*R3))
checkQ=R1/(R2+R3)/(R2*R3/(R2+R3)+R1)*sqrt(C4/C5*R2*R3)
checkG=R2/R1


