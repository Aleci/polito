clear all; close all; clc; format short e

% Cutting frequency
f=26e3;

%% Spec stage 1
Gdb1=3.7; %was 5.2
f01=f*.3962;
Q1=.6836;

%Independent components values
R12=1e3;
C14=100e-9; % 100nF

designVaules1=designLPFilter(R12,C14,Gdb1,f01,Q1)'
checkF01= cutFreq(designVaules1(2),designVaules1(3), ...
                 designVaules1(4),designVaules1(5))
checkQ1= qFact(designVaules1(1),designVaules1(2),designVaules1(3), ...
             designVaules1(4),designVaules1(5))

%Dependent component values 
R11=6.4e2; %was 5.6e2
R13=75;
C15=.033e-6; %33nF

doubleCheckF1= cutFreq(R12,R13,C14,C15)
doubleCheckQ1= qFact(R11,R12,R13,C14,C15)
doubleCheckG1= gain(R11,R12)
doubleCheckGdb1= mag2db(doubleCheckG1)

%% Spec stage 2
Gdb2=1.3; % was 1
f02=f*.7681;
Q2=1.8104;

%Independent components values
R22=2.7e3;
C24=100e-9; % 100nF

designVaules2=designLPFilter(R22,C24,Gdb2,f02,Q2)'
checkF02= cutFreq(designVaules2(2),designVaules2(3), ...
                 designVaules2(4),designVaules2(5))
checkQ2= qFact(designVaules2(1),designVaules2(2),designVaules2(3), ...
             designVaules2(4),designVaules2(5))

%Dependent component values 
R21=2.2e3; % was 2.4e3
R23=82;
C25=3e-9; %3nF

doubleCheckF2= cutFreq(R22,R23,C24,C25)
doubleCheckQ2= qFact(R21,R22,R23,C24,C25)
doubleCheckG2= gain(R21,R22)
doubleCheckGdb2= mag2db(doubleCheckG2)

%% Spec stage 3
Gdb3=1;
f03=f*1.0114;
Q3=6.5128;

%Independent components values
R32=2.7e3;
C34=100e-9; % 100nF

designVaules3=designLPFilter(R32,C34,Gdb3,f03,Q3)'
checkF03= cutFreq(designVaules3(2),designVaules3(3), ...
                 designVaules3(4),designVaules3(5))
checkQ3= qFact(designVaules3(1),designVaules3(2),designVaules3(3), ...
             designVaules3(4),designVaules3(5))

%% Dependent component values 
R31=2.4e3;
R33=2.4e2;
C35=560e-12; %560pF

doubleCheckF3= cutFreq(R32,R33,C34,C35)
doubleCheckQ3= qFact(R31,R32,R33,C34,C35)
doubleCheckG3= gain(R31,R32)
doubleCheckGdb3= mag2db(doubleCheckG3)

%% Compute design functions
design=0;
if (design == 1)
    syms R1 R2 G positive
    eqnG= G==R2/R1;
    solve(eqnG,R1,'ReturnConditions',true)

    syms C4 FI F0 positive
    eqnF= F0==1/(2*pi*sqrt(C4*FI*R2))
    solve(eqnF,FI,'ReturnConditions',true)

    syms C5 Q positive
    eqnQ= Q==R1/( (R2+FI/C5) * ((R2*(R1+FI/C5)) / (R1+R2+FI/C5)) ) * sqrt(C4/C5*R2*FI/C5);
    solve(eqnQ,C5,'ReturnConditions',true)
end

%% Design functions

function param=designLPFilter(R2,C4,G,F0,Q)
    R1=getR1(R2,G);
    FI=getFI(R2,C4,F0);
    C5=getC5(R1,R2,C4,FI,Q);
    R3=getR3(FI,C5);
    param=[R1 R2 R3 C4 C5];
end

% SET R2, C4 => COMPUTE FI=R3*C5
function FI=getFI(R2,C4,F0)
    FI=1/(4*C4*F0^2*R2*pi^2);
end

% SET R1
function R1=getR1(R2,Gdb)
    G=db2mag(Gdb);
    R1=R2/G;
end

% COMPUTE C5
function C5=getC5(R1,R2,C4,FI,Q)
    C51=(2*(FI^(3/2)*Q*R2^(1/2) - C4^(1/2)*FI*R1))/(((C4^(1/2)*R1 - FI^(1/2)*Q*R2^(1/2))*(C4^(1/2)*R1^3 + C4^(1/2)*R1*R2^2 + 2*C4^(1/2)*R1^2*R2 - FI^(1/2)*Q*R2^(5/2) + 2*FI^(1/2)*Q*R1*R2^(3/2) - FI^(1/2)*Q*R1^2*R2^(1/2)))^(1/2) + C4^(1/2)*R1^2 - FI^(1/2)*Q*R2^(3/2) + C4^(1/2)*R1*R2 - FI^(1/2)*Q*R1*R2^(1/2));
    C52=-(2*(FI^(3/2)*Q*R2^(1/2) - C4^(1/2)*FI*R1))/(((C4^(1/2)*R1 - FI^(1/2)*Q*R2^(1/2))*(C4^(1/2)*R1^3 + C4^(1/2)*R1*R2^2 + 2*C4^(1/2)*R1^2*R2 - FI^(1/2)*Q*R2^(5/2) + 2*FI^(1/2)*Q*R1*R2^(3/2) - FI^(1/2)*Q*R1^2*R2^(1/2)))^(1/2) - C4^(1/2)*R1^2 + FI^(1/2)*Q*R2^(3/2) - C4^(1/2)*R1*R2 + FI^(1/2)*Q*R1*R2^(1/2));
    C5=max(C51,C52);
 end

% COMPUTE R3
function R3=getR3(FI,C5)
    R3=FI/C5;
end

%% Check functions
function G=gain(R1,R2)
  G=R2/R1;
end

function f0=cutFreq(R2,R3,C4,C5)
  f0=1/(2*pi*sqrt(C4*C5*R2*R3));
end

function Q=qFact(R1,R2,R3,C4,C5)
  Q=R1/( (R2+R3) * ((R2*(R1+R3)) / (R1+R2+R3)) ) * sqrt(C4/C5*R2*R3);
end

