close all
clear all
clc

%% 
% Set in the following lines the desired cutting frequency (f)
% the Q and f0 factor (according to the cell they'll change)
%% 2 Cell SET THE PARAMETERS
% parameters
Q=1.3988;
f0=0.6552;
f=100e3;    %cutting frequency of the filter
f2=f*f0;

%% Possible Resistors
R0=[10 12 15 18 22 27 33 39 47 56 68 82];

for i=1:6
    for n=1:12
        R(12*(i-1)+n)=R0(n)*(10^(i-1));
    end
end

R_useful=R(21:54);

%% Possible Capacitance
C0=[100 150 220 330 470 680]*10^(-12);

for i=1:4
    for n=1:6
        C(6*(i-1)+n)=C0(n)*(10^(i-1));
    end
end

C_useful=C;

%% Chebichebv Filter

syms R1 R2 R3   %definition of the unknown

for i=1:18 
    for j=1:18

        eqn1 = R1/((R2+R3)*(R2*R3/(R2+R3)+R1))*sqrt(C_useful(j)*R2*R3/C_useful(i))== Q; 
        eqn2 = 1/(2*pi*sqrt(C_useful(j)*C_useful(i)*R2*R3))== f2 ; 
        eqn3 = R1==R2; 
        [R1s,R2s,R3s]=solve([eqn1, eqn2, eqn3]); 
        if ( imag(R1s)==0 & imag(R3s)==0 )
            R1a(j,i)=double(R1s(1));
            R2a(j,i)=double(R2s(1));
            R3a(j,i)=double(R3s(1)); 
            R1b(j,i)=double(R1s(2)); 
            R2b(j,i)=double(R2s(2)); 
            R3b(j,i)=double(R3s(2)); 
        else  
            R1a(j,i)=0; 
            R2a(j,i)=0;
            R3a(j,i)=0; 
            R1b(j,i)=0; 
            R2b(j,i)=0; 
            R3b(j,i)=0; 
        end 
    end 
end

 for k=1:2
    for i=1:18
        for j=1:18
            % matrix A: 1)C4 value 2)C5 value 3)R1a value 4)R2a value 5)R3a Value
            n= j+(18*(i-1));
            p=n+324*(k-1);
            R_sol( p , 1 ) = C_useful(j); %setting the value of C4 for the respective solution
            R_sol( p , 2 ) = C_useful(i); %Setting the value of C5 for the respective solution

            if(p<=324)   %A solutions
                R_sol( p , 3 ) = R1a(j,i);
                R_sol( p , 4 ) = R2a(j,i);
                R_sol( p , 5 ) = R3a(j,i);
            
            else        %B solutions
                R_sol( p , 3 ) = R1b(j,i);
                R_sol( p , 4 ) = R2b(j,i);
                R_sol( p , 5 ) = R3b(j,i);
            end
            
        end
    end
 end

%% remove the null solutions (useless solutions)
 R_not0=R_sol( (R_sol(:,3)~=0 & R_sol(:,5)~=0) , : );

% remove all the resistances with values lower than 530 ohm
R_mod = R_not0( (R_not0(:,3)>530 & R_not0(:,5)>530) , : );

% take only the values of the resistances inside the bound (5% percentage)
for i=1:length(R_useful)
    Bound(i)=.1*R_useful(i);
end

k=1;
flag=1;
for j=1:length(R_mod)
    for i=1:length(R_useful)
        if( (R_mod(j,3)<=(R_useful(i)+Bound(i)) && R_mod(j,3)>=(R_useful(i)-Bound(i))) && flag )
            for z=1:length(R_useful)
                if ( (R_mod(j,5)<=(R_useful(z)+Bound(z)) && R_mod(j,5)>=(R_useful(z)-Bound(z))) && flag )
                    R_final(k,:)=R_mod(j,:);
                    k=k+1;
                    flag=0;
                end
            end
        end
    end
    flag=1;
end

