%% Equations valid for lpf in multiple reactions topology (good for high Q and high Gain)
clear all; clc; format short

G=1; f=1; Q=.71;

%
%a=1/(4*pi^2*f^2);
%b=1/(2*pi*Q);
a=1.4142;
b=1;
C4=100e-9;
C5=C4*4*b*(1+G)/a^2;
R2=(a*C5-sqrt(a^2*C5^2-4*b*C4*C5*(1+G)))/(4*pi*f*C4*C5)
R1=R2/G
R3=b/(4*pi*f*C4*C5)

% Check
checkF=1/(2*pi*sqrt(C4*C5*R2*R3))
checkQ=R1/(R2+R3)/(R2*(R3+R1)/(R2+R3+R1))*sqrt(C4/C5*R2*R3)
checkG=R2/R1



