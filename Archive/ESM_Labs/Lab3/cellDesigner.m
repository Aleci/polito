clear all; close all; clc; format short
%% Data
Q=2;  f=1000; G=3;

%% Component selection
% Set
C4=6.5e-8, C5=1e-9
% Get
delta=4*Q^2*(G+1);
R2=1+sqrt(1-delta*C5/C4)/(4*pi*f*C5*Q)
R1=R2/G
R3=1/(4*pi^2*f^2*C4*C5)

%% Checks
checkCaps=C4/C5-delta >= 0
checkF=1/(2*pi*sqrt(C4*C5*R2*R3))
checkQ=R1/(R2+R3)/(R2*R3/(R2+R3)+R1)*sqrt(C4/C5*R2*R3)
checkG=R2/R1


