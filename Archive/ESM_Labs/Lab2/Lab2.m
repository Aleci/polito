close all; clear all; clc
cd ~/Git/PoliTO/ESM_Labs/Lab2/
addpath("..")

%% TransImpedance
ioTI05V=Lib.plotCsv("Data/TI_0.5V_2kHz.csv",1,1,1,"TransImpedance 0.5V","Vin","Vout","s","V");
ioTI1V=Lib.plotCsv("Data/TI_1V_2kHz.csv",1,1,1,"TransImpedance 1V","Vin","Vout","s","V");
ioTI2V=Lib.plotCsv("Data/TI_2V_2kHz.csv",1,1,1,"TransImpedance 2V","Vin","Vout","s","V");
ioTI3V=Lib.plotCsv("Data/TI_3V_2kHz.csv",1,1,1,"TransImpedance 3V","Vin","Vout","s","V");
ioTI39V=Lib.plotCsv("Data/TI_3.9V_2kHz.csv",1,1,1,"Transimpedance .5,1,2,3,3.9V I/O Raw","Vin","Vout","Time [s]","Voltage [V]");

ioTIGain(1)=Lib.estimateGainNormalizedData(ioTI05V,2e3);
ioTIV(1)=.5;
ioTIGain(2)=Lib.estimateGainNormalizedData(ioTI1V,2e3);
ioTIV(2)=1;
ioTIGain(3)=Lib.estimateGainNormalizedData(ioTI2V,2e3);
ioTIV(3)=2;
ioTIGain(4)=Lib.estimateGainNormalizedData(ioTI3V,2e3);
ioTIV(4)=3;
ioTIGain(5)=Lib.estimateGainNormalizedData(ioTI39V,2e3);
ioTIV(5)=3.9;
ioTIGain=ioTIGain'

figure(11),plot(ioTIV,ioTIGain,'r-o'),title("Transimpedence Amplifier Gain"),xlabel("Input Voltage [V]"),ylabel("|Gain|")

%% TransConductance
ioTC1V=Lib.plotCsv("Data/TC_1V_2kHz.csv",2,1,1,"TransConductance 1V","Vin","Vout","s","V");
ioTC2V=Lib.plotCsv("Data/TC_2V_2kHz.csv",2,1,1,"TransConductance 2V","Vin","Vout","s","V");
ioTC3V=Lib.plotCsv("Data/TC_3V_2kHz.csv",2,1,1,"TransConductance 3V","Vin","Vout","s","V");
ioTC4V=Lib.plotCsv("Data/TC_4V_2kHz.csv",2,1,1,"TransConductance 4V","Vin","Vout","s","V");
ioTC5V=Lib.plotCsv("Data/TC_5V_2kHz.csv",2,1,1,"TransConductance 5V","Vin","Vout","s","V");
ioTC6V=Lib.plotCsv("Data/TC_6V_2kHz.csv",2,1,1,"(Real Data) Transconductance 1-6V Input","Vins","Vouts","s","V");

ioTCfake1V=table(ioTC1V.Var1,ioTC1V.Var2,ioTC1V.Var3-ioTC1V.Var2);
ioTCfake2V=table(ioTC2V.Var1,ioTC2V.Var2,ioTC2V.Var3-ioTC2V.Var2);
ioTCfake3V=table(ioTC3V.Var1,ioTC3V.Var2,ioTC3V.Var3-ioTC3V.Var2);
ioTCfake4V=table(ioTC4V.Var1,ioTC4V.Var2,ioTC4V.Var3-ioTC4V.Var2);
ioTCfake5V=table(ioTC5V.Var1,ioTC5V.Var2,ioTC5V.Var3-ioTC5V.Var2);
ioTCfake6V=table(ioTC6V.Var1,ioTC6V.Var2,ioTC6V.Var3-ioTC6V.Var2);

Lib.plotData(ioTCfake1V,22,1,1,"Transconductance 1-6V Input","Vins","Vouts","s","V");
Lib.plotData(ioTCfake2V,22,1,1,"Transconductance 1-6V Input","Vins","Vouts","s","V");
Lib.plotData(ioTCfake3V,22,1,1,"Transconductance 1-6V Input","Vins","Vouts","s","V");
Lib.plotData(ioTCfake4V,22,1,1,"Transconductance 1-6V Input","Vins","Vouts","s","V");
Lib.plotData(ioTCfake5V,22,1,1,"Transconductance 1-6V Input","Vins","Vouts","s","V");
Lib.plotData(ioTCfake6V,22,1,1,"Transconductance 1-6V I/O Raw","Vin","Vout","Time [s]","[V]");

fakeGain(1)=Lib.estimateGainNormalizedData(ioTCfake1V,2e3);
fakeGain(2)=Lib.estimateGainNormalizedData(ioTCfake2V,2e3);
fakeGain(3)=Lib.estimateGainNormalizedData(ioTCfake3V,2e3);
fakeGain(4)=Lib.estimateGainNormalizedData(ioTCfake4V,2e3);
fakeGain(5)=Lib.estimateGainNormalizedData(ioTCfake5V,2e3);
%fakeGain(6)=Lib.estimateGainNormalizedData(ioTCfake6V,2e3); %SATURATED DATA
fakeGain=fakeGain'

ioTCGain(1)=Lib.estimateGainNormalizedData(ioTC1V,2e3);
ioTcV(1)=1;
ioTCGain(2)=Lib.estimateGainNormalizedData(ioTC2V,2e3);
ioTcV(2)=2;
ioTCGain(3)=Lib.estimateGainNormalizedData(ioTC3V,2e3);
ioTcV(3)=3;
ioTCGain(4)=Lib.estimateGainNormalizedData(ioTC4V,2e3);
ioTcV(4)=4;
ioTCGain(5)=Lib.estimateGainNormalizedData(ioTC5V,2e3);
ioTcV(5)=5;
%ioTCGain(6)=Lib.estimateGainNormalizedData(ioTC6V,2e3); %SATURATED DATA
%ioTcV(6)=6;
ioTCGain=ioTCGain'
figure(21),plot(ioTcV,fakeGain,'r-o'),title("Transconductance Amplifier Gain"),xlabel("Input Voltage [V]"),ylabel("|Gain|"); yline(4.689)


%% Instrumental Amplifier
ioIA1V=Lib.plotCsv("Data/IA_1V_2kHz.csv",3,1,1,"InstAmp 1V", "Vin","Vout","s" , "V");
ioIA2V=Lib.plotCsv("Data/IA_2V_2kHz.csv",3,1,1,"InstAmp 2V", "Vin","Vout","s" , "V");
ioIA3V=Lib.plotCsv("Data/IA_3V_2kHz.csv",3,1,1,"InstAmp 3V", "Vin","Vout","s" , "V");
ioIA4V=Lib.plotCsv("Data/IA_4V_2kHz.csv",3,1,1,"InstAmp 4V", "Vin","Vout","s" , "V");
ioIA5V=Lib.plotCsv("Data/IA_5V_2kHz.csv",3,1,1,"InstAmp 5V", "Vin","Vout","s" , "V");
ioIA6V=Lib.plotCsv("Data/IA_6V_2kHz.csv",3,1,1,"InstAmp 6V", "Vin","Vout","s" , "V");
ioIA7V=Lib.plotCsv("Data/IA_7V_2kHz.csv",3,1,1,"InstAmp 7V", "Vin","Vout","s" , "V");
ioIA8V=Lib.plotCsv("Data/IA_8V_2kHz.csv",3,1,1,"Instrumentation Amplifier 1-8V I/O Raw", "Vin","Vout","Time [s]" , "Voltage [V]");

ioIAGain(1)=Lib.estimateGainNormalizedData(ioIA1V,2e3);
ioIAV(1)=1;
ioIAGain(2)=Lib.estimateGainNormalizedData(ioIA2V,2e3);
ioIAV(2)=2;
ioIAGain(3)=Lib.estimateGainNormalizedData(ioIA3V,2e3);
ioIAV(3)=3;
ioIAGain(4)=Lib.estimateGainNormalizedData(ioIA4V,2e3);
ioIAV(4)=4;
ioIAGain(5)=Lib.estimateGainNormalizedData(ioIA5V,2e3);
ioIAV(5)=5;
ioIAGain(6)=Lib.estimateGainNormalizedData(ioIA6V,2e3);
ioIAV(6)=6;
ioIAGain(7)=Lib.estimateGainNormalizedData(ioIA7V,2e3);
ioIAV(7)=7;
ioIAGain(8)=Lib.estimateGainNormalizedData(ioIA8V,2e3);
ioIAV(8)=8;
ioIAGain=ioIAGain'
figure(31),plot(ioIAV,ioIAGain,'r-o'),title("Instrumentation Amplifier Gain"),xlabel("Input Voltage [V]"),ylabel("|Gain|")

%% Differential Amplifier
ioDA2V=Lib.plotCsvSub("Data/DA_2V_2kHz.csv",4,1,1,"DiffAmp 2V","Vin","Vdiff","s","V");
ioDA3V=Lib.plotCsvSub("Data/DA_3V_2kHz.csv",4,1,1,"DiffAmp 3V","Vin","Vdiff","s","V");
ioDA4V=Lib.plotCsvSub("Data/DA_4V_2kHz.csv",4,1,1,"DiffAmp 4V","Vin","Vdiff","s","V");
ioDA5V=Lib.plotCsvSub("Data/DA_5V_2kHz.csv",4,1,1,"DiffAmp 5V","Vin","Vdiff","s","V");
ioDA6V=Lib.plotCsvSub("Data/DA_6V_2kHz.csv",4,1,1,"DiffAmp 6V","Vin","Vdiff","s","V");
ioDA7V=Lib.plotCsvSub("Data/DA_7V_2kHz.csv",4,1,1,"DiffAmp 7V","Vin","Vdiff","s","V");
ioDA8V=Lib.plotCsvSub("Data/DA_8V_2kHz.csv",4,1,1,"DiffAmp 8V","Vin","Vdiff","s","V");
ioDA9V=Lib.plotCsvSub("Data/DA_9V_2kHz.csv",4,1,1,"Differential Amplifier 2-8V I/O Raw","Vin","Vdiff","Time [s]","Voltage [V]");

ioDA2Vfull=Lib.loadData("Data/DA_2V_2kHz.csv");
figure(45), plot(ioDA2Vfull.Var1,ioDA2Vfull.Var2,'k'),hold on
plot(ioDA2Vfull.Var1,ioDA2Vfull.Var3,'b'),title("Differential Amplifier Raw Signals 2V")
plot(ioDA2Vfull.Var1,ioDA2Vfull.Var4,'c')
plot(ioDA2Vfull.Var1,ioDA2Vfull.Var3-ioDA2Vfull.Var4,'r'),legend("Vin","V1","V2","V1-V2"),xlabel("Time [s]"),ylabel("Voltage [V]");

%ioDA3Vfull=Lib.plotCsv("Data/DA_3V_2kHz.csv",4,1,1,"DiffAmp 3V","Vin","Vdiff","s","V");
%ioDA4Vfull=Lib.plotCsv("Data/DA_4V_2kHz.csv",4,1,1,"DiffAmp 4V","Vin","Vdiff","s","V");
%ioDA5Vfull=Lib.plotCsv("Data/DA_5V_2kHz.csv",4,1,1,"DiffAmp 5V","Vin","Vdiff","s","V");
%ioDA6Vfull=Lib.plotCsv("Data/DA_6V_2kHz.csv",4,1,1,"DiffAmp 6V","Vin","Vdiff","s","V");
%ioDA7Vfull=Lib.plotCsv("Data/DA_7V_2kHz.csv",4,1,1,"DiffAmp 7V","Vin","Vdiff","s","V");
%ioDA8Vfull=Lib.plotCsv("Data/DA_8V_2kHz.csv",4,1,1,"DiffAmp 8V","Vin","Vdiff","s","V");
%ioDA9Vfull=Lib.plotCsv("Data/DA_9V_2kHz.csv",4,1,1,"Differential Amplifier 2-8V I/O Raw","Vin","Vdiff","Time [s]","Voltage [V]");

ioDAGain(1)=Lib.estimateGainNormalizedData(ioDA2V,2e3);
ioDAV(1)=2;
ioDAGain(2)=Lib.estimateGainNormalizedData(ioDA3V,2e3);
ioDAV(2)=3;
ioDAGain(3)=Lib.estimateGainNormalizedData(ioDA4V,2e3);
ioDAV(3)=4;
ioDAGain(4)=Lib.estimateGainNormalizedData(ioDA5V,2e3);
ioDAV(4)=5;
ioDAGain(5)=Lib.estimateGainNormalizedData(ioDA6V,2e3);
ioDAV(5)=6;
ioDAGain(6)=Lib.estimateGainNormalizedData(ioDA7V,2e3);
ioDAV(6)=7;
ioDAGain(7)=Lib.estimateGainNormalizedData(ioDA8V,2e3);
ioDAV(7)=8;
ioDAGain(8)=Lib.estimateGainNormalizedData(ioDA9V,2e3);
ioDAV(8)=9;
ioDAGain=ioDAGain'
figure(41),plot(ioDAV,ioDAGain,'r-o'),title("Differential Amplifier Gain"),xlabel("Input Voltage [V]"),ylabel("|Gain|")
