clear all; close all; clc
cd ~/Git/PoliTO/ESM_Labs/Lab5/ % UNDER WINDOWS COMMENT THIS LINE AND PRESS "CHANGE CURRENT FOLDER" DURING THE FIRST RUN
addpath("..")

%% Steady state output levels
figure
io=Lib.loadData("Data/IO_Char.csv"); %Var3: -1=overflow xy=oscillating between x and y
y=linspace(0,5,length(io.Var1));
plot(io.Var1,io.Var2,"o"), title("I/O Steady State Levels"), hold on
plot(io.Var1,y,'r')
yline(io.Var2(2)),yline(io.Var2(16))
xlabel("Max oscillating bits value"),ylabel("Voltage [V]")

%% Characteristic line
figure
tmp=Lib.loadData("Data/LineChar1k.csv");
vline=Lib.dataTrim(tmp,240,1021);%zero starts at 225 but 240 renders nicer
plot(vline.Var1,movmean(vline.Var2,5),"k"), hold on
q=mean(vline.Var2(1:30)) %average of the first 30 samples
fi=linspace(0,5,length(vline.Var1))';
m=fi\vline.Var2
plot(vline.Var1,m*fi+q), title("Characteristic Line")
xlabel("Time [s]"), ylabel("Voltage [V]")
legend("Raw data (moving average 5)","Characteristic line","Location","southeast")

%% Non monotonic characteristic
figure
tmp=Lib.loadData("Data/RS-LineChar10k.csv");
nmchar=Lib.dataTrim(tmp,54,845); %Warning: this drops the clock track
%plot(nmchar.Var1,movmean(nmchar.Var2,10), ...
 plot(nmchar.Var1,movmean(nmchar.Var3,5))
title("Non Monotonic Behaviour"), xlabel("Time [s]"), ylabel("Voltage [V]")
%legend("Input low freq. wave (moving average 10)","Reconstruction signal (moving average 5)","Location","southeast")

%% Missing code error
figure
mc10=Lib.loadData("Data/RS-Sin14-10hz10k.csv");
%plot(mc10.Var1,mc10.Var3,mc10.Var1,mc10.Var2) %BAD DATA
mc333=Lib.loadData("Data/RS-Sin14-333hz10k.csv");
plot(mc333.Var1,mc333.Var3,mc333.Var1,mc333.Var2)
title("Missing Code"), xlabel("Time [s]"), ylabel("Voltage [V]")
legend("Output","Input","Location","south")

%% Overload point search
figure
ovs100k=Lib.loadData("Data/Sin14-50hz100k.csv");
ovs10k=Lib.loadData("Data/Sin14-50hz10k.csv");
ovs3k=Lib.loadData("Data/Sin14-50hz3k.csv");
ovs1k=Lib.loadData("Data/Sin14-50hz1k.csv");
subplot(2,2,1), plot(ovs100k.Var1,ovs100k.Var3,ovs100k.Var1,ovs100k.Var2)
xlabel("Time [s]"), ylabel("Voltage [V]"), title("50Hz Wave, 100kHz Clock")
subplot(2,2,2), plot(ovs10k.Var1,ovs10k.Var3,ovs10k.Var1,ovs10k.Var2)
xlabel("Time [s]"), ylabel("Voltage [V]"), title("50Hz Wave, 10kHz Clock")
subplot(2,2,3), plot(ovs3k.Var1,ovs3k.Var3,ovs3k.Var1,ovs3k.Var2)
xlabel("Time [s]"), ylabel("Voltage [V]"), title("50Hz Wave, 3kHz Clock")
subplot(2,2,4), plot(ovs1k.Var1,ovs1k.Var3,ovs1k.Var1,ovs1k.Var2)
xlabel("Time [s]"), ylabel("Voltage [V]"), title("50Hz Wave, 1kHz Clock")
%% Overload point determination rule
figure
ov1k5=Lib.loadData("Data/Sin14-50hz1k5.csv");
ov10k=Lib.loadData("Data/Sin14-333hz10k.csv");
subplot(2,1,1), plot(ov1k5.Var1,ov1k5.Var3,ov1k5.Var1,ov1k5.Var2)
xlabel("Time [s]"), ylabel("Voltage [V]"),title("Overload Condition")
subplot(2,1,2), plot(ov10k.Var1,ov10k.Var3,ov10k.Var1,ov10k.Var2)
xlabel("Time [s]"), ylabel("Voltage [V]")

%% Good working 10hz 1-4V wave vs Oveflow 10hz 0-5V wave
figure
gw=Lib.loadData("Data/Sin14-10hz10k.csv");
subplot(2,1,1)
plot(gw.Var1,gw.Var3,gw.Var1,gw.Var2), title("Normal Contition")
xlabel("Time [s]"), ylabel("Voltage [V]")
subplot(2,1,2)
bw=Lib.loadData("Data/Sin05-10hz10k.csv");
plot(bw.Var1,bw.Var3,bw.Var1,bw.Var2), title("Counter Overflow")
xlabel("Time [s]"), ylabel("Voltage [V]")

%% Rise time
figure
tmp=Lib.loadData("Data/Sq14-10hz1k.csv");
rt1k=Lib.dataTrim(tmp,250,750);
subplot(2,1,1)
plot(rt1k.Var1,rt1k.Var3,rt1k.Var1,rt1k.Var2), title("Clock 1kHz")
xlabel("Time [s]"), ylabel("Voltage [V]")
subplot(2,1,2)
tmp=Lib.loadData("Data/Sq14-10hz10k.csv");
rt10k=Lib.dataTrim(tmp,550,700);
plot(rt10k.Var1,rt10k.Var3,rt10k.Var1,rt10k.Var2), title("Clock 10kHz")
xlabel("Time [s]"), ylabel("Voltage [V]")

%% Triangular wave distortion performances
figure
tmp=Lib.loadData("Data/Sin14-500hz10k.csv");
sn=tmp;
subplot(2,1,1)
plot(sn.Var1,sn.Var3,sn.Var1,sn.Var2), title("Sinusoidal 500Hz @ 10kHz")
xlabel("Time [s]"), ylabel("Voltage [V]")
subplot(2,1,2)
tmp=Lib.loadData("Data/Tri14-500hz10k.csv");
tri=tmp;
plot(tri.Var1,tri.Var3,tri.Var1,tri.Var2), title("Triangular 500Hz @ 10kHz")
xlabel("Time [s]"), ylabel("Voltage [V]")

%% ZOH transfer function
% ZOH in time domain = step(t)-step(t-T)
% ZOH in frequency domain = [1/s - 1/s * exp(-s*T)]
%      = 2/2 * (T/2)/(T/2) * exp(-s*T/2) * [exp(s*T/2)-exp(-s*T/2)]/s
% euler:   sin(x)=[exp(i*x)-exp(-i*x)]/(2*i)
% control: sinc(x)=sin(x)/x ; s=i*omega ; omega=2*pi*f
%      = T * exp(-s*T/2) * sinc(omega*T/2)
T=6.6667e-4; % 1.5 kHz
f = linspace(1,10000,10000);
ZOH_magnitude =abs( T*sinc((2*pi*f)*T/2) );
ZOH_phase = -(2*pi*f)*T/2 + angle( sin(f*T/2) );

subplot(2,1,1)
semilogx(f,ZOH_magnitude/T,'r') ; % Sampling divides by T, so overall gain=1
title("Magnitude"),xlabel("Frequency [Hz]"),ylabel("|Gain|")
subplot(2,1,2)
semilogx(f,mod(ZOH_phase,-pi/2),'r')
title("Phase"),xlabel("Frequency [Hz]"),ylabel("Phase shift [Rad]")






