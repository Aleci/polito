METADATA:

Metadata are encoded in the filenames with the following pattern
WAVETYPE MINVOLT MAXVOLT - INPUTFREQ hz CLOCKFREQ .csv

For ordering purposes 1.5k is represented as 1k5

In case of two set of data acquired with the same conditions an _2 is
appended to the name of the second set.

The files with the RS prefix are the ones acquired with the modified
ladder network. The modification consists in the addition of an 10k
resistor in series of the 2R resistor of the most significative bit's
branch.

The overload condition was serched using as a reference a 50hz sinewave
and the overload condition was found with a clock frequency of 1.5khz.
The Clock/Signal frequency ratio was then double checked with a 333hz
sinewave and 10khz clock. 

The channels of the oscilloscope were connected as follows:
CH1: Input Wave
CH2: Reconstructed Wave
CH3: Clock Wave

