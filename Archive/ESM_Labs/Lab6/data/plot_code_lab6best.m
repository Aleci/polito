Test1=Lib.loadData ("exp1.csv")
Test2=Lib.loadData ("exp2.csv")
Test3=Lib.loadData ("exp3.csv")
Test3ZOOM=Lib.loadData ("exp3zoom.csv")
Test4=Lib.loadData ("exp4.csv")
Test5=Lib.loadData ("exp5.csv")
Test5ZOOM=Lib.loadData ("exp5zoom.csv")
Test6=Lib.loadData ("exp6.csv")
Test7=Lib.loadData ("exp7.csv")
Test8=Lib.loadData ("exp8.csv")
Test9=Lib.loadData ("exp9.csv")

%% esperiment 1-2
R100=98.7;
R100_2=99.03;
R120=118.82;
Ro=R100/0.7-R100;
R22=21.83;
R15=14.99;
%using a cable of 24meters
hold on
plot(Test1.Var1,Test1.Var2)
xlabel('Time [s]')
ylabel('Voltage [V]')
figure
%plot(Test2.Var1,Test2.Var2/R100,Test2.Var1,Test2.Var3/R100)
plot(Test2.Var1,Test2.Var2,Test2.Var1,Test2.Var3)
%figure
%plot(Test2.Var1,Test2.Var3-Test2.Var2)
xlabel('Time [s]')
ylabel('Voltage [V]')
legend('generator signal','100Ohm load voltage','Location','southeast')

%% experiment 3
hold on
subplot(2,1,1) %how many plots in colum, rows then id of column
plot(Test3.Var1,Test3.Var2,Test3.Var1,Test3.Var3)
xlabel('Time [s]')
ylabel('Voltage [V]')
subplot(2,1,2)
plot(Test3ZOOM.Var1,Test3ZOOM.Var2,Test3ZOOM.Var1,Test3ZOOM.Var3)
legend('generator signal','output signal','Location','southeast')
xlabel('Time [s]')
ylabel('Voltage [V]')
%% experiment 4
hold on
plot(Test4.Var1,Test4.Var2,Test4.Var1,Test4.Var3)
%plot(Test4.Var1,movmean(Test4.Var2,20),Test4.Var1,movmean(Test4.Var3,20))
legend('generator signal','output signal','Location','southeast')
xlabel('Time [s]')
ylabel('Voltage [V]')
%% experiment 5
hold on
subplot(2,1,1)
plot(Test5.Var1,Test5.Var2,Test5.Var1,Test5.Var3)
xlabel('Time [s]')
ylabel('Voltage [V]')
subplot(2,1,2)
plot(Test5ZOOM.Var1,Test5ZOOM.Var2,Test5ZOOM.Var1,Test5ZOOM.Var3)
legend('generator signal','output signal')
xlabel('Time [s]')
ylabel('Voltage [V]')
%% experiment 6
hold on
plot(Test6.Var1,Test6.Var2,Test6.Var1,Test6.Var3)
%plot(Test6.Var1,movmean(Test6.Var2,40),Test6.Var1,movmean(Test6.Var3,40))
legend('generator signal','output signal','Location','southeast')
xlabel('Time [s]')
ylabel('Voltage [V]')
%% experiment 7
hold on
plot(Test7.Var1,Test7.Var2,Test7.Var1,Test7.Var3)
%plot(Test7.Var1,movmean(Test7.Var2,30),Test7.Var1,movmean(Test7.Var3,30))
legend('generator signal','output signal','Location','southeast')
xlabel('Time [s]')
ylabel('Voltage [V]')
%% experiment 8
hold on
plot(Test8.Var1,Test8.Var3,Test8.Var1,Test8.Var4)
legend('generator signal','output signal','Location','southeast')
xlabel('Time [s]')
ylabel('Voltage [V]')
%% experiment 9
hold on
%%
plot(Test9.Var1,movmean(Test9.Var3,5),Test9.Var1,movmean(Test9.Var4,5))
%%
plot(Test9.Var1,movmean(Test9.Var2,20),Test9.Var1,movmean(Test9.Var3,20),Test9.Var1,movmean(Test9.Var4,20))
legend('generator signal','after parallel resistance','end of coaxial cable','Location','southeast')
xlabel('Time [s]')
ylabel('Voltage [V]')

hold on
subplot(2,1,1)
plot(Test9.Var1,Test9.Var2,Test9.Var1,Test9.Var3,Test9.Var1,Test9.Var4)
xlabel('Time [s]')
ylabel('Voltage [V]')
title('Measured outputs from the oscilloscope')
subplot(2,1,2)
plot(Test9.Var1,movmean(Test9.Var2,20),Test9.Var1,movmean(Test9.Var3,20),Test9.Var1,movmean(Test9.Var4,20))
legend('generator signal','after parallel resistance','end of coaxial cable','Location','southeast')
xlabel('Time [s]')
ylabel('Voltage [V]')
title('Measured outputs with moving average of 20 for 1200points')