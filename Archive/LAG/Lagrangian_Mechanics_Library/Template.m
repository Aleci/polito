% This script is a template,
% so it is not directly executable, because otherwise it could leave several bug difficoult to find.
% It is just a quick reference on what is the api of this library.

clear all; close all; clc; format short; addpath("Path/to/Library");
pkg load symbolic % octave package symbolic v3.0.1; sympy v1.10.1

% Define literal constants (real symbolic variables)
syms t M1 M2 lzero k beta1 beta2 g F real
% Define generlized variables (symbolic functions of the already defined symvar t)
syms q1(t) q2(t) q3(t)

% Kinematic definition
% ATTENTION: A single rototranslation is a rotation plus a translation on the ORIGINAL axis
T01=mkT(Rot("j",q1(t)),[0;0;0]); % rotate around y0 of q1
T12=mkT(Rot("k",-q2(t)),[0;3;0]); % rotate around z1=z2 of -q2 after a translation of 3 along y1
T13=mkT(eye(3),[q3(t);0;0]);  % only translate along x2=x3 of q3(t)

% Mobile frame 1 (R1)  (each frame is attached to a rigid body with dynamical properties)
frame(1).parent=0;
frame(1).T=T01;
dynamics.body(1).pointMasses={}; % no point masses here
% since R1 only rotates, a complex mass distribution can be given as inertia instead of points
dynamicd.body(1).gamma=[some given 3x3 tensor]; % wrt the 

% Mobile frame 2 (R2)
frame(2).parent=1;
frame(2).T=T12;              % ATTENTION: ALL THE VECTORS MUST BE COLUMN VECTORS!
dynamics.body(2).pointMasses={ {M,[px;py;pz]} }; % a single point mass with coordinates wrt R2

% Mobile frame 3 (R3)
frame(3).parent=1; % Note: here R2 and R3 have the same parent
frame(3).T=T13;
dynamics.body(3).pointMasses={ {m1,[p1x;p2y;p3z]} , {m2,[p2x;p2y;p2z]} ]; % two point masses with coordinates wrt R3

% Mobile frame 4 (R4)
% It is possible to have more frames than generalized coordinates. Clearly, in that case, the movements
% of the frames will be not totally independent.

kinematics.frame=frame; % mandatory step

dynamics.genCoord(1).sym=q1(t);
dynamics.genCoord(1).k=k1;
dynamics.genCoord(1).restPos=l0;
dynamics.genCoord(1).elastic_relation=q1(t); % the displacement is just q1
dynamics.genCoord(1).beta=beta; 
dynamics.genCoord(1).damping_relation=q1(t);  % q1dot is damped!

dynamics.genCoord(2).sym=q2(t);
dynamics.genCoord(2).k=k2;
dynamics.genCoord(2).restPos=0;
dynamics.genCoord(2).elastic_relation=q2(t)-q1(t); % the displacement is the difference !
dynamics.genCoord(2).beta=0; % there is no damping here

dynamics.genCoord(3).sym=q3(t);
dynamics.genCoord(3).k=0; % no compliance here!
dynamics.genCoord(3).beta=beta;
dynamics.genCoord(3).damping_relation=q3(t)-q1(t); % the difference in velocity between R3 and R1 is damped!

dynamics.gravityVect=[0;0;-Gvect]; % define the gravity convention wrt the inertial frame

 % ATTENTION: ALL THE VECTORS MUST BE COLUMN VECTORS!
dynamics.forces={ {[0;F1;0],[1;0;0]} , {[F2;0;0],[q3(t);0;0]} };

[eq,kin,dyn]=automaticDerivation(kinematics,dynamics,t);

% Use the curly braces here!
eq{1},eq{2},eq{3}
