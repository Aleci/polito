clear all; close all; clc; format short; addpath("../../Library");
pkg load symbolic % octave package symbolic v3.0.1; sympy v1.10.1

% In this exercise the problem raised by the rope, which is not a rigid body, can be worked around
% with the standard selection of the frames. In fact, each mass can have attached a mobile frame
% with a known position wrt the inertial frame. Clearly the number of frames will be greather than
% the number of generalized coordinates, but this does not constitute a problem for the library.

% Define literal constants (real symbolic variables)
syms t m g L theta real
% Define generlized variables (symbolic functions of the already defined symvar t)
syms q(t)

% Kinematic definition
% ATTENTION: A single rototranslation is a rotation plus a translation on the ORIGINAL axis
T01=mkT(eye(3),[q(t);0;0]); % frame attached to the wedge
T02=mkT(Rot("j",theta),[q(t);0;0])*mkT(eye(3),[L-q(t);0;0]); % frame attached to the mass


% Mobile frame 1 (R1)  (each frame is attached to a rigid body with dynamical properties)
frame(1).parent=0;
frame(1).T=T01;
dynamics.body(1).pointMasses={ {m,[0;0;0]} };

% Mobile frame 2 (R2)
frame(2).parent=0;
frame(2).T=T02;
dynamics.body(2).pointMasses={ {m,[0;0;0]} }; % a single point mass with coordinates wrt R2

kinematics.frame=frame; % mandatory step

dynamics.genCoord(1).sym=q(t);
dynamics.genCoord(1).k=0;
dynamics.genCoord(1).beta=0; 

dynamics.gravityVect=[0;0;-g]; % define the gravity convention wrt the inertial frame

 % ATTENTION: ALL THE VECTORS MUST BE COLUMN VECTORS!
dynamics.forces={ };

[eq,kin,dyn]=automaticDerivation(kinematics,dynamics,t);

% Use the curly braces here!
eq{1}
