%% Exercise
clear all; close all; clc; format short; addpath("../../Library")
pkg load symbolic % sympy v1.10.1
% Notation: Axy=A^x_y => the geometrical object Ay computed With Respect To (wrt) the frame x

%% Choose generalized coordinates and define a frame for each body so that Vorigin=Vcm holds in the fixed frame
syms t m L g theta
syms q(t)
% Frame 0 fixed
% Frame 1 on the wedge
t01=[q(t); 0; 0];
R01=eye(3);
T01=[R01, t01; [0 0 0 1] ];
% Frame 2 on the mass with x axis parallel to the wedge inclined edge
T12rot=[[ cos(theta) sin(theta)   0         0   ];
        [-sin(theta) cos(theta)   0         0   ];
        [    0           0        1         0   ];
        [    0           0        0         1   ]];
T12tra=[eye(3), [L-q(t); 0; 0]; [0 0 0 1]];
T02=T01*T12rot*T12tra;
[R02,t02]=explode(T02);
%% Localize all the point masses composing each body 
wedge={ {m,[0; 0; 0]} }; % wrt R1
mass={ {m,[0; 0; 0]} }; % wrt R2
%% Compute center of mass value and position of each body
[cmW, r1cmW]=centerOfMass(wedge); %Trivial computation (fake cm position, but not relevant)
[cmM, r2cmM]=centerOfMass(mass); %Trivial computation
r0cmW=transform(T01,r1cmW);
r0cmM=transform(T02,r2cmM);

%% Compute angular velocity of each frame
% Frame 0 is the fixed frame
% Omega 1 in frame 0
omega01=omega(R01,t);
omega02=omega(R02,t);

%% Compute linear velocity of the origin of each frame
% Velocity of frame 1 wrt frame 0
V01=diff(t01,t);
V02=diff(t02,t);
 
%% Compute kinetic energy of each body
% Kinetic energy of the center of mass computed in frame 0
K1=kineticCoEnergy(cmW,V01,omega01,r1cmW,zeros(3),R01);
K2=kineticCoEnergy(cmM,V02,omega02,r2cmM,zeros(3),R02); 

K=K1+K2;
%% Compute potential energy of each body
% Potential energy of the center of mass computed in frame 0
G=[0;-g;0];
U=gravitationalEnergy(cmM,r0cmM,G)+gravitationalEnergy(cmW,r0cmW,G);

%% Compute the Lagrangian and apply the Euler-Lagrange equation
L=simplify(K-U);

% Compute the equations wrt all the generalized coordinates
eq1=ithEulerLagrangeEquation(q(t),L,0,0,t)

