% This script is a template,
% so is not directly executable, because otherwise it could leave several bug difficoult to find.
% It is just a quick reference on what is the api of this library.

clear all; close all; clc; format short; addpath("../../Library");
pkg load symbolic % octave package sympy v3.0.1; sympy v1.10.1

% Define literal constants (real symbolic variables)
syms t M m g l F real
% Define generlized variables (symbolic functions of the already defined symvar t)
syms q1(t) q2(t)

% Kinematic definition
% ATTENTION: A single rototranslation is a rotation plus a translation on the ORIGINAL axis
T01=[ eye(3), [q1(t);0;0]; [0 0 0 1]];
T12=[[cos(q2(t)) -sin(q2(t)) 0    0]; 
     [sin(q2(t)) cos(q2(t))  0    0]; % use this notation to define the matrixes!
     [    0         0        1    0];
     [    0       0      0        1]];

% Mobile frame 1 (R1)
frame(1).parent=0;
frame(1).T=T01;
dynamics.body(1).pointMasses={ {M,[q1(t);0;0]} };
% Mobile frame 2 (R2)
frame(2).parent=1;
frame(2).T=T12;              % ATTENTION: ALL THE VECTORS MUST BE COLUMN VECTORS!
dynamics.body(2).pointMasses={ {m,[0;l;0]} }; % a single point mass with coordinates wrt R2

kinematics.frame=frame; % mandatory step

dynamics.genCoord(1).sym=q1(t);
dynamics.genCoord(1).k=0;
dynamics.genCoord(1).beta=0; 

dynamics.genCoord(2).sym=q2(t);
dynamics.genCoord(2).k=0;
dynamics.genCoord(2).beta=0; % there is no damping here

dynamics.gravityVect=[0;-g;0]; % define the gravity convention
 % points of application wrt the inertial frame
 % ATTENTION: ALL THE VECTORS MUST BE COLUMN VECTORS!

dynamics.forces={ {[F;0;0],0,[q1(t);0;0],0} , {[F;0;0],0,[0;l;0],2} };

[eq,kin,dyn]=automaticDerivation(kinematics,dynamics,t);

% Use the curly braces here!
eq{1},eq{2}
