clear all; close all; clc; format short; addpath("../../Library");
pkg load symbolic % octave package sympy v3.0.1; sympy v1.10.1

% Define literal constants (real symbolic variables)
syms t m l0 k h g real
% Define generlized variables (symbolic functions of the already defined symvar t)
syms q1(t) q2(t)

% Kinematic definition
% ATTENTION: A single rototranslation is a rotation plus a translation on the ORIGINAL axis
T01=[[cos(q1(t))  0 sin(q1(t))  0]; 
     [    0       1    0        0]; % just rotate of q1
     [-sin(q1(t)) 0 cos(q1(t))  0];
     [    0       0    0        1]];
T12=[eye(3), [0;0;q2(t)];[0,0,0,1]]; %  just translate of q2

frame(1).parent=0;
frame(1).T=T01;
dynamics.body(1).pointMasses={};

frame(2).parent=1;
frame(2).T=T12;
dynamics.body(2).pointMasses={ {m,[0;0;0]} };

kinematics.frame=frame; % mandatory step

dynamics.genCoord(1).sym=q1(t);
dynamics.genCoord(1).k=0;

dynamics.genCoord(2).sym=q2(t);
dynamics.genCoord(2).k=k;
dynamics.genCoord(2).restPos=l0;
dynamics.genCoord(2).elastic_relation=q2(t);
dynamics.genCoord(2).beta=h; 
dynamics.genCoord(2).damping_relation=q2(t);

dynamics.gravityVect=[0;0; g]; % define the gravity convention
dynamics.forces={};

[eq,kin,dyn]=automaticDerivation(kinematics,dynamics,t);
eq{1},eq{2}
