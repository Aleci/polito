%% Exercise 14a (More complex solution for didactical purposes)
clear all; close all; clc; format short; addpath("../../Library");
pkg load symbolic % sympy v1.10.1
% Notation: Axy=A^x_y => the geometrical object Ay computed With Respect To (wrt) the frame x
% NOTE: In this example it has been decided to use a not standard body frame position.
%       This choice leads to a time varying mass position vector wrt the body frame. 
%       This is not an optimal choice in terms of simplicity because it requires 
%       the explicit computation of the standard positioned body frame velocity 
%       (needed in the linear kinetic co energy formula) wrt the 
%       actual body frame, which corresponds to the velocity of a point rigidly attached 
%       to the body (e.g. the cm). This procedure is prone to errors thus it's recommended 
%       to place body frames so that all the positions of point masses are constant, allowing the
%       use of relative velocities between reference frames only (because Vcm=Vorigin).

%% Choose generalized coordinates and define a frame for each body
syms t m l0 k beta g real 
syms q1(t) q2(t)
% Frame 0 fixed
% Frame 1 WITH THE SAME ORIGIN OF 0 but rotating of q1 degrees
T01=[[cos(q1(t))      0    sin(q1(t))   0  ];
     [    0           1        0        0  ];
     [-sin(q1(t))     0    cos(q1(t))   0  ];
     [    0           0        0        1  ]];
[R01 t01]=explode(T01);

%% Localize all the point masses composing each body 
pointMass={ {m,[0; 0; q2(t)]} };
%% Compute center of mass value and position of each body
[cm, r1cm]=centerOfMass(pointMass); %Trivial computation
r0cm=transform(T01,r1cm);

%% Compute angular velocity of each frame
% Frame 0 is the fixed frame
% Omega 1 in frame 0
omega01=omega(R01,t);

%% Compute linear velocity of the origin of each frame
% Velocity of frame 1 wrt frame 0
V01=diff(t01,t);
% Velocity of the center of mass
V0cm=absVelocity(r1cm, V01, omega01, R01, t);
V1cm=diff(r1cm,t);
 
%% Compute intertia tensors
% Inertia around origin of frame 1 wrt frame 1
gamma11=gamma(pointMass);
% Inertia tensors convertion wrt frame 0
gamma01=tensorTransform(gamma11,R01);

%% Compute kinetic energy of each body
% Kinetic energy of the center of mass computed in frame 0
K=kineticCoEnergy(cm,V0cm,omega01,r1cm,gamma01,R01); 

%% Compute potential energy of each body
% Potential energy of the center of mass computed in frame 0
G=[0;0;g];
U=elasticEnergy(q2(t),k,l0) + gravitationalEnergy(cm,r0cm,G);

%% Compute the Rayleigh function
% No viscous term relative to the generalized coordinate q1
% Viscous term relative to the generalized coordinate q2
D=rayleigh(q2(t),beta,t);

%% Compute the generalized forces
appliedForces={ {[0;0;0],[0;0;0]} }; % not useful in this exercise
% Compute the generalized forces wrt the generalized coordinates
Fg1=ithGeneralizedForce(q1(t),appliedForces);
Fg2=ithGeneralizedForce(q2(t),appliedForces);

%% Compute the Lagrangian and apply the Euler-Lagrange equation
L=K-U;

% Compute the equations wrt all the generalized coordinates
eq1=ithEulerLagrangeEquation(q1(t),L,D,Fg1,t)
eq2=ithEulerLagrangeEquation(q2(t),L,D,Fg2,t)

