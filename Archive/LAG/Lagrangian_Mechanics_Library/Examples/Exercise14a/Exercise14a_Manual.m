%% Exercise 14a (Standard solution compared with the professor's one)
clear all; close all; clc; format short; addpath("../../Library");
pkg load symbolic % sympy v1.10.1
% Notation: Axy=A^x_y => the geometrical object Ay computed With Respect To (wrt) the frame x

%% Choose generalized coordinates and define a frame for each body so that Vorigin=Vcm holds in the fixed frame
syms t m l0 k beta g real 
syms q1(t) q2(t)
% Frame 0 fixed
% Frame 1 on the point mass 
% ATTENTION: A single rototranslation is a rotation plus a translation on the ORIGINAL axis
% Rotation about original axis j
T01rot=[[cos(q1(t))      0    sin(q1(t))    0   ];
        [    0           1        0         0   ];
        [-sin(q1(t))     0    cos(q1(t))    0   ];
        [    0           0        0         1   ]];
% Translation about current axis k
T01tra=[[    1           0        1         0   ];
        [    0           1        0         0   ];
        [    0           0        1        q2(t)];
        [    0           0        0         1   ]];
T01=T01rot*T01tra;
[R01 t01]=explode(T01);

%% Localize all the point masses composing each body 
pointMass={ {m,[0; 0; 0]} };
%% Compute center of mass value and position of each body
[cm, r1cm]=centerOfMass(pointMass); %Trivial computation
r0cm=transform(T01,r1cm);

%% Compute angular velocity of each frame
% Frame 0 is the fixed frame
% Omega 1 in frame 0
omega01=omega(R01,t);

%% Compute linear velocity of the origin of each frame
% Velocity of frame 1 wrt frame 0
V01=diff(t01,t);
 
%% Compute intertia tensors
% Inertia around origin of frame 1 wrt frame 1
gamma11=gamma(pointMass);
% Inertia tensors convertion wrt frame 0
gamma01=tensorTransform(gamma11,R01);

%% Compute kinetic energy of each body
% Kinetic energy of the center of mass computed in frame 0
K=kineticCoEnergy(cm,V01,omega01,r1cm,gamma01,R01); 

%% Compute potential energy of each body
% Potential energy of the center of mass computed in frame 0
G=[0;0;g];
U=elasticEnergy(q2(t),k,l0) + gravitationalEnergy(cm,r0cm,G);

%% Compute the Rayleigh function
% No viscous term relative to the generalized coordinate q1
% Viscous term relative to the generalized coordinate q2
D=rayleigh(q2(t),beta,t);

%% Compute the generalized forces
appliedForces={ {[0;0;0],[0;0;0]} }; % not useful in this exercise
% Compute the generalized forces wrt the generalized coordinates
Fg1=ithGeneralizedForce(q1(t),appliedForces);
Fg2=ithGeneralizedForce(q2(t),appliedForces);

%% Compute the Lagrangian and apply the Euler-Lagrange equation
L=K-U;

% Compute the equations wrt all the generalized coordinates
eq1=ithEulerLagrangeEquation(q1(t),L,D,Fg1,t)
eq2=ithEulerLagrangeEquation(q2(t),L,D,Fg2,t)

