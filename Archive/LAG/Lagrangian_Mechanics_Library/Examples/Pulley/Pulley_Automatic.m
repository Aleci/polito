% ATTENTION:
% The rope is not a rigid body! Thus this library can't work properly automatically!
% In order to have the correct result a further modification in the automatically computed
% expression is needed. In particular all the calculations will be performed on a RIGID body
% of mass Mr. The non rigidity of the rope changes the equivalent expression of Mr in terms of
% the given masses. In fact the inertia of the system is proportional to Mr=mA+mB while the
% potential energy is proportional to Mr=mB-mA, since mA counterweights mB.
   
clear all; close all; clc; format short; addpath("../../Library");
pkg load symbolic % octave package simbolic v3.0.1; sympy v1.10.1

% Define literal constants (real symbolic variables)
syms t Mr k d l0 g real
% Define generlized variables (symbolic functions of the already defined symvar t)
syms q(t)

% Kinematic definition
% ATTENTION: A single rototranslation is a rotation plus a translation on the ORIGINAL axis
T01=mkT(eye(3),[0;0;q(t)]);

% Mobile frame 1 (R1)  (each frame is attached to a rigid body with dynamical properties)
frame(1).parent=0;
frame(1).T=T01;
dynamics.body(1).pointMasses={ {Mr,[0;0;0]} }; 

kinematics.frame=frame; % mandatory step

dynamics.genCoord(1).sym=q(t);
dynamics.genCoord(1).k=k;
dynamics.genCoord(1).restPos=l0;
dynamics.genCoord(1).elastic_relation=q(t); % the displacement is just q1
dynamics.genCoord(1).beta=d; 
dynamics.genCoord(1).damping_relation=q(t);  % q1dot is damped

dynamics.gravityVect=[0;0;-g]; % define the gravity convention

dynamics.forces={};

[eq,kin,dyn]=automaticDerivation(kinematics,dynamics,t);

% Use the curly braces here!
eq{1}
