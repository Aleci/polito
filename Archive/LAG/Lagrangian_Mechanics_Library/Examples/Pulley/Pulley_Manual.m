%% Exercise 
clear all; close all; clc; format short; addpath("../../Library")
pkg load symbolic % sympy v1.10.1
% Notation: Axy=A^x_y => the geometrical object Ay computed With Respect To (wrt) the frame x

%% Choose generalized coordinates and define a frame for each body so that Vorigin=Vcm holds in the fixed frame
syms t ma mb k d l0 g
syms q(t)
% Frame 0 fixed
% Frame 1 on the mass b
t01=[0; 0; q(t)];
R01=eye(3);
T01=[R01, t01; [0 0 0 1] ];

%% Localize all the point masses composing each body 
% not meaningful in this exercise
%% Compute center of mass value and position of each body
rMb1=[0;0;0];
rMb0=transform(T01,rMb1); %localize mb in fixed frame

%% Compute angular velocity of each frame
% Frame 0 is the fixed frame
% Omega 1 in frame 0
omega=zeros(3,1);

%% Compute linear velocity of the origin of each frame
% Velocity of frame 1 wrt frame 0
V01=diff(t01,t);

 
%% Compute intertia tensors
% Inertia around origin of frame 1 wrt frame 1
gamma=0;% no rotations

%% Compute kinetic energy of each body
% Kinetic energy of the center of mass computed in frame 0
% SINCE WE HAVE A PULLEY THE SUM OF THE MASSES IS INVOLVED IN THE KINETIC ENERGY (they move togheter)
K=kineticCoEnergy(ma+mb,V01,omega,rMb1,gamma,R01);

%% Compute potential energy of each body
% Potential energy of the center of mass computed in frame 0
% WHILE THE DIFFERENCE BETWEEN THE TWO IS INVOLVED IN THE POTENTIAL ENERGY (the umbalance generates work)
% Actually only mb has a radius vector so it's also the "positive" mass
G=[0;0;-g]; % pay attention to the gravity vector correctness!
U=gravitationalEnergy(mb-ma,rMb0,G)+elasticEnergy(q(t),k,l0);


%% Compute the Rayleigh function
% No viscous term relative to the generalized coordinate q1
% Viscous term relative to the generalized coordinate q2
D=rayleigh(q(t),d,t);

%% Compute the generalized forces
appliedForces={}; % not useful in this exercise
% Compute the generalized forces wrt the generalized coordinates
Fg1=ithGeneralizedForce(q(t),appliedForces);


%% Compute the Lagrangian and apply the Euler-Lagrange equation
L=K-U;

% Compute the equations wrt all the generalized coordinates
eq1=ithEulerLagrangeEquation(q(t),L,D,Fg1,t)

