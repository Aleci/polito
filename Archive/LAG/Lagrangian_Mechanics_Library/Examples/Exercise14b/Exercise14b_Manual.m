%% Exercise 14b
% A rigid body is a set of point masses with constant position wrt the body frame
% use .' transpose instead of complex transpose ' to keep the equations simple
clear all; close all; clc; format short; addpath("../../Library")
pkg load symbolic % sympy v1.10.1
syms t h l D theta0 m M k beta Fext g real 

%% Define generalized coordinates
syms q1(t) q2(t) q3(t)

%% Define a frame for each body
% Frame 0 inertial
% Frame 1 wrt frame 0
T01=[[cos(q2(t)) -sin(q2(t))   0   q1(t)];
     [sin(q2(t))  cos(q2(t))   0     h  ];
     [    0           0        1     0  ];
     [    0           0        0     1  ]];
[R01 t01]=explode(T01);
% Frame 2 wrt frame 0
T02=[[cos(q3(t)) -sin(q3(t))  0   q1(t)+D];
     [sin(q3(t))  cos(q3(t))  0      h   ];
     [    0           0       1      0   ];
     [    0           0       0      1   ]];
[R02 t02]=explode(T02);
% Frame 3 wrt frame 0
R03=eye(3);
t03=[q1(t); 0; 0];
T03=[[R03 t03];
     [0 0 0 1]];

%% Localize for each body all the point masses
% Position of left mass wrt frame 1
body1={ {m,[l; 0; 0]} };
% Position of left mass wrt frame 2
body2={ {m,[l; 0; 0]} };
% Position of left mass wrt frame 3
body3={ {M,[D/2; h/2; 0]} };

%% Compute center of mass value and position
% Center of mass of body 1 in frame 1
[cm1, r11]=centerOfMass(body1);
% Center of mass of body 2 in frame 2
[cm2, r22]=centerOfMass(body2);
% Center of mass of body 3 in frame 3
[cm3, r33]=centerOfMass(body3);
% Convert coordinates in the fixed frame
r01=transform(T01,r11);
r02=transform(T02,r22);
r03=transform(T03,r33);

%% Compute angular velocity of each frame
% Frame 0 is the fixed frame
%omega00=zeros(3,1);
% Omega 1 in frame 0
omega01=omega(R01,t);
% Frame 2 in frame 0
omega02=omega(R02,t);
% Frame 3 in frame 0
omega03=omega(R03,t);

%% Compute linear velocity of the origin of each frame
% Velocity of frame 1 wrt frame 0
v01=diff(t01,t);
% Velocity of frame 2 wrt frame 0
v02=diff(t02,t);
% Velocity of frame 3 wrt frame 0
v03=diff(t03,t);

%% Compute intertia tensors
% Inertia around origin of frame 1 wrt frame 1
gamma11=gamma(body1);
% Inertia around origin of frame 2 wrt frame 2
gamma22=gamma(body2);
% Inertia tensors convertion wrt frame 0
gamma01=tensorTransform(gamma11,R01);
gamma02=tensorTransform(gamma22,R02);

%% Compute kinetic energy of each body
% Kinetic energy of body 1 computed in frame 0
K1=kineticCoEnergy(cm1,v01,omega01,r11,gamma01,R01);
% Kinetic energy of body 2 computed in frame 0
K2=kineticCoEnergy(cm2,v02,omega02,r22,gamma02,R02);
% Kinetic energy of body 3 computed in frame 0
K3=kineticCoEnergy(cm3,v03,omega03,r33,eye(3),R03); % real gamma not provided by data and not useful
K=K1+K2+K3;

%% Compute potential energy of each body
G=[0; -g; 0];
% Potential energy of body 1
U1=elasticEnergy(q2(t),k,theta0) + gravitationalEnergy(cm1,r01,G);
% Potential energy of body 2
U2=elasticEnergy(q3(t),k,theta0) + gravitationalEnergy(cm2,r02,G);
% Potential energy of body 3 (constant)
U3=gravitationalEnergy(cm3,r03,G);
U=U1+U2+U3;

%% Compute the Rayleigh function
% Viscous term relative to the generalized coordinate q1
D1=rayleigh(q1(t),beta,t);
% Viscous term relative to the generalized coordinate q2
D2=rayleigh(q2(t),beta,t);
% Viscous term relative to the generalized coordinate q3
D3=rayleigh(q3(t),beta,t);
D=D1+D2+D3;

%% Compute the generalized forces
appliedForces={ {[Fext;0;0],[q1(t)+D;0;0]} };
% Compute the generalized forces wrt the generalized coordinates
Fg1=ithGeneralizedForce(q1(t),appliedForces);
Fg2=ithGeneralizedForce(q2(t),appliedForces);
Fg3=ithGeneralizedForce(q3(t),appliedForces);


%% Compute the Lagrangian and apply the Euler-Lagrange equation
L=K-U;
% Compute the equations wrt all the generalized coordinates
eq1=ithEulerLagrangeEquation(q1(t),L,D,Fg1,t)
eq2=ithEulerLagrangeEquation(q2(t),L,D,Fg2,t)
eq3=ithEulerLagrangeEquation(q3(t),L,D,Fg3,t)
