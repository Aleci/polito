%% Complete use of all the library functions in this exercise resolution 
clear all; close all; clc; format short ; addpath("../../Library")
pkg load symbolic % octave package symbolic v3.0.1; sympy v1.10.1
% Notation: Axy=A^x_y => the geometrical object Ay computed With Respect To (wrt) the frame x

%% Choose generalized coordinates and define a frame for each body so that Vorigin=Vcm holds in the fixed frame
syms t m M l k beta g F real 
syms q1(t) q2(t) q3(t)
% Frame 0 fixed
% Frame 1 on the point mass 
% ATTENTION: A single rototranslation is a rotation plus a translation on the ORIGINAL axis
% Rotation about original axis j
% R1 on the mass M
R01=eye(3);
t01=[q1(t);0;0];
T01=[ R01, t01; [0 0 0 1]];

% R2 on the sprung hinge, rotating with it
T12=[[cos(q2(t)) -sin(q2(t))  0  0  ];
     [sin(q2(t))  cos(q2(t))  0  0  ];
     [    0           0       1  0  ];
     [    0           0       0  1  ]];
[R12,t12]=explode(T12);
% R3 on the normal hinge, rotating with it
T23=[[ cos(q3(t)) -sin(q3(t)) 0 2*l];
     [ sin(q3(t))  cos(q3(t)) 0  0 ];
     [     0          0       1  0 ];
     [     0          0       0  1 ]];
[R23,t23]=explode(T23);
%
R02=R01*R12;
T03=T01*T12*T23;
[R03,t03]=explode(T03);

%% Localize all the point masses composing each body 
mass={ {M,[0; 0; 0]} }; %wrt frame 1
barbell={ {m,[0; -l; 0]}, {m,[0; l; 0]} }; %wrt frame 3

%% Compute center of mass value and position of each body
[cM, r1cM]=centerOfMass(mass); %Trivial computation
r0cM=transform(T01,r1cM);
[cm, r3cm]=centerOfMass(barbell);
r0cm=transform(T03,r3cm);

%% Compute angular velocity of each frame
% Frame 0 is the fixed frame
% Omega 1 in frame 0
omega01=omega(R01,t);
omega12=omega(R12,t);
omega02=R01*omega12;
omega23=omega(R23,t);
omega03=omega02+R02*omega23;

%% Compute linear velocity of the origin of each frame
% Velocity of frame 1 wrt frame 0
V01=diff(t01,t);
V02=absVelocity(t12, V01, omega01, R01, t); % a=R0 b=R1 p=Origin of R2
V03=absVelocity(t23, V02, omega02, R02, t); % a=R0 b=R2 p=Origin of R3

%% Compute intertia tensors
% Inertia around origin of frame 3 wrt frame 3
gammaNull=zeros(3,3); %the mass doesn't rotate
gamma33=gamma(barbell);
% Inertia tensors convertion wrt frame 0
gamma03=tensorTransform(gamma33,R03);

%% Compute kinetic energy of each body
Km=kineticCoEnergy(cm,V03,omega03,r3cm,gamma03,R03);
KM=kineticCoEnergy(cM,V01,omega01,r1cM,gammaNull,R01);
K=Km+KM

%% Compute potential energy of each body
G=[0;-g;0];
Um=elasticEnergy(q2(t),k,0) + gravitationalEnergy(cm,r0cm,G);
UM=elasticEnergy(q1(t),k,0) + gravitationalEnergy(cM,r0cM,G);
U=Um+UM
%% Compute the Rayleigh function
% No viscous term relative to the generalized coordinate q1
% Viscous term relative to the generalized coordinate q2
D=rayleigh(q1(t),beta,t)

%% Compute the generalized forces
appliedForces={ {[F;0;0],[q1(t);0;0]} }; % not useful in this exercise
% Compute the generalized forces wrt the generalized coordinates
Fg1=ithGeneralizedForce(q1(t),appliedForces);
Fg2=ithGeneralizedForce(q2(t),appliedForces);
Fg3=ithGeneralizedForce(q3(t),appliedForces);

%% Compute the Lagrangian and apply the Euler-Lagrange equation
L=K-U;

% Compute the equations wrt all the generalized coordinates
eq1=ithEulerLagrangeEquation(q1(t),L,D,Fg1,t)
eq2=ithEulerLagrangeEquation(q2(t),L,D,Fg2,t)

