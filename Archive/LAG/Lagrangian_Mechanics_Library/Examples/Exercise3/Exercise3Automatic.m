%% Complete use of all the library functions in this exercise resolution 
clear all; close all; clc; format short; addpath("../../Library");
pkg load symbolic % octave package symbolic v3.0.1; sympy v1.10.1
% Notation: Axy=A^x_y => the geometrical object Ay computed With Respect To (wrt) the frame x

%% Choose generalized coordinates and define a frame for each body so that Vorigin=Vcm holds in the fixed frame
syms t m M l k beta g F real 
syms q1(t) q2(t) q3(t)
% Frame 0 fixed
% Frame 1 on the point mass 
% ATTENTION: A single rototranslation is a rotation plus a translation on the ORIGINAL axis
% Rotation about original axis j%
% R1 on the mass M

R01=eye(3);
t01=[q1(t);0;0];
T01=[ R01, t01; [0 0 0 1]];

% R2 on the sprung hinge, rotating with it
T12=[[cos(q2(t)) -sin(q2(t))  0  0  ];
     [sin(q2(t))  cos(q2(t))  0  0  ];
     [    0           0       1  0  ];
     [    0           0       0  1  ]];

% R3 on the normal hinge, rotating with it
T23=[[ cos(q3(t)) -sin(q3(t)) 0 2*l];
     [ sin(q3(t))  cos(q3(t)) 0  0 ];
     [     0          0       1  0 ];
     [     0          0       0  1 ]];

%% Localize all the point masses composing each body 
mass={ {M,[0; 0; 0]} }; %wrt frame 1
barbell={ {m,[0; -l; 0]}, {m,[0; l; 0]} }; %wrt frame 3

%% Localize all the applied forces in the inertial frame
appliedForces={ {[F;0;0],0,[q1(t);0;0],0} }; 

frame(1).parent=0;
frame(1).T=T01;
dynamics.body(1).pointMasses=mass;

frame(2).parent=1;
frame(2).T=T12;
dynamics.body(2).pointMasses={};

frame(3).parent=2;
frame(3).T=T23;
dynamics.body(3).pointMasses=barbell;

kinematics.frame=frame;

dynamics.gravityVect=[0;-g;0];

dynamics.genCoord(1).sym=q1(t);
dynamics.genCoord(1).k=k;
dynamics.genCoord(1).restPos=0;
dynamics.genCoord(1).elastic_relation=q1(t);
dynamics.genCoord(1).beta=beta;
dynamics.genCoord(1).damping_relation=q1(t);

dynamics.genCoord(2).sym=q2(t);
dynamics.genCoord(2).k=k;
dynamics.genCoord(2).restPos=0;
dynamics.genCoord(2).elastic_relation=q2(t);
dynamics.genCoord(2).beta=0;

dynamics.genCoord(3).sym=q3(t);
dynamics.genCoord(3).k=0;
dynamics.genCoord(3).beta=0;

dynamics.forces=appliedForces;

[eq,kin,dyn]=automaticDerivation(kinematics,dynamics,t);

eq{1},eq{2}



