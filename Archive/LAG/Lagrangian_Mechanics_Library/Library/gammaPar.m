% Returns the inertial contribution of the mass m at distance P from the
% center of rotation
function GammaP=gammaPar(PointMassCell)
    m=PointMassCell{1};
    P=PointMassCell{2};
    GammaP=m*[P(2)^2+P(3)^2    -P(1)*P(2)    -P(1)*P(3)
              -P(1)*P(2)    P(1)^2+P(3)^2    -P(2)*P(3)
              -P(1)*P(3)    -P(2)*P(3)    P(1)^2+P(2)^2];
end
