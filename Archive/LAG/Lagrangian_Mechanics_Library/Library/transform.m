% Returns apply the transformation v(Ra) -> T -> v(Rb)
function vb=transform(Tba,va)
    vap=[va;1];
    vbp=Tba*vap;
    vb=vbp(1:3);
end
