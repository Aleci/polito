% Returns the inertia tensor gammaA computed wrt the frame Rb
function gammaB=tensorTransform(gammaA,Rba)
    gammaB=Rba*gammaA*Rba.';
    gammaB=simplify(gammaB);
end
