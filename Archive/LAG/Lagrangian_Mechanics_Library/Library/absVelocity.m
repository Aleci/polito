% Returns the absolute velocity in frame a
% of the point p expressed with coordinate in frame b
% usually p is the distance between the frame b and the next frame c (tbc)
% NOTE: The time t has to be a symbolic variable
function Vabs=absVelocity(r_bp, v_ab, omega_ab, Rab, t)
  Vrel=simplify(Rab*diff(r_bp,t));
  Vdrag=simplify(v_ab + skew(omega_ab)*Rab*r_bp);
  Vabs=simplify(Vrel+Vdrag);
end
