% Returns the vector representing the relative angular velocity 
% of frame b wrt frame a (R=Rab)
function Omega=omega(R,t)
    skewOmega=diff(R,t)*R.';
    Omega=uFromSkew(skewOmega);
    Omega=simplify(Omega);
end
