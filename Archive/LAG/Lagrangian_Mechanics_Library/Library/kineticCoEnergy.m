% Returns the kinetic co-energy of the body
% NOTE: if the frame is not inertial then Omegaab and/or Vab are 
%       different from zero       
function K=kineticCoEnergy(Mtot,Vab,Omegaab,rbCM,GammaaOb,Rab)
  Klin=simplify(sym(1)/2*Mtot*Vab.'*Vab);
  KrotNonCM=simplify(Mtot*Vab.'*skew(Omegaab)*Rab*rbCM);
  Krot=simplify(sym(1)/2*Omegaab.'*GammaaOb*Omegaab);
  K=simplify(Klin+KrotNonCM+Krot);
end
