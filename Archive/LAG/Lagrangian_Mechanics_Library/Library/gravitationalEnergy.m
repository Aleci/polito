% Returns the gravitational potential energy of the body
% NOTE: The radius vector has to be expressed in the same frame where
%       the kinetic energy was computed
function Ug=gravitationalEnergy(Mtot, rcm,g)
    Ug=-Mtot*g.'*rcm;
    Ug=simplify(Ug);
end
