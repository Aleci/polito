% Returns the damping term which takes into account the viscous friction
% relative to the generalized coordinate q, provided beta, the damping
% coefficientn
% NOTE: The generalized coordinate has to be a symbolic function such as q(t)
function D=rayleigh(q, beta, t)
    D=sym(1)/2*beta*diff(q,t)^2;
end
