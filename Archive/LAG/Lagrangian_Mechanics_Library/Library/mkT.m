% Build a transformation matrix given the rotation and the translation
function T=mkT(R,t)
  T=[R,t;[0,0,0,1]];
end
