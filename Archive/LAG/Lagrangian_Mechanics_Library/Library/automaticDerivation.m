% WARNING: Kinematics frames must be ordered such that the index of the parent should be always
%          lower than the indexes of the children
% NOTE: All the structures uses the notation in which all the quantities with the trailing 0
%       ar referred to the inertial frame, while the others are referred to the current frame
%       or are independent from the reference frame system.
function [Eqn,kinematics,dynamics]=automaticDerivation(kinematics,dynamics,timeVar)
  N=length(kinematics.frame); % Number of rigid bodies (one per mobile frame)
  n=length(dynamics.genCoord); % Number of generalized coordinates
  
% For each frame, derive all the kinematic transformations
  for jj=1:N
    parent=kinematics.frame(jj).parent;
    if(parent==0)
      kinematics.frame(jj).T0=kinematics.frame(jj).T;
    else
      T0Parent=kinematics.frame(parent).T0; clear parent;
      kinematics.frame(jj).T0=T0Parent*kinematics.frame(jj).T;
    end
    
    [R0,tr0]=explode(kinematics.frame(jj).T0);
    kinematics.frame(jj).R0=R0; clear R0;
    kinematics.frame(jj).t0=tr0; clear tr0;
    
    [R,tr]=explode(kinematics.frame(jj).T);
    kinematics.frame(jj).R=R; clear R;
    kinematics.frame(jj).t=tr; clear tr;
  end
  
% For each frame, compute the angular velocity of each frame
  for jj=1:N
    % First compute the angular velocity wrt the parent frame
    omegaCurrent=omega(kinematics.frame(jj).R,timeVar);
    kinematics.frame(jj).omega=omegaCurrent;
    % Then compute the angular velocity wrt the inertial frame
    parent=kinematics.frame(jj).parent;
    if(parent==0) % if the parent is the inertial frame, don't do nothing
      kinematics.frame(jj).omega0=omegaCurrent;
    else % otherwise add the parent angular velocity to the child one (already rotated)
      R0Par=kinematics.frame(parent).R0;
      omega0Par=kinematics.frame(parent).omega0;
      kinematics.frame(jj).omega0=omega0Par+R0Par*omegaCurrent; clear("parent","R0Par","omegaCurrent");   
    end
  end

% For each frame, compute the absolute velocity of its origin
  for jj=1:N
    parent=kinematics.frame(jj).parent;
    pos=kinematics.frame(jj).t;
    % if the parent is inertial, the velocity is the derivative of the position
    if(parent==0) 
      kinematics.frame(jj).V0=diff(pos,timeVar);
    else
      vel0Par=kinematics.frame(parent).V0;
      omega0Par=kinematics.frame(parent).omega0;
      R0Par=kinematics.frame(parent).R0;
      kinematics.frame(jj).V0=absVelocity(pos, vel0Par, omega0Par, R0Par, timeVar);
    end
  end

% For each body, compute the center of mass value and position
  for jj=1:N
    % First compute all the quantities in the current body frame
    [cm,rCm]=centerOfMass(dynamics.body(jj).pointMasses);
    dynamics.body(jj).cm=cm; clear cm;
    dynamics.body(jj).rCm=rCm;
    % Then convert the position in the inertial frame
    T0=kinematics.frame(jj).T0;
    kinematics.frame(jj).r0Cm=transform(T0,rCm); clear("T0","rCm")
  end

% For each body, compute the inertia tensor    
  for jj=1:N
    % First compute the tensor in the current body frame
    gamma=gamma(dynamics.body(jj).pointMasses);
    if(exist("dynamics.body(jj).gamma")) % if a gamma is already given, add it to the final tensor
      gamma=gamma+dynamics.body(jj).gamma;
    end
    dynamics.body(jj).gamma=gamma;
    % Then convert it in the inertial frame
    dynamics.body(jj).gamma0=tensorTransform(gamma,kinematics.frame(jj).R0); clear gamma;
  end

% For each body, compute the kinetic co-energy in the inertial frame
  for jj=1:N
    cm=dynamics.body(jj).cm;
    rCm=dynamics.body(jj).rCm;
    V0=kinematics.frame(jj).V0;
    omega0=kinematics.frame(jj).omega0;
    gamma0=dynamics.body(jj).gamma0;
    R0=kinematics.frame(jj).R0;
    %
    if(cm!=0)
      dynamics.body(jj).Kstar=kineticCoEnergy(cm,V0,omega0,rCm,gamma0,R0);
    end
    clear("cm","rCm","V0","omega0","gamma0","R0");
  end

% Compute the total kinetic co-energy of the whole system
  dynamics.Kstar=sym(0);
  for jj=1:N
    % Sum up all the non-zero kinetic co-energies
    if(not(isempty(dynamics.body(jj).Kstar)))
      dynamics.Kstar=simplify( dynamics.body(jj).Kstar + dynamics.Kstar );
    end
  end

% For each body, compute the gravitational potential energy     
  for jj=1:N
    cm=dynamics.body(jj).cm;
    if(cm==0)
      dynamics.body(jj).Ugravity=0;
    else
      r0Cm=kinematics.frame(jj).r0Cm;      
      dynamics.body(jj).Ugravity=gravitationalEnergy(cm,r0Cm,dynamics.gravityVect); clear r0Cm
    end
    clear cm
  end

% For each generalized coordinate, compute the related elastic potential energy
  for jj=1:n
    k=dynamics.genCoord(jj).k;
    if(k==0)
      dynamics.genCoord(jj).Uelastic=0;
    else
      expr_in_q=dynamics.genCoord(jj).elastic_relation;
      restPos=dynamics.genCoord(jj).restPos;
      dynamics.genCoord(jj).Uelastic=elasticEnergy(expr_in_q,k,restPos); clear("expr_in_q","restPos");
    end
    clear k
  end

% Compute the potential energy of the whole system
  U=sym(0);
  % For each body
  for jj=1:N
    % Sum up all the non-zero gravitational energies
    if(not(isempty(dynamics.body(jj).Ugravity)))
      U=simplify( U+dynamics.body(jj).Ugravity );
    end
  end
  % For each generalized coordinate
  for jj=1:n
    % Sum up all the non-zero elastic energies
    if(not(isempty(dynamics.genCoord(jj).Uelastic)))
      U=simplify( U+dynamics.genCoord(jj).Uelastic );
    end
  end
  dynamics.U=U;

% Compute the Lagrangian
  dynamics.L=simplify(dynamics.Kstar - dynamics.U);
  
% For each generalized coordinate, compute the relative Rayleigh function
  for jj=1:n
    beta=dynamics.genCoord(jj).beta;
    if(beta==0)
      dynamics.genCoord(jj).D=0;
    else
      expr_in_q=dynamics.genCoord(jj).damping_relation;
      dynamics.genCoord(jj).D=rayleigh(expr_in_q,beta,timeVar); clear expr_in_q;
    end
    clear beta
  end

% Compute the Rayleigh function for the whole system
  dynamics.D=sym(0);
  for jj=1:n
    if(dynamics.genCoord(jj).beta != 0)
      dynamics.D=simplify( dynamics.D+dynamics.genCoord(jj).D );
    end
  end
  
% Convert all the given forces in the inertial frame
  dynamics.forces0={};
  for jj=1:size(dynamics.forces,2)
    x=dynamics.forces{jj};
    if(x{2}!=0) % convert the force
      f=simplify( kinematics.frame(x{2}).R0*x{1} )
    else
      f=x{1};
    end
    if(x{4}!=0)
      p=simplify( transform(kinematics.frame(x{4}).T0,x{3}) );
    else
      p=x{3};
    end
    dynamics.forces0{jj}={f,p};
  end
  
% For each generalized coordinate, compute the relative generalized force
  for jj=1:n
    symVar=dynamics.genCoord(jj).sym;
    dynamics.genCoord(jj).genForce=ithGeneralizedForce(symVar,dynamics.forces0); clear symVar
  end
  
% For each generalized coordinate, compute the dynamical equation
  for jj=1:n
    symVar=dynamics.genCoord(jj).sym;
    Fg=dynamics.genCoord(jj).genForce;
    Eqn{jj}=ithEulerLagrangeEquation(symVar,dynamics.L,dynamics.D,Fg,timeVar);
    dynamics.genCoord(jj).dynEq=Eqn{jj}; clear("F","symVar");
  end
end
