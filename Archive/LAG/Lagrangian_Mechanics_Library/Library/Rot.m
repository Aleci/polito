% Define the rotation matrix from the more common angle axis representations
% Note: to rotate clockwise use negative thetas instead of inverting the axis sense
% The only available axis are "i", "j" or "k"
% Theta can be a symbolic function of t
function R=Rot(axis,theta)
  if(axis=="i")
    R=[[ 1      0           0      ];
       [ 0  cos(theta) -sin(theta) ];
       [ 0  sin(theta)  cos(theta) ]];
  end
  if(axis=="j")
    R=[[ cos(theta) 0 sin(theta) ]; 
       [    0       1    0       ]; 
       [-sin(theta) 0 cos(theta) ]];

  end
  if(axis=="k")
    R=[[ cos(theta) -sin(theta) 0 ];
       [ sin(theta)  cos(theta) 0 ];
       [    0           0       1 ]];
  end
end
