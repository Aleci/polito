% Returns the elastic potential energy relative to the generalize
% coordinate q, provided the stiffness K and the rest position r
% NOTE: The generalized coordinate has to be a symbolic function such as q(t)
function Ue=elasticEnergy(q, K, r)
    Ue=sym(1)/2*K*(q-r)^2;
end
