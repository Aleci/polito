% Returns the vector that compose the skew simmetrix matrix
function u=uFromSkew(skewMatrix)
    u=[skewMatrix(3,2); skewMatrix(1,3); skewMatrix(2,1)];
end
