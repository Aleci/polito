% Returns the center of mass of the body constituted by a set of point
% masses localized in the same frame
function [Mcm Pcm]=centerOfMass(body)
  if(isempty(body))
    Mcm={};
    Pcm={};
  end
  Mtot=0;
  Ptot=zeros(3,1);
  for k=1:length(body)
    pointMassKth=body{k};
    Mtot=Mtot + pointMassKth{1};
    Ptot=Ptot + pointMassKth{1}*pointMassKth{2};
  end
  Mcm=Mtot;
  Pcm=Ptot/Mtot;
end
