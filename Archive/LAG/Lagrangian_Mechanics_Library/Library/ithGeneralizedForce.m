% Returns the generalized force relative to the generalized coordinate q
% The forceList has to be a cell array of tuples of the following shape
% {Non-conservative force, Point of application}
% NOTE: q has to be a symbolic function such as q(t) and all the point of
%       application have to be expressed in the same frame were
%       the Lagrangian was computed
function Fgi=ithGeneralizedForce(q, forcesList)
  if(isempty(forcesList))
    Fgi=0;
  else
    Fgi=0;
    for i=1:length(forcesList)
      Fnc=forcesList{i}{1};
      Pap=forcesList{i}{2};
      Fgi=Fgi + Fnc.' * diff(Pap,q);
    end
  end
end
