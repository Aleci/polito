% Returns the inertia tensor representing the list point masses with the
% corresponding position wrt the center of rotation (the origin of the body frame)
% Example: gamma({ {m1,p1} , {m2,p2} .... {mN,pN} })
function Gamma=gamma(body)
  if(isempty(body))
    Gamma=zeros(3,3);
  else
    Gamma=zeros(3,3);
    for k=1:length(body)
      Gamma=Gamma+gammaPar(body{k});
    end
  end
end
