% Returns the rotation matrix and the translation vector of a translation
function [R t]=explode(T)
    R=T(1:3,1:3);
    t=T(1:3,4);
end
