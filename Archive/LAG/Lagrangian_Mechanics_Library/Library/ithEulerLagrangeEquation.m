% Returns the equation that regulates the dynamics of the ith generalized
% coordinate provided the lagrangian, the Rayleigh function D and the
% generalized non conservative forces Fg
% NOTE: The generalized coordinate q has to be a symbolic function such as
% q(t) whereas the time t has to be a symbolic variable
function eqi=ithEulerLagrangeEquation(q, L, D, Fg, t)
  qdot=diff(q,t);
  eqi=diff( diff(L,qdot) ,t) - diff(L,q) + diff(D,qdot) == Fg;
  eqi=simplify(eqi);
end
