% Find the beam waist and its location starting from the given q
% NOTE: the given q should be the complex parameter obtained after a transformation
%       otherwise the actual waist is not guaranteed to be found
function [w0,z0]=waistRadius(q,lambda,step)
  z=step;
  precZ=0;
  precW=beamRadius(q,lambda);
  while(z<1e100)% The radius should decrease first and then increase again
    w=beamRadius(q+z,lambda);
    if(w>precW) % When it increases precW is the waist
      w0=precW; %generate an error if the cycle is endless
      z0=precZ;
      break
    end
    precW=w;
    precZ=z;
    z=z+step;
  end
end
