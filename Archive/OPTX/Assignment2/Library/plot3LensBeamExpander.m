% Compute and plot the waist profile (from an initial waist w0) of a three lens beam expander
% The computed quantities are computed with respect to the propagation
% refractive index nMedium and the wavelength lambda0
% The gaussian beam quality can be worsened using the M2 parameter
% Convention:      (Note: wavelength, waist radius, distances and focal lengths are in mm)
% q1 ---d1--> q2 (Lens1) q3 ---d2--> q4 (Lens2) q5 ---d3--> q6 (Lens3) q7 ---d4--> q8
% The returned data are in mm
% The optional paremeter suppresses the plot when its value is true
function [w,x,data]=plot3LensBeamExpander(nMedium,lambda0,w0,d1,d2,d3,d4,f1,f2,f3,M2,suppress)
  if nargin<12, suppress=false; end

  lambda=lambda0/nMedium;
  BPP=M2*lambda/pi; % the BPP is fixed [mm*rad]
  thetaIn=BPP/w0; % so also the input divergence

  % Define the transformation matrix for each lens
  lens1=thinLens(f1);
  lens2=thinLens(f2);
  lens3=thinLens(f3);

  % Compute the input complex beam parameter
  zr=rayleighRange(w0,M2,lambda,1);
  q1=i*zr; % input q;

  % Compute the first propagation
  D1=linspace(0,d1,1000);
  q2=q1+D1;
  % Transform the beam through the first lens
  q3=opticalTransform(q2(end),lens1);

  % Compute the second propagation 
  D2=linspace(0,d2,1000);
  q4=q3+D2;
  % Transform the beam through the second lens
  q5=opticalTransform(q4(end),lens2);

  % Compute the third propagation
  D3=linspace(0,d3,1000);
  q6=q5+D3;
  % Transform the beam through the third lens
  q7=opticalTransform(q6(end),lens3);

  % Compute the last propagation
  D4=linspace(0,d4,1000);
  q8=q7+D4;

  % Compute ideal waist radii along all the distances
  w=[beamRadius(q2,lambda) beamRadius(q4,lambda) ...
               beamRadius(q6,lambda) beamRadius(q8,lambda)];

  % Compute the output divergence using the waists
  w1=waistRadius(q3,lambda,1);
  thetaMid=BPP/w1;
  %
  w2=waistRadius(q5,lambda,1);
  thetaMid2=BPP/w2;
  %
  w3=waistRadius(q7,lambda,1);
  thetaOut=BPP/w3;

  x=[-D1(end:-1:1), D2, d2+D3, d2+d3+D4];
  if (not(suppress))
   % Plot result
    x=x/10;  % convert mm -> cm
    FL1=f1/10; FL2=f2/10; FL3=f3/10; d1c=d1/10; d2c=d2/10; d3c=d3/10; % convert mm -> cm
    mx=max(w);
    figure
    h=plot(x,w,"b",x,-w,"b", ...       % Plot waist radius
           -FL1,-mx,"r*",FL1,-mx,"r*", ...   % Plot focal points of each lens
           d2c-FL2,-3*mx/4,"g*",d2c+FL2,-3*mx/4,"g*", ...
           d3c+d2c-FL3,-mx/2,"m*",d3c+d2c+FL3,-mx/2,"m*");
    title("Three lens beam expander")
                             % Add divergence information to the graph
    text(-d1c,-mx/2,sprintf("%s:\n%f rad\n%s:\n%f mm","ThetaIn",thetaIn,"wIn",w0))
    text(d2c/2,mx,sprintf("%s:\n%f rad\n%s:\n%f mm","ThetaMid",thetaMid,"wMid",w1))
    text(d2c+d3c/2,0,sprintf("%s:\n%f rad\n%s:\n%f mm","ThetaMid2",thetaMid2,"wMid2",w2))
    text(d2c+2.5*d3c,0,sprintf("%s:\n%f rad\n%s:\n%f mm","ThetaOut",thetaOut,"wOut",w3))
    set(gca,"FontSize",14)
    set(h,"LineWidth",2)
    xlabel("Position on the Optical Axis [cm]"),ylabel("Waist Radius [mm]")
    x=x*10; %Convert back to mm
  end

  % Return additional data
  data.wIn=w0;
  data.wMid=w1;
  data.wMid2=w2;
  data.wOut=w3;
  data.thetaIn=thetaIn;
  data.thetaMid=thetaMid;
  data.thetaMid2=thetaMid2;
  data.thetaOut=thetaOut;
end
