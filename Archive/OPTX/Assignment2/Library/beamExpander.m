% Compute and plot the waist profile (from an initial waist w0) of a three lens beam expander
% Convention:      (Note: wavelength, waist radius, distances and focal lengths are in mm)
% q1 ---d1--> q2 (Lens1) q3 ---d2--> q4 (Lens2) q5 ---d3--> q6 (Lens3) q7 ---2*d3--> q8
% The returned data are in mm
% The optional paremeter suppresses the plot when its value is true
function [w,x,data]=beamExpander(lambda0,w0,d1,d2,d3,f1,f2,f3,suppress)
  if nargin<9, suppress=false; end
  [w,x,data]=plot3LensBeamExpander(1,lambda0,w0,d1,d2,d3,2*d3,f1,f2,f3,1,suppress);
end
