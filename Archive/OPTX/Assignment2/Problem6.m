close all; clear all; clc; addpath("Library"); format short
% Fixed data in mm
lambda0=0.0006328;
d1=100; d2=20; w0=0.5;
f1=10; f2=-10; f3=100; % invert the focal lenght

% First idea
d3=95; % reduce this distance until the divergence is minimized
[w,x]=beamExpander(lambda0,w0,d1,d2,d3,f1,f2,f3);% mag=20

% Final idea
f1=200; % increasing f1 increase the tunability of the magnification 
f2=-10; d3=489; f3=500; % Galilean beam expander

d2=160; % configuration for magnificatiox 10x
beamExpander(lambda0,w0,d1,d2,d3,f1,f2,f3);

d2=115; % configuration for magnification 20x
beamExpander(lambda0,w0,d1,d2,d3,f1,f2,f3);

d2=32; % configuration for magnification 40x
beamExpander(lambda0,w0,d1,d2,d3,f1,f2,f3);
