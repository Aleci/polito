close all; clear all; clc; addpath("Library"); format short
% Notation conversion:
% myScript |  paper
%----------+--------
% lambda0  | lambda
%  -d1     |   s1
%   d2     |   d1
%   d3     |   d2
%   d4     |   s3'
%   w0     |   w01
%   f1     |   f1'
%   f2     |   f2'
%   f3     |   f3'

% Fixed data in mm
lambda0=0.0006328;
d1=100; w0=0.5;
f1=-10; f2=10; f3=100;

% Row 1 OK
%d2=10; d3=120.006; [w,x]=beamExpander(lambda0,w0,d1,d2,d3,f1,f2,f3); mag=10;
% Row 2 OK
d2=20; d3=115.002; [w,x]=beamExpander(lambda0,w0,d1,d2,d3,f1,f2,f3); mag=20;
% Row 3 OK
%d2=30; d3=113.334; [w,x]=beamExpander(lambda0,w0,d1,d2,d3,f1,f2,f3); mag=30;
% Row 4 OK
%d2=40; d3=112.5; [w,x]=beamExpander(lambda0,w0,d1,d2,d3,f1,f2,f3); mag=40;
% Row 5 OK
%d2=50; d3=112; [w,x]=beamExpander(lambda0,w0,d1,d2,d3,f1,f2,f3); mag=50;

% Search the beam waist of the output beam over a longer distance 
[w,x]=plot3LensBeamExpander(1,lambda0,w0,d1,d2,d3,100*d3,f1,f2,f3,1); % 100 is good for mag=20
% Find the index where the third lens is located 
indx=find(x >= d2+d3); indx=indx(1);
% Compute the minimum waist from the last lens and the end of the plot
[Wmin Wind]=min(w(indx:end));
% Plot results
xMagAbs=x(Wind+indx-1); %absolute position of the waist
xMagLens=xMagAbs-d2-d3  %position of the waist wrt the last lens
wMag=w(indx+Wind-1), hold on, plot(xMagAbs/10,wMag,"c*") % waist radius
wLens=w(indx) %radius at the lens

% Compute the actual magnification behaviour in linear operation
d3=113.334; % fix d3 to the central operating range and vary only d2
for inDist=[10:50]
  [w,x,data]=beamExpander(lambda0,w0,d1,inDist,d3,f1,f2,f3,true);
  outMag(inDist-9)=data.wOut;
end
inDist=[10:50];
figure, plot(inDist,inDist*w0), hold on; % Plot desired output
avgSlope1=inDist'\outMag'; % compute the best fit in the ls sense of the slope
plot(inDist,outMag)
%
d3=112; % fix d3 to the end of the operating range and vary only d2
for inDist=[10:50]
  [w,x,data]=beamExpander(lambda0,w0,d1,inDist,d3,f1,f2,f3,true);
  outMag(inDist-9)=data.wOut;
end
inDist=[10:50];
avgSlope2=inDist'\outMag'; % compute the best fit in the ls sense of the slope
plot(inDist,outMag)
title("Relation between the distance of the first two lenses and the magnification")
str1=sprintf("Operating point: 30x\n Actual average slope: %f",avgSlope1);
str2=sprintf("Operating point: 50x\n Actual average slope: %f",avgSlope2);
legend("Reference output\nReference slope: w0=0.5",str1,str2,"location","northwest");
