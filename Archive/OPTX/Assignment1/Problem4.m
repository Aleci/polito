clear all; close all; clc; addpath("Library")

% Working range
am15=load("AM15.m","-ascii");
workingRange=am15(:,1)*1e-9;
powerDensity=am15(:,2);

% Paper refractive index settings 
nSi=3.4;
nSiO2=1.5;
nTiO2=2.5;
nAir=1;

% SL-ARC
d=6.4071e-8;
G=mlayer([nAir nTiO2 nSi],[d],workingRange)';
Ref1=abs(G).^2;
Reff1=effRef(Ref1,powerDensity);

% SL-ARC Paper
d=81.1e-9;
G=mlayer([nAir nSiO2 nSi],[d],workingRange)';
Ref2=abs(G).^2;
Reff2=effRef(Ref2,powerDensity);

% DL-ARC
dT=2.0300e-8;
dS=3.0944e-8;
G=mlayer([nAir nTiO2 nSiO2 nSi],[dT dS],workingRange)';
Ref3=abs(G).^2;
Reff3=effRef(Ref3,powerDensity);

% DL-ARC Paper
dT=18e-9;
dS=40.7e-9;
G=mlayer([nAir nTiO2 nSiO2 nSi],[dT dS],workingRange)';
Ref4=abs(G).^2;
Reff4=effRef(Ref4,powerDensity);

% Results
nicePlot(workingRange*1e9,{Ref1,Ref2,Ref3,Ref4},"Design comparison",...
     "Wavelength [nm]","Reflectivity")
legend(sprintf("SL:  Reff=%.3f",Reff1),...
       sprintf("SL*: Reff=%.3f",Reff2),...
       sprintf("DL:  Reff=%.3f",Reff3),...
       sprintf("DL*: Reff=%.3f",Reff4),"location","north")
