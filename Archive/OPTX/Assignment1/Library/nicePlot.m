function nicePlot(t,f,myTitle,xLabel,yLabel)
  h=[]; hold on
  for ii=1:length(f)
    h(ii) = plot(t,f{ii});
  end
  hold off
  set(gca,"FontSize",14)
  arrayfun(@(x) set(x,"LineWidth",2),h)
  title(myTitle)
  xlabel(xLabel,"FontSize",16)
  ylabel(yLabel,"FontSize",16)
  grid
end
