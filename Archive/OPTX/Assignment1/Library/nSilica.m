% Sellmeier's Equation for Silica
function n=nSilica(lambda)
  Aj=[0.6962,0.4079,0.8975];
  Lj=[0.0684,0.1162,9.8962]*1e-6;
  n=sellmeierEq(lambda,Aj,Lj);
end
