function Gamma = mlayer(n,L,lambda,theta,pol)
%Gamma = mlayer(n,L,lambda,theta,pol)
%
%          na | n1 | n2 | ... | nM | nb
% left medium | L1 | L2 | ... | LM | right medium 
%   interface 1    2    3     M   M+1
%
% Usage: [Gamma] = m1ayer(n,L,lambda,theta,pol)
%        [Gamma] = m1ayer(n,L,lambda,theta)       (equivalent to pol='te')
%        [Gamma] = m1ayer(n,L,lambda)             (equivalent to theta=0)
%
% n      = vector of refractive indices [na,n1,n2,...,nM,nb]
% L      = vector of thickness of layers [l1,...,lM], in the same units of lambda
% lambda = vector of free-space wavelengths at which to evaluate input impedance
% theta  = incidence angle from leftmost medium (in degrees)
% pol    = 'tm' or 'te', for parallel/perpendicular polarizations
%
% Gamma  = reflection coefficient at interface-1 into leftmost medium evaluated at lambda
%
% Example: lambda/2 glass layer in air, normal incidence
% lam = linspace(400,700,101)/550;
% [g] = mlayer([1 1.5 1], 0.5,lam);
%
% (C) Guido Perrone - Politecnico di Torino, 2014-20
% Note: this may not be the most efficient coding, but it follows the 
% approach outlined during the lectures. 


if nargin<=4, pol='te'; end  % sets the default polarization 
if nargin==3, theta=0; end   % sets the default angle

theta = theta * pi/180; % converts the angles from deg to rad


M = length(n)-2; % number of slabs
if M==0, L = []; end   % single interface, no slabs

Z0 = 120*pi;           % this is Z0


nlam = length(lambda);
for ii = 1:nlam,
    k0 = 2*pi/lambda(ii);         % this is k0
    
    % here we compute kz in each layer in which we have to 
    % propagate the refl coeff (not first and last) and we 
    % verify that kz = beta -j alpha
    kz = zeros(M,1);       % initialize kz
    kz = k0*sqrt(n(2:M+1).^2-n(1).^2*sin(theta).^2);
    qq = find(imag(kz)>0);
    kz(qq) = conj(kz(qq));

    % now we compute the characteristic impedances of each layer
    zinf = zeros(M+2,1);       % initialize zinf
    if pol=='te' | pol=='TE',
        zinf = Z0 ./ sqrt(n.^2-n(1).^2*sin(theta).^2);
        qq = find(imag(zinf)<0);
        zinf(qq) = conj(zinf(qq));
    else
        zinf = Z0 ./(n.^2) .* sqrt(n.^2-n(1).^2*sin(theta).^2);
        qq = find(imag(zinf)>0);
        zinf(qq) = conj(zinf(qq));                            
    end 


    % computation of Fresnel reflection coeff
    % note there is a shift of the vector index wiht respect to the 
    % lecture's note since Matlab vectors do not start from 0
    rho = zeros(M+1,1);
    rho(1:M+1) = (zinf(2:M+2)-zinf(1:M+1))./(zinf(2:M+2)+zinf(1:M+1));

    g_old = rho(M+1);
    for jj=M:-1:1
        g_new = (rho(jj)+g_old*exp(-2j*kz(jj)*L(jj)))/...
        (1+rho(jj)*g_old*exp(-2j*kz(jj)*L(jj)));
        g_old = g_new;
    end
    Gamma(ii) = g_new;
end



end

