% Equivalent characteristic impedance for TM waves
function Ztm=pZinf(n, NA)
  c=300e6;
  epsilon0= 8.854e-12;
  epsilonr=n.^2;
  epsilon=epsilon0*epsilonr;
  KzNORM=sqrt(n.^2 - NA.^2); % NA=Kx/K0 => KzNORM=Kz/K0
% Kz should have only negative imaginary sign because the convention of the exponential decay
  if(imag(KzNORM)>0)
    KzNORM=conj(KzNORM);
  end
  % ZTM=Kz/(omega*epsilon)=Kz/(2*pi*f*epsilon)=Kz/(2*pi*(c/lambda)*epsilon)
  % but KzNORM=Kz/K0 => independent from the frequency
  Ztm=KzNORM./(c*epsilon);
% Now Kz is at the numerator so Ztm should have the same imaginary sign
  if(imag(Ztm)>0)
    Ztm=conj(Ztm);
  end
end
