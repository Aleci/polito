% Compute the Sellmeier's Equation for the given parameters
% Aj and Lj should have same length
function n=sellmeierEq(lambda,Aj,Lj)
  tmp=0;
  for k=1:length(Aj);
    tmp=tmp+Aj(k)*lambda^2 / (lambda^2 - Lj(k)^2);
  end
  n=sqrt(tmp+1);
end
