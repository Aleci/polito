% Compute the effective reflectivity
% Rv and Am_v are the vectors of the reflectivity and power density at the given wavelength
function er=effRef(Rv,Am_v)
  er=Rv' * Am_v / sum(Am_v);
end
