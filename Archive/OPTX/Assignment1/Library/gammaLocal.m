% Compute the local reflection coefficient at the interface
function g=gammaLocal(Zinf,ZinfLoad)
  g=(ZinfLoad-Zinf)./(ZinfLoad+Zinf);
end
