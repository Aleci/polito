clear all; close all; clc; addpath("Library")

lambda0=500e-9;
nCell=nSilicon(lambda0);

n2=nTitania(lambda0);% Titania
n1=nSilica(lambda0);% Silica
nair=1;

% Naming convention:
% -+A--+B--+C--|
%  Za  Z2  Z1  Zg
% --.---.---.--|

NA=Kx=0; % Normal incidence 
Zg=pZinf(nCell,NA);
Z1=pZinf(n1,NA);
Z2=pZinf(n2,NA);
Za=pZinf(nair,NA);

gammaCplus=gammaLocal(Z1,Zg);
% variable propagation in layer 1
lambdaM1=lambda0/n1;
d1=linspace(0,0.5,1000)*lambdaM1; %vary d1 between 0 and lambda/2 IN the material
Kz=k=2*pi/lambdaM1;
ZBplus=impedancePropagation(gammaCplus,d1,Kz,Z1);
gammaBplus=gammaLocal(Z2,ZBplus);
% variable propagation in layer 2 for each d1
lambdaM2=lambda0/n2;
Kz=k=2*pi/lambdaM2;
for k=[1:1000]
  d2=(k-1)/1000*lambdaM2/2; %vary d2 between 0 and lambda/2 IN the material
  ZAplus(k,:)=impedancePropagation(gammaBplus,d2,Kz,Z2);
end

% ZAplus structure
% d1\d2 ->
% v | ZA(d1(1),d2(1)  ZA(d1(1),d2(2) |
%   | ZA(d1(2),d2(1)  ZA(d1(2),d2(2) |

% minimize the impedance mismatch (min sum of the abs(RE)+j*abs(IM))
% find all the minimuma within the same column with the index to recover the optimal d2
[mincol,indx2]=min(abs(real(ZAplus))-abs(real(Za)) + j*(abs(imag(ZAplus))-abs(imag(Za))));
% find the global minimum between the minima of the columns with the index to recover the optimal d1
[minMisMatch,indx1]=min(mincol);

% find the physical thickness from the indexes
% NOTE: the distances had been generated using the for loop counter
thickness1=(indx1-1)/1000*lambdaM1/2;
thickness2=(indx2(indx1)-1)/1000*lambdaM2/2;

% Check results
workingRange=linspace(400,1100,1000)*1e-9;
GammaReal=mlayer([nair nTitania(lambda0) nSilica(lambda0) nSilicon(lambda0)]...
                    ,[thickness2 thickness1],workingRange);
RefReal=abs(GammaReal).^2;

figure
nicePlot(workingRange*1e9,{RefReal},...
         "Reflectivity in the working range of the dual layer design",...
         "Wavelength [nm]","Reflectivity")
