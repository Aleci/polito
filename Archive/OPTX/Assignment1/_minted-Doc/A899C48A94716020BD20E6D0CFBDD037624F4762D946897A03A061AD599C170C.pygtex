\begin{Verbatim}[commandchars=\\\{\}]
\PYG{n}{clear} \PYG{n+nb}{all}\PYG{p}{;} \PYG{n+nb}{close} \PYG{n+nb}{all}\PYG{p}{;} \PYG{n+nb}{clc}\PYG{p}{;} \PYG{n+nb}{addpath}\PYG{p}{(}\PYG{l+s}{\PYGZdq{}Library\PYGZdq{}}\PYG{p}{)}

\PYG{n}{lambda0}\PYG{p}{=}\PYG{l+m+mf}{500e\PYGZhy{}9}\PYG{p}{;}
\PYG{n}{nCell}\PYG{p}{=}\PYG{n}{nSilicon}\PYG{p}{(}\PYG{n}{lambda0}\PYG{p}{);}

\PYG{n}{n2}\PYG{p}{=}\PYG{n}{nTitania}\PYG{p}{(}\PYG{n}{lambda0}\PYG{p}{);}\PYG{c}{\PYGZpc{} Titania}
\PYG{n}{n1}\PYG{p}{=}\PYG{n}{nSilica}\PYG{p}{(}\PYG{n}{lambda0}\PYG{p}{);}\PYG{c}{\PYGZpc{} Silica}
\PYG{n}{nair}\PYG{p}{=}\PYG{l+m+mi}{1}\PYG{p}{;}

\PYG{c}{\PYGZpc{} Naming convention:}
\PYG{c}{\PYGZpc{} \PYGZhy{}+A\PYGZhy{}\PYGZhy{}+B\PYGZhy{}\PYGZhy{}+C\PYGZhy{}\PYGZhy{}|}
\PYG{c}{\PYGZpc{}  Za  Z2  Z1  Zg}
\PYG{c}{\PYGZpc{} \PYGZhy{}\PYGZhy{}.\PYGZhy{}\PYGZhy{}\PYGZhy{}.\PYGZhy{}\PYGZhy{}\PYGZhy{}.\PYGZhy{}\PYGZhy{}|}

\PYG{n+no}{NA}\PYG{p}{=}\PYG{n}{Kx}\PYG{p}{=}\PYG{l+m+mi}{0}\PYG{p}{;} \PYG{c}{\PYGZpc{} Normal incidence}
\PYG{n}{Zg}\PYG{p}{=}\PYG{n}{pZinf}\PYG{p}{(}\PYG{n}{nCell}\PYG{p}{,}\PYG{n+no}{NA}\PYG{p}{);}
\PYG{n}{Z1}\PYG{p}{=}\PYG{n}{pZinf}\PYG{p}{(}\PYG{n}{n1}\PYG{p}{,}\PYG{n+no}{NA}\PYG{p}{);}
\PYG{n}{Z2}\PYG{p}{=}\PYG{n}{pZinf}\PYG{p}{(}\PYG{n}{n2}\PYG{p}{,}\PYG{n+no}{NA}\PYG{p}{);}
\PYG{n}{Za}\PYG{p}{=}\PYG{n}{pZinf}\PYG{p}{(}\PYG{n}{nair}\PYG{p}{,}\PYG{n+no}{NA}\PYG{p}{);}

\PYG{n}{gammaCplus}\PYG{p}{=}\PYG{n}{gammaLocal}\PYG{p}{(}\PYG{n}{Z1}\PYG{p}{,}\PYG{n}{Zg}\PYG{p}{);}
\PYG{c}{\PYGZpc{} variable propagation in layer 1}
\PYG{n}{lambdaM1}\PYG{p}{=}\PYG{n}{lambda0}\PYG{o}{/}\PYG{n}{n1}\PYG{p}{;}
\PYG{n}{d1}\PYG{p}{=}\PYG{n+nb}{linspace}\PYG{p}{(}\PYG{l+m+mi}{0}\PYG{p}{,}\PYG{l+m+mf}{0.5}\PYG{p}{,}\PYG{l+m+mi}{1000}\PYG{p}{)}\PYG{o}{*}\PYG{n}{lambdaM1}\PYG{p}{;} \PYG{c}{\PYGZpc{}vary d1 between 0 and lambda/2 IN the material}
\PYG{n}{Kz}\PYG{p}{=}\PYG{n}{k}\PYG{p}{=}\PYG{l+m+mi}{2}\PYG{o}{*}\PYG{n+nb}{pi}\PYG{o}{/}\PYG{n}{lambdaM1}\PYG{p}{;}
\PYG{n}{ZBplus}\PYG{p}{=}\PYG{n}{impedancePropagation}\PYG{p}{(}\PYG{n}{gammaCplus}\PYG{p}{,}\PYG{n}{d1}\PYG{p}{,}\PYG{n}{Kz}\PYG{p}{,}\PYG{n}{Z1}\PYG{p}{);}
\PYG{n}{gammaBplus}\PYG{p}{=}\PYG{n}{gammaLocal}\PYG{p}{(}\PYG{n}{Z2}\PYG{p}{,}\PYG{n}{ZBplus}\PYG{p}{);}
\PYG{c}{\PYGZpc{} variable propagation in layer 2 for each d1}
\PYG{n}{lambdaM2}\PYG{p}{=}\PYG{n}{lambda0}\PYG{o}{/}\PYG{n}{n2}\PYG{p}{;}
\PYG{n}{Kz}\PYG{p}{=}\PYG{n}{k}\PYG{p}{=}\PYG{l+m+mi}{2}\PYG{o}{*}\PYG{n+nb}{pi}\PYG{o}{/}\PYG{n}{lambdaM2}\PYG{p}{;}
\PYG{k}{for} \PYG{n}{k}\PYG{p}{=[}\PYG{l+m+mi}{1}\PYG{p}{:}\PYG{l+m+mi}{1000}\PYG{p}{]}
  \PYG{n}{d2}\PYG{p}{=(}\PYG{n}{k}\PYG{o}{\PYGZhy{}}\PYG{l+m+mi}{1}\PYG{p}{)}\PYG{o}{/}\PYG{l+m+mi}{1000}\PYG{o}{*}\PYG{n}{lambdaM2}\PYG{o}{/}\PYG{l+m+mi}{2}\PYG{p}{;} \PYG{c}{\PYGZpc{}vary d2 between 0 and lambda/2 IN the material}
  \PYG{n}{ZAplus}\PYG{p}{(}\PYG{n}{k}\PYG{p}{,:)=}\PYG{n}{impedancePropagation}\PYG{p}{(}\PYG{n}{gammaBplus}\PYG{p}{,}\PYG{n}{d2}\PYG{p}{,}\PYG{n}{Kz}\PYG{p}{,}\PYG{n}{Z2}\PYG{p}{);}
\PYG{k}{end}

\PYG{c}{\PYGZpc{} ZAplus structure}
\PYG{c}{\PYGZpc{} d1\PYGZbs{}d2 \PYGZhy{}\PYGZgt{}}
\PYG{c}{\PYGZpc{} v | ZA(d1(1),d2(1)  ZA(d1(1),d2(2) |}
\PYG{c}{\PYGZpc{}   | ZA(d1(2),d2(1)  ZA(d1(2),d2(2) |}

\PYG{c}{\PYGZpc{} minimize the impedance mismatch (min sum of the abs(RE)+j*abs(IM))}
\PYG{c}{\PYGZpc{} find all the minimuma within the same column with the index to recover the optimal d2}
\PYG{p}{[}\PYG{n}{mincol}\PYG{p}{,}\PYG{n}{indx2}\PYG{p}{]=}\PYG{n+nb}{min}\PYG{p}{(}\PYG{n+nb}{abs}\PYG{p}{(}\PYG{n+nb}{real}\PYG{p}{(}\PYG{n}{ZAplus}\PYG{p}{))}\PYG{o}{\PYGZhy{}}\PYG{n+nb}{abs}\PYG{p}{(}\PYG{n+nb}{real}\PYG{p}{(}\PYG{n}{Za}\PYG{p}{))} \PYG{o}{+} \PYG{n}{j}\PYG{o}{*}\PYG{p}{(}\PYG{n+nb}{abs}\PYG{p}{(}\PYG{n+nb}{imag}\PYG{p}{(}\PYG{n}{ZAplus}\PYG{p}{))}\PYG{o}{\PYGZhy{}}\PYG{n+nb}{abs}\PYG{p}{(}\PYG{n+nb}{imag}\PYG{p}{(}\PYG{n}{Za}\PYG{p}{))));}
\PYG{c}{\PYGZpc{} find the global minimum between the minima of the columns with the index to recover the optimal d1}
\PYG{p}{[}\PYG{n}{minMisMatch}\PYG{p}{,}\PYG{n}{indx1}\PYG{p}{]=}\PYG{n+nb}{min}\PYG{p}{(}\PYG{n}{mincol}\PYG{p}{);}

\PYG{c}{\PYGZpc{} find the physical thickness from the indexes}
\PYG{c}{\PYGZpc{} NOTE: the distances had been generated using the for loop counter}
\PYG{n}{thickness1}\PYG{p}{=(}\PYG{n}{indx1}\PYG{o}{\PYGZhy{}}\PYG{l+m+mi}{1}\PYG{p}{)}\PYG{o}{/}\PYG{l+m+mi}{1000}\PYG{o}{*}\PYG{n}{lambdaM1}\PYG{o}{/}\PYG{l+m+mi}{2}\PYG{p}{;}
\PYG{n}{thickness2}\PYG{p}{=(}\PYG{n}{indx2}\PYG{p}{(}\PYG{n}{indx1}\PYG{p}{)}\PYG{o}{\PYGZhy{}}\PYG{l+m+mi}{1}\PYG{p}{)}\PYG{o}{/}\PYG{l+m+mi}{1000}\PYG{o}{*}\PYG{n}{lambdaM2}\PYG{o}{/}\PYG{l+m+mi}{2}\PYG{p}{;}

\PYG{c}{\PYGZpc{} Check results}
\PYG{n}{workingRange}\PYG{p}{=}\PYG{n+nb}{linspace}\PYG{p}{(}\PYG{l+m+mi}{400}\PYG{p}{,}\PYG{l+m+mi}{1100}\PYG{p}{,}\PYG{l+m+mi}{1000}\PYG{p}{)}\PYG{o}{*}\PYG{l+m+mf}{1e\PYGZhy{}9}\PYG{p}{;}
\PYG{n}{GammaReal}\PYG{p}{=}\PYG{n}{mlayer}\PYG{p}{([}\PYG{n}{nair} \PYG{n}{nTitania}\PYG{p}{(}\PYG{n}{lambda0}\PYG{p}{)} \PYG{n}{nSilica}\PYG{p}{(}\PYG{n}{lambda0}\PYG{p}{)} \PYG{n}{nSilicon}\PYG{p}{(}\PYG{n}{lambda0}\PYG{p}{)]...}
		    \PYG{p}{,[}\PYG{n}{thickness2} \PYG{n}{thickness1}\PYG{p}{],}\PYG{n}{workingRange}\PYG{p}{);}
\PYG{n}{RefReal}\PYG{p}{=}\PYG{n+nb}{abs}\PYG{p}{(}\PYG{n}{GammaReal}\PYG{p}{)}\PYG{o}{.\PYGZca{}}\PYG{l+m+mi}{2}\PYG{p}{;}

\PYG{n+nb}{figure}
\PYG{n}{nicePlot}\PYG{p}{(}\PYG{n}{workingRange}\PYG{o}{*}\PYG{l+m+mf}{1e9}\PYG{p}{,\PYGZob{}}\PYG{n}{RefReal}\PYG{p}{\PYGZcb{},...}
	 \PYG{l+s}{\PYGZdq{}Reflectivity in the working range of the dual layer design\PYGZdq{}}\PYG{p}{,...}
	 \PYG{l+s}{\PYGZdq{}Wavelength [nm]\PYGZdq{}}\PYG{p}{,}\PYG{l+s}{\PYGZdq{}Reflectivity\PYGZdq{}}\PYG{p}{)}
\end{Verbatim}
