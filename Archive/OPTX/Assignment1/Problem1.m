close all; clear all; clc; addpath("Library")
% Variables initialization
lambda0=linspace(400,1100,1000)*1e-9;
l0=lambda0*1e9;% plotting purposes
k0=2*pi./lambda0;
% Useless passage: compute the refractive index at each wavelength
nsil=arrayfun(@(x) nSilicon(x),lambda0);

Kx=0; % The normal incidence leads to a null Kx.
ZtmAir=pZinf(1, Kx);
% basically useless computation (nSilicon is constant, lambda cancels out)
ZtmSilicon=pZinf(nsil, Kx); 
gamma=gammaLocal(ZtmAir,ZtmSilicon); % silicon is the load
ref=abs(gamma).^2;
%
subplot(2,1,1),nicePlot(l0,{ref},...
                        "Reflectivity for TEM",...
                        "Wavelength [nm]","Reflectivity")

Kx=k0*1*sin(pi/6);
% NA is the constant defined by the Snell's Law
% it is the true parameter from which the impedances
% depends on and it is frequency independent in case
% of no dispersion
NA=Kx/k0;
% TM
ZtmAir=pZinf(1, NA);
% again useless unless non zero dispersion
ZtmSilicon=pZinf(nsil, NA); 
gammaTM=gammaLocal(ZtmAir,ZtmSilicon); % silicon is the load
refTM=abs(gammaTM).^2;
% TE
ZteAir=sZinf(1, NA,1);
% again useless unless non zero dispersion
ZteSilicon=sZinf(nsil, NA,1); 
gammaTE=gammaLocal(ZteAir,ZteSilicon); % silicon is the load
refTE=abs(gammaTE).^2;
%
subplot(2,1,2),nicePlot(l0,{refTE,refTM},...
         "Reflectivity for TE,TM with AOI 30 deg",...
         "Wavelength [nm]","Reflectivity")
legend("TE","TM","location","east")
