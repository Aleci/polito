clear all; close all; clc; addpath("Library")

lambda0=500e-9;
nCell=nSilicon(lambda0);

n2=nTitania(lambda0);% Titania
n1=nSilica(lambda0);% Silica
d2design=2.0300e-8;
d1design=3.0944e-8;
nair=1;

% Naming convention:
% -+A--+B--+C--|
%  Za  Z2  Z1  Zg
% --.---.---.--|

NA=Kx=0; % Normal incidence 
Zg=pZinf(nCell,NA);
Z1=pZinf(n1,NA);
Z2=pZinf(n2,NA);
Za=pZinf(nair,NA);

gammaCplus=gammaLocal(Z1,Zg);
% variable propagation in layer 1
lambdaM1=lambda0/n1;
d1=linspace(0.98,1.02,1000)*d1design; % d1design +- 2%
Kz=k=2*pi/lambdaM1;
ZBplus=impedancePropagation(gammaCplus,d1,Kz,Z1);
gammaBplus=gammaLocal(Z2,ZBplus);
% variable propagation in layer 2 for each d1
lambdaM2=lambda0/n2;
Kz=k=2*pi/lambdaM2;
for k=[1:1000]
  d2=(0.98 + ((k-1)/1000)*0.04) *d2design; % d2design +- 2%
  ZAplus(k,:)=impedancePropagation(gammaBplus,d2,Kz,Z2);
end

% ZAplus structure
% d1\d2 ->
% v | ZA(d1(1),d2(1)  ZA(d1(1),d2(2) |
%   | ZA(d1(2),d2(1)  ZA(d1(2),d2(2) |

% maximize the impedance mismatch (max sum of the abs(RE)+j*abs(IM))
% find all the maxima within the same column with the index to recover the worst d2
[maxcol,indx2]=min(abs(real(ZAplus))-abs(real(Za)) + j*(abs(imag(ZAplus))-abs(imag(Za))));
% find the global maximum between the maxima of the columns with the index to recover the worst d1
[maxMisMatch,indx1]=max(maxcol);

% find the physical thickness from the indexes
% NOTE: the distances had been generated using the for loop counter
thickness1=d1(indx1);
thickness2=(0.98 + ((indx2(indx1)-1)/1000)*0.04)*d2design;

% Compute reflectivity
workingRange=linspace(400,1100,1000)*1e-9;
GammaIdeal=mlayer([nair nTitania(lambda0) nSilica(lambda0) nSilicon(lambda0)]...
                    ,[d2design d1design],workingRange);
RefIdeal=abs(GammaIdeal).^2;
%
GammaReal=mlayer([nair nTitania(lambda0) nSilica(lambda0) nSilicon(lambda0)]...
                    ,[thickness2 thickness1],workingRange);
RefReal=abs(GammaReal).^2;

RefDiff=min(RefReal)/min(RefIdeal)

figure
nicePlot(workingRange*1e9,{RefIdeal,RefReal},...
         "Change in reflectivity due to tolerances",...
         "Wavelength [nm]","Reflectivity")
legend("Design","Worst case")
