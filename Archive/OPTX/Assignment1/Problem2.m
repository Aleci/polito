clear all; close all; clc; addpath("Library")

lambda0=500e-9;
nair=1;
lambdaAir=lambda0/nair;
nCell=nSilicon(lambda0);
lambdaCell=lambda0/nCell;

% we are interested in normal incidence
NA=Kx=0;
% 1 interface air/arc
% 2 interface arc/Photovoltaic Cell

% Naming convention:
% -+A--+B--|
%  Za  Zc Zg
% --.---.--|

% we want the impedance in A equal to the characteristic impedance of the air
Zair=pZinf(nair,NA);

% we use the glass as a load
Zload=pZinf(nCell,NA);

% we compute the arc impedance function of the refractive index in order to find the best match
nc=linspace(1,6,1000);
Zarc=arrayfun(@(x) pZinf(x,NA),nc); 
% Start from load
gammaBplus=gammaLocal(Zarc,Zload);
lambdaARC=lambda0./nc;
k=2*pi./lambdaARC; % propagation in the arc

Kz=k; %normal incidence
quarterWaveDistance=lambdaARC/4; % We have to travel a quarter wave in the material

ZA=impedancePropagation(gammaBplus,quarterWaveDistance,Kz,Zarc);
reflectivityQW=abs(gammaLocal(Zair,ZA)).^2;

% find the minimum reflectivity and the related refractive index
[minRef,indx]=min(reflectivityQW);
ncIdeal=nc(indx)
QWOT=quarterWaveDistance(indx);
ImpedanceMismatch=Zair-ZA(indx) % Check correct working of the script

nicePlot(nc,{reflectivityQW},"Reflectivity function of the ARC refractive index",...
         "Coating refractive index", "Reflectivity")
hold on, plot(ncIdeal,minRef,"*r"), hold off

workingRange=linspace(400,1100,1000)*1e-9; 

% Mantaining the assumption on zero dispersion
GammaIdeal=mlayer([nair ncIdeal nSilicon(lambda0)],QWOT,workingRange);
RefIdeal=abs(GammaIdeal).^2;
GammaSilica=mlayer([nair nSilica(lambda0) nSilicon(lambda0)],QWOT,workingRange);
RefSilica=abs(GammaSilica).^2;
GammaTitania=mlayer([nair nTitania(lambda0) nSilicon(lambda0)],QWOT,workingRange);
RefTitania=abs(GammaTitania).^2;
% Removing the assumption on zero dispersion 
GammaSilica2=arrayfun(@(l) mlayer([nair nSilica(l) nSilicon(l)],QWOT,l),workingRange);
RefSilica2=abs(GammaSilica2).^2;

figure
nicePlot(workingRange*1e9,{RefIdeal,RefSilica,RefSilica2,RefTitania},...
         "Reflectivity in the working range per material",...
         "Wavelength [nm]","Reflectivity")
legend("Ideal","Silica","Silica*","Titania")
