clear all; close all; clc

lambda0=1080e-9;
nair=1;
lambdaAir=lambda0/nair;
nglass=nSilica(lambda0);
lambdaGlass=lambda0/nglass;

% we have normal incidence so Zte=Ztm => Kx=0
Kx=0;
% 1 interface air/arc
% 2 interface arc/glass

% Naming convention:
% -+A--+B--|
%  Za  Zc Zg
% --.---.--|

% we want the impedance in A equal to the characteristic impedance of the air
Zair=pZinf(nair,Kx,lambda0);

% we use the glass as a load
Zload=pZinf(nglass,Kx,lambda0);

% we compute the arc impedance function of the refractive index in order to find the best match
nc=linspace(1,4,1000);
Zarc=arrayfun(@(x) pZinf(x,Kx,lambda0),nc); 
% Start from load
gammaBplus=gamma(Zarc,Zload);
lambdaARC=lambda0./nc;
k=2*pi./lambdaARC; % propagation in the arc

Kz=k; %normal incidence
quarterWaveDistance=lambdaARC/4; % We have to travel a quarter wave in the material

ZA=impedancePropagation(gammaBplus,quarterWaveDistance,Kz,Zarc);
reflectivityQW=abs(gamma(Zair,ZA)).^2;

% Best available choice
nCryolite=1.35;
QWphisicalThickness=(lambda0/nCryolite)/4

% quarter wave plot
subplot(2,1,1)
h = plot(nc,ZA);
set(gca,"FontSize",14)
set(h,"LineWidth",2)
title("Adapted Load, function of the quarter wave ARC refractive index")
xlabel("Coating Refractive Index","FontSize",16)
ylabel("Impedance [Ohm]","FontSize",16)
line(nc,Zair*ones(size(nc))), hold on
plot(nCryolite,299.4,"*k")
legend("Input Impedance","Air Impedance","Cryolite")
grid
subplot(2,1,2)
h = plot(nc,reflectivityQW);
set(gca,"FontSize",14)
set(h,"LineWidth",2)
title("Reflectivity, function of the quarter wave ARC refractive index")
xlabel("Coating Refractive Index","FontSize",16)
ylabel("Reflectivity","FontSize",16),grid
hold on, plot(nCryolite,0.013,"*k")
legend("Reflectivity","Cryolite","location","southeast")

% Multilayer design
clear all;
lambda0=1080e-9;
nglass=nSilica(lambda0);

n1=3.5;% Silicon              % MEDIA ORDER MATTERS
n2=1.38;% Magnesium Floride
nair=1;

% Naming convention:
% -+A--+B--+C--|
%  Za  Z2  Z1  Zg
% --.---.---.--|

Kx=0; % Normal incidence 
Zg=pZinf(nglass,Kx,lambda0);
Z1=pZinf(n1,Kx,lambda0);
Z2=pZinf(n2,Kx,lambda0);
Za=pZinf(nair,Kx,lambda0);

gammaCplus=gamma(Z1,Zg);
% variable propagation in layer 1
lambdaM1=lambda0/n1;
d1=linspace(0,0.5,1000)*lambdaM1; %vary d1 between 0 and lambda/2 IN the material
Kz=k=2*pi/lambdaM1;
ZBplus=impedancePropagation(gammaCplus,d1,Kz,Z1);
gammaBplus=gamma(Z2,ZBplus);
% variable propagation in layer 2 for each d1
lambdaM2=lambda0/n2;
Kz=k=2*pi/lambdaM2;
for k=[1:1000]
  d2=(k-1)/1000*lambdaM2/2; %vary d2 between 0 and lambda/2 IN the material
  ZAplus(k,:)=impedancePropagation(gammaBplus,d2,Kz,Z2);
end

% ZAplus structure
% d1\d2 ->
% v | ZA(d1(1),d2(1)  ZA(d1(1),d2(2) |
%   | ZA(d1(2),d2(1)  ZA(d1(2),d2(2) |

% minimize the impedance mismatch (min sum of the abs(RE)+j*abs(IM))
% find all the minimuma within the same column with the index to recover the optimal d2
[mincol,indx2]=min(abs(real(ZAplus))-abs(real(Za)) + j*(abs(imag(ZAplus))-abs(imag(Za))));
% find the global minimum between the minima of the columns with the index to recover the optimal d1
[minMisMatch,indx1]=min(mincol);

% find the physical thickness from the indexes
thickness1=(indx1-1)/1000*lambdaM1/2
thickness2=(indx2(indx1)-1)/1000*lambdaM2/2

% check the actual reflectivity achieved
gammaCplus=gamma(Z1,Zg);
% propagation in layer 1
d1=thickness1;
Kz=k=2*pi/lambdaM1;
ZBplus=impedancePropagation(gammaCplus,d1,Kz,Z1);
gammaBplus=gamma(Z2,ZBplus);
% propagation in layer 2
Kz=k=2*pi/lambdaM2;
d2=thickness2;
ZAplus=impedancePropagation(gammaBplus,d2,Kz,Z2);
MLreflectivity=abs(gamma(Za,ZAplus))^2
