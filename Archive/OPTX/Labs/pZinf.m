% Equivalent characteristic impedance for TM waves
function Ztm=pZinf(n, Kx, lambda)
  c=300e6;
  epsilon0= 8.854e-12;
  epsilonr=n^2;
  epsilon=epsilon0*epsilonr;
  k=2*pi*n/lambda;
  Kz=sqrt(k^2-Kx^2);
% Kz should have only negative imaginary sign because the convention of the exponential decay
  if(imag(Kz)>0)
    Kz=conj(Kz);
  end
  % ZTM=Kz/(omega*epsilon)=Kz/(2*pi*f*epsilon)=Kz/(2*pi*(c/lambda)*epsilon)
  Ztm=Kz*lambda/(2*pi*c*epsilon);
% Now Kz is at the numerator so Ztm should have the same imaginary sign
  if(imag(Ztm)>0)
    Ztm=conj(Ztm);
  end
end
