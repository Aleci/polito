% Attention! this lab is not precise in several aspect

clear all; close all; clc

function qOut=opticalTransform(qIn,deviceMatrix)
  A=deviceMatrix(1,1);
  B=deviceMatrix(1,2);
  C=deviceMatrix(2,1);
  D=deviceMatrix(2,2);
  qOut=(A.*qIn+B)./(C.*qIn+D);
end

function w=wristRadius(q,lambdaM)
  w=sqrt( -lambdaM./(pi*imag(1./q)) );
end

problem=1;

%--------------------------------------------------------------
% Problem 1: Collimate the beam with a single lens
% => minimize theta => maximize the spot radius after the lens
%--------------------------------------------------------------

% To collimate a beam the laser must be placed at the focus of the lens
% If the laser is too close the output beam diverges
% The more the laser is placed far from the focal point,
% the more the output beam tend to converge precisely at the focal point.

% COLLIMATOR (DISCARD LAST PART OF THE GRAPH)
if(problem==1)
  % DATA
  lambda0=633e-6;        % mm
  w0=.48/2; % initial spot radius in mm
  M2=1;
  %
  DIST=1000;             % mm  
  FL1=1000;              % mm
  DIST2=10000;           % mm
  %
  FL2=1; % not used
  DIST3=1; % not used
  mytitle="Single lens beam collimator";
  
  % DATA cancella
  lambda0=633e-6;        % mm
  w0=.48/2; % initial spot radius in mm
  M2=1;
  %
  DIST=1000;             % mm  
  FL1=1000;              % mm
  DIST2=10000;           % mm
  %
  FL2=1; % not used
  DIST3=1; % not used
  mytitle="Single lens beam collimator";
end

%------------------------------------------------------------------
% Problem 2: Focus the previous collimated beam with an other lens
%------------------------------------------------------------------

% To focus the beam at the second focal point the distance between
% the two must be the sum of the two focal lenghts.
% This device is useful to focus a beam into a small spot using a
% small distance. In fact to focus into a small spot the input beam should be
% more collimated as possible or the source must be very distant from the lens

if(problem==2)
  % DATA
  lambda0=633e-6; % mm
  w0=.48/2; % initial spot radius in mm
  M2=1;
  %
  DIST=1000;         % mm  
  FL1=1000;              % mm
  DIST2=1500;           % mm
  FL2=500;             % mm
  DIST3=600;            % mm
  mytitle="Focus after 2 lenses";
end

%------------------------------------------------------------------
% Problem 3: Design a galilean beam expander
%------------------------------------------------------------------

% GALILEAN BEAM EXPANDER CONFIGS
if(problem==3)
  % DATA
  lambda0=635e-6; % mm
  w0=3/2; % initial spot radius in mm
  M2=1;
  %
  DIST=100;         % mm  
  FL1=-100;              % mm
  DIST2=400;           % mm
  FL2=500;             % mm
  DIST3=1100;            % mm
  mytitle="Galilean beam expander";
end

% KEPLERIAN BEAM EXPANDER CONFIGS
if(problem==4)
  % DATA
  lambda0=635e-6; % mm
  w0=3/2; % initial spot radius in mm
  M2=1;
  %
  DIST=100;         % mm  
  FL1=100;              % mm
  DIST2=600;           % mm
  FL2=500;             % mm
  DIST3=1100;            % mm
  mytitle="Keplerian beam expander";
end

%------------------------------------------------------------------
% Problem 5: Compute difference in wrist radius with a non unitary M2
%------------------------------------------------------------------

% The wrist radius increases of M
% To perform the calculation compute everithing as it were ideal,
% then multiply the radius by the M
% To compute theta is necessary to use the BPP multiplied by M2

% GALILEAN BEAM EXPANDER WITH NONIDEAL GAUSSIAN BEAM
if(problem==5)
  % DATA
  lambda0=635e-6; % mm
  w0=3/2; % initial spot radius in mm
  M2=10;
  %
  DIST=100;         % mm  
  FL1=-100;              % mm
  DIST2=400;           % mm
  FL2=500;             % mm
  DIST3=1100;            % mm
  mytitle="Galilean beam expander (M2=10)";
end


% Fixed Data
nair=1;
lambdaAir=lambda0/nair;

BPP=M2*lambdaAir/pi; % the BPP is fixed [mm*rad]
thetaIn=BPP/w0 *180/pi;
%
zr=pi*w0^2/(lambdaAir); % the ideal Rayleigh Range
q1=i*zr; % input q;

% Assume that the lens is placed at a distance DIST from the input beam
d1=linspace(0,DIST,1000);
q2=q1+d1;

% Assume a thin lens with focal length FL1
lens1=[1,0;-1/FL1,1];

% Transform the beam
q3=opticalTransform(q2(end),lens1);

% Propagate the beam
d2=linspace(0,DIST2,10000);
q4=q3+d2;

% Assume a thin lens with focal length FL2
lens2=[1,0;-1/FL2,1];

% Transform the beam
q5=opticalTransform(q4(end),lens2);

% Propagate the beam
d3=linspace(0,DIST3,1000);
q6=q5+d3;

% Compute ideal wrist radius along the three distances
w=[wristRadius(q2,lambdaAir) wristRadius(q4,lambdaAir) wristRadius(q6,lambdaAir)];
[dsds,x]=min(wristRadius(q4,lambdaAir)*sqrt(M2))
dsdsX=(d2(2)-1)*x
% Compute the real wrist radius
w=w*sqrt(M2);

% Compute the output divergence in the focal point of each lens (far field)
qFL1=q3+FL1;
wFL1=wristRadius(qFL1,lambdaAir);
thetaMid=BPP/wFL1 *180/pi;
%
qFL2=q5+FL2;
wFL2=wristRadius(qFL2,lambdaAir);
thetaOut=BPP/wFL2 *180/pi;


% Plot result
t=[-d1(end:-1:1), d2, DIST2+d3]/10;
FL1c=FL1/10;FL2c=FL2/10;DIST2c=DIST2/10; % convert in cm
h=plot(t,w,"b",t,-w,"b",-FL1c,0,"r*",FL1c,0,"r*",DIST2c-FL2c,0,"g*",DIST2c+FL2c,0,"g*");
title(mytitle)
text(-FL1c,0,sprintf("   ThetaIn:\n %f deg",thetaIn))
text(FL1c,0,sprintf("   ThetaMid:\n %f deg",thetaMid))
text(DIST2c+FL2c,0,sprintf("   ThetaOut:\n %f deg",thetaOut))
set(gca,"FontSize",14)
set(h,"LineWidth",2)
xlabel("Optical Axis Position [cm]"),ylabel("Waist Radius [mm]")
