clear all; close all; clc

lambda0=633e-9; % red
%lambda0=700e-7;
nair=1;
nglass=nSilica(lambda0);
k0=2*pi/lambda0;

% Assume to have an antireflection coating on the first and fourth interfaces
% The first interface is air/silica at 0 degrees, with fully refraction
% The second interface is silica/air at 45 degrees, from which Kx is computed
% The third interface is air/silica with known Kx
% The fouth interface is silica/air with fully refraction, so it is not considered.
% We want reflectivity = .5 for te waves at the second interface

% interface 1 is simplified
% interface 2 provides the Kx (seen from the first medium, glass)
Kx=k0*nglass*sin(pi/4)

% then the two characteristic impedances are computed from the Kx, constant among the mediums
ZinfGlass=sZinf(nglass, Kx, lambda0, 1) 
ZinfAir=sZinf(nair, Kx, lambda0, 1) 
% we consider the glass as the infinite medium
Zload=ZinfGlass;

% Naming convention:
% -+A--+B--|
%  Zg  Za Zg
% --.---.--|


d=linspace(0,1,2000)*1e-6; % [m]
% Compute reflection coefficient of the last nontrivial interface
GammaBPlus=(Zload-ZinfAir)/(Zload+ZinfAir);

% Propagate the reflection coefficient backwards of a distance d (In the direction of Kz)
k=2*pi*nair/lambda0; % compute k in air
Kz=sqrt(k^2-Kx^2); % consider only the propagation component of the k
if(imag(Kz)>0)
    Kz=conj(Kz);
end
GammaA=GammaBPlus*exp(-j*2*Kz*d); %compute the propagation
% Since only the impedance is continuous in A compute it
ZA=ZinfAir*(1+GammaA)./(1-GammaA);
% Compute the reflection coefficient with ZA as the new load
GammaAPlus=(ZA-ZinfGlass)./(ZA+ZinfGlass);
reflectivityTE=abs(GammaAPlus).^2;


% Half reflection for d=179 nanometers
dselected=179e-9;

% Check the reflection coefficient for TM waves for the selected distance

% we have the same Kx of the te case
% the impedances changes but the reason is the same
ZinfGlass=pZinf(nglass, Kx, lambda0) 
ZinfAir=pZinf(nair, Kx, lambda0) 
%
Zload=ZinfGlass;
GammaBPlus=(Zload-ZinfAir)/(Zload+ZinfAir);
k=2*pi*nair/lambda0;
Kz=sqrt(k^2-Kx^2);
GammaA=GammaBPlus*exp(-j*2*Kz*d); %compute the propagation
ZA=ZinfAir*(1+GammaA)./(1-GammaA);
GammaAPlus=(ZA-ZinfGlass)./(ZA+ZinfGlass);
reflectivityTM=abs(GammaAPlus).^2;

% Plot results
h = plot(d/1e-9,reflectivityTE,d/1e-9,reflectivityTM);
set(gca,"FontSize",14)
set(h,"LineWidth",2)
title("Reflectivity, function of the gap distance")
xlabel("Distance [nm]","FontSize",16)
ylabel("Input Reflectivity","FontSize",16)
grid,hold on
plot(179,.5,"*k"),legend("TE","TM","d","location","southeast")
