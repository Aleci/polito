clear all; close all; clc

% Data
lambdaPass=905*1e-9;
lambdaStop=930*1e-9;
%lambdaRange=linspace(90,1935,1000)*1e-9; % Coarse range
lambdaRange=linspace(900,960,100)*1e-9;   % Fine range

% Increasing AOI, the peak moves towards shorter wavelengths
AOI=45;
N=80;

if(AOI==45)
  lambdaStop=1091*1e-9; % equalize for AOI at 45 degrees
  N=80; % place the pass valley on 905nm
end

% lambdaPass -> substrate -> mirror -> lambdaTot
% lambdaPass -> mirror -> lambdaTot
nAir=1;
nH=1.41;
nL=1.37;
nSub=1.45;
theta=AOI/180*pi;


% correct refractive indexes as it was normal incidence by wavelength shifting
%lambdaStop=lambdaStop/sqrt(1-sin(theta)^2) % not working 

%
QWOT_H=lambdaStop/(4*nH);
QWOT_L=lambdaStop/(4*nL);
mirror=[repmat([nH,nL],1,N),nH];
quarterWaveL=[repmat([QWOT_H,QWOT_L],1,N),QWOT_H];

				% DOMANDA
% Come posso equalizzare l'angolo a 45 gradi introdotto per permettere la progettazione
% dello spessore dei layer tale che si ottenga il picco comunque nella frequenza di progetto?

% compute performances
GammaTE=mlayer([nAir mirror nSub],[quarterWaveL],lambdaRange,AOI,"te"); 
GammaTM=mlayer([nAir mirror nSub],[quarterWaveL],lambdaRange,AOI,"tm"); 
refTE=abs(GammaTE).^2;
refTM=abs(GammaTM).^2;

nicePlot(lambdaRange*1e9,{refTE,refTM},"Dichroic Mirror Reflectivity","Wavelength [nm]","Reflectivity")
legend("TE","TM")

% Is better to work with TE, because there will be no Brewster's angle effect and with the same
% number of layers a larger reflectivity can be achieved.

