% Equivalent characteristic impedance for TE waves
function Zte=sZinf(n, snellConst, mur)
  c=300e6;
  mu0=1.257e-6;
  KzNORM=sqrt(n.^2 - snellConst.^2); % snellConst=Kx/K0 => KzNORM=Kz/K0
% Kz should have only negative imaginary sign because the convention of the exponential decay
  if(imag(KzNORM)>0)
    KzNORM=conj(KzNORM);
  end
  % ZTE=omega*mu/Kz=2*pi*f*mu/Kz=2*pi*(c/lambda)*mu/Kz
  % but KzNORM=Kz/K0 => independent from the frequency
  Zte=c*mu0*mur./KzNORM;
% Since Kz has negative imaginary part Zte should have positive imaginary part
  if(imag(Zte)<0)
    Zte=conj(Zte);
  end
end
