% compute the reflection coefficient for a vector
function g=gamma(Zinf,Zload)
  g=(Zload-Zinf)./(Zload+Zinf);
end