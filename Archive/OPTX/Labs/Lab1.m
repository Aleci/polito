clear all; close all; clc

function RefIN=reflection(Zinf2,Zinf1,d,Zload,k)
  gammaB=(Zload-Zinf1)./(Zload+Zinf1);
  gammaA=gammaB*exp(-2*j*k*d);

  Za=Zinf1*(1+gammaA)./(1-gammaA);
  
  gammaIN=(Za-Zinf2)./(Za+Zinf2);

  RefIN=abs(gammaIN).^2;
end
 
var1=linspace(.7,1.4,1000)';
var2=linspace(.1,3.5,100000)';

c=300e6;
Zload=250;
Zinf1=307;
Zinf2=377;

% ex 1
k=2*pi./var1;  
d=1/4;
RefIN=reflection(Zinf2,Zinf1,d,Zload,k);
subplot(2,2,1)
plot(var1,RefIN), title("Rin(λ): Narrowed Scope")
xlabel("Normalized Wavelength [λ/λ0]")
ylabel("Input Reflectivity")
% ex 2
k=2*pi./var2;  
d=1/4;
RefIN=reflection(Zinf2,Zinf1,d,Zload,k);
subplot(2,2,2)
plot(var2,RefIN), title("Rin(λ): Broadened Scope")
xlabel("Normalized Wavelength [λ/λ0]")
ylabel("Input Reflectivity")
% ex 3
k=2*pi*var1/c;
d=1/4*c;
RefIN=reflection(Zinf2,Zinf1,d,Zload,k);
subplot(2,2,3)
plot(var1,RefIN), title("Rin(f): Narrowed Scope")
xlabel("Normalized Frequency [f/f0]")
ylabel("Input Reflectivity")
% ex 4
k=2*pi*var2/c;
d=1/4*c;
RefIN=reflection(Zinf2,Zinf1,d,Zload,k);
subplot(2,2,4)
plot(var2,RefIN), title("Rin(f): Broadened Scope")
xlabel("Normalized Frequency [f/f0]")
ylabel("Input Reflectivity")
