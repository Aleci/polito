% propagates an impedance using the reflection coefficient
function Z=impedancePropagation(gamma,d,Kz,Zinf)
  gamma_at_d=gamma.*exp(-j*2.*Kz.*d); % propagate back the reflection coefficient
  Z=Zinf.*(1+gamma_at_d)./(1-gamma_at_d); % translate it into a local impedance
end