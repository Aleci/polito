% Equivalent characteristic impedance for TE waves
function Zte=sZinf(n, Kx, lambda, mur)
  c=300e6;
  mu0=1.257e-6;
  k=2*pi*n/lambda;
  Kz=sqrt(k^2-Kx^2);
% Kz should have only negative imaginary sign because the convention of the exponential decay
  if(imag(Kz)>0)
    Kz=conj(Kz);
  end
  % ZTE=omega*mu/Kz=2*pi*f*mu/Kz=2*pi*(c/lambda)*mu/Kz
  Zte=2*pi*c*mu0*mur/(Kz*lambda);
% Since Kz has negative imaginary part Zte should have positive imaginary part
  if(imag(Zte)<0)
    Zte=conj(Zte);
  end
end
