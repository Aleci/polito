clear all; close all; clc; addpath("Library"); format short;

% Data 
lambdaPass=linspace(380,750,100)*1e-9; % low reflectivity in the visible band
lambdaStop=linspace(1050,1090,100)*1e-9; % High reflectivity in the laser band
lambda=1070*1e-9; % Design frequency for AOI=0 

% Increasing AOI, the peak moves towards shorter wavelengths
AOI=45;

% Since the AOI is different from 0 the design wavelength must be compensated
lambda=lambda* 1.17; 

% Laser -> mirror -> head
% Head -> mirror -> mirror substrate -> camera
nAir=1;
nSub=1.5228; % BK7, good for low power visible signals

% Dichroic mirror parameters
% High number of layers -> higher reflectivity also for TM
N=250;
% Small differences in refractive index -> more spacing between reflection peaks
nH=1.38; % Magnesium Fluoride
nL=1.35; % Cryolite

% correct refractive indexes as it was normal incidence by wavelength shifting
%lambdaStop=lambdaStop/sqrt(1-sin(theta)^2) % not working 

%
QWOT_H=lambda/(4*nH);
QWOT_L=lambda/(4*nL);
mirror=[repmat([nH,nL],1,N),nH];
quarterWaveL=[repmat([QWOT_H,QWOT_L],1,N),QWOT_H];

theta=AOI/180*pi;
if(false)
  % Numerically find the suitable correction for the design wavelenght
  lambdaCoarse=linspace(100,2000,1000)*1e-9;
  GammaTE=mlayer([nAir mirror nSub],[quarterWaveL],lambdaCoarse,AOI,"te");
  refTE=abs(GammaTE).^2;
  nicePlot(lambdaCoarse*1e9,{refTE},"Broad Band Coarse Characteristic",
           "Wavelength [nm]","Reflectivity")
end

% compute performances in the stop band
figure
GammaTE=mlayer([nAir mirror nSub],[quarterWaveL],lambdaStop,AOI,"te"); 
GammaTM=mlayer([nAir mirror nSub],[quarterWaveL],lambdaStop,AOI,"tm"); 
refTE=abs(GammaTE).^2;
refTM=abs(GammaTM).^2;

nicePlot(lambdaStop*1e9,{refTE,refTM},"Stop Band Characteristic",
         "Wavelength [nm]","Reflectivity")
legend("TE","TM")

% compute performances in the pass band
figure
GammaTE=mlayer([nAir mirror nSub],[quarterWaveL],lambdaPass,AOI,"te"); 
GammaTM=mlayer([nAir mirror nSub],[quarterWaveL],lambdaPass,AOI,"tm"); 
refTE=abs(GammaTE).^2;
refTM=abs(GammaTM).^2;

nicePlot(lambdaPass*1e9,{refTE,refTM},"Pass Band Characteristic",
         "Wavelength [nm]","Reflectivity")
legend("TE","TM")
