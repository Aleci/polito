clear all; close all; clc; addpath("Library"); format short
laser=1;
wLens=12; % mm
FL=200; % mm

lambda=1070e-6; %mm 

for laser=[1:2]
  if(laser==1)
    % Beam quality
    M2=1.2;
    BPP=M2*(lambda*1e3/pi);   % lambda should be in um = mm*10-3 => mm*mrad. 
    % Power
    P=1e3;
  else
    % Beam quality
    BPP=5; % mm * mrad
    %BPP=2.05; % This beam quality equates the performances of the single-mode (M2=6)
    M2=BPP/(lambda*1e3/pi);   % lambda should be in um = mm*10-3 => mm*mrad. 
    % Power
    P=5e3;
  end

  printf("\n\nLaser %d:\n",laser)

  % Method 1: Explicit calculations through q parameter propagation
  % Compute the waist
  zrCollimated=rayleighRange(wLens,M2,lambda,1);
  qIn=i*zrCollimated;
  lens=thinLens(FL);
  qOut=opticalTransform(qIn,lens);
  [w0 zW0]=waistRadius(qOut,lambda,1);
  % Compute the peak power density
  I0=2*P/(pi*(w0*1e3)^2); %(convert the waist in m for the comparison with the power)

  % Method 2: Calculation of the peak intensity through approximation of the waist
  % Approximate the waist
  w0approx=FL*(BPP*1e-3)/wLens;% mm (remember to compensate for the mrad in the BPP!)
  % Compute the peak power density
  I0approx=2*P/(pi*(w0approx*1e3)^2); %(convert the waist in m for the comparison with the power)

  % Method 3: Direct approximation of the peak intensity
  % Approximate the brightness of the laser
  B=P/(pi*BPP)^2;
  % Approximate the peak power density
  I0approx2=2*pi*B*(wLens/FL)^2;

  % Select from which method generate the plots
  w0,w0approx,I0,I0approx,I0approx2,zW0,B
  I=I0;
  Wmin=w0;

  % Compute the irradiance profile along the waist cross-section
  r=linspace(-1.5*Wmin,1.5*Wmin,1000); % consider almost all the power
  IrradianceT{laser}=irradiance(r,Wmin,Wmin,I);

  zr{laser}=pi*Wmin^2/lambda;
  z=linspace(-3*zr{laser},3*zr{laser},1000);
  q=i*zr{laser} + z;
  w=beamRadius(q,lambda);
  IrradianceL{laser}=irradiance(0,w,Wmin,I);
end

% Plot irradiance transversal profile
nicePlot(r,{IrradianceT{1}*100,IrradianceT{2}*100}, ...
         "Trasversal Distribution of the Irrandiance", ...
         "Radial position [mm]","Irradiace [W/cm^2]")
legend("Single-mode","Multi-mode")
% Plot irradiance longitudinal profile
figure
z=linspace(-zr{2},zr{2},1000);
nicePlot(z,{IrradianceL{1}*100,IrradianceL{2}*100}, ...
         "Longitudinal Distribution of the Irrandiance", ...
         "Longitudinal position [mm]","Irradiace [W/cm^2]"),hold on
plot([-zr{1},zr{1}],1560*[1,1],"c")
plot([-zr{2},zr{2}],67*[1,1],"r")
legend("Single-mode","Multi-mode","Rayleigh Range SM","Rayleigh Range MM")
