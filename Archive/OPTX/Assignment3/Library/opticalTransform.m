% Apply the given transformation matrix to the input gaussian beam in q representation
function qOut=opticalTransform(qIn,deviceMatrix)
  A=deviceMatrix(1,1);
  B=deviceMatrix(1,2);
  C=deviceMatrix(2,1);
  D=deviceMatrix(2,2);
  qOut=(A.*qIn+B)./(C.*qIn+D);
end
