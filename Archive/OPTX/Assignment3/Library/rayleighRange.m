% Compute the Rayleigh range of a beam with waist w0 and beam quality M2
% The wavelength is lambda and the refractive index of the medium in which is propagating is n
function zr=rayleighRange(w0,M2,lambda,n)
  zr=pi*w0^2*n/(lambda*M2); % the Rayleigh Range in air
end
