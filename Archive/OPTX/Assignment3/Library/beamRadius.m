% Compute the beam radius from the given q representation at the wavelength lambdaM
function w=beamRadius(q,lambdaM)
  w=sqrt( -lambdaM./(pi*imag(1./q)) );
end
