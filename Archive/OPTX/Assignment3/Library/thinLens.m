function M=thinLens(FL)
  M=[1,0;-1/FL,1];
end
