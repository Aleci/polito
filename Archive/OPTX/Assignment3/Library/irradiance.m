% Compute the irradiance in the centered circle of radius r
% The beam has waist w0 and the cross-section considered has radius w
% The peak intensity is Ipeak=irradiance(0,w0,w0,Ipeak)
function I=irradiance(r,w,w0,Ipeak)
  I=Ipeak*(w0./w).^2 .* exp(-2*r.^2./w.^2);
end
