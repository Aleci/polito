clear all; close all; clc

% Ex1 p1
T1_2=rot_mat([3;2;3],[pi/3;-pi/6;pi/4]);
r2=[-1;4;2];
r1=T1_2*r2;

% Ex1 p2
figure(1), hold on, grid
mArrow3([0;0;0],r2); title("Viewed in the same frame the matrix is a rotation");
mArrow3([0;0;0],r1,'color','r');
daspect(ones(1,3))
view(60,20)

% Ex1 p3
equalNorms=round(norm(r1),4)==round(norm(r2),4)

% Ex1 p4
syms ps th ph
T323=rot_mat([3,2,3],[ps,th,ph])

% Ex1 p5
[v,d]=eig(T1_2) % the axis of rotation corresponds to the eigenvalue 1
mArrow3([0;0;0],v(:,3),'color','g');

% Ex2 p1
r0=[0;0;2];  % this is the original black vector
T1=rot_mat([1,1,1],[pi/2,0,0]); % Rotation along x
T3=rot_mat([3,3,3],[pi/2,0,0]); % Rotation along z

r1=T1*r0;     % this is a new rotated vector (red)
r2=T3*T1*r0;  % this is an other rotated vector (green)
r3=T3*r0;     % since the rotation is along z r0==r3
r4=T1*T3*r0;  % r4 = T1*r3 = T1*r0 = r1

figure(2),hold on
mArrow3([0;0;0],r0*1.1); title("Different rotation combinations");
mArrow3([0;0;0],r1*1.1,'color','r');
mArrow3([0;0;0],r2*1.1,'color','g');
mArrow3([0;0;0],r3,'color','m');
mArrow3([0;0;0],r4,'color','c');
daspect(ones(1,3))
view(60,20)

% Ex3 p1
Qr0=[0;0;2];  % this is the original black vector
q1=dcm2qua(T1); % Rotation along x
q3=dcm2qua(T3); % Rotation along z

Qr1=vec_rot_quat(q1,Qr0);  
Qr2=vec_rot_quat( quatprod(q3,q1) ,Qr0);
Qr3=vec_rot_quat(q3,Qr0);
Qr4=vec_rot_quat( quatprod(q1,q3) ,Qr0);
rotAndQuatEquals=all(round(r1,4)==round(Qr1,4)) && all(round(r2,4)==round(Qr2,4)) && ...
		 all(round(r3,4)==round(Qr3,4)) && all(round(r4,4)==round(Qr4,4))

% Ex4 p1
q=[0.866, 0.4319, 0.216, 0.1296]'
p=[0.9659, 0.183,     0, 0.183 ]'
I=[1,0,0,0]';

q=q/norm(q); qInv=-q; qInv(1)=-qInv(1) % the inverse is the conjugate only in unitary quaternions
p=p/norm(p); pInv=-p; pInv(1)=-pInv(1)

% Ex4 p2
dotDirect=q'*p
dotInverse=p'*q

% Ex4 p3
qp=quatprod(q,p)
pq=quatprod(p,q)

% Ex4 p4
Ip=quatprod(I,p);
pI=quatprod(p,I)

% Ex5 p1
T=[0.2276 -0.9354 0.2706; 0.7571 -0.004773 -0.6533; 0.6124 0.3536 0.7071];
qT=dcm2qua(T)

% Ex5 p2
qR=[ 0.588, 0.03378, -0.2566, 0.7663]';
TR=qua2dcm(qR)
EulR=qua2euler(qR) % se le librerie le fai col culo non sono problemi miei...
%% ATTENZIONE: qua2euler restituisce T321 invece di T313 o T323 come ci si aspetterebbe
