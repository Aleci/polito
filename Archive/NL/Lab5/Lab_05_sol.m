%% Exercise 1 

clear all
close all
clc

% Point 1
ps=pi/3; th=-pi/6; ph=pi/4;
T31=[cos(ps) -sin(ps) 0;sin(ps) cos(ps) 0;0 0 1];
T2=[cos(th) 0 sin(th);0 1 0;-sin(th) 0 cos(th)];
T32=[cos(ph) -sin(ph) 0;sin(ph) cos(ph) 0;0 0 1];
T=T31*T2*T32;
%T=rot_mat([3 2 3],[ps th ph]);
r=[-1;4;2]
r1=T*r

% Point 2
figure, hold on, grid
mArrow3([0;0;0],r);
mArrow3([0;0;0],r1,'color','r');
daspect(ones(1,3))
view(60,20)

% Point 3
norm(r)
norm(r1)

% Point 4
clear ps th ph
syms ps th ph
T31=[cos(ps) -sin(ps) 0;sin(ps) cos(ps) 0;0 0 1];
T2=[cos(th) 0 sin(th);0 1 0;-sin(th) 0 cos(th)];
T32=[cos(ph) -sin(ph) 0;sin(ph) cos(ph) 0;0 0 1];
T=T31*T2*T32

%% Exercise 2 

clear all
close all
clc

% Points 1-3
r=[0;0;2];
r1=rot_mat([1 2 3],[pi/2 0 0])*r;
r2=rot_mat([3 2 1],[pi/2 0 pi/2])*r;
r3=rot_mat([1 2 3],[0 0 pi/2])*r;
r4=rot_mat([1 2 3],[pi/2 0 pi/2])*r;
figure, grid, hold on
mArrow3(zeros(3,1),r,'color','k','stemWidth',0.01);
mArrow3(zeros(3,1),r1,'color','b','stemWidth',0.01);
mArrow3(zeros(3,1),r2,'color','r','stemWidth',0.01);
daspect([1 1 1])
view(60,20)
xlabel('x')
ylabel('y')
zlabel('z')
figure, grid, hold on
mArrow3(zeros(3,1),r,'color','k','stemWidth',0.01);
mArrow3(zeros(3,1),r3,'color','b','stemWidth',0.01);
mArrow3(zeros(3,1),r4,'color','r','stemWidth',0.01);
daspect([1 1 1])
view(60,20)
xlabel('x')
ylabel('y')
zlabel('z')

% Points 2,3: Evident from the plots.

% Point 4
T=rot_mat([3 2 1],[pi/2 0 pi/2]);
[V,S]=eig(T)
u=V(:,3) % this eigenvector (corresponding to the eigenvalue 1) defines the rotation axis
% Alternative method:
beta=acos((sum(diag(T))+1)/2)
u=[T(3,2)-T(2,3);T(1,3)-T(3,1);T(2,1)-T(1,2)]/2/sin(beta) 

%% Exercise 3 

clear all
close all
clc

ps=pi/3; th=-pi/6; ph=pi/4;
q31=[cos(ps/2);0;0;sin(ps/2)];
q2=[cos(th/2);0;sin(th/2);0];
q32=[cos(ph/2);0;0;sin(ph/2)];
q=quatprod(q31,quatprod(q2,q32));
qs=[q(1);-q(2:4)];

% Point 1
r=[-1;4;2]
r1=quatprod(q,quatprod([0;r],qs));
r1=r1(2:4)

% Point 2
figure, hold on, grid
mArrow3([0;0;0],r);
mArrow3([0;0;0],r1,'color','r');
daspect(ones(1,3))
view(60,20)

% Point 3
norm(r)
norm(r1)

%% Exercise 4 

clear all
close all
clc

b1=pi/3;
u1=[1;0.5;0.3]; u1=u1/norm(u1);
q=[cos(b1/2);u1*sin(b1/2)]
b2=pi/6;
u2=[1;0;1]; u2=u2/norm(u2);
p=[cos(b2/2);u2*sin(b2/2)]

% Point 1
qs=[cos(b1/2);-u1*sin(b1/2)]
ps=[cos(b2/2);-u2*sin(b2/2)]

% Point 2
q'*p
p'*q

% Point 3
quatprod(q,p)
quatprod(p,q)

% Point 4
I=[1;zeros(3,1)];
quatprod(I,p)
quatprod(p,I)
p

%% Exercise 5 

clear all
close all
clc

% Point 1
ps=pi/8
th=pi/4
ph=pi/3
T=rot_mat([3 1 3],[ps th ph])
q(1)=sqrt(sum(diag(T))+1)/2;
q(2:4)=[T(3,2)-T(2,3);T(1,3)-T(3,1);T(2,1)-T(1,2)]/4/q(1);
q=q'

% Point 2
q=[0.5880    0.0338   -0.2566    0.7663]';
T=[q(1)^2+q(2)^2-q(3)^2-q(4)^2 2*(q(2)*q(3)-q(1)*q(4)) 2*(q(2)*q(4)+q(1)*q(3))
    2*(q(2)*q(3)+q(1)*q(4)) q(1)^2-q(2)^2+q(3)^2-q(4)^2 2*(q(3)*q(4)-q(1)*q(2))
    2*(q(2)*q(4)-q(1)*q(3)) 2*(q(3)*q(4)+q(1)*q(2)) q(1)^2-q(2)^2-q(3)^2+q(4)^2]
ph=atan2(T(1,3),-T(2,3))
th=atan2(sin(ph)*T(1,3)-cos(ph)*T(2,3),T(3,3))
ps=atan2(-cos(ph)*T(1,2)-sin(ph)*T(2,2),cos(ph)*T(1,1)+sin(ph)*T(2,1))









