%% Problem settings
clear all; close all; clc
s=tf('s');

% Emulate partial function application in Caclab framework
global myPid
global KI
global ref

% Define symbolic variables
x=sym('x',[3 1],'real'); % this is a symbolic vector
syms u real

% Define the system
alpha=10.4;
beta=16.5; 
R=0.1;
%
rho= 0.041*x(1)^3 -1.16*x(1);
% xdot=f(x)+g(x)*u; y=h(x)
f=[alpha*(x(2)-x(1)-rho)
   x(1)-x(2)+x(3)
   -beta*x(2)-R*x(3)];
g=[0;1;0];
h=x(1); C=[1,0,0];
% Create a function in a file from the symbolic definition
matlabFunction(f+g*u,'File','chuaSys','Vars',[u;x]);

%% Exercise 1: Feedback linearize the system
% Compute the cancellation term to add to the desired control action to linearize the system
% io_fl creates the function
% control_action = u_$NAME([linear_control_action; state])
% y_and_derivatives = mu_$NAME(state)
[u,mu,ga,a,b,MU]=io_fl(f,g,h,'chua');
% (mu_dot(1)=y_dot=mu(2); mu_dot(2)=is not combination of mu but of the control action)
A=[0 1; 0 0]; % companion matrix
B=[0; 1]; Cred=C(1:2);

% Control system design
M=ss(A,B,Cred,0);
tunedPid=pidtune(M,'pidf',5); % Use a PID controller for cutting frequency 5rad/s

% Convert the pid data type in an other suitable for a non simulink environment
myPid.Kp=tunedPid.Kp; myPid.Ki=tunedPid.Ki; myPid.Kd=tunedPid.Kd;
[myPid.A,myPid.B,myPid.C,myPid.D]=tf2ss([1 0],[tunedPid.Tf 1]);

%% Simulate the controlled system
t=[0,30]; % set the simulation time interval
ref=1; %% set the reference to 1

% Integrate differential equations
[t,xe]=ode45(@myLinearizedCtrlSys, t,[0;0;0;0;0]);
y=xe(:,1); %% the output is the first state

% Print result
plot(t,y);

%% Exercise 2: Pointwise linearization
A=jacobian(f,x); eq=zeros(3,1);
A=subs(A,x,eq);
A=double(A);
C=[0 1 0]; % The professor changed this datum from the previous exercise
M=ss(A,g,C,0); zpk(M) %view tf

% Integral action addition
Aa=[A zeros(3,1);C 0]; Ba=[g;0]; % Augmented system
Mc=ctrb(Aa,Ba); rank4=rank(Mc)  % System controllable if rank maximum (4)
KI=place(Aa,Ba,-(1:4));

%% Simulate the controlled system
t=[0,150]; % set the simulation time interval

% Emulate partial function application in Caclab framework
ref=1; %% set the reference to 1

% Integrate differential equations
[t,xe]=ode23t(@myCtrlSys, t,[0;0;0;0]);
y=xe(:,2); %% the output is the second state

% Print result
plot(t,y);

%% Simulink models counterpart
% Exercise 1: PIDF controller
function dState=myLinearizedCtrlSys(t,state)
    global ref;
    global myPid;
    %persistent dErr; if isempty(dErr), dErr=0; end (pid case)

    % Rename important signals
    y=state(1);
    err=ref-y;
    integralOfErr=state(4);

    % Compute realizable derivative (pidf case)
    derState=myPid.A*state(5)+myPid.B*err;
    derOut=myPid.C*state(5)+myPid.D*err;
    
    % Compute the linear control action
    v=myPid.Kp*err +myPid.Ki*integralOfErr +myPid.Kd*derOut;

    % Feedback linearize the system
    u=u_chua([v;state]);

    % Chua system state variation
    cState=chuaSys(u,state(1),state(2),state(3));

    % Save the error derivative for the next iteration (pid case)
    %dErr=-cState(1); %% e=ref-state(1) => dErr=eRef-dState(1)=-dState(1)
    
    % Compute control loop state variation
    dState=[cState;    % variation of the chua system state
	    err;       % variation of integral of the error
            derState]; % variation of the real derivator
end

% Exercise 2: PI state regulation
function dState=myCtrlSys(t,state)
    global ref;
    global KI;
    y=state(2); % The output is the second state
    err=ref-y; % Integral variation
    u=-KI*state; % Compute state regulation
    dState=[chuaSys(u,state(1),state(2),state(3)); err]; % State variations
end

