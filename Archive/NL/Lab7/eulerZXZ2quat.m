function q = eulerZXZ2quat(ea)
% case 313
  q1=[cos(ea(1)/2);0;0;sin(ea(1)/2)];
  q2=[cos(ea(2)/2);sin(ea(2)/2);0;0];
  q3=[cos(ea(3)/2);0;0;sin(ea(3)/2)];
  q=quatprod(q1,quatprod(q2,q3));
end
