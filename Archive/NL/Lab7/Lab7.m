clear all; close all; clc
global ref; global param; global Ctrl;

% Simulation settings
tFin=500;
simYes=true;
smoothness=1000;
simSpeed=10;
Ctrl.PD=false;
Ex1Settings=false;  % set true for data of exercize 1 

% System definition and initial conditions
if Ex1Settings
  param.J = diag([10, 9, 12])*1e3;
  
  q0 = [0.6853, 0.6953, 0.1531, 0.1531]';
  omega0 = [0.53, 0.53, 0.053]';
else
  param.J=[399 -2.81 -1.31;
	   -2.81 377 2.54;
	   -1.31 2.54 377];
  
  %q0=[0;0;1;0];
  q0=[1/2;0;1/2;0];
  omega0=[0.1;-0.9;0.12];
end
param.Jinv=inv(param.J);


% Reference definition
t=linspace(1,tFin,smoothness);
ref(:,1)=t;
if(Ctrl.PD) % set as constant reference the identity quaternion
  ref(:,2)=ones(1,smoothness);  
  ref(:,3)=zeros(1,smoothness); % regulation only
  ref(:,4)=zeros(1,smoothness);
  ref(:,5)=zeros(1,smoothness);
else % set as constant reference the angular variation on each ZXZ axis
  if Ex1Settings
    fiDotRef=zeros(1,smoothness); % regulation
    thetaDotRef=zeros(1,smoothness);
    psiDotRef=zeros(1,smoothness);
  else
    fiDotRef=0.001745*ones(1,smoothness); % tracking
    thetaDotRef=zeros(1,smoothness);
    psiDotRef=0.04859*ones(1,smoothness);
  end
  euler_angles_dot=[fiDotRef; thetaDotRef; psiDotRef]';
  
  % Derive the reference signals actually used
  %
  % compute euler angles
  fiRef=cumtrapz(fiDotRef);    % integrate the angular variations
  thetaRef=cumtrapz(thetaDotRef);
  psiRef=cumtrapz(psiDotRef);
  euler_angles=[fiRef; thetaRef; psiRef]';
  % convert them to quaterions
  for k=1:smoothness; ref(k,2:5)=eulerZXZ2quat(euler_angles(k,:)); end 
  % convert them into angular velocity
  for k=1:smoothness; ref(k,6:8)=eulerZXZ2omega(euler_angles_dot(k,:)',euler_angles(k,:)); end
  % differentiate the angular velocity
  ref(:,9:11)=gradient(ref(:,6:8)')';
end

% Controller parameters selection
if(Ctrl.PD) % PD controller
  Ctrl.kd=500; % first fix the angular velocity
  Ctrl.kp=20;  % then fix the position
else % Sliding mode controller
  Ctrl.k1=1;
  Ctrl.k2=1;
  Ctrl.eta=0.2;
end

[~,xSol]=ode45(@satellite,t,[q0;omega0]);
for k=1:length(t); M(k,:)=attitudeController(t(k),ref,xSol(k,:)'); end

%% Create animation or plots
if simYes
  global hr; load cube; hr=diag([.9 .5 .1])*hr;
  for i=[1:length(t)]
    animation_rot(hr,xSol(i,1:4)');
%  if(i~=1), pause(t(i)-t(i-1)/simSpeed); end % variable step realtime simulation
    pause(0.1); % fixed step simulation
  end
else
  figure(2)
  subplot(2,2,1); plot(t,xSol(:,1)); title("q(1)");
  subplot(2,2,2); plot(t,xSol(:,2)); title("q(2)");
  subplot(2,2,3); plot(t,xSol(:,3)); title("q(3)");
  subplot(2,2,4); plot(t,xSol(:,4)); title("q(4)");
  figure(3)
  subplot(3,1,1); plot(t,M(:,1)); title("M(1)");
  subplot(3,1,2); plot(t,M(:,2)); title("M(2)");
  subplot(3,1,3); plot(t,M(:,3)); title("M(3)");
end


%% Simulink counterparts
function dState=satellite(t,state)
  % State 1:4 = quaternionPosition
  % State 5:7 = angularVelocity
  global ref; global param;
  dState=zeros(6,1);

  % Compute control action
  M=attitudeController(t,ref,state);
   
  % Dynamic equation (no damping)
  dState(5:7)=param.Jinv*M - param.Jinv*skew(state(5:7))*param.J*state(5:7);

  % Kinematic equation
  dState(1:4)= .5 * quatprod(state(1:4),[0;state(5:7)]);
end

function u=attitudeController(t,ref,state)
  global Ctrl; global param;
  if Ctrl.PD  % attitude regulator (PD)
    qRef=[getFromTrajectory(t,[ref(:,1),ref(:,2)]);...
          getFromTrajectory(t,[ref(:,1),ref(:,3)]);...
	  getFromTrajectory(t,[ref(:,1),ref(:,4)]);...		       
          getFromTrajectory(t,[ref(:,1),ref(:,5)])];

    % Rename important signals
    qState=state(1:4)/norm(state(1:4));
    qStateInv=[qState(1);-qState(2:4)];
    qErr=quatprod(qStateInv,qRef);
    omegaErr=-state(5:7);

%u = Ctrl.kp*qErr(2:4) + Ctrl.kd*omegaErr; % simply PD control action
%u = Ctrl.kp*sign(qErr(1))*qErr(2:4) + Ctrl.kd*omegaErr; % modified PD for shortest path
%u = Ctrl.kp*qErr(2:4) + Ctrl.kd*(1+qErr(2:4)'*qErr(2:4))*omegaErr; %modified PD for performances
    
    %modified PD for shortest path and performances
    u = Ctrl.kp*sign(qErr(1))*qErr(2:4) + Ctrl.kd*(1+qErr(2:4)'*qErr(2:4))*omegaErr; 
%------------------------------------------------------------------------------------------------    
  else % sliding mode controller

    qRef=[getFromTrajectory(t,[ref(:,1),ref(:,2)]);...
          getFromTrajectory(t,[ref(:,1),ref(:,3)]);...
	  getFromTrajectory(t,[ref(:,1),ref(:,4)]);...		       
          getFromTrajectory(t,[ref(:,1),ref(:,5)])];
    omegaRef=[getFromTrajectory(t,[ref(:,1),ref(:,6)]);...
              getFromTrajectory(t,[ref(:,1),ref(:,7)]);...
              getFromTrajectory(t,[ref(:,1),ref(:,8)])];
    omegaRefDot=[getFromTrajectory(t,[ref(:,1),ref(:,9)]);...
		 getFromTrajectory(t,[ref(:,1),ref(:,10)]);...
		 getFromTrajectory(t,[ref(:,1),ref(:,11)])];

    % Rename important signals
    qState=state(1:4)/norm(state(1:4));
    qStateInv=[qState(1);-qState(2:4)];
    qErr=quatprod(qStateInv,qRef);
    omega=state(5:7);
    omegaErr=omegaRef-omega;

    % Sliding mode control 
%     fs=omegaErr+Ctrl.k2*qErr(2:4); % sliding surface
%     u_fl=param.J*(omegaRefDot +     ...   % linearizing control action
%		  Ctrl.k2/2*(qErr(1)*omegaRef+ skew(qErr(2:4))*(omegaRef+omega)) ) ...
%        + skew(omega)*param.J*omega;

    % variant with shortest path optimization
    fs=omegaErr+Ctrl.k2*qErr(2:4)*sign(qErr(1)); % sliding surface
    u_fl=param.J*(omegaRefDot +     ...   % linearizing control action
		  Ctrl.k2/2*(qErr(1)*omegaRef+ sign(qErr(1))*skew(qErr(2:4))*(omegaRef+omega)) ) ...
         + skew(omega)*param.J*omega;
    u= u_fl + Ctrl.k1*param.J*tanh(Ctrl.eta*fs); % final control action
  end

  % Saturate the control action
  u( find(u >  10) ) =  10;
  u( find(u < -10) ) = -10;
end
