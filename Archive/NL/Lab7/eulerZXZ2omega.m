function omega = eulerZXZ2omega(ea_dot,ea)

A=[sin(ea(2))*sin(ea(3))    cos(ea(3))   0
   sin(ea(2))*cos(ea(3))   -sin(ea(3))   0
   cos(ea(2))                  0         1];

omega=A*ea_dot;
