clear all
close all
clc

% The solution reported here is for Exercises 1 and 2.
% The solution of the other exercise is similar.

%% Rigid body data

global hr
load cube
hr=diag([1 1.5 3]/2)*hr;

J=diag([937.5 833.3 270.8]);
IJ=inv(J);

%% Simulation 

w0=[0;1;0];
% q0=[1;0;0;0];
q0=[0.5451    0.1873   -0.1858   -0.7958]';
q0=q0/norm(q0);
a0=flipud(qua2euler(q0));

M=zeros(3,1);

Tfin=50;

open('lab_6_sim')
sim('lab_6_sim')

