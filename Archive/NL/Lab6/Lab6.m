clear all; close all; clc
global ref; global param;
param.J = diag([937.5, 833.3, 270.8]);
param.Jinv=inv(param.J);

t=linspace(1,10,100);
ref(:,1)=t;
ref(:,2)=linspace(100,1000,100); % Evolution of moments on x,y,z axis
ref(:,3)=zeros(1,100);
ref(:,4)=zeros(1,100);

% Initial condition
q0=[1;0;0;0];
omega0=[0; 0; 0];

[~,xSol]=ode45(@satellite,t,[q0;omega0]);


%% Create animation
global hr; load cube;
for i=[1:length(t)]
  animation_rot(hr,xSol(i,1:4)');
  if(i~=1), pause(t(i)-t(i-1)); end
end

function dState=satellite(t,state)
  % State 1:4 = quaternionPosition
  % State 5:7 = angularVelocity
  global ref; global param;
  dState=zeros(6,1);
  
  M=[getFromTrajectory(t,[ref(:,1),ref(:,2)]);...
     getFromTrajectory(t,[ref(:,1),ref(:,3)]);...		       
     getFromTrajectory(t,[ref(:,1),ref(:,4)])];
  
  % Dynamic equation (no damping)
  dState(5:7)=param.Jinv*M - param.Jinv*skew(state(5:7))*param.J*state(5:7);

  % Kinematic equation
  dState(1:4)= .5 * quatprod(state(1:4),[0;state(5:7)]);
end
