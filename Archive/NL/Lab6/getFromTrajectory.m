% A trajectory is a matrix nx2 where each row is a couple (time_instant, value)
% t_inst is the instant at wich we want evaluate the trajectory
% If a time instant is not on the trajectory, the function will return the previous time instant value
function value=getFromTrajectory(t_inst,trajectory)
  indx=find(t_inst >= trajectory(:,1));
  value=trajectory(indx(end),2);
end
