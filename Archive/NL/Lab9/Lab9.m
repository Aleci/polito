%% Problem 1
clear all
close all
clc

%% Plant parameters
global MU RE ve VE
MU=4e5;
RE=6371;
ve=30;
VE=0.465;
m0=30e3;

%% NMPC design
par.model=@model_launch; % del prof
par.n=7;
par.Ts=50;
par.Tp=450;
par.R=0.1*eye(3);
par.P=diag([1e2,1e6*ones(1,4)]);
par.Q=par.P;
par.tol=[5;0.01*ones(4,1)];

K=nmpc_design_st(par);
    
ar=RE+500;
er=[0;0;0];
cir=1;
ref=[ar;er;cir];
x0=[RE;0;0;0;VE;0;m0];

%% Simulation
open("orb_control_blocks.slx")
out=sim("orb_control_blocks.slx");

%% Plot
plot(x.data(:,1),x.data(:,2))

