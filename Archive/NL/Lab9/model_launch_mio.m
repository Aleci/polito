function [xd,h] = model_launch_mio(t,x,u)
% NMPC predicion model
% t: time; vector of dimenensions 1*N, where
% N is the number of samples in the time interval [t,t+Tp];
% N is automatically chosen by the NMPC solver.
% t is useful only in the case of time-varying system.
% x: state of the system; matrix of dimension nx*N.
% u: input of the system; matrix of dimension nu*N.
% f,h: functions of the state equations: 
% xdot=f(t,x,u); y=h(t,x,u).

  global MU
  
  r=norm(x(1:3,:));
  r3=r.^3;

  xd(1:3,:)=x(4:6,:); % <-- Aggiungere ,: a tutti e operatori pointwise
  xd(4:6,:)=-MU*x(1:3,:)./r3 + u./x(7,:);
				% ^-- Aggiungere u
  
  xd(7,:)=0; %  <-- Aggiungere derivata massa

  h = rv2oe(x);
  h(1,:)= r;
    
end

