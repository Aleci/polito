clear all; close all; clc; format short e

global MU RE ve
MU=4e5;
RE=6371;
ve=4.4; % km/s

mb=4000; % kg    //NUN SERVE EMBEDDED NEL PLANT DER PROF!
m0=10000; % kg mb+fuel


usat=132;

% Initial orbit
a0=6871; %km
e0=[0;0;0];
ci=1;
% convert in rv
r0=a0;
v0=sqrt(MU/a0);
x0=[r0; 0; 0; 0; v0; 0; m0];

% reference
ar=8932; % km
er=[0.25; 0; 0];
cir=1;
ref=[ar; er; cir];

%% NMPC design
par.model=@model_launch_mio; % del prof
par.n=7;
par.Ts=30;
par.Tp=90;
par.R=0.1*eye(3);
par.P=diag([1,1e5*ones(1,4)]);
par.Q=par.P;
par.ub=132*ones(3,1);
par.lb=-132*ones(3,1);
par.tol=[1;0.005*ones(4,1)];
par.Tctrl=500;

K=nmpc_design_st(par);

%% Simulation
open("orb_control_blocks.slx")
out=sim("orb_control_blocks.slx");

%% Plot
plot(x.data(:,1),x.data(:,2))
