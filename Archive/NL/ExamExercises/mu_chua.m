function mu = mu_chua(in1)
%MU_CHUA
%    MU = MU_CHUA(IN1)

%    This function was generated by the Symbolic Math Toolbox version 9.2.
%    13-Jul-2023 20:44:31

x1 = in1(1,:);
x2 = in1(2,:);
mu = [x1;x2.*(5.41e+2./5.0e+1)+sin(x1.*pi.*(1.0e+1./1.3e+1)).*2.3804];
