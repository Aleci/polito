function [xd,y]=pred_model_15(t,x,u)
% NMPC prediction model
% t: time; vector of dimenensions 1*N, where
% N is the number of samples in the time interval [t,t+Tp];
% N is automatically chosen by the NMPC solver.
% t is useful only in the case of time-varying system.
% x: state of the system; matrix of dimension nx*N.
% u: input of the system; matrix of dimension nu*N.
% f,h: functions of the state equations: 
% xdot=f(t,x,u); y=h(t,x,u).

    global MU RE ve;

    r=vecnorm(x(1:3,:),2,1);
    v=vecnorm(x(4:6,:),2,1);    
   
    xd(1:3,:)=x(4:6,:);
    xd(4:6,:)=-MU*x(1:3,:)./r.^3+u./x(7,:);
    xd(7,:)=0;

    y=rv2oe(x,MU);
    %y(1)=r; % per i lanci

%     r=x(1:3,:);
%     r3=norm(r)^3;
%     v=x(4:6);
% 
%     dx=[ v
%         -MU*r/r3+ 1/x(7)*u
%          0];
% 
%     y=rv2oe(x,MU);

end