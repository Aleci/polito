clear all; close all; clc; format short e

global J k1 k2 eta;

J=diag([10400,8200,9200]);
IJ=inv(J);
q0=[.68438,-.56827,-.10002,0.44574]';
w0=[0;0;0];

omegar=[1;0;0];

k1=100;
k2=100;
eta=1;

open("SM.slx")
sim("SM.slx")