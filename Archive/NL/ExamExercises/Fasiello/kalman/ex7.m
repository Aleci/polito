%% Plant simulation
clc;close all;clear all

% define symbolic system
x = sym('x',[2 1],'Real'); udecimated = sym('u',[1 1], 'Real'); du = sym('du',[1 1], 'Real');
%
f = [ x(2)
      -2/5*x(1)^3 + x(1) - 3*x(2)];
%
g = [0
     -1];
h = x(1); 
matlabFunction(f+g*udecimated, 'File', 'given_system');

% Fictitious linear A matrix: A*x=f
A = [ 0, 1
     -2/5*x(1)^2+1, -3];
B = g;
C=[1,0];
matlabFunction(A, 'File','A_sys');

% Simulate data acquisition
x0 = [ 3; -9/2 ];
Tfin = 60;
tsampleSys = 0.05;
open('system_sis.slx');
out = sim('system_sis.slx');
tdata = out.y.Time; xdata = out.x.Data; ydata = out.y.Data; udata = out.u.Data;

%% Kalman filter sampled at tau=0.1 (exam request)
% Perform decimation to match the desired sampling time tau
ydecimated = ydata(1:2:end); udecimated = udata(1:2:end); 
tdecimated = tdata(1:2:end); xdecimated = xdata(1:2:end,:);
tau=0.1;

%% Extended Kalman Filtering
NoiseParametersAssumed=true;
if (NoiseParametersAssumed)
  Qd = 10*eye(2);
  Rd = 1;
else % Noise parameter known
  Qd = [0 0
	    0 0.01];    
  Rd = 0.01;
end

xhat(:,1) = [0,0]';
P = zeros(2);
for k = 2:size(ydecimated)

% System discretization trough forward euler method  
% I'm integrating so the result is the previous state (I)
% plus the sampled variation tau*A (sampler multiplies by sampling time)
  F = eye(2) + tau*A_sys(xhat(1,k-1));
  G = tau*B;
  H = C;

% prediction
  xp(:,k) = F*xhat(:,k-1);
  Pp = F*P*F' + Qd;
  
% update
  S = H*Pp*H' + Rd;
  K = Pp*H'*inv(S);

  e = ydecimated(k) - H*xp(:,k);
  xhat(:,k) = xp(:,k) + K*e;
  P = (eye(2) - K*H)*Pp;

  yhat(k) = xhat(1,k);
end

%% plot
figure, plot(tdecimated, xdecimated(:,:)), hold on, grid on,
plot(tdecimated, xhat(:,:)); title("Fictitious data comparison on simulated actual states")
legend('Actual x1', 'Actual x2', 'Filtered x1hat', 'Filtered x2hat', 'Location','south')

figure, plot(tdecimated, ydecimated), hold on, grid on,
plot(tdecimated, yhat); title("Real data comparison on collected noisy output")
legend('Noisy y','Filtered yhat')

RMSE = rms(xdecimated-xhat', "all")
