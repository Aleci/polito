function out1 = given_system(u1,x1,x2)
%GIVEN_SYSTEM
%    OUT1 = GIVEN_SYSTEM(U1,X1,X2)

%    This function was generated by the Symbolic Math Toolbox version 9.2.
%    20-Jul-2023 18:28:51

out1 = [x2;-u1+x1-x2.*3.0-x1.^3.*(2.0./5.0)];
