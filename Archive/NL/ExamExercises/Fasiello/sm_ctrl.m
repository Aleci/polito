function u = sm_ctrl(omegar,qr,q,omega)
    global J eta k1 k2;

    omegarDot=[0;0;0];
    
    omegaTilde=omegar-omega;
    qTilde=quat_error(qr,q);

    fs=omegaTilde+k2*sign(qTilde(1))*qTilde(2:4);
    us=J*(omegarDot+k2/2*(qTilde(1)*omegaTilde+crossprod(qTilde(2:4),omegar+omega)))+crossprod(omega,J*omega);
    u=us+k1*J*tanh(eta*fs);
end