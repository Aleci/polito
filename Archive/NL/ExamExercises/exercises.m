%% 1 Integrate differential equation
% clear all, close all, clc
% tau = 0.01; theta0=pi/4; thetadot0=0; tFin=20;
% open("pendolo.slx")
% sim("pendolo.slx")

%% 2 Is it an attractor limit cycle? (yes)
% clear all, close all, clc
% 
% % declare f2 below
% 
% x0 = [1;1];
% Tsim = 20;
% 
% [t,x]=ode45(@f2,[0,Tsim],x0);
% 
% plot(x(:,1),x(:,2))
% 
% dx1=gradient(x(:,1))
% dx2=gradient(x(:,2))
% q=quiver(x(:,1),x(:,2),dx1,dx2); % plot with arrows x1 and x2
% 
% function dx=f2(t,x)
%     dx = [x(2)
%         2*(1-x(1)^2)*x(2)-x(1)];
% end


%% 3 What kind of behaviour does the system show ? (Chaotic)
% clear all; close all; clc
% x1=10*rand()-5;
% x2=10*rand()-5;
% x3=5*rand();
% x0=[x1;x2;x3]
% 
% [t,x]=ode45(@f3,[0,20],x0)
% 
% plot(x(:,1),x(:,2)), hold on
% plot(x(1,1),x(1,2),"*g")
% plot(x(end,1),x(end,2),"*r")
% 
% function dx=f3(t,x)
%     dx=[10*(x(2)-x(1))
%         x(1)*(28-x(3))-x(2)
%         x(1)*x(2)-2.6667*x(3)]
% end

%% 4 Lyapunov stability (No question)

%% 5 Jacobian linearization of a pendulum

%% 6 Is the system stable at the origin? (yes, it's locally asymptotically stable)
% clear all, close all, clc
% x = sym("x",[2,1],'real');
% f=[ x(2); -x(2)-sin(x(1))];
% V = 2*(1-cos(x(1)))+(x(2)^2+(x(1)+x(2))^2/2);
% V_dot = gradient(V,x)'*f;
% figure(1),fsurf(V),title("V")
% figure(2),fsurf(V_dot),title("Vdot"),
% figure(3),fcontour(V_dot),title("Vdot"),colorbar

%% 7 Feedback linearization
% clear all; close all; clc; format short e
% sympref('FloatingPointOutput');
% x=sym('x',[3,1]); alpha=sym("alpha","real"); u=sym("u"); beta=sym("beta"); 
% R=sym("R"); rho=sym("rho");
% f=[ alpha*(x(2)-x(1)-rho)
%     x(1)-x(2)+x(3)
%     -beta*x(2)-R*x(3)];
% g=[0;1;0];
% h=x(1);
% [u,mu,ga,a,b,MU]=io_fl(f,g,h);
% u % control law
% mu % state transformation
% ga % relative degree
% intDyn=f(ga+1:end) % internal dynamics

%% 8 Sliding Mode Controller (free plus forced evolution)
% clear all, close all, clc, format short
% 
% x = sym('x',[3 1], 'real');
% syms u real;
% a = 10.82; b = 14.286; R = 0.01; c = [1.3 0.22]; 
% 
% q = -c(2)*sin(pi*x(1)/c(1));
% f = [a*(x(2)-q)
%     x(1)-x(2)+x(3)
%     -b*x(2)-R*x(3)];
% g = [0 1 0]';
% ff = f + g*u;
% h=x(1);
% C=[1 0 0];
% matlabFunction(ff,'File','fg_chua');
% x0 = [0.1 -0.2 0.15]'; % problem 1
% [t,sol]=ode23t(@mysys,[0,100],x0);
% 
% t(end)
% sol(end,:) % bha non torna
% figure(1),plot3(sol(:,1),sol(:,2,sol(:,3)));
% 
% % problem 2
% x0 = zeros(3,1); % problem 2
% 
% [u,mu,ga,a,b,MU]=io_fl(f,g,h,'chua');
% yr=ref_gen(ga,'1','chua');
% 
% tfin=100;
% 
% open("SM8.slx");
% out=sim("SM8.slx");
% 
% out.x.data(end,:)
% %plot3(out.x.data(:,1),out.x.data(:,2),out.x.data(:,3))
% 
% function dx=mysys(t,x)
%     dx=fg_chua(0,x(1),x(2),x(3));
% end

%% 9 Rotazione simbolica (Banale)
% clear all, close all, clc
% 
% syms phi theta psi real;
% T = rot_mat([2 1 2],[phi theta psi])


%% 10 Rotazione bastarda
% clear all, close all, clc
% 
% % Text:
% % o1=o2 rotated (2 different set of vectors)
% % => o1^2=T1^2*o2^2
% T = rot_mat([2 1 2],[0.3*pi -0.4*pi 1.5*pi]);
% 
% % Question:
% % given r^2 compute r^1 (same vector)
% % => r^1=T2^1*r^2=T1^2'*r^2
% r2 = [3 1 2]';
% r1 = T'*r2

%% 11 Compute rotation with quaternions
% clear all, close all, clc
% 
% % Text:
% % o1=o2 rotated (2 different set of vectors)
% % => o1^2=T1^2*o2^2
% T = rot_mat([2 1 2],[0.3*pi -0.4*pi 1.5*pi]);
% r2 = [3 1 2]';
% 
% % Question:
% % given r^2 compute r^1 (same vector)
% q=dcm2qua(T')
% r1=vec_rot_quat(q,r2)

%% 12 Attitude kinematics
% clear all; close all; clc
% w=[1.5,-0.3,2.1]; % omega const => qdot=1/2*Q*q LTI
% q=[0 w];
% %scrivi qua2omega copia q
% Q=[q(1) -q(2) -q(3) -q(4);
%     q(2) q(1) -q(4) q(3);
%     q(3) q(4) q(1) -q(2);
%     q(4) -q(3) q(2) q(1)];
% lambdas=eig(Q)
% % Re(lambdas)=0 with algMult>1 => not asymptotically stable
% minPolyRoots=roots(minpoly(Q)) % lambdas element appear just once within minPolyRoots => geoMult=1
% % geoMult=1 => the system is internally marginally stable

%% 13 Attitude dynamics
% clear all, close all; clc; format short e
% J=[1500 0 -1000; 0 2700 0; -1000 0 3000];
% [PrincDirections,PrincMoments]=eig(J)

%% 14 Orbital dynamics: Three body simulation around L4
% clear all; close all; clc;
% global D0 D2 omegap mu0 mu2;
% D0=4674e3;  D2=380073e3; omegap=2.662e-6;
% mu0=0.3986e15; mu2=4.9e12;
% x0=[187699500, 336532000, 0, 30, 0, 0];
% 
% [t,sol]=ode45(@fr3b,[0,3e7],x0);
% 
% plot(sol(:,1),sol(:,2),'r') % plot trajectory
% hold on;
% plot(-D0,0,'*b') % earth
% plot(D2,0,'*k') % moon
% % generate circle of radius D0+D2 in cartesian form
% th=linspace(0,2*pi);
% cx=(D0+D2)*cos(th); cy=(D0+D2)*sin(th);
% plot(cx-D0,cy) % earth circle
% plot(cx+D2,cy) % moon circle
% 
% function dx=fr3b(t,x)
%     global D0 D2 omegap mu0 mu2;
%     r1=x(1:3);
%     v1=x(4:6);
%     r10=[D0;0;0]+r1;
%     r12=r1-[D2;0;0];
%     dx=[ v1(1);
%          v1(2);
%          0 %v1(3);
%          2*omegap*v1(2)+omegap^2*r1(1)-mu0*(r1(1)+D0)/(norm(r10)^3)-mu2*(r1(1)-D2)/(norm(r12)^3)
%         -2*omegap*v1(1)+omegap^2*r1(2)-mu0*r1(2)/(norm(r10)^3)-mu2*r1(2)/(norm(r12)^3)
%          0]; %-mu0*r1(3)/(norm(r10)^3)-mu2*r1(3)/(norm(r12)^3)];
% end

%% 15 Orbital control: LEO-GEO transfer
% clear all; close all; clc;
% rp=6771; % km first orbit radius
% rg=42164; % km target orbit radius
% m0=60e3;
% 
% global MU RE ve;
% MU=4e5;
% RE=6370;
% ve=4.4;
% 
% % INITIAL CONDITION
% vp=sqrt(MU/rp);
% x0=[rp,0,0, 0,vp,0, m0]';
% 
% % REFERENCE 
% vg=sqrt(MU/rg);
% refRV=[rg,0,0, 0,vg,0]';
% ref=rv2oe(refRV,MU);
% 
% % NMPC
% par.model=@pred_model_15;
% par.n=7;
% 
% par.Ts=30;
% par.Tp=90;
% 
% par.P=diag([10,1e5*ones(1,4)]); % oe weights
% %par.Q=par.P/10;
% par.R=0.1*diag([1,1,1]); % u weights
% %par.tol=[10, 0.05, 0.05, 0.05, 0.05]';
% % par.ub=[132,132,132]; par.lb=[-132,132,132];
% 
% tFin=2*24*60*60; % 2 days hours
% 
% K=nmpc_design_st(par);
% 
% open("NMPC15.slx");
% out=sim("NMPC15.slx");
% %% Draw plots
% figure(1)
% plot(out.sol.data(:,1),out.sol.data(:,2));
% title("Orbit trajectory"),hold on
% 
% 
% % plot reference orbits
% th=linspace(0,2*pi,100);
% cx=cos(th); cy=sin(th);
% plot(rp*cx,rp*cy,'g');
% plot(rg*cx,rg*cy,'r');
% 
% figure(2)
% plot(out.tout,out.sol.data(:,7))
% yline(4000,'r'), title("Fuel consumption")
% 
% figure(3)
% plot(out.u.Time,out.u.data(:,1),out.u.Time,out.u.data(:,2), ...
%     out.u.Time,out.u.data(:,3)), title("Command activity");
% hold on, yline(132,"r"), yline(-132,"r")
