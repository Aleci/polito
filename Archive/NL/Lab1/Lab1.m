clear all; close all; clc;

% Define symbolic variables
x=sym('x',[3 1],'real');
syms u real

% Define the system
a=10;
b=100/7; 
R=0;
%
rho=2/7*x(1)^3-8/7*x(1);
%
f=[a*(x(2)-x(1)-rho)
   x(1)-x(2)+x(3)+u
   -b*x(2)-R*x(3)];

%% Exercise 1 (Find equilibria for u=0, linearize and check stability)
f1=subs(f,u,0);
[x01,x02,x03]=solve(f1); % 3 equilibria found
x0=double([x01,x02,x03]);

% Compute the symbolic jacobian
jacf=jacobian(f1);

% For each equilibrium
for eqj=1:length(x0)
  equilibrium=x0(eqj,:)
  % Compute the linearized system 
  linmod{eqj}=double(subs(jacf,x,equilibrium'));
  % Check stability
  eigenvaules=eig(linmod{eqj}) % all unstable equilibria
end

%% Exercise 2 (Simulate the free evolution of the system from two close initial conditions)
% Define simulation data
% generate a function block callable from simulink chuasystem(u,x)
matlabFunction(f,'File','chuasystem'); 
Tfin=50;
x0sim=x0(1,:)'+[0.001;0.001;0.001];
x0sim1=x0(1,:)'+ones(3,1)*0.005;

% Simulate
open('simFree')
out=sim('simFree')         

% Generate 3d plot of the phase plane
figure(1),grid, hold on
% plot equilibrium points
for eqj=1:length(x0)
  plot3(x0(eqj,1),x0(eqj,2),x0(eqj,3),"r*")
end
% plot state signals
plot3(out.xsim.data(:,1),out.xsim.data(:,2),out.xsim.data(:,3))
plot3(out.xsim1.data(:,1),out.xsim1.data(:,2),out.xsim1.data(:,3))
view(20,50)

%% Exercise 3 (synthetize a controller and simulate in feedback)
eq=x0(1,:)';
K=place(linmod{1},[0;1;0],[-1,-2,-3]);     % place(A,B,eigDes)

open('simFeedback')
out=sim('simFeedback')         

% Generate 3d plot of the phase plane
figure(2),grid, hold on
% plot equilibrium points
for eqj=1:length(x0)
  plot3(x0(eqj,1),x0(eqj,2),x0(eqj,3),"r*")
end
% plot state signals
plot3(out.xsim.data(:,1),out.xsim.data(:,2),out.xsim.data(:,3))
plot3(out.xsim1.data(:,1),out.xsim1.data(:,2),out.xsim1.data(:,3))
view(20,50)
