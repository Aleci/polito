clear all
close all
clc

% Declaration of symbolic variables
x=sym('x',[3 1],'real');
syms u real

%% System definition

a=10;
b=100/7; 
R=0;

q=2/7*x(1)^3-8/7*x(1);

figure
ezplot(q)
xlabel('$x_1$','interpreter','latex','fontsize',20)
ylabel('$\rho(x_1)$','interpreter','latex','fontsize',20)

f=[a*(x(2)-x(1)-q)
   x(1)-x(2)+x(3)
   -b*x(2)-R*x(3)]
g=[0;1;0]
h=x(1);
C=[1 0 0];
ff=f+g*u;

matlabFunction(ff,'File','fg_chua');
% This command allows you to generate a Matlab function starting from the
% symbolic definition of the functions. This operation is not really
% required if you don't neet symbolic variables for other reasons. 
% You can directly write the Matlab function.

%% Exercise 1

% equilibrium points
[xe1,xe2,xe3]=solve(f);     
for i=1:length(xe1)
    xeq(:,i)=double([xe1(i); xe2(i); xe3(i)])
end

F=jacobian(ff,x)            % linearization

for i=1:length(xe1) 
    %xeq(:,i)=double([xe1(i); xe2(i); xe3(i)])
    subs(ff,[x;u],[xeq(:,i);0]) 
    As=subs(F,x,xeq(:,i))
    A{i}=double(As);
    eig(A{i})                  % stability study
end

% return
%%  Exercise 2 

Tfin=50;
x0=randn(3,1)*0.01
xp0=x0+[0.01;0;0];

open('chua_sim')
sim('chua_sim')         % simulation

figure
grid, hold on
for i=1:length(xe1)
    plot3(xeq(1,i),xeq(2,i),xeq(3,i),'.k','markersize',14)  % equilibrium points
end
plot3(x(:,1),x(:,2),x(:,3))             % trajectory
plot3(xp(:,1),xp(:,2),xp(:,3))          % perturbed trajectory
xlabel('$x_1$','interpreter','latex','fontsize',20)
ylabel('$x_2$','interpreter','latex','fontsize',20)
zlabel('$x_3$','interpreter','latex','fontsize',20)
view(20,50)

return
%% Exercise 3

ie=1;
K=place(A{ie},g,[-1,-2,-3]);     % linear controller

Tfin=30;
x0=randn(3,1)*0.2

open('chua_c_sim')
sim('chua_c_sim')         % simulation

figure
grid, hold on
for i=1:length(xe1)
    plot3(xeq(1,i),xeq(2,i),xeq(3,i),'.k','markersize',14)  % equilibrium points
end
plot3(x(:,1),x(:,2),x(:,3))             % trajectory
xlabel('$x_1$','interpreter','latex','fontsize',20)
ylabel('$x_2$','interpreter','latex','fontsize',20)
zlabel('$x_3$','interpreter','latex','fontsize',20)
view(20,50)


