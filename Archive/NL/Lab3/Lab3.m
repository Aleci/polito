% Workspace initialization
clear all; close all; clc;
global K; global refs; global eta
global knownModel; global slidingMode
x=sym('x',[3 1],'real');
u=sym('u',[1 1],'real');

% Simulation settings
slidingMode=true;
knownModel=false;
eta=1; % If you increase this the tanh behaves more like a sign function
ref=-3;
tSim=60;

%% Plant definition
a=10.4;
b=16.5;
R=0.1;
q=-1.16*x(1)+0.041*x(1)^3;  % Real non linearity
f=[a*(x(2)-x(1)-q)
    x(1)-x(2)+x(3)
    -b*x(2)-R*x(3)];
qm=-1.194*x(1)+0.0422*x(1)^3;  % Non linearity model used in the design phase
fm=[a*(x(2)-x(1)-qm)
    x(1)-x(2)+x(3)
    -b*x(2)-R*x(3)];
g=[0;1;0];
h=x(1);
C=[1 0 0];

% Create a function in a file from the symbolic definition
matlabFunction(f+g*u,'File','chuaSys','Vars',[u;x]);

%% Feedback linearization 
% non_linear_control_action = u_$NAME([linear_control_action; state])
% y_and_derivatives = mu_$NAME(state)
[u,mu,r]=io_fl(f,g,h,'chua');    % Correct formula based of complete knowledge of the system
[u,mu,r]=io_fl(fm,g,h,'chuaMdl'); % Approximated formula based of the available model

%% Controller desing
K=[80;50];

%% Simulation
t=[0,tSim]; % set the simulation time interval
refs=[ref;0;0]; 

% Integrate differential equations
[t,xe]=ode45(@feedbackLinCtrl, t,[0;0;0]);
y=xe(:,1); %% the output is the first state
for k=1:length(t), uSig(k)=controlAction(xe(k,:)'); end % Recompute the actual control action required

% Print result
subplot(2,1,1),plot(t,y),title("Output");
subplot(2,1,2),plot(t,uSig),title("Control activity");

% Feedback linearization control system with and without sliding mode
function dState=feedbackLinCtrl(t,state)
  % Compute the control action from the current state
  u=controlAction(state);
  
  % Compute the state differential
  dState=chuaSys(u,state(1),state(2),state(3));
end

function u=controlAction(state)
  global K; global refs; global eta
  global knownModel; global slidingMode

  % Rename relevant signals
  if knownModel
    mu=mu_chua(state);     % y and derivatives until gamma-1  (correct formula)
  else
    mu=mu_chuaMdl(state);     % y and derivatives until gamma-1  (approximated formula)
  end
  mu_r=refs(1:end-1);    % ref and derivatives until gamma-1
  refGamma=refs(end);    % ref derived gamma times
  mu_err=mu_r-mu;
  % Compute control action
  if slidingMode
    % Compute sliding mode control action
    slidingSurface=mu_err(2) + K(2)*mu_err(1); % ytilde_dot + K2*ytilde
    v=refGamma + K(2)*mu_err(2) + K(1)*tanh(eta*slidingSurface);
  else
    % Compute the proportional control action
    v=refGamma + K'*mu_err;
  end
  % Feedback linearize the system 
  if knownModel
    % Compute the correct non linear control action
    u=u_chua([v;state]);
  else
    % Compute the non linear control action according the available model
    u=u_chuaMdl([v;state]);
  end
end
