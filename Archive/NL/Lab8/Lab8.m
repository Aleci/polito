%% Exercise 1
clear all; close all; clc; format short
MU=4e14;

% initial conditions
r0=[6.54e6;0;0];
v0=[0;7.88e3;0];
x0=[r0;v0];

[t,x]=ode45(@(t,x) fr2b(t,x,MU),[0,2e4],x0)

[oEnd,~,th]=rv2oe(x',MU)

plot(t,th) % sticazzi dei plot che chiede, a

%% Exercise 2
clear all; close all; clc; format short
MU=4e14

a=40e6
e=0.5
i=50*pi/180
Omega=30*pi/180
omega=100*pi/180

oel=[a,e,i,Omega,omega];

th=linspace(0,2*pi,300);

[r,~,~]=oe2rv(oel,th,MU);

% Rendering
figure, daspect([1 1 1]), view(110,20), hold on
RE=6.38e6;
[Xph,Yph,Zph]=sphere(50);
mesh(RE*Xph,RE*Yph,RE*Zph);
plot3(r(1,:),r(2,:),r(3,:)),xlabel("xGE"),ylabel("yGE"),zlabel("zGE")

