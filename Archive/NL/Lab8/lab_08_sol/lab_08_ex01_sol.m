 clc 
clear all
close all

% Parameters 
MU=0.3986e15;
RE=6.378137e6;

% Initial conditions
r0=[6.54e6;0;0];
v0=[0;7.88e3;0];
x0=[r0;v0];

%% Simulation 

t=linspace(0,2e4,1e3);

[~,x]=ode23s(@(t,x)fr2b(t,x,MU),t,x0);

r=x(:,1:3);
v=x(:,4:6);
h=crossprod(r',v')';
E=vecnorm(v,2,2).^2/2-MU./vecnorm(r,2,2);

%% Figures
close all

figure('Name','Orbit','NumberTitle','off');
[Xph,Yph,Zph] = sphere(100);
mesh(RE*Xph,RE*Yph,RE*Zph);
hold on
plot3(r(:,1),r(:,2),r(:,3),'k','LineWidth',1)
axis equal
grid on
xlabel('x [m]')
ylabel('y [m]')
zlabel('z [m]')

figure('Name','Position')
stairs(t,r)
grid on
xlabel('time [s]')
ylabel('Position [m]')

figure('Name','Velocity')
stairs(t,v)
grid on
xlabel('time [s]')
ylabel('Velocity [m/s]')

figure('Name','Angular Moment')
stairs(t,h)
grid on
xlabel('time [s]')
ylabel('Angular Moment [m^2/s]')

figure('Name','Energy per unit mass')
stairs(t,E)
grid on
xlabel('time [s]')
ylabel('Energy pum [J/kg]')
ylim([-3.5e7 0])

