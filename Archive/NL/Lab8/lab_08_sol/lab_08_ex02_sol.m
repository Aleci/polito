clear all
close all
clc

%% Point 1

a=40e6
e=0.5
i=50*pi/180
Omega=30*pi/180
omega=100*pi/180

p=a*(1-e^2);

th=linspace(0,2*pi,100);
rs=p./(1+e*cos(th));

r=[rs.*cos(th)
	rs.*sin(th)
	zeros(size(th))];

T=rot_mat([3 1 3],[Omega i omega]);
r1=T*r;

figure, daspect([1 1 1]), view(110,20), hold on
RE=6.38e6;
[Xph,Yph,Zph]=sphere(50);
mesh(RE*Xph,RE*Yph,RE*Zph);
% plot3(r(1,:),r(2,:),r(3,:))
plot3(r1(1,:),r1(2,:),r1(3,:))
fs=22;
xlabel('$x_{GE}$ [m]','interpreter','latex','fontsize',fs)
ylabel('$y_{GE}$ [m]','interpreter','latex','fontsize',fs)
zlabel('$z_{GE}$ [m]','interpreter','latex','fontsize',fs)

%% Point 2

%global mu
mu=0.3986e15;
P=2*pi*sqrt(a^3/mu);

v=[0;sqrt(mu/p)*(e+1);0];
v1=T*v

x0=[r1(:,1);v1];

t=linspace(0,2*P,1000);
[~,x]=ode23(@(t,x)fr2b(t,x,mu),t,x0); 

plot3(x(:,1),x(:,2),x(:,3),'r')

