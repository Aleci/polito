clear all
close all
clc

%% HCW equations
MU=0.3986e15;                     
RE=6.38e6;
a=RE+400e3;
w=sqrt(MU/a^3);

A= [0 0 0 1 0 0
    0 0 0 0 1 0
    0 0 0 0 0 1
   3*w^2 0 0 0 2*w 0
   0 0 0 -2*w 0 0
   0 0 -w^2 0 0 0];

B=[zeros(3);eye(3)];

H=ss(A,B,eye(6),zeros(6,3));

eigA=eig(A)

%% NMPC design
par.model=@pred_model;
par.nlc=@nl_constraints;
par.n=6;
par.Ts=3;
par.Tp=100;
par.Q=0.1*eye(6);
par.P=10*eye(6);
par.R=1*eye(3);
par.lb=-1*ones(3,1);
par.ub=1*ones(3,1);

K=nmpc_design_st(par);

%% Simulation initialization
ref=[20 0 0 0 0 0]';
x0=[-500; 0; 0;0;0;0];
tfin=1000;

open('nMPC.slx');
mysim=sim('nMPC.slx');

%% Plot results
pos=mysim.pos.data;
plot3(0,0,0,"r*"), hold on, plot3(pos(:,1),pos(:,2),pos(:,3),"k")


