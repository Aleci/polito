function F = nl_constraints(x,y)
% NMPC constraint function
% x: state of the system; matrix of dimension nx*N.
% y: output of the system; matrix of dimension ny*N.
% N is the number of samples in the time interval [t,t+Tp]
% N is automatically chosen by the NMPC solver.
% F: constraint function; Nc*N matrix, where
% Nc is the number of constraints.
% Constraints are written in the standard form F(x,y)<=0.

  %F(1,:) = 0; % no constraints
  %F(1,:) = 10 - norm(y); % usa sempre le x, meno sbatti sul simulink
  F(1,:)=10-vecnorm(x(1:3,:));
end



