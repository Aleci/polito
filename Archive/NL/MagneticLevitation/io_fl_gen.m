function [uSel,uSol,mu,gamma,a,b,MU]=io_fl_gen(f1,f2,h,uSym,nome,sel)
% f1: state gradient wrt the states
% f2: invertible state gradient wrt the control input
% h: output mapping
% uSym: symbolic variable wrt the equation must be inverted  
% nome: name of the Matlab function created from the symbolic expressions 
% sel: select the solution to be written in the file (index of solU.u)
% solU: feedback linearization control law
% mu: state transformation
% gamma: relative degree
% MU: augmented state transformation (MU(1:r)=mu)
% a,b: functions of the normal form

n=size(f1,1);
mu=sym('mu',[n 1],'real');
MU=sym('MU',[n 1],'real');
syms v real

[~,Lf1h]=lieder(f1,h,n);
gamma=1;

for i=0:n-1
    if i==0
        Lf2h{i+1}=lieder(f2,h,1);
        mu(i+1)=h;
    else
        Lf2h{i+1}=lieder(f2,Lf1h{i},1);
        mu(i+1)=Lf1h{i}+Lf2h{i};
    end
    if isequal(Lf2h{i+1},sym(0))
        gamma=i+2;
    end
end

if gamma==1
    b=lieder(f2,h,1);
else
    b=lieder(f2,Lf1h{gamma-1},1);
end
a=Lf1h{gamma};
uSol=solve(v==a+b,uSym,ReturnConditions=true);

MU=mu(1:n);
mu=mu(1:gamma);

uSel="sel input not used";

if nargin>3 && ~isempty(nome) && sel>0
    uSel=uSol.u(sel);
    x=sym('x',[n 1],'real');
    matlabFunction(uSel,'File',['u_' nome],'Vars',{[v;x]});
    matlabFunction(mu,'File',['mu_' nome],'Vars',{x});
end

end
