%% ============================ COMMON DATA ==========================
%% Workspace initialization
clear all
close all
clc
sympref('FloatingPointOutput',true);

%% Declaration of symbolic variables
x=sym('x',[2 1],'real');
syms u

m=.02;              % mass of the ball [kg]
g=9.81;             % gravity [m/s^2]
Voff=1.973;         % bias voltage [V]
Hp=708.27;          % Sensor constant [V/m]
Km=1.52e-4;         % Magnet constant [Nm^2/A^2]
Kamp=0.406;         % Amplification constant [A/V]
z0=0.022;           % equilibrium [m]
I0=z0*sqrt(m*g/Km); % bias current [A]
u0=I0/Kamp-Voff;    % input at equilibrium [V]

%% Plant definition
f1=[ x(2) 
      g  ];                             % state contribution to the state gradient
f2=[               0
    - Km/m*(Kamp*(u+Voff))^2/x(1)^2 ];  % actuation contribution to the state gradient
f=f1+f2;                                % complete state gradient
h=Hp*x(1)-z0;                           % sensor function
%
matlabFunction(f,'File','plantModel','Vars',[u;x]);



%% ============== Simulation settings =============================
tFin=10; t=linspace(0,tFin,300); initVel=0;

%% Reference trajectories generation
global refs;
refs(:,1)=t;

initPos=0.02; % Initial position
Mid=0.02;    % Working point
Am=0.007;  % Amplitude
Fr=.5;      % Frequency
omegaCut=200; % Omega

Om=2*pi*Fr;     % Pulsation
%refs(:,2)=Mid*ones(size(t)); reftitle="Constant reference at " + -Mid*100 + " cm, Initial position: "+initPos*100+" cm"; 
refs(:,2)=Mid+Am*sin(Om*t); reftitle="Sinusoidal - Amplitude: "+Am*100+" cm, Frequency: "+Fr+" Hz, WorkingPoint: "+Mid*100+" cm, Initial position: "+initPos*100+" cm";
%refs(:,2)=Mid+Am*2*(ceil(2*sin(Om*t)/10)-.5); reftitle="Rectangular - Amplitude: " + Am*100 + " cm, Frequency: " + Fr + "Hz, WorkingPoint: " + Mid*100+" cm, Initial position: "+initPos*100+" cm";
%refs(:,2)=movmean(refs(:,2),5); reftitle="LPFed " + reftitle;

refs(:,3)=gradient(refs(:,2));
refs(:,4)=gradient(refs(:,3));
%-------------------------------------------------------------------

%% ======================= SLIDING MODE ===============================
global data;
data.Hp=Hp; data.z0=z0; data.ub=10; data.lb=-Voff;
[data.A,data.B,data.C,data.D]=tf2ss([1 0],[100 1]);

%% Feedback linearization
[uSel,uSol,mu,ga] = io_fl_gen(f1,f2,h,u,'magnet_fl',2); % Generailized fl function for f2 not affine but invertible

%% Sliding mode controller
global K; global eta;
eta=1000; K=[6000;300];

%% Integrate differential equations
[t,xSol]=ode45(@feedbackLinCtrl, t,[initPos;initVel;0]);
y=xSol(:,1); %% the output is the first state

%% Recompute hidden signals
for k=1:length(t), uSig(k)=controlAction(t(k),xSol(k,:)'); end % Recompute the actual control action required
I=Kamp*(uSig+Voff);

%% Plot results
figure(3),subplot(2,2,1),plot(t,-xSol(:,1)*100)
hold on, plot(t,-refs(:,2)*100), ylabel("Centimeters"),
title("Actual Ball Distance From The Magnet (Sliding Mode)");
subplot(2,2,3),plot(t,I), title(["Control Current";"K:"+mat2str(K,4)+" Eta:"+mat2str(eta,4)]),xlabel("Seconds"),ylabel("Ampere")



%% =================== PID =============================================
%% Jacobian calculation
A=jacobian(f,x);
B=jacobian(f,u);

%% System linearization at the equilibrium
Aj=double(subs(A,[x; u],[z0;0;u0]));
Bj=double(subs(B,[x; u],[z0;0;u0]));
Cj=double([Hp 0; 0 0]); % The controller expects a reference Hp times bigger
Dj=[0;0];
[n,d]=ss2tf(Aj,Bj,Cj,Dj); %A has two rows but just one is controllable
linSys=tf(n(1,:),d);

%% Control system synthesis
C=pidtune(linSys,"pidf",omegaCut);

%% Simulation on the linearized model 
ctrlSysLoop=feedback(C*linSys,1);
%figure(1),step(ctrlSysLoop),title("Linearized Simulation of Ball Position");

%% Controller discretization for actual implementation
discPid=c2d(C,0.001,'zoh');
[num,den]=tfdata(discPid);

%% Real plant simulation for linear controller
global myPid; global ref; global eq; global HP; global uBounds;
eq=z0; HP=Hp; uBounds.ub=10; uBounds.lb=-Voff;
ref=[refs(:,1), refs(:,2)];

% Convert the pid data type into an other, suitable for a non simulink environment
myPid.Kp=C.Kp; myPid.Ki=C.Ki; myPid.Kd=C.Kd;
[myPid.A,myPid.B,myPid.C,myPid.D]=tf2ss([1 0],[C.Tf 1]);
%
% Integrate the solution
[~,xSol]=ode45(@linCtrlSys, t, [initPos;initVel;0;0]);
% 
% Recompute hidden signals
for k=[1:length(t)]
  r(k)=getFromTrajectory(t(k),ref);
  err(k)=r(k)-xSol(k,1);
  u(k)=pidController(myPid, err(k), xSol(k,3), xSol(k,4),uBounds);
end
I=Kamp*(u+Voff);

%% Plot results
figure(3),subplot(2,2,2),plot(t,-xSol(:,1)*100)
hold on, plot(t,-ref(:,2)*100), ylabel("Centimeters")
title("Actual Ball Distance From The Magnet (PID)");
subplot(2,2,4),plot(t,I), title(["Control Current - Omega Tuning: "+omegaCut;"Num:"+mat2str(num{1},4)+" Den:"+mat2str(den{1},4)])
xlabel("Seconds"),ylabel("Ampere"), sgtitle(reftitle);
saveas(gcf,"Figures/"+string(datetime)+".png")



%% ================= Control schemes =========================

% Control system with the linear controller
function dState=linCtrlSys(t,state)
  global myPid; global ref; global eq; global HP; global uBounds;
  % State1=ball_position; State2=ball_velocity;
  % State3=error_integral; State4=error_causal_derivative
  
  % Controller inputs
  r=getFromTrajectory(t,ref);
  y=HP*(state(1)-eq); % sensor measurement 

  % Rename important signals
  yHat=y + HP*eq; % simplest noise free estimator
  
  %y=HP*(state(1)-r); xHat=y; % fallback solution

  err=HP*r-yHat;
  integralOfErr=state(3);

  % Compute realizable derivative (pidf case)
  derState=myPid.A*state(4)+myPid.B*err;
  derOut=myPid.C*state(4)+myPid.D*err;

  % Compute saturated control action
  u=pidController(myPid, err, integralOfErr, derOut, uBounds);
  
  % Compute plant state variations
  dPlantState=plantModel(u,state(1),state(2));

  % Return state variation vector
  dState=[dPlantState; err; derState];
end

% Pid control action computation
function u=pidController(myPid, err, integralOfErr, derivativeOfErr, bounds)
  % Compute control action
  u=myPid.Kp*err +myPid.Ki*integralOfErr +myPid.Kd*derivativeOfErr;

  % Saturate control action
  u( find(u > bounds.ub) ) = bounds.ub;
  u( find(u < bounds.lb) ) = bounds.lb;
end

% Control system with feedback linearization
function dState=feedbackLinCtrl(t,state)
  global data;
  % State1=ball_position; State2=ball_velocity;
  % State3=output_causal_derivative;

  % Compute position derivative 
  x_dot_derivative=data.A*state(3)+data.B*state(1);
  
  % Compute the control action from the current state
  u=controlAction(t,state);
  
  % Compute the state differential
  dState=[plantModel(u,state(1),state(2))
	  x_dot_derivative];
end

% Compute the nonlinear control action of the Sliding Mode controller
function u=controlAction(t,state)
  global K; global refs; global eta; global data;

  % Controller inputs
  ref=getFromTrajectory(t,[refs(:,1), refs(:,2)]);
  ref_dot=getFromTrajectory(t,[refs(:,1), refs(:,3)]);
  ref_dotdot=getFromTrajectory(t,[refs(:,1), refs(:,4)]);
  y=data.Hp*(state(1)-data.z0); % this is the measurement obtained from the sensor
  
  % Rename relevant signals
  xHat_dot=data.Hp*(data.C*state(3)+data.D*state(1));   % Velocity computed from measurements HPFed
  xHat=1/data.Hp*(y+ data.Hp*data.z0); % simplest noise free estimator
  xHatTilde=ref - xHat;
  xHatTilde_dot=xHat_dot-ref_dot;
  
  % Compute sliding mode control action
  slidingSurface=xHatTilde_dot + K(2)*xHatTilde;
  v=ref_dotdot + K(2)*xHatTilde_dot + K(1)*tanh(eta*slidingSurface);
  
  % Feedback linearize the system 
  u=u_magnet_fl([v;[xHat;0]]);
  
  % Saturate control action
  u( find(u > data.ub) ) = data.ub;
  u( find(u < data.lb) ) = data.lb;
end
