%% COMMANDS CHEATSHEET
clear all; close all; clc

%% KINEMATICS COMMANDS

% Create rotations
R=rotz(pi/2)
R2=R*roty(pi/2)
R3=eul2r(.1,.2,.3)

% Derive euler angles
eul=tr2eul(R2)
rpy=tr2rpy(R3)

% Create homogeneus matrix of a translation
Ttran=transl(1,2,1)

% Create homogeneus matrix of a rotation
Trot=troty(pi/5)

% Create a generic homogeneus matrix
T=Ttran*Trot

% Extract rotation from homogeneus matrix
R4=t2r(T)

% Plot result
figure(1),tranimate(R3,"rgb");
figure(2),trplot(T,"rgb");

%% ROBOT SIMULATION TOOLS

% Links declaration
L(1)=Revolute('a',1), L(1).A(0)
L(1).offset=.5; L(1).A(0)
L(2)=Revolute('a',2)

% Robot declaration
robot=SerialLink(L)

% Evaluate forward kinematics function in specific point
robot.fkine([0 0])

% Move the robot
robot.plot([2 2])

%% ESERCIZIO KINEWITHOUTSOLUTIONS
clear all; close all; clc

% Define robot
l(1)=Revolute('a',1)
l(2)=Revolute('a',1,'d',.5)

robotEx=SerialLink(l)
robotEx.name=['Robot']

% Plot inverse and direct kinematics evaluated in the point
dir9090=robotEx.fkine([pi/2 pi/2])
xyz9090EndEffector=dir9090*[0 0 1]'w


% Display robot
robotEx.plot([pi/2 pi/2])











