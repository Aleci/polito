%% Initialization
clear all; close all; clc
rng('default');

%% Output generation from unknown system
theta0=[-.93; 1.5; -3];
N=500;
u=-10 + 20.*rand(N,1);
stddev=3;
e=stddev.*randn(N,1);

for k=1:N
    if(k==1)
        fi(1,:)=[0; 0; 0];
    elseif (k==2)
        fi(2,:)=[y(k-1) u(k-1) 0];
    else
        fi(k,:)=[y(k-1) u(k-1) u(k-2)];
    end
    y(k,1)=fi(k,:) * theta0 + e(k);
end


%% Estimate theta0 using LS
for k=2:N
    theta(:,k)=pinv(fi(1:k,:))*y(1:k);
end
thetaHat=theta(:,N);

%% Whiteness test for thetaHat
yHat=fi*thetaHat;
err=y-yHat;
err_avg=mean(err);
confidenceLevel=.954;
nSkip=10;
% automatic detection
[r,lags]=xcorr(err,'normalized');
expectedSTDev=1/sqrt(N); % autocorrelation of white noise should be like this
maxErr=sqrt(2)*erfinv(confidenceLevel) * expectedSTDev;
rPrime=abs(r(N:end))-maxErr; %consider just positive lag index
nOut=sum(rPrime(20:end) > 0)
nOutMax=floor((1-confidenceLevel)*N)
% manual detection
figure,plot(lags(N:end),r(N:end))
yline(maxErr),yline(-maxErr)

%% Present results
x=linspace(1,N,N);
figure
for k=1:3
    subplot(3,1,k)
    plot(x,theta0(k)*ones(N,1),x,theta(k,:))
end

figure,plot(x,y,x,yHat)
figure,plot(x,err),yline(err_avg)