%% Initialize workspace
clear all; close all; clc
format short e
load dryer2.mat

%% Settings
N=500;
Nv=1000-N;
% data split
ue=u2(1:N);
uv=u2(N+1:end);
ye=y2(1:N);
yv=y2(N+1:end);
% data unbiasing
ue=ue-mean(ue);
uv=uv-mean(uv);
ye=ye-mean(ye);
yv=yv-mean(yv);
% data check
%figure, plot(1:N,ue,N+1:1000,uv)
%figure, plot(1:N,ye,N+1:1000,yv)
ed=[ye,ue];
vd=[yv,uv];

skip=10;

%% arx
arxi=1;
for k=1:4
    for n=1:7
        m=arx(ed,[n,n,k]);
        % White test on estimation dataset
        if(whiteTest(m,ed,30))
            fprintf("ARX %-2d: n=%d, nk=%d\n",arxi,n,k)          
            arxi=arxi+1;
        end

        % Cross validation on validation dataset
        [arxfpe(n,k) arxaic(n,k) arxmdl(n,k) arxbf(n,k)]=crossValidation(vd,m,n*2,skip);
    end
end

easyPlot(arxfpe,"arx fpe") %almost best k=3 na=nb=2
easyPlot(arxaic,"arx aic") %best tradeoff k=3 na=nb=2
easyPlot(arxmdl,"arx mdl") %best tradeoff k=3 na=nb=2
easyPlot(arxbf,"arx bf") %best tradeoff k=3 na=nb=2

%% armax
armaxi=1;
for k=1:4
    for n=1:7
        m=armax(ed,[n,n,n,k]);
        % White test on estimation dataset
        if(whiteTest(m,ed,30))
            fprintf("ARMAX %-2d: n=%d, nk=%d\n",armaxi,n,k)          
            armaxi=armaxi+1;
        end

        % Cross validation on validation dataset
        [armaxfpe(n,k) armaxaic(n,k) armaxmdl(n,k) armaxbf(n,k)]=crossValidation(vd,m,n*3,skip);
    end
end

easyPlot(armaxfpe,"armax fpe") %almost best k=3 na=nb=nc=2
easyPlot(armaxaic,"armax aic") %best tradeoff k=3 na=nb=nc=2
easyPlot(armaxmdl,"armax mdl") %almost best k=3 na=nb=nc=2
easyPlot(armaxbf,"armax bf") %almost best k=3 na=nb=nc=2

%% oe
oei=1;
for k=1:4
    for n=1:7
        m=oe(ed,[n,n,k]);
        % White test on estimation dataset
        if(whiteTest(m,ed,30))
            fprintf("OE %-2d: n=%d, nk=%d\n",oei,n,k)          
            oei=oei+1;
        end

        % Cross validation on validation dataset
        [oefpe(n,k) oeaic(n,k) oemdl(n,k) oebf(n,k)]=crossValidation(vd,m,n*2,skip);
    end
end

% NO WHITE TEST IS PASSED FOR THE BEST MODELS IN THE CROSS VALIDATION!
easyPlot(oefpe,"oe fpe") %almost best k=3 nb=nf=2
easyPlot(oeaic,"oe aic") %almost best k=3 nb=nf=2
easyPlot(oemdl,"oe mdl") %almost best k=3 nb=nf=2
easyPlot(oebf,"oe bf") %almost best k=3 nb=nf=2

%% Functions
function easyPlot(test,mytitle)
    figure,plot(1:7,test(:,1),1:7,test(:,2),1:7,test(:,3),1:7,test(:,4)) 
    title(mytitle),legend("k=1","k=2","k=3","k=4")
end

function [fpe aic mdl bf]=crossValidation(dataset,m,order,skip)
    y=dataset(:,1);
    N=length(y);
    yhat=compare(dataset,m,1);
    err=y-yhat;
    err=err(skip+1:end);
    mse=mean(err.^2);
    fpe=fpeTest(m,mse,order,N-skip);
    aic=aicTest(m,mse,order,N-skip);
    mdl=mdlTest(m,mse,order,N-skip);
    bf=bfTest(m,mse,y,order,N-skip);
end

function bf=bfTest(m,mse,y,n,N)
    bf=1-sqrt(mse/mean( (y-mean(y)).^2 ));
end

function mdl=mdlTest(m,mse,n,N)
    mdl=n*log(N)/N+log(mse);
end

function aic=aicTest(m,mse,n,N)
    aic=n*2/N+log(mse);
end

function fpe=fpeTest(m,mse,n,N)
    fpe=(N+n)/(N-n)*mse;
end
    
% anderson white test
function passed=whiteTest(m,dataset,samples)
    confidence=.99;
    % output prediction
    y=dataset(:,1); N=length(y); 
    yhat=compare(dataset,m,1);
    % prediction error
    err=y-yhat;
    % error autocorrelation in absolute value
    [r,lag]=xcorr(err,'normalized');
    rPrime=abs(r(N+1:end)); %skip lag 0
    % limit due to the expected variance [1/sqrt(N)]
    limit=sqrt(2)*erfinv(confidence)/sqrt(N);
    % test
    nOut=sum(rPrime(1:samples) > limit);
    % usually the professor is more generous than the definition
    nOutMax=floor(samples*(1-confidence)); 
    nOutMax=5;
    passed= nOut <= nOutMax;
    if false % resid plot suppression
       figure,plot(1:samples+1,r(N:N+samples),"*"),refline(0,limit); 
    end
end