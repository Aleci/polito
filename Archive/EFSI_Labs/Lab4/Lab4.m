%% Lab Settings
clear all; close all; clc

% Load input signal
load("data.mat")
N=4000;

% System data
A= [0.96, 0.5, 0.27, 0.28;
   -0.125 0.96 -0.08 -0.07;
     0,   0,    0.85, 0.97;
     0,   0,      0,  0.99];
B= [1; -1; 2; 1];
C= [0 2 0 0];
D= 0;
n=4;

% Noise data
% v1 (zero mean white noise in state equation)
Cholv1=sqrt(15)*[0.5; 0; 0; 1];
V1=Cholv1*Cholv1';
% v2 (zero mean white noise in output equation)
V2=2000;
% uncorrelated
V12=0;

% Initial variance
P0=0.5*eye(4);
% Initial state (assumed as possible initial realization)
x0=[30; 40; -70; -10];

%% Create noise data and output
rng("default"); %generate the same random numbers for debug
v1=mvnrnd(zeros(1,n),V1,N);
%cov(v1) %check ans≃V1
v2=sqrt(V2)*randn(N,1); %change variance from unitary to V2 of the gaussian distribution
%cov(v2)  %check ans≃V2

x(:,1)=x0;
for k=1:N
    x(:,k+1)=A*x(:,k)+B*u(k)+v1(k);
    y(k)=C*x(:,k)+D*u(k)+v2(k);
end

%% Dynamic 1 step Kalaman predictor and filter
% guessed initial conditions
x_h=zeros(n,1); P{1}=P0;

for k=1:N
% dynamic predictor K
    y_h(k)=C*x_h(:,k);
    e(k)=y(k)-y_h(k);
    K{k}=(A*P{k}*C'+V12)*inv(C*P{k}*C'+V2);
    x_h(:,k+1)=A*x_h(:,k)+B*u(k)+K{k}*e(k); 
    P{k+1}=A*P{k}*A'+V1-K{k}*(C*P{k}*C'+V2)*K{k}';
% dynamic filter F
    K0{k}=P{k}*C'*inv(C*P{k}*C'+V2);
    x_f(:,k)=x_h(:,k)+K0{k}*e(k);
    y_f(k)=C*x_f(:,k);  
end

%% Static 1 step Kalaman predictor and filter
% guessed initial conditions
x_hs=zeros(n,1);

% off-line steady state matrixes computation
KalSys=ss(A,[B, eye(n)],C,[D, zeros(1,n)],1);
[Kalman_predictor,Kbar,Pbar,K0bar]=kalman(KalSys,V1,V2,0);
%Kbar  % check Kbar ≃ K{N}
%K0bar           % check K0bar ≃ K0{N}
%A_K0bar=A*K0bar % check Kbar ≃ A*K0bar

for k=1:N
% Steady state predictor
    y_hs(k)=C*x_hs(:,k);
    e(k)=y(k)-y_hs(k);
    x_hs(:,k+1)=A*x_hs(:,k)+B*u(k)+Kbar*e(k);
% Steady state filter
    x_hsf(:,k)=A*x_hs(:,k)+K0bar*e(k);
    y_hsf(k)=C*x_hsf(:,k);
end

% Steady state Linear Quadratic optimal regulator
Klq=dlqr(A',C',V1',V2') % check Klq ≃ Kbar

%% Dynamic Kalaman predictor/corrector form
% guessed initial conditions
x_h_pc=zeros(n,1); P{1}=P0;

for k=1:N
    K0{k}=P{k}*C'*inv(C*P{k}*C'+V2);
    P_0{k}=(eye(n)-K0{k}*C)*P{k};
    P{k+1}=A*P_0{k}*A'+V1;
    y_h_pc(k)=C*x_h_pc(:,k);
    e_pc(k)=y(k)-y_h_pc(k);
    x_f_pc(:,k)=x_h_pc(:,k)+K0{k}*e_pc(k);
    x_h_pc(:,k+1)=A*x_f_pc(:,k)+B*u(k);
end

%% Compare results
ed=y-y_h;
es=y-y_hs;
ed_pc=y-y_h_pc;

realRMSdynamic=rms(ed)
preciseRMSdynamic=preciseRms(ed,n)

realRMSdynamic_pc=rms(ed_pc)
preciseRMSdynamic_pc=preciseRms(ed_pc,n)

realRMSstatic=rms(es)
preciseRMSstatic=preciseRms(es,n)

% Although they are equal in RMS there is a small difference
diff_predictor_corrector=preciseRms(y_h-y_h_pc,n)

% The difference between static and dynamic case is evident
diff_static_dynamic=preciseRms(y_hs-y_h,n)

%% Visualize results
k=1:N;
% State prediction
figure(1)
printStates(x,'k') % real
printStates(x_h,'r') % dynamically predicted
printStates(x_hs,'g--') % statically predicted

% State filtering
figure(2)
printStates(x,'k') % real
printStates(x_f,'r') % dynamically filtered
printStates(x_hsf,'g--') % statically predicted

% Estimated noise predicted minus filtered sequence
figure(3)
printStates(x_h(:,1:N)-x_f,'m') % dynamic case
printStates(x_hs(:,1:N)-x_hsf,'c') % static case

% Output prediction
figure(4), hold on
plot(k,y,'k') % real
plot(k,y_h,'r') % dynamically predicted
hold on
plot(k,y_hs,'g--') % statically predicted

%% Functions
function printStates(x,format)
    [n,N]=size(x);
    t=1:N;
    for k=1:n
        subplot(n,1,k)
        plot(t,x(k,:),format)
        hold on
    end
end

% rms(x) = preciseRms(x,0)
function rms=preciseRms(vector,n)
    N=length(vector);
    rms=sqrt(1/(N-n) * sum(vector.^2));
end