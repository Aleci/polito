%% Exercise 1
clear 
close all
clc

%% System description 
load data1b.mat

u = u - mean(u);
y = y - mean(y);

N = length(u);
Ne = 7000;
Nv = N-Ne;
N0 = 35;

ue = u(1:Ne); uv = u(Ne+1:end);
ye = y(1:Ne); yv = y(Ne+1:end);
ze = [ye ue]; zv = [yv uv];

figure
subplot(121) 
plot(1:Ne,ye,'r',Ne+1:N,yv,'b'), grid on, title('Output - no mean')
xlabel('Time'), ylabel('Amplitude'), xlim([1 12000])
subplot(122) 
plot(1:Ne,ue,'r',Ne+1:N,uv,'b'), grid on, title('Input - no mean')
xlabel('Time'), ylabel('Amplitude'), xlim([1 12000])

%% ARX simulation 
arx_res = 2;

if arx_res == 1
    for nk = 1:3
        for na = 1:10
            
            nb = na;
            M = arx(ze,[na nb nk]);
            
            lag_max = 30;
            figure, resid(ze,M,'Corr',lag_max')
            
        end
        
        pause, close all
        
    end
end

if arx_res == 0
    
    RMSE_arx = zeros(3,10);
    AIC_arx = zeros(3,10);
    complx_arx = zeros(1,10);
    
    for nk = 1:3
        for na = 1:10 
            
            nb = na;
            M = arx(ze,[na nb nk]);
            yh = compare(zv,M,inf);
            
            n = na;
            complx_arx(na) = n;
            MSE = (1/(Nv-N0))*norm(yv(N0+1:end) - yh(N0+1:end))^2;
            RMSE_arx(nk,na) = sqrt(MSE);
            AIC_arx(nk,na) = 2*n/(Nv-N0) + log(MSE);
            
        end
    end
    
    figure
    plot(complx_arx,RMSE_arx','-o'), grid on, title('ARX - RMSE criterion')
    xlabel('Complexity'), ylabel('RMSE')
    legend('n_k = 1','n_k = 2','n_k = 3')
    
    figure
    plot(complx_arx,AIC_arx','-o'), grid on, title('ARX - AIC criterion')
    xlabel('Complexity'), ylabel('AIC')
    legend('n_k = 1','n_k = 2','n_k = 3')
    
end    

%% ARMAX simulation 

armax_res = 2;

if armax_res == 1
    for nk = 1:3
        for na = 1:10
            
            nb = na;
            nc = na;
            M = armax(ze,[na nb nc nk]);
            
            lag_max = 30;
            figure, resid(ze,M,'Corr',lag_max')
            
        end
        
        pause, close all
        
    end
end

if armax_res == 0
    
    RMSE_armax = zeros(3,10);
    AIC_armax = zeros(3,10);
    complx_armax = zeros(1,10);
    
    for nk = 1:3
        for na = 1:10 
            
            nb = na;
            nc = na;
            M = armax(ze,[na nb nc nk]);
            yh = compare(zv,M,inf);
            
            n = na;
            complx_armax(na) = n;
            MSE = (1/(Nv-N0))*norm(yv(N0+1:end) - yh(N0+1:end))^2;
            RMSE_armax(nk,na) = sqrt(MSE);
            AIC_armax(nk,na) = 2*n/(Nv-N0) + log(MSE);
            
        end
    end
    
    figure
    plot(complx_armax,RMSE_armax','-o'), grid on, title('ARMAX - RMSE criterion')
    xlabel('Complexity'), ylabel('RMSE')
    legend('n_k = 1','n_k = 2','n_k = 3')
    
    figure
    plot(complx_armax,AIC_armax','-o'), grid on, title('ARMAX - AIC criterion')
    xlabel('Complexity'), ylabel('AIC')
    legend('n_k = 1','n_k = 2','n_k = 3')
    
end    

%% OE simulation 
oe_res = 0;

if oe_res == 1
    for nk = 1:3
        for nb = 1:10
            
            nf = nb;
            M = oe(ze,[nb nf nk]);
            
            lag_max = 30;
            figure, resid(ze,M,'Corr',lag_max')
            
        end
        
        pause, close all
        
    end
end

%% Model choice 
best_model = armax(ze,[10 10 10 1]);

%% LS and RLS estimation 
PHI = [-y(3:N-1) -y(2:N-2) -y(1:N-3) u(2:N-2) u(1:N-3)];
Y = y(4:N);

theta_LS = PHI\Y;

n = length(theta_LS);
V = eye(n);
theta_RLS = zeros(n,N);

for t = 4:N
    
    phi = [-y(t-1) -y(t-2) -y(t-3) u(t-2) u(t-3)]';
    beta = 1 + phi'*V*phi;
    V = V - inv(beta)*V*phi*phi'*V;
    K = V*phi;
    e = y(t) - phi'*theta_RLS(:,t-1);
    theta_RLS(:,t) = theta_RLS(:,t-1) + K*e;
    
end


for i = 1:n
    
    name1 = sprintf('RLS-3 >> Theta %d',i);
    name2 = sprintf('LS >> Theta %d',i);
    theta_v = theta_LS(i)*ones(1,N);
    
    figure
    plot(1:N,theta_RLS(i,1:N),'r',1:N,theta_v,'b'), grid on
    title('RLS vs LS'), xlabel('Time'), ylabel('\theta')
    legend(name1,name2)
    
end

%% EUI_2
A = inv(PHI'*PHI);

EUI_2 = zeros(n,2);
epsilon = 2;

for j = 1:n
    
    sigma = sqrt(A(j,j));
    EUI_2(j,1) = theta_LS(j) - sigma*epsilon;
    EUI_2(j,2) = theta_LS(j) + sigma*epsilon;
    
end

alpha_2 = norm(Y - PHI*theta_LS)^2;
epsilon_2 = epsilon^2;

if alpha_2 > epsilon_2
    fprintf('PUI_2 cannot be computed \n')
else
    fprintf('PUI_2 can be computed \n')
end

%% EUI_2
A = pinv(PHI);

EUI_inf = zeros(n,2);
epsilon = 0.1;

for j = 1:n
    
    EUI_inf(j,1) = A(j,:)*(Y - epsilon*sign(A(j,:))');
    EUI_inf(j,2) = A(j,:)*(Y + epsilon*sign(A(j,:))');
    
end
