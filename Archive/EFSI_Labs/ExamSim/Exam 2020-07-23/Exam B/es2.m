%% Exercise 2
clear 
close all 
clc

rng('default')

%% System description 
A = [0 0 0 -0.7225;
    1 0 0 1.87;
    0 1 0 -2.42;
    0 0 1 2.2];
B = [0 0 1 2]';
C = [0 0 0 1];
D = 0;

load data2b.mat

n = 4;
N = length(u);
N0 = [0 10 100];

Bv1 = 0.01*[-1 1 -1 1]';
V1 = Bv1*Bv1';
V2 = 1;
P = eye(n);

x = zeros(n,N); y = zeros(1,N);
v1 = zeros(n,N); v2 = zeros(1,N);
x(:,1) = [30 35 40 45]';

for t = 1:N
    
    v1(:,t) = mvnrnd(zeros(1,n),V1);
    v2(t) = sqrt(V2)*randn;
    
    x(:,t+1) = A*x(:,t) + B*u(t) + v1(:,t);
    y(t) = C*x(:,t) + D*u(t) + v2(t);
    
end

V1_check = cov(v1');
V2_check = cov(v2');

figure
plot(1:N,y,'b'), grid on, title('Output')
xlabel('Time'), ylabel('Amplitude')

for i = 1:n
    
    name = sprintf('State x_%d',i);
    
    figure
    plot(1:N,x(i,1:N),'b'), grid on, title(name)
    xlabel('Time'), ylabel('Amplitude')
    
end

%% Steady state Kalman predictor 
xk_ss = zeros(n,N);
yk_ss = zeros(1,N);

S = ss(A,[B eye(n)],C,[D zeros(1,n)],1);
[Kal,Kbar,Pbar,K0bar] = kalman(S,V1,V2,0);

for t = 1:N
    
    yk_ss(t) = C*xk_ss(:,t);
    e = y(t) - yk_ss(t);
    
    xk_ss(:,t+1) = A*xk_ss(:,t) + B*u(t) + Kbar*e;
    
end

RMSE_xk_ss = zeros(n,3);
RMSE_yk_ss = zeros(1,3);


for i = 1:3
    
    for j = 1:n
        
        RMSE_xk_ss(j,i) = sqrt((1/(N-N0(i)))*norm(x(j,N0(i)+1:N) - xk_ss(j,N0(i)+1:N))^2);
        
    end
    
    RMSE_yk_ss(i) = sqrt((1/(N-N0(i)))*norm(y(N0(i)+1:N) - yk_ss(N0(i)+1:N))^2);
    
end

figure
plot(1:N,y,'b',1:N,yk_ss,'r--'), grid on, title('Output - Predictor')
xlabel('Time'), ylabel('Amplitude')
legend('Output','Prediction')

for i = 1:n
    
    name = sprintf('State x_%d - Prediction',i);
    
    figure
    plot(1:N,x(i,1:N),'b',1:N,xk_ss(i,1:N),'r--'), grid on, title(name)
    xlabel('Time'), ylabel('Amplitude')
    legend('Output','Prediction')
    
end

%% Dyanmic Filter in predictor/corrector form
xk_pc = zeros(n,N); xf_pc = zeros(n,N);
yk_pc = zeros(1,N); yf_pc = zeros(1,N);


for t = 1:N
    
    K0 = P*C'*inv(C*P*C' + V2);
    P0 = (eye(n) - K0*C)*P;
    P = A*P0*A' + V1;
    
    yk_pc(t) = C*xk_pc(:,t);
    e = y(t) - yk_pc(t);
    
    xf_pc(:,t) = xk_pc(:,t) + K0*e;
    yf_pc(t) = C*xf_pc(:,t);
    
    xk_pc(:,t+1) = A*xf_pc(:,t) + B*u(t);
    
end

RMSE_xf_pc = zeros(n,3);
RMSE_yf_pc = zeros(1,3);


for i = 1:3
    
    for j = 1:n
        
        RMSE_xf_pc(j,i) = sqrt((1/(N-N0(i)))*norm(x(j,N0(i)+1:N) - xf_pc(j,N0(i)+1:N))^2);
        
    end
    
    RMSE_yf_pc(i) = sqrt((1/(N-N0(i)))*norm(y(N0(i)+1:N) - yf_pc(N0(i)+1:N))^2);
    
end

figure
plot(1:N,y,'b',1:N,yf_pc,'r--'), grid on, title('Output - Filter')
xlabel('Time'), ylabel('Amplitude')
legend('Output','Filter')

for i = 1:n
    
    name = sprintf('State x_%d - Filter',i);
    
    figure
    plot(1:N,x(i,1:N),'b',1:N,xf_pc(i,1:N),'r--'), grid on, title(name)
    xlabel('Time'), ylabel('Amplitude')
    legend('Output','Filter')
    
end




    
   

    







