%Ex1 
clear all,close all,clc
load data1a.mat
%% Pre-processing
u = u - mean(u);
y = y - mean(y);

Ntot = length(u);
Ne = 6500;
Nv = Ntot - Ne;

ue = u(1:Ne);
ye = y(1:Ne);
uv = u(Ne+1:Ntot);
yv = y(Ne+1:Ntot);
figure,
subplot(211),plot(1:Ne,ue,'r',Ne+1:Ntot,uv,'b'),title('System input')
legend('Estimation data','Validation data')
subplot(212),plot(1:Ne,ue,'r',Ne+1:Ntot,uv,'b'),title('System output')
legend('Estimation data','Validation data')

N0 = 25;
data_e = [ye,ue];
data_v = [yv,uv];
%% ARX IDENTIFICATION (SIMULATION MODE)
for nk = 1:3
    for na = 1:10
        nb = na;
        n = na;
        M_arx = arx(data_e,[na nb nk]);
        fprintf('\n ARX(%d,%d,%d) \n',na,nb,nk)
        %       figure,resid(data_e,M_arx,'CORR',30)
        ys_arx = compare(data_v,M_arx,inf);
        n_arx(na) = n;
        MSE_arx = (1/(Nv-N0)) * norm(yv(N0+1:end)-ys_arx(N0+1:end))^2;
        RMSE_arx(nk,na) = sqrt(MSE_arx);
        AIC_arx(nk,na) = n * (2/(Nv-N0)) + log(MSE_arx);
    end
    %    pause,close all
end
%No ARX model passed the whiteness test

%% ARMAX IDENTIFICATION (SIMULATION MODE)
for nk = 1:3
    for na = 1:10
        nb = na;
        nc = na;
        n = na;
        M_armax = armax(data_e,[na nb nc nk]);
        fprintf('\n ARMAX(%d,%d,%d,%d) \n',na,nb,nc,nk)
        %         figure,resid(data_e,M_armax,'CORR',30)
        ys_armax = compare(data_v,M_armax,inf);
        n_armax(na) = n;
        MSE_armax = (1/(Nv-N0)) * norm(yv(N0+1:end)-ys_armax(N0+1:end))^2;
        RMSE_armax(nk,na) = sqrt(MSE_armax);
        AIC_armax(nk,na) = n * (2/(Nv-N0)) + log(MSE_armax);
    end
    %        pause,close all
end
RMSE_armax,AIC_armax

%% OE IDENTIFICATION (SIMULATION MODE)
for nk = 1:3
    for nf = 1:10
        nb = nf;
        n = nf;
        M_oe = oe(data_e,[nb nf nk]);
        fprintf('\n OE(%d,%d,%d) \n',nb,nf,nk)
        %         figure,resid(data_e,M_oe,'CORR',30)
        ys_oe = compare(data_v,M_oe,inf);
        n_oe(nf) = n;
        MSE_oe = (1/(Nv-N0)) * norm(yv(N0+1:end)-ys_oe(N0+1:end))^2;
        RMSE_oe(nk,nf) = sqrt(MSE_oe);
        AIC_oe(nk,nf) = n * (2/(Nv-N0)) + log(MSE_oe);
    end
    %     pause,close all
end

%% Best trade-off
fprintf('\n Best ARMAX model: ARMAX(10,10,10,2), n = %d, RMSE = %f, AIC = %f \n',...
    n_armax(10),RMSE_armax(2,10),AIC_armax(2,10))
best_tradeoff = armax(data_e,[10 10 10 2]);
% present(best_tradeoff)
[num,den] = th2tf(best_tradeoff);
tf_best_tradeoff = tf(num,den,1,'Variable','z^-1')

[A,B,C,D,F] = polydata(best_tradeoff)
G_z = tf(B,A,1,'Variable','z^-1')
H_z = tf(C,A,1,'Variable','z^-1')

%% STANDARD LS ESTIMATE OF ARX(3,2,2)
% y(t) + a1 * y(t-1) + a2 * y(t-2) + a3 * y(t-3) = b1 * u(t-2) + b2 *
% u(t-3);
clear all,close all,clc
load data1a.mat
u = u - mean(u);
y = y - mean(y);
N = length(u);
nk=2;na=3;nb=2; n=na+nb;
Tin = max([na+1,nk+nb]);
PHI = [];
for t = Tin:N
   PHI = [PHI;-y(t-1) -y(t-2) -y(t-3) u(t-2) u(t-3)]; 
end
Y = y(Tin:N);
R_tolerance = 1e-10;
R_hat = (PHI' * PHI)/N;
if abs(det(R_hat)) > R_tolerance
    fprintf('Matrix Rt is not singular.\n')
    theta_LS = PHI\Y
else
    disp('Matrix Rt is singular.\n')
end

%% RLS-3 ESTIMATE
theta_RLS = zeros(n,Tin);
alfa = 500;
V = alfa * eye(n);

for t = Tin:N
   phi_RLS = [-y(t-1) -y(t-2) -y(t-3) u(t-2) u(t-3)]';
   beta = 1 + phi_RLS' * V * phi_RLS;
   V = V - (1/beta) * V * phi_RLS * phi_RLS' * V;
   K = V * phi_RLS;
   e = y(t) - phi_RLS' * theta_RLS(:,t-1);
   theta_RLS(:,t) = theta_RLS(:,t-1) + K * e;
end
theta_RLS_final = theta_RLS(:,end)
plot(1:N,theta_RLS),hold on ,grid on
for i = 1:size(theta_LS)
    refline(0,theta_LS(i))
end 
title('RLS-3 convergence')
legend('a1','a2','a3','b1','b2')
%% EUI2 
eps = 1;
sigma = sqrt(diag(inv(PHI' * PHI)));
theta_EUI2_min = theta_LS - eps * sigma;
theta_EUI2_max = theta_LS + eps * sigma;
EUI2 = [theta_EUI2_min,theta_EUI2_max]

%% PUI2
alfa = sqrt((Y-PHI*theta_LS)'*(Y-PHI*theta_LS));
if alfa^2 <= eps^2
    theta_PUI2_min = theta_LS - sigma * sqrt(eps^2 - alfa^2);
    theta_PUI2_max = theta_LS + sigma * sqrt(eps^2 - alfa^2);
    PUI2 = [theta_PUI2_min,theta_PUI2_max]
else 
    disp('FPS2 is empty and PUI2 cannot be computed.')
end

%% EUI_inf
eps_EUI_inf = 0.05;
A = pinv(PHI);
for j = 1:n
   theta_EUIinf_min(j,1) = A(j,:) * (Y - eps_EUI_inf * sign(A(j,:))'); 
end
theta_EUIinf_max = 2 * theta_LS - theta_EUIinf_min;
EUI_inf = [theta_EUIinf_min,theta_EUIinf_max]