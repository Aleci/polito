%Ex2 
clear all,close all,clc
load data2a.mat
rng('default');

A = [0 1 0 0;...
    0 0 1 0;...
    0 0 0 1;...
    -0.7225 1.87 -2.42 2.2]
B = [0;0;0;1];
C = [0 0 1 2];
D = 0;
Bv1 = 0.01 * [-1 1 -1 1]';
V1 = Bv1 * Bv1';
V2 = 1;
V12 = 0;

N = length(u);
eig(A),rank(obsv(A,C)),rank(ctrb(A,Bv1)) % Asymptotically stable

%% System Simulation
x(:,1) = [30 35 40 45]';
n = length(x(:,1));
for t = 1:N
    v1(:,t) = mvnrnd(zeros(1,n),V1)';
    v2(t) = sqrt(V2) * randn;
    x(:,t+1) = A * x(:,t) + B * u(t) + v1(:,t);
    y(t) = C * x(:,t) + v2(t);
end

T=1:N; 
for k=1:n,
    figure, plot(T,x(k,1:N),'g'), title(['State x_',num2str(k),'(t)'])
end
figure, plot(T,y(1:N),'g'), title('Output y(t)')

%% Steady-state Kalman predictor Kinf
S = ss(A, [B,eye(n)],C,[D,zeros(1,n)],1);
[Kalman_predictor,Kbar,Pbar,K0bar] = kalman(S,V1,V2,0);
x_h_ss(:,1) = zeros(n,1);
for t = 1:N
    y_h_ss(t) = C * x_h_ss(:,t);
    e_ss(t) = y(t) - y_h_ss(t);
    x_h_ss(:,t+1) = A * x_h_ss(:,t) + B*u(t) + Kbar * e_ss(t);
    x_f_ss(:,t) = x_h_ss(:,t) + K0bar * e_ss(t);
    y_f_ss(:,t) = C * x_f_ss(:,t);
end
Kbar,K0bar
%% Dynamic Kalman filter Fpc (PREDICTOR-CORRECTOR)
x_h_pc(:,1) = zeros(n,1);
x_f_pc(:,1) = zeros(n,1);
P = eye(4);
for t = 1:N
    K0 = P*C'*inv(C*P*C'+V2);
    P0 =(eye(4)-K0*C)*P;
    
    y_h_pc(t) = C * x_h_pc(:,t);
    e_pc(t) = y(t) - y_h_pc(t);
    x_f_pc(:,t) = x_h_pc(:,t)+K0*e_pc(t);
    x_h_pc(:,t+1) = A * x_f_pc(:,t) + B*u(t);
    y_f_pc(:,t) = C * x_f_pc(:,t);
    P = A*P0*A' + V1;
end
Kpc = A * K0

%% RMSE EVALUATION
N0 = [0 20 100];
for j = 1:length(N0)
    for k = 1:n
        RMSE_x_h_ss(k,j) = norm(x(k,N0(j)+1:N)-x_h_ss(k,N0(j)+1:N))/(sqrt(N-N0(j)));
        RMSE_x_f_pc(k,j) = norm(x(k,N0(j)+1:N)-x_f_pc(k,N0(j)+1:N))/(sqrt(N-N0(j)));
    end
    RMSE_y_h_ss(j) = norm(y(N0(j)+1:N) - y_h_ss(N0(j)+1:N))/(sqrt(N-N0(j)));
    RMSE_y_f_pc(j) = norm(y(N0(j)+1:N) - y_f_pc(N0(j)+1:N))/(sqrt(N-N0(j)));

end

fprintf('\n Steady-state Kalman predictor Kinf: N0 = 0 20 100 \n')
RMSE_x_h_ss,RMSE_y_h_ss
fprintf('\n Dynamic Kalman filter Fpc: N0 = 0 20 100 \n')
RMSE_x_f_pc,RMSE_y_f_pc

%% Graphical comparison
T = 1:N;
for k = 1:n
    figure,
    plot(T,x(k,T),'g',T,x_h_ss(k,T),'r',T,x_f_pc(k,T),'b')
    title(['state x_',num2str(k),'(t)'])
    legend('System S','K^\infty','F_pc')
end
    figure,
    plot(T,y(T),'g',T,y_h_ss(T),'r',T,y_f_pc(T),'b')
    title('Output y(t)')
    legend('System S','K^\infty','F_pc')