%% Workspace initialization
close all; clear all; clc; format short e
load data1

%% Data preprocessing
N=1000;
utot=u-mean(u);
ytot=y-mean(y);
ue=utot(1:N);
ye=ytot(1:N);
uv=utot(N+1:end);
yv=ytot(N+1:end);
ed=[ye,ue]; vd=[yv,uv];

%% Model caching
arxm=cell(7,7);
yEarx=cell(7,7);
yVarx=cell(7,7);

armaxm=cell(7,7);
yEarmax=cell(7,7);
yVarmax=cell(7,7);

oem=cell(7,7);
yEoe=cell(7,7);
yVoe=cell(7,7);
for n=1:7
  for k=1:7
    arxm{n,k}=arx(ed,[n,n,k]);
    yEarx{n,k}=compare(ed,arxm{n,k},1);
    yVarx{n,k}=compare(vd,arxm{n,k},1);
    armaxm{n,k}=armax(ed,[n,n,n,k]);
    yEarmax{n,k}=compare(ed,armaxm{n,k},1); 
    yVarmax{n,k}=compare(vd,armaxm{n,k},1);
    oem{n,k}=oe(ed,[n,n,k]);
    yEoe{n,k}=compare(ed,oem{n,k},1);
    yVoe{n,k}=compare(vd,oem{n,k},1);
  end
end

%% ARX
rmse_arx=modEval(yEarx,yVarx,ye,yv,2,"ARX");
plot1(rmse_arx,"ARX rmse");
% best white k=4, n=7
% best white tradeoff k=5 n=2

%% ARMAX
rmse_armax=modEval(yEarmax,yVarmax,ye,yv,3,"ARMAX");
plot1(rmse_armax,"ARMAX rmse");
% best white k=2, n=3
% best tradeoff k=2, n=2

%% OE
rmse_oe=modEval(yEoe,yVoe,ye,yv,2,"OE");
plot1(rmse_oe,"OE rmse");
% best white and tradeoff k=2, n=2

%% MANUAL LSE of ARX(3,3,1)
%scalable iff na=nb=n and nk=1
n=3;nk=1;
N=2000;
ye=ytot;
ue=utot;

ye=[12,27,38,44,56,69,70]';
ue=[1,2,3,4,5,6,7]';
N=7;
n=3;

yeUsable=ye(n+1:N);
% y(k) = -a1 y(k-1) -a2 y(k-2) -a3 y(k-3) +b1 u(k-1) +b2 u(k-2) +b3 u(k-3)
fi=[];
for j=1:n
    fi=[fi, -ye(n+1-j:N-j)];
end
for j=1:n
    fi=[fi, ue(n+1-j:N-j)];
end
theta=fi\yeUsable %use also the last value of y as new data, trim y to be same lenght of fi

% if n=3 k=1 should match the prof's solution
fiProf=[-ye(3:N-1), -ye(2:N-2), -ye(1:N-3), ue(3:N-1), ue(2:N-2), ue(1:N-3)]; %use old data to build the regressor
isequal(fi,fiProf)

%% Intervals calculation
err=yeUsable-fi*theta;
% 2 norm
e=4;
sigma=inv(fi'*fi);
sigmaJ=sqrt(diag(sigma));
EUI2=[theta-sigmaJ*e, theta+sigmaJ*e]
alpha2=norm(err)^2;
PUI2=[theta-sigmaJ*sqrt(e^2-alpha2), theta+sigmaJ*sqrt(e^2-alpha2)]
% infinite norm
e=.1;
A=pinv(fi);
Aprime=A.*sign(A);
mintheta=A*yeUsable-Aprime*e*ones(size(yeUsable))
EUI8=[mintheta, theta*2-mintheta]

%% why is it wrong?
% u=[1,2,3,4,5,6,7]
% y=[12,27,38,44,56,69,70]
% 
% arx na=nb=n=2, nk=2
% 
% y(k)=-y(k-1)-y(k-2)+u(k-2)+u(k-3)
% y(k+3)=-y(k+2)-y(k+1)+u(k+1)+u(k)
% 44       38     27     2     1
% 56       44     38     3     2
% 69       56     44     4     3
% 70       69     56     5     4
n=2;nk=2;
ye=[12,27,38,44,56,69,70]';
ue=[1,2,3,4,5,6,7]';
N=7;
fi=[-38,-27,2,1;
    -44,-38,3,2;
    -56,-44,4,3;
    -69,-56,5,4];
yU=[44,56,69,70]';
theta=fi\yU

% if different should match the automatically estimated model parameters
[A,B]=polydata(arx([ye,ue],[n,n,nk]));
thetaAuto=[A,B]'

%% Functions
function rmse=modEval(y_hE,y_hV,yest,yval,cost,mytitle)
  for n=1:7
    for k=1:7
      [pass(n,k),nOut(n,k)]=whiteTest(y_hE{n,k},yest);
      rmse(n,k)=crossVal(y_hV{n,k},yval);
    end
  end
  fprintf("%s\nk\\n ->\n|\nv\n",mytitle)
  nOut,pass
end

function [pass,nOut]=whiteTest(yhat,y)
  confidence=.99;
  err=y-yhat;
  N=length(err);
  [r,lag]=xcorr(err,"normalize");
  rprime=abs(r(N+1:end)); 
  threshold=erfinv(confidence)*sqrt(2)/sqrt(N);
  nOut=sum(rprime(1:30) >= threshold); %% TRIM HERE
  pass=nOut < 3; % Custom threshold
end

function rmse=crossVal(yhat,y)
  err=y-yhat;
  N=length(err);
  rmse=rms(err(10+1:end));
end

function plot1(val,label)
  k=[1:length(val(:,1))];
  figure,
  plot(k,val(:,1),k,val(:,2),k,val(:,3),k,val(:,4),k,val(:,5),k,val(:,6),k,val(:,7)),title(label);
  legend("k=1","k=2","k=3","k=4","k=5","k=6","k=7")
end
