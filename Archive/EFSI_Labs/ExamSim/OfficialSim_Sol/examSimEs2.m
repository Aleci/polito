%% Workspace Initalization
clear all; close all; clc; format short e
load data2
N=length(u); n=3;
A=[2.7258, -1.2424, 0.7577;2, 0, 0;0, .5, 0];
B=[0.026; 0; 0];
C=[1 -0.45 -0.05];
D=0;
CholV1=.05*[.026; 0; 0];
V1=CholV1*CholV1';
V2=.0004;
P{1}=eye(n);
x{1}=[.5; .3; -.9]; %invented initial state

rng('default');
v1=mvnrnd(zeros(1,n),V1,N);
%cov(v1) %check ans≃V1
v2=sqrt(V2)*randn(N,1); %change variance from unitary to V2 of the gaussian distribution
%cov(v1) %check ans≃V1

%% Real data generation
for k=1:N
  x{k+1}=A*x{k}+B*u(k)+v1(k);
  y(k)=C*x{k}+v2(k);
end

%% Dynamic 1 step predictor
xDynP{1}=[0;0;0]; %Best guess of x
for k=1:N
  K0{k}=P{k}*C'*inv(C*P{k}*C'+V2);
  P0{k}=(eye(n)-K0{k}*C)*P{k};
  e(k)=y(k)-C*xDynP{k};
  xDynF{k}=xDynP{k}+K0{k}*e(k);
  yDynF(k)=C*xDynF{k};
  P{k+1}=A*P0{k}*A'+V1;
  xDynP{k+1}=A*xDynF{k}+B*u(k);
end

%% Steady state kalaman filter
mysys=ss(A,[B,eye(n)],C,[D,zeros(1,n)],1);
[sys,Kbar,Pbar,K0bar]=kalman(mysys,V1,V2,0);
xP{1}=[0;0;0];%best guess of x
for k=1:N
  e(k)=y(k)-C*xP{k};
  xP{k+1}=A*xP{k}+B*u(k)+Kbar*e(k);
  xF{k}=xP{k}+K0bar*e(k);
  yF(k)=C*xF{k};
end

%% State error evaluation
rms_ss=rmscell(x,xF,20)
rms_dyn=rmscell(x,xDynF,0)

%% Data presentation
x=[1:N];
figure
plot(x,y,"k-o"),hold on
plot(x,yDynF,"b")
plot(x,yF,"r--")


function rms=rmscell(real,est,skip)
    sum=0;
    N=length(est);
    for k=skip+1:N;
        sum=sum+(real{k}-est{k}).^2;
    end
    rms=sum/(N-skip);
end