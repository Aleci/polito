clear all, close all

%% Step 1: load data and plot it
load data1a

figure, 
subplot(2,1,1), plot(u)
subplot(2,1,2), plot(y)

%% Step 2: remove mean value and plot the result

u_mean = u-mean(u);
y_mean = y-mean(y);

figure, 
subplot(2,1,1), plot(u_mean)
subplot(2,1,2), plot(y_mean)

%% Step 3: define parameters

Ntot = length(u_mean);
Ne = 4000;
Nv = Ntot-Ne;

ue = u_mean(1:Ne);
ye = y_mean(1:Ne);

uv = u_mean(Ne+1:end);
yv = y_mean(Ne+1:end);

Ze = [ye,ue];
Zv = [yv,uv];

figure, 
subplot(2,1,1), plot(1:Ne,ue,'r',Ne+1:Ntot,uv,'b')
subplot(2,1,2), plot(1:Ne,ye,'r',Ne+1:Ntot,yv,'b')

%% Step 4: ARX model in prediction mode

for nk=1:3,
    for na=1:10,
        nb=na;
       
        model = arx(Ze,[na nb nk]);
%         figure, resid(Ze,model,'Corr',30)
        % nk = 1,2,3 Ok-> na=nb>=5 
        
        yh = compare(Zv,model,1);
        
        N0 = 10;
        n = na+nb;
        n_arx(na) = n;
        
        MSE = 1/(Nv-N0)*norm(yv(N0+1:end)-yh(N0+1:end))^2;
        BestFit_arx(nk,na) = 1-sqrt(MSE/(1/(Nv-N0)*norm(yv(N0+1:end)-mean(yv(N0+1:end)))^2));
        AIC_arx(nk,na) = n*2/(Nv-N0)+log(MSE);
  
    end
%     pause, close all
end

% BestFit_arx,AIC_arx

% figure, plot(n_arx,BestFit_arx(1,:),'r',n_arx,BestFit_arx(2,:),'g',n_arx,BestFit_arx(3,:),'b')
% title('BestFit arx'), legend('nk=1','nk=2','nk=3')
% figure, plot(n_arx,AIC_arx(1,:),'r',n_arx,AIC_arx(2,:),'g',n_arx,AIC_arx(3,:),'b')
% title('AIC arx'), legend('nk=1','nk=2','nk=3')


% BestFIT -> ARX(5,5,1)
% AIC -> ARX(5,5,1)
comp(1,1) = BestFit_arx(1,5);
comp(1,2) = AIC_arx(1,5);

% %% Step 5: ARMAX model in prediction mode

for nk=1:3,
    for na=1:10,
        nb=na;
        nc=na;
       
        model = armax(Ze,[na nb nc nk]);
%         figure, resid(Ze,model,'Corr',30)
        % nk = 1 OK-> na=nb=nc>=3
        % nk = 2,3 OK-> na=nb=nc>=2
        
        yh = compare(Zv,model,1);
        
        N0 = 10;
        n = na+nb+nc;
        n_armax(na) = n;
        
        MSE = 1/(Nv-N0)*norm(yv(N0+1:end)-yh(N0+1:end))^2;
        BestFit_armax(nk,na) = 1-sqrt(MSE/(1/(Nv-N0)*norm(yv(N0+1:end)-mean(yv(N0+1:end)))^2));
        AIC_armax(nk,na) = n*2/(Nv-N0)+log(MSE);
  
    end
%     pause, close all
end

% BestFit_armax
% AIC_armax

% figure, plot(n_armax,BestFit_armax(1,:),'r',n_armax,BestFit_armax(2,:),'g',n_armax,BestFit_armax(3,:),'b')
% title('BestFit armax'), legend('nk=1','nk=2','nk=3')
% figure, plot(n_armax,AIC_armax(1,:),'r',n_armax,AIC_armax(2,:),'g',n_armax,AIC_armax(3,:),'b')
% title('AIC armax'), legend('nk=1','nk=2','nk=3')


% BestFIT -> ARMAX(3,3,3,1)
% AIC -> ARMAX(3,3,3,1)
comp(2,1) = BestFit_armax(1,3);
comp(2,2) = AIC_armax(1,3);


%% Step 6: OE model in prediction mode

for nk=1:3,
    for nf=1:10,
        nb=nf;
       
        model = oe(Ze,[nb nf nk]);
%         figure, resid(Ze,model,'Corr',30)
        % nk = 1 OK-> nb=nf>=3
        % nk = 2 OK-> nb=nf>=4 excluding 9
        % nk = 3 OK-> nb=nf>=4
        
        yh = compare(Zv,model,1);
        

        N0 = 10;
        n = nb+nf;
        n_oe(nf) = n;
        
        MSE = 1/(Nv-N0)*norm(yv(N0+1:end)-yh(N0+1:end))^2;
        BestFit_oe(nk,nf) = 1-sqrt(MSE/(1/(Nv-N0)*norm(yv(N0+1:end)-mean(yv(N0+1:end)))^2));
        AIC_oe(nk,nf) = n*2/(Nv-N0)+log(MSE);
  
    end
%     pause, close all
end

% BestFit_oe,AIC_oe

% figure, plot(n_oe,BestFit_oe(1,:),'r',n_oe,BestFit_oe(2,:),'g',n_oe,BestFit_oe(3,:),'b')
% title('BestFit oe'), legend('nk=1','nk=2','nk=3')
% figure, plot(n_oe,AIC_oe(1,:),'r',n_oe,AIC_oe(2,:),'g',n_oe,AIC_oe(3,:),'b')
% title('AIC oe'), legend('nk=1','nk=2','nk=3')


% BestFIT -> OE(3,3,1)
% AIC -> OE(3,3,1)
comp(3,1) = BestFit_oe(1,3);
comp(3,2) = AIC_oe(1,3);

% comp

%% Step 7: best model computation tf functions

best = oe(Ze,[3 3 1]);
[A B C D F] = polydata(best);

G_z = tf(B,F,1,'Variable','z^-1');
H_z = 0;

%% Step 8: ARX(3,2,2) estimation using LS & RLS-3
load data1a

u = u-mean(u);
y = y-mean(y);



N = length(u);

na = 3;
nb = 2;
nk = 2;
n = na+nb;

t_min = max([na+1,nk+nb]);
Phi = [];
alpha = 1;
V = alpha*eye(n);
theta_RLS = zeros(n,N);

for t=t_min:N,
    phi_RLS = [-y(t-1) -y(t-2) -y(t-3) u(t-2) u(t-3)]';
    Phi = [Phi;phi_RLS'];
    beta = 1+phi_RLS'*V*phi_RLS;
    V = V-1/beta*V*(phi_RLS*phi_RLS')*V;
    K = V*phi_RLS;
    e = y(t)-phi_RLS'*theta_RLS(:,t-1);
    theta_RLS(:,t) = theta_RLS(:,t-1)+K*e;
end

theta_RLS_final = theta_RLS(:,N);
theta_LS = pinv(Phi)*y(t_min:N);

theta_RLS_final
theta_LS

%% Step 9: compute EUI2 & PUI2

eps = 4;
alfa = norm(y(t_min:N)-Phi*theta_LS);
sigma = sqrt(diag(inv(Phi'*Phi)));
theta_EUI2_min = theta_LS-eps*sigma;
theta_EUI2_max = theta_LS+eps*sigma;

EUI2 = [theta_EUI2_min,theta_EUI2_max];

if alfa^2<=eps^2
    theta_PUI2_min = theta_LS-sigma*sqrt(eps^2-alfa^2);
    theta_PUI2_max = theta_LS+sigma*sqrt(eps^2-alfa^2);
else
    display('Alfa is greater than eps, PUI2 cannot be computed')
end

%% Step 10: compute EUIinf

eps_inf = 0.1;
A = pinv(Phi);

for j=1:n,
    theta_EUIinf_min(j,1) = A(j,:)*(y(t_min:N)-eps_inf*sign(A(j,:))');
end   

theta_EUIinf_max = 2*theta_LS-theta_EUIinf_min;
EUIinf = [theta_EUIinf_min,theta_EUIinf_max]









