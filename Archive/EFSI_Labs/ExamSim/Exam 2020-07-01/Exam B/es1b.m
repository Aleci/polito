%% Exercise 1
clear
close all
clc

%% 1 - System description 
load data1b.mat 

u = u - mean(u);
y = y - mean(y);

N = length(u);
Ne = N/2;
Nv = N - Ne;
N0 = 10;

ue = u(1:Ne); uv = u(Ne+1:N);
ye = y(1:Ne); yv = y(Ne+1:N);
ze = [ye ue]; zv = [yv uv];

figure

subplot(121)
plot(1:Ne,ue,'r',Ne+1:N,uv,'b'), grid on, title('Input - no mean')
xlabel('Time'), ylabel('Amplitude'), xlim([1 1100])
legend('Estimation set','Validation set')

subplot(122)
plot(1:Ne,ye,'r',Ne+1:N,yv,'b'), grid on, title('Output - no mean')
xlabel('Time'), ylabel('Amplitude'), xlim([1 1100])
legend('Estimation set','Validation set')

%% 2 - ARX predictor 
arx_res = 0;

if arx_res == 1
    for nk = 1:3
        for na = 1:10
            
            nb = na;
            M = arx(ze,[na nb nk]);
            
            lag_max = 25;
            figure, resid(ze,M,'Corr',lag_max)
            
        end
        
        pause, close all
        
    end
end

% ARX models which passed the test:
% > Threshold = 3;
% > nk = 1,2 - na = nb = 1:10
% > nk = 3 - na = nb = 4:10

complx_arx = zeros(1,10);
MDL_arx = zeros(3,10);
FIT_arx = zeros(3,10);

if arx_res == 0
    for nk = 1:3
        for na = 1:10
            
            nb = na;
            M = arx(ze,[na nb nk]);
            yh = compare(zv,M,1);
            
            n = na + nb;
            complx_arx(na) = n;
            
            MSE = (1/(Nv-N0))*norm(yv(N0+1:end) - yh(N0+1:end))^2;
            
            MDL_arx(nk,na) = n*log(Nv-N0)/(Nv-N0) + log(MSE);
            
            A = (1/(Nv-N0))*norm(yv(N0+1:end) - mean(yv(N0+1:end)))^2;
            FIT_arx(nk,na) = 1 - sqrt(MSE/A);
            
        end
    end
    
    figure
    plot(complx_arx,MDL_arx,'o-'), grid on, title('ARX - MDL criterion')
    xlabel('Complexity'), ylabel('MDL value')
    legend('n_k = 1','n_k = 2','n_k = 3')

    figure
    plot(complx_arx,FIT_arx,'o-'), grid on, title('ARX - FIT criterion')
    xlabel('Complexity'), ylabel('FIT value')
    legend('n_k = 1','n_k = 2','n_k = 3')
    
end

% The best model is

best_arx = arx(ze,[7 7 1]); % MDL = 3.583 - FIT = 0.701

%% 3 - ARMAX predictor 
armax_res = 0;

if armax_res == 1
    for nk = 1:3
        for na = 1:10
            
            nb = na;
            nc = na;
            M = armax(ze,[na nb nc nk]);
            
            lag_max = 25;
            figure, resid(ze,M,'Corr',lag_max)
            
        end
        
        pause, close all
        
    end
end

% ARMAX models which passed the test:
% > Threshold = 3;
% > nk = 1 - na = nb = nc = 3:10
% > nk = 2 - na = nb = nc = 1:10
% > nk = 3 - na = nb = nc = 3:10

complx_armax = zeros(1,10);
MDL_armax = zeros(3,10);
FIT_armax = zeros(3,10);

if armax_res == 0
    for nk = 1:3
        for na = 1:10
            
            nb = na;
            nc = na;
            M = armax(ze,[na nb nc nk]);
            yh = compare(zv,M,1);
            
            n = na + nb +nc;
            complx_armax(na) = n;
            
            MSE = (1/(Nv-N0))*norm(yv(N0+1:end) - yh(N0+1:end))^2;
            
            MDL_armax(nk,na) = n*log(Nv-N0)/(Nv-N0) + log(MSE);
            
            A = (1/(Nv-N0))*norm(yv(N0+1:end) - mean(yv(N0+1:end)))^2;
            FIT_armax(nk,na) = 1 - sqrt(MSE/A);
            
        end
    end
    
    figure
    plot(complx_armax,MDL_armax,'o-'), grid on, title('ARMAX - MDL criterion')
    xlabel('Complexity'), ylabel('MDL value')
    legend('n_k = 1','n_k = 2','n_k = 3')

    figure
    plot(complx_armax,FIT_armax,'o-'), grid on, title('ARMAX - FIT criterion')
    xlabel('Complexity'), ylabel('FIT value')
    legend('n_k = 1','n_k = 2','n_k = 3')
    
end

% The best model is

best_armax = armax(ze,[4 4 4 1]); % MDL = 3.264 - FIT = 0.7421

%% 4 - OE predictor 
oe_res = 0;

if oe_res == 1
    for nk = 1:3
        for nb = 1:10
            
            nf = nb;
            M = oe(ze,[nb nf nk]);
            
            lag_max = 25;
            figure, resid(ze,M,'Corr',lag_max)
            
        end
        
        pause, close all
        
    end
end

% OE models which passed the test:
% > Threshold = 3;
% > nk = 1 - nf = nb = 3:10
% > nk = 2 - nf = nb = 2:10
% > nk = 3 - nf = nb = 2:10

complx_oe = zeros(1,10);
MDL_oe = zeros(3,10);
FIT_oe = zeros(3,10);

if oe_res == 0
    for nk = 1:3
        for nb = 1:10
            
            nf = nb;
            M = oe(ze,[nb nf nk]);
            yh = compare(zv,M,1);
            
            n = nb + nf;
            complx_oe(nb) = n;
            
            MSE = (1/(Nv-N0))*norm(yv(N0+1:end) - yh(N0+1:end))^2;
            
            MDL_oe(nk,nb) = n*log(Nv-N0)/(Nv-N0) + log(MSE);
            
            A = (1/(Nv-N0))*norm(yv(N0+1:end) - mean(yv(N0+1:end)))^2;
            FIT_oe(nk,nb) = 1 - sqrt(MSE/A);
            
        end
    end
    
    figure
    plot(complx_oe,MDL_oe,'o-'), grid on, title('OE - MDL criterion')
    xlabel('Complexity'), ylabel('MDL value')
    legend('n_k = 1','n_k = 2','n_k = 3')

    figure
    plot(complx_oe,FIT_oe,'o-'), grid on, title('OE - FIT criterion')
    xlabel('Complexity'), ylabel('FIT value')
    legend('n_k = 1','n_k = 2','n_k = 3')
    
end

% The best model is

best_oe = oe(ze,[4 4 1]); % MDL = 3.214 - FIT = 0.7425

% The model chosen is:

present(best_oe)
[A,B,C,D,F] = polydata(best_oe);
best_model = tf(B,F,-1);

%% 5 - LS estimation 
PHI = [-y(4:N-1) -y(3:N-2) u(3:N-2) u(2:N-3) u(1:N-4)];
Y = y(5:N);

theta_LS = PHI\Y;

%% 6 - Set Membership - EUI_2
EUI_2 = zeros(length(theta_LS),2);
A = inv(PHI'*PHI);
epsilon = 200;

for j = 1:length(theta_LS)
    
    sigma = sqrt(A(j,j));
    EUI_2(j,1) = theta_LS(j) - epsilon*sigma;
    EUI_2(j,2) = theta_LS(j) + epsilon*sigma;
    
end

alpha_2 = norm(Y - PHI*theta_LS)^2;
epsilon_2 = epsilon^2;

if alpha_2 > epsilon_2
    fprintf('The system is NOT feasible \n')
else
    fprintf('The system is feasible \n')
end

%% 6 - Set Membership - EUI_inf
EUI_inf = zeros(length(theta_LS),2);
A = pinv(PHI);
epsilon = 20;

for j = 1:length(theta_LS)
    
    EUI_inf(j,1) = A(j,:)*(Y - epsilon*sign(A(j,:))');
    EUI_inf(j,2) = A(j,:)*(Y + epsilon*sign(A(j,:))');
    
end



            
            






