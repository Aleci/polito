%% Exercise 2
clear 
close all
clc

rng('default')

%% 1 - System description 
A = [0 0 0.7577; 
    1 0 -2.4848;
    0 1 2.7258];
B = [0.005 0.1 -0.1]'; 
C = [0 0 1];
D = 0;

n = 3;

Bv1 = 0.06*[-1 1 -1]';
V1 = Bv1*Bv1'; 
V2 = 0.0025;
P = eye(n);

load data2a.mat

N = length(u);
N0 = [0 10 100];

x = zeros(n,N+1); y = zeros(1,N);
v1 = zeros(n,N); v2 = zeros(1,N);
x(:,1) = [30 -60 30]';


for t = 1:N
    
    v1(:,t) = mvnrnd(zeros(1,n), V1);
    v2(t) = sqrt(V2)*randn;
    
    x(:,t+1) = A*x(:,t) + B*u(t) + v1(:,t);
    y(t) = C*x(:,t) + D*u(t) + v2(t);
    
end

figure
plot(1:N,y,'b'), grid on, title('System output')
xlabel('Time'), ylabel('Amplitude'), xlim([1 1100])

for i = 1:n
    
    name = sprintf('System state x_%d',i);
    
    figure
    plot(1:N,x(i,1:N),'b'), grid on, title(name)
    xlabel('Time'), ylabel('Amplitude'), xlim([1 1100])
    
end

%% 2 - Steady State Kalman Filter 
xf_ss = zeros(n,N+1); yf_ss = zeros(1,N);
xk_ss = zeros(n,N+1); yk_ss = zeros(1,N);

S = ss(A,[B eye(n)],C,[D zeros(1,n)],1);
[Kal,Kbar,Pbar,K0bar] = kalman(S,V1,V2,0);

for t = 1:N
    
    yk_ss(t) = C*xk_ss(:,t);
    e = y(t) - yk_ss(t);
    
    xf_ss(:,t) = xk_ss(:,t) + K0bar*e;
    yf_ss(t) = C*xf_ss(:,t);
    xk_ss(:,t+1) = A*xk_ss(:,t) + B*u(t) + Kbar*e;
    
end

RMSE_xf_ss = zeros(3,n);
RMSE_yf_ss = zeros(3,1);

for i = 1:3
    
    RMSE_xf_ss(1,i) = sqrt((1/(N-N0(i)))*norm(x(1,N0(i)+1:N) - xf_ss(1,N0(i)+1:N))^2);
    RMSE_xf_ss(2,i) = sqrt((1/(N-N0(i)))*norm(x(2,N0(i)+1:N) - xf_ss(2,N0(i)+1:N))^2);
    RMSE_xf_ss(3,i) = sqrt((1/(N-N0(i)))*norm(x(3,N0(i)+1:N) - xf_ss(3,N0(i)+1:N))^2);
    
    RMSE_yf_ss(i) = sqrt((1/(N-N0(i)))*norm(y(N0(i)+1:N) - yf_ss(N0(i)+1:N))^2);
    
end

figure
plot(1:N,y,'b',1:N,yf_ss,'r--'), grid on, title('System output - Filter')
xlabel('Time'), ylabel('Amplitude'), xlim([1 1100])

for i = 1:n
    
    name = sprintf('System state x_%d - Filter',i);
    
    figure
    plot(1:N,x(i,1:N),'b',1:N,xf_ss(i,1:N),'r--'), grid on, title(name)
    xlabel('Time'), ylabel('Amplitude'), xlim([1 1100])
    
end

%% 3 - Steady State Kalman Filter 
xk = zeros(n,N+1); 
yk = zeros(1,N);

for t = 1:N
    
    yk(t) = C*xk(:,t);
    e = y(t) - yk(t);
    
    K = A*P*C'*inv(C*P*C' + V2);
    P = A*P*A' + V1 - K*(C*P*C' + V2)*K';
    
    xk(:,t+1) = A*xk(:,t) + B*u(t) + K*e;
    
end

RMSE_xk = zeros(3,n);
RMSE_yk = zeros(3,1);

for i = 1:3
    
    RMSE_xk(1,i) = sqrt((1/(N-N0(i)))*norm(x(1,N0(i)+1:N) - xk(1,N0(i)+1:N))^2);
    RMSE_xk(2,i) = sqrt((1/(N-N0(i)))*norm(x(2,N0(i)+1:N) - xk(2,N0(i)+1:N))^2);
    RMSE_xk(3,i) = sqrt((1/(N-N0(i)))*norm(x(3,N0(i)+1:N) - xk(3,N0(i)+1:N))^2);
    
    RMSE_yk(i) = sqrt((1/(N-N0(i)))*norm(y(N0(i)+1:N) - yk(N0(i)+1:N))^2);
    
end

figure
plot(1:N,y,'b',1:N,yk,'r--'), grid on, title('System output - Predictor')
xlabel('Time'), ylabel('Amplitude'), xlim([1 1100])

for i = 1:n
    
    name = sprintf('System state x_%d - Predictor',i);
    
    figure
    plot(1:N,x(i,1:N),'b',1:N,xk(i,1:N),'r--'), grid on, title(name)
    xlabel('Time'), ylabel('Amplitude'), xlim([1 1100])
    
end