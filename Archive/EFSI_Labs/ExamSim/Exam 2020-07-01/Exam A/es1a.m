%% Exercise 1
clear
close all
clc

%% 1 - System description
load data1a.mat 

u = u - mean(u);
y = y - mean(y);

N = length(u);
Ne = 550;
Nv = N - Ne;
N0 = 10;

ue = u(1:Ne); uv = u(Ne+1:N);
ye = y(1:Ne); yv = y(Ne+1:N);
ze = [ye ue]; zv = [yv uv];

figure
subplot(121)
plot(1:Ne,ye,'r',Ne+1:N,yv,'b'), grid on, title('Output - no mean')
xlabel('Time'), ylabel('Amplitude'), xlim([1 1100])
subplot(122)
plot(1:Ne,ue,'r',Ne+1:N,uv,'b'), grid on, title('Input - no mean')
xlabel('Time'), ylabel('Amplitude'), xlim([1 1100])

%% 2 - ARX predictor 
arx_res = 0;

if arx_res == 1
    for nk = 1:3
        for na = 1:10
            
            nb = na;
            M = arx(ze,[na nb nk]);
            
            lag_max = 25;
            figure, resid(ze,M,'Corr',lag_max)
            
        end
        
        pause, close all
        
    end
end

% Threshold = 3
% > nk = 1 - na = nb = 3:10
% > nk = 2 - na = nb = 2:10
% > nk = 3 - na = nb = 4:10

complx_arx = zeros(1,10);
MDL_arx = zeros(3,10);
FIT_arx = zeros(3,10);

if arx_res == 0
    for nk = 1:3
        for na = 1:10
            
            nb = na;
            M = arx(ze,[na nb nk]);
            yh = compare(zv,M,1);
            
            n = na + nb;
            complx_arx(na) = n;
            
            MSE = (1/(N-N0))*norm(yv(N0+1:end) - yh(N0+1:end))^2;
            MDL_arx(nk,na) = n*log(Nv - N0)/(Nv - N0) + log(MSE);
            
            A = (1/(N-N0))*norm(yv(N0+1:end) - mean(yv(N0+1:end)))^2;
            FIT_arx(nk,na) = 1 - sqrt(MSE/A);
            
        end
    end
    
    figure
    plot(complx_arx,MDL_arx','-o'), grid on, title('ARX - MDL criterion')
    xlabel('Complexity'), ylabel('MDL value')
    legend('n_k = 1','n_k = 2','n_k = 3')

    figure
    plot(complx_arx,FIT_arx','-o'), grid on, title('ARX - FIT criterion')
    xlabel('Complexity'), ylabel('FIT value')
    legend('n_k = 1','n_k = 2','n_k = 3')
    
end

% The best model is 

best_arx = arx(ze,[7 7 1]); % MDL = 2.881 - FIT = 0.701

%% 3 - ARMAX predictor 
armax_res = 0;

if armax_res == 1
    for nk = 1:3
        for na = 1:10
            
            nb = na;
            nc = na;
            M = armax(ze,[na nb nc nk]);
            
            lag_max = 25;
            figure, resid(ze,M,'Corr',lag_max)
            
        end
        
        pause, close all
        
    end
end

% Threshold = 3
% > nk = 1 - na = nb = nc = 3:10
% > nk = 2 - na = nb = nc = 2:10
% > nk = 3 - na = nb = nc = 3:10

complx_armax = zeros(1,10);
MDL_armax = zeros(3,10);
FIT_armax = zeros(3,10);

if armax_res == 0
    for nk = 1:3
        for na = 1:10
            
            nb = na;
            nc = na;
            M = armax(ze,[na nb nc nk]);
            yh = compare(zv,M,1);
            
            n = na + nb + nc;
            complx_armax(na) = n;
            
            MSE = (1/(N-N0))*norm(yv(N0+1:end) - yh(N0+1:end))^2;
            MDL_armax(nk,na) = n*log(Nv - N0)/(Nv - N0) + log(MSE);
            
            A = (1/(N-N0))*norm(yv(N0+1:end) - mean(yv(N0+1:end)))^2;
            FIT_armax(nk,na) = 1 - sqrt(MSE/A);
            
        end
    end
    
    figure
    plot(complx_armax,MDL_armax','-o'), grid on, title('ARMAX - MDL criterion')
    xlabel('Complexity'), ylabel('MDL value')
    legend('n_k = 1','n_k = 2','n_k = 3')

    figure
    plot(complx_armax,FIT_armax','-o'), grid on, title('ARMAX - FIT criterion')
    xlabel('Complexity'), ylabel('FIT value')
    legend('n_k = 1','n_k = 2','n_k = 3')
    
end

% The best model is 

best_armax = armax(ze,[4 4 4 1]); % MDL = 2.561 - FIT = 0.7421

%% 4 - OE predictor 
oe_res = 0;

if oe_res == 1
    for nk = 1:3
        for nb = 1:10
            
            nf = nb;
            M = oe(ze,[nb nf nk]);
            
            lag_max = 25;
            figure, resid(ze,M,'Corr',lag_max)
            
        end
        
        pause, close all
        
    end
end

% Threshold = 3
% > nk = 1 - nf = nb = 3:10
% > nk = 2 - nf = nb = 2:10
% > nk = 3 - nf = nb = 2:8 and 10

complx_oe = zeros(1,10);
MDL_oe = zeros(3,10);
FIT_oe = zeros(3,10);

if oe_res == 0
    for nk = 1:3
        for nb = 1:10
            
            nf = nb;
            M = oe(ze,[nb nf nk]);
            yh = compare(zv,M,1);
            
            n = nb + nf;
            complx_oe(nb) = n;
            
            MSE = (1/(N-N0))*norm(yv(N0+1:end) - yh(N0+1:end))^2;
            MDL_oe(nk,nb) = n*log(Nv - N0)/(Nv - N0) + log(MSE);
            
            A = (1/(N-N0))*norm(yv(N0+1:end) - mean(yv(N0+1:end)))^2;
            FIT_oe(nk,nb) = 1 - sqrt(MSE/A);
            
        end
    end
    
    figure
    plot(complx_oe,MDL_oe','-o'), grid on, title('OE - MDL criterion')
    xlabel('Complexity'), ylabel('MDL value')
    legend('n_k = 1','n_k = 2','n_k = 3')

    figure
    plot(complx_oe,FIT_oe','-o'), grid on, title('OE - FIT criterion')
    xlabel('Complexity'), ylabel('FIT value')
    legend('n_k = 1','n_k = 2','n_k = 3')
    
end

% The best model is 

best_oe = oe(ze,[4 4 1]); % MDL = 2.512 - FIT = 0.7425

%% 5 - Model choice

best_model = best_oe;
present(best_model)
[A,B,C,D,F] = polydata(best_model);

% The tf of the model is 
% H(z) = 0
% G(z) = B(z)/F(z) = 2.043*z^-1 + 0.845*z^-2 + 0.3347*z^-3 - 0.1103*z^-4/
%                    1 - 2.176*z^-1 + 2.412*z^-2 - 1.912*z^-3 + 0.7517*z^-4

%% 6 - LS estimation 
PHI = [-y(4:N-1) -y(3:N-2) u(3:N-2) u(2:N-3) u(1:N-4)];
Y = y(5:N);

theta_LS = PHI\Y;
n = length(theta_LS);

%% 7 - Set Membership - EUI_2
A = inv(PHI'*PHI);
epsilon = 200;

EUI_2 = zeros(n,2);

for j = 1:n
    
    sigma = sqrt(A(j,j));
    EUI_2(j,1) = theta_LS(j) - sigma*epsilon;
    EUI_2(j,2) = theta_LS(j) + sigma*epsilon;
    
end

alpha_2 = norm(Y - PHI*theta_LS)^2;
epsilon_2 = epsilon^2;

if alpha_2 > epsilon_2 
    fprintf('PUI_2 CANNOT be computed \n alpha^2 = %d \n epsilon^2 = %d \n',...
        alpha_2,epsilon_2)
else
    fprintf('PUI_2 CAN be computed \n alpha^2 = %d \n epsilon^2 = %d \n',...
        alpha_2,epsilon_2)
end

%% 8 - Set Membership - EUI_inf
A = pinv(PHI);
epsilon = 20;

EUI_inf = zeros(n,2);

for j = 1:n
    
    EUI_inf(j,1) = A(j,:)*(Y - epsilon*sign(A(j,:))');
    EUI_inf(j,2) = A(j,:)*(Y + epsilon*sign(A(j,:))');
    
end






            
            
            