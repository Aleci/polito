close all, clear all

%% Step 1: Load data and plot it

load data1a

figure, 
subplot(211), plot(u)
subplot(212), plot(y)

%% Step 2: Remove mean value and plot the results

u = u-mean(u);
y = y-mean(y);

figure, 
subplot(211), plot(u)
subplot(212), plot(y)

%% Step 3: Define the parameters and plot the results

Ntot = length(u);
Ne = 4500;
Nv = Ntot-Ne;

ue = u(1:Ne);
ye = y(1:Ne);

uv = u(Ne+1:end);
yv = y(Ne+1:end);

Ze = [ye, ue];
Zv = [yv, uv];

figure, 
subplot(211), plot(1:Ne, ue, 'b', Ne+1:Ntot, uv, 'r')
subplot(212), plot(1:Ne, ye, 'b', Ne+1:Ntot, yv, 'r')

%% Step 4: ARX computation

for nk=1:3,
    for na=1:10,
        nb = na;
        
        model = arx(Ze,[na nb nk]);
        maxLag = 30;
%         figure, resid(Ze,model,'Corr',maxLag)
        
        yh = compare(Zv,model,inf);
        
        n = na;
        n_arx(na) = n;
        N0 = 15;
        
        MSE = 1/(Nv-N0)*norm(yv(N0+1:end)-yh(N0+1:end))^2;
        FPE_arx(nk,na) = (Nv-N0+n)/(Nv-N0-n)*MSE;
        MDL_arx(nk,na) = n*log(Nv-N0)/(Nv-N0)+log(MSE);
        
    end
%     pause, close all
end

FPE_arx, MDL_arx

figure, plot(n_arx,FPE_arx(1,:),'r',n_arx,FPE_arx(2,:),'g',n_arx,FPE_arx(3,:),'b')
title('FPE arx'), legend('nk=1','nk=2','nk=3')
figure, plot(n_arx,MDL_arx(1,:),'r',n_arx,MDL_arx(2,:),'g',n_arx,MDL_arx(3,:),'b')
title('MDL arx'), legend('nk=1','nk=2','nk=3')

%% Step 5: ARMAX computation

for nk=1:3,
    for na=1:10,
        nb = na;
        nc = na;
        
        model = armax(Ze,[na nb nc nk]);
        maxLag = 30;
%         figure, resid(Ze,model,'Corr',maxLag)
        
        yh = compare(Zv,model,inf);
        
        n = na;
        n_armax(na) = n;
        N0 = 15;
        
        MSE = 1/(Nv-N0)*norm(yv(N0+1:end)-yh(N0+1:end))^2;
        FPE_armax(nk,na) = (Nv-N0+n)/(Nv-N0-n)*MSE;
        MDL_armax(nk,na) = n*log(Nv-N0)/(Nv-N0)+log(MSE);
        
    end
%     pause, close all
end

FPE_armax, MDL_armax

figure, plot(n_armax,FPE_armax(1,:),'r',n_armax,FPE_armax(2,:),'g',n_armax,FPE_armax(3,:),'b')
title('FPE armax'), legend('nk=1','nk=2','nk=3')
figure, plot(n_armax,MDL_armax(1,:),'r',n_armax,MDL_armax(2,:),'g',n_armax,MDL_armax(3,:),'b')
title('MDL armax'), legend('nk=1','nk=2','nk=3')

%% Step 6: OE computation

for nk=1:3,
    for nf=1:10,
        nb = nf;
        
        model = oe(Ze,[nb nf nk]);
        maxLag = 30;
%         figure, resid(Ze,model,'Corr',maxLag)
        
        yh = compare(Zv,model,inf);
        
        n = nf;
        n_oe(nf) = n;
        N0 = 15;
        
        MSE = 1/(Nv-N0)*norm(yv(N0+1:end)-yh(N0+1:end))^2;
        FPE_oe(nk,nf) = (Nv-N0+n)/(Nv-N0-n)*MSE;
        MDL_oe(nk,nf) = n*log(Nv-N0)/(Nv-N0)+log(MSE);
        
    end
%     pause, close all
end

FPE_oe, MDL_oe

figure, plot(n_oe,FPE_oe(1,:),'r',n_oe,FPE_oe(2,:),'g',n_oe,FPE_oe(3,:),'b')
title('FPE oe'), legend('nk=1','nk=2','nk=3')
figure, plot(n_oe,MDL_oe(1,:),'r',n_oe,MDL_oe(2,:),'g',n_oe,MDL_oe(3,:),'b')
title('MDL oe'), legend('nk=1','nk=2','nk=3')

%% Step 7: computation of best model tranfer functions

best = oe(Ze,[3 3 1]);
[A B C D F ] = polydata(best);

G_z = tf(B,F,1,'Variable','z^-1');
H_z = 1;

%% Step 8: computation of ARX(2,3,2) of RLS-1 and LS algorithms

close all, clear all
load data1a

u = u-mean(u);
y = y-mean(y);

N = length(u);
na = 2;
nb = 3;
nk = 2;

n = na+nb;
alpha = 1;
V = alpha*eye(n);
S = inv(V);
t_min = max([na+1,nb+nk]);
Phi = [];
Y = y(t_min:N);
theta_RLS1 = zeros(n,N);

for t=t_min:N,
    phi_RLS1 = [-y(t-1) -y(t-2) u(t-2) u(t-3) u(t-4)]';
    Phi = [Phi;phi_RLS1'];
    S = S + phi_RLS1*phi_RLS1';
    K = S\phi_RLS1;
    e(t) = y(t)-phi_RLS1'*theta_RLS1(:,t-1);
    theta_RLS1(:,t) = theta_RLS1(:,t-1) + K*e(t);
end

theta_RLS1_final = theta_RLS1(:,N);
theta_LS = Phi\Y;

% figure, plot(1:N,theta_RLS1(1,:)), refline(0,theta_LS(1))
% figure, plot(1:N,theta_RLS1(2,:)), refline(0,theta_LS(2))
% figure, plot(1:N,theta_RLS1(3,:)), refline(0,theta_LS(3))
% figure, plot(1:N,theta_RLS1(4,:)), refline(0,theta_LS(4))
% figure, plot(1:N,theta_RLS1(5,:)), refline(0,theta_LS(5))

%% Step 9: computation of EUI2 and if possible of PUI2

eps = 2;
sigma = sqrt(diag(inv(Phi'*Phi)));

% EUI2
theta_min_EUI2 = theta_LS-eps*sigma;
theta_max_EUI2 = theta_LS+eps*sigma;

EUI2 = [theta_min_EUI2, theta_max_EUI2];

% PUI2

alfa = norm(Y-Phi*theta_LS);

if alfa^2 <= eps^2,
    theta_min_PUI2 = theta_LS - sigma*sqrt(eps^2-alfa^2);
    theta_max_PUI2 = theta_LS + sigma*sqrt(eps^2-alfa^2);
else
    display('Alfa^2 is greater than eps^2 therefore PUI2 intervals cannot be computed')
end

%% Step 10: computation of EUIinf

eps_inf = 0.1;
A = pinv(Phi);

for j=1:n,
    theta_min_EUIinf(j,1) = A(j,:)*(Y - eps_inf*sign(A(j,:))');
end

theta_max_EUIinf = 2*theta_LS - theta_min_EUIinf;

EUIinf = [theta_min_EUIinf,theta_max_EUIinf];


    




        
        
        
        
        





