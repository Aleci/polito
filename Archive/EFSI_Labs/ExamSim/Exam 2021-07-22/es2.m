clear all, close all

load data2a

%% Step 1: Definition of parameters
A = [2.7258 -6.212 -18.9425; 0.4 0 0; 0 -0.1 0];
B = [0.13 0 0]';
C = [0.8 -1.8 1.0];
D = 0;

N = length(u);
[n,nn] = size(A);

Bv1 = 0.05*[0.13 0 0]';
V1 = Bv1*Bv1';
V2 = 0.01;
V12 = 0;

P{1} = eye(3);

%% Step 2: Simulation
x(:,1) = [-10 20 -10]';

for t=1:N,
    v1(:,t) = mvnrnd(zeros(n,1),V1);
    v2(t) = sqrt(V2)*randn;
    x(:,t+1) = A*x(:,t) + B*u(t) + v1(:,t);
    y(t) = C*x(:,t) + v2(t);
end

figure, plot(1:N,x(1,1:N)), grid on, title('Sim state x_1(t)')
figure, plot(1:N,x(2,1:N)), grid on, title('Sim state x_2(t)')
figure, plot(1:N,x(3,1:N)), grid on, title('Sim state x_3(t)')
figure, plot(1:N,y(1:N)), grid on, title('Sim output y(t)')

%% Step 3: Steady state filter Finf 

x_h_ss(:,1) = zeros(n,1);
S = ss(A,[B,eye(n)],C,[D,zeros(1,n)],1);
[Kalman_predictor,Kbar,Pbar,K0bar]=kalman(S,V1,V2,0);

for t=1:N,
    y_h_ss(t) = C*x_h_ss(:,t);
    e(t) = y(t) - y_h_ss(t);
    x_f_ss(:,t) = x_h_ss(:,t) + K0bar*e(t);
    y_f_ss(t) = C*x_f_ss(:,t);
    x_h_ss(:,t+1) = A*x_h_ss(:,t) + B*u(t) + Kbar*e(t);
end    

Kbar,K0bar

figure, plot(1:N,x(1,1:N),1:N,x_f_ss(1,1:N)), title('Simulation vs SS filter state x_1(t)'), legend('Simulated','Filter')
figure, plot(1:N,x(2,1:N),1:N,x_f_ss(2,1:N)), title('Simulation vs SS filter state x_2(t)'), legend('Simulated','Filter')
figure, plot(1:N,x(3,1:N),1:N,x_f_ss(3,1:N)), title('Simulation vs SS filter state x_3(t)'), legend('Simulated','Filter')
figure, plot(1:N,y(1:N),1:N,y_f_ss(1:N)), title('Simulation vs SS filter output y(t)'), legend('Simulated','Filter')

%% Step 4: One step predictor Kd in standard form

x_h(:,1) = zeros(n,1);

for t=1:N,
    
    K{t} = (A*P{t}*C' + V12)/(C*P{t}*C'+V2);
    P{t+1} = A*P{t}*A' + V1 - K{t}*(C*P{t}*C' + V2)*K{t}';
    
    y_h(t) = C*x_h(:,t);
    e(t) = y(t) - y_h(t);
    x_h(:,t+1) = A*x_h(:,t) + B*u(t) + K{t}*e(t);
end

Kd_final = K{N};

figure, plot(1:N,x(1,1:N),1:N,x_f_ss(1,1:N),1:N,x_h(1,1:N)), title('Simulation vs SS filter vs 1-step Predictor state x_1(t)'), legend('Simulated','Filter','Predictor')
figure, plot(1:N,x(2,1:N),1:N,x_f_ss(2,1:N),1:N,x_h(2,1:N)), title('Simulation vs SS filter vs 1-step Predictor state x_2(t)'), legend('Simulated','Filter','Predictor')
figure, plot(1:N,x(3,1:N),1:N,x_f_ss(3,1:N),1:N,x_h(3,1:N)), title('Simulation vs SS filter vs 1-step Predictor state x_3(t)'), legend('Simulated','Filter','Predictor')
figure, plot(1:N,y(1:N),1:N,y_f_ss(1:N),1:N,y_h(1:N)), title('Simulation vs SS filter vs 1-step Predictor output y(t)'), legend('Simulated','Filter','Predictor')

%% Step 5: RMSE computation

N0_vect = [0 20 100];

for ind=1:length(N0_vect),
    N0 = N0_vect(ind);
    
    for k=1:n,
        RMSE_Finf_x(ind,k) = norm(x(k,N0+1:N)-x_f_ss(N0+1:N))/sqrt(N-N0);
        RMSE_Kd_x(ind,k) = norm(x(k,N0+1:N)-x_h(N0+1:N))/sqrt(N-N0);
    end
    RMSE_Finf_y(ind) = norm(y(N0+1:N)-y_f_ss(N0+1:N))/sqrt(N-N0);
    RMSE_Kd_y(ind) = norm(y(N0+1:N)-y_h(N0+1:N))/sqrt(N-N0);
end

RMSE_Finf_x,RMSE_Kd_x
RMSE_Finf_y,RMSE_Kd_y
