%% Workspace initialization
clear all; close all; clc; %format short e
load heater.mat

%% Data preprocessing
ueOld=ue; ue=ue-mean(ue);
uvOld=uv; uv=uv-mean(uv);
yeOld=ye; ye=ye-mean(ye);
yvOld=yv; yv=yv-mean(yv);
ed=[ye ue];
vd=[yv uv];

%% Model caching
arx_cache=cell(5,5);
armax_cache=cell(5,5);
oe_cache=cell(5,5);
for k=1:5
    for n=1:5
        arx_cache{n,k}=arx(ed,[n,n,k]);
        %armax_cache{n,k}=armax(ed,[n,n,n,k]);
        oe_cache{n,k}=oe(ed,[n,n,k]); 
    end
end

%% Arx
[arx_rmse arx_fpe arx_aic arx_mdl arx_bf]=modEval(arx_cache,ed,vd,2,"ARX");
easyPlot(arx_mdl,"arx mdl") %best tradeoff na=nb=2 nk=4
easyPlot(arx_rmse,"arx rmse") %best tradeoff na=nb=2 nk=4

%% Armax (not required)
%[armax_rmse armax_fpe armax_aic armax_mdl armax_bf]=modEval(armax_cache,ed,vd,3,"ARMAX");
%easyPlot(armax_mdl,"armax mdl")
%easyPlot(armax_rmse,"armax rmse")

%% Oe
[oe_rmse oe_fpe oe_aic oe_mdl oe_bf]=modEval(oe_cache,ed,vd,2,"OE");
easyPlot(oe_mdl,"oe mdl")
easyPlot(oe_rmse,"oe rmse") % all discarded

%% Debug
%m=arx(ed,[2,2,5]);
%whiteTest(m,ed,30) %unsuppress the resid output

%% Functions
function easyPlot(test,mytitle)
    figure,plot(1:5,test(:,1),1:5,test(:,2),1:5,test(:,3),1:5,test(:,4),1:5,test(:,5)) 
    title(mytitle),legend("k=1","k=2","k=3","k=4","k=5")
end

function [m_rmse m_fpe m_aic m_mdl m_bf]=modEval(cache,ed,vd,cost,name)
    for k=1:5
        for n=1:5
            m=cache{n,k};
            
        % White test on estimation dataset
            [passed,nOut,nOutMax]=whiteTest(m,ed,30);
            nOutM(n,k)=nOut;
            passedM(n,k)=passed;

        % Cross validation on validation dataset
            [m_rmse(n,k) m_fpe(n,k) m_aic(n,k) m_mdl(n,k) m_bf(n,k)]=crossValidation(vd,m,n*cost,10);
        end
    end
    [rmse_normalized,minval]=normalizeWhite(m_rmse,passedM,false);

    % Print results
    fprintf("%s results:\nk\\n ->\n|\nv\n",name)
    nOut=nOutM',passed=passedM';threshold=nOutMax;
    rmse_normalized=rmse_normalized',minval
end

function [normStat val]=normalizeWhite(stat,pass,max)
    A=stat.*pass; % set to zero the non white element
    B=sort(reshape(A,1,5*5)); %arrange to find the min nonzero element
    j=find(B);
    if(isempty(j))
        normStat=0;val=0;return
    end
    if(max)
        val=B(j(end)); %take the max
    else
        val=B(j(1)); %take the min
    end
    normStat=A/val;
end

function [rmse fpe aic mdl bf]=crossValidation(dataset,m,order,skip)
    y=dataset(:,1);
    N=length(y);
    yhat=compare(dataset,m,1);
    err=y-yhat;
    err=err(skip+1:end);
    mse=mean(err.^2);
    rmse=sqrt(mse);
    fpe=fpeTest(m,mse,order,N-skip);
    aic=aicTest(m,mse,order,N-skip);
    mdl=mdlTest(m,mse,order,N-skip);
    bf=bfTest(m,mse,y,order,N-skip);
end

function bf=bfTest(m,mse,y,n,N)
    bf=1-sqrt(mse/mean( (y-mean(y)).^2 ));
end

function mdl=mdlTest(m,mse,n,N)
    mdl=n*log(N)/N+log(mse);
end

function aic=aicTest(m,mse,n,N)
    aic=n*2/N+log(mse);
end

function fpe=fpeTest(m,mse,n,N)
    fpe=(N+n)/(N-n)*mse;
end

function [passed,nOut,nOutMax]=whiteTest(m,dataset,samples)
    confidence=.99;
    % output prediction
    y=dataset(:,1); N=length(y); 
    yhat=compare(dataset,m,1);
    % prediction error
    err=y-yhat;
    % error autocorrelation in absolute value
    [r,lag]=xcorr(err,'normalized');
    rPrime=abs(r(N+1:end)); %skip lag 0
    % limit due to the expected variance [1/sqrt(N)]
    limit=sqrt(2)*erfinv(confidence)/sqrt(N);
    % test
    nOut=sum(rPrime(1:samples) > limit);
    nOutMax=floor(samples*(1-confidence)); %definition
    nOutMax=3; %custom threshold
    passed= nOut <= nOutMax;
    if false % resid plot suppression
       figure,plot(1:samples+1,r(N:N+samples),"*")
       refline(0,limit),refline(0,-limit),nOut,nOutMax
       figure,resid(dataset,m,samples)
    end
end