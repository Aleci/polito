%% Workspace initialization
clear all; close all; clc; format short e
load heater.mat

%% Data preprocessing
ueOld=ue; ue=ue-mean(ue);
uvOld=uv; uv=uv-mean(uv);
yeOld=ye; ye=ye-mean(ye);
yvOld=yv; yv=yv-mean(yv);
ed=[ye ue];
vd=[yv uv];

%% Model caching
Nhidden=6;
NetDef=netDef(Nhidden);
% Increase iterations for the non linear optimization algoritm
trparms=settrain; trparms=settrain(trparms,'maxiter',500);
W1=cell(5,5);
W2=cell(5,5);
for k=1:5
    for n=1:5    %% PAY ATTENTION TO THE DATASET TRANSPOSE HERE!
        [W1{n,k} W2{n,k}]=nnarx(NetDef,[n,n,k],[],[],trparms,ye',ue');
    end
end

%% NNARX
[nnarx_rmse nnarx_fpe nnarx_aic nnarx_mdl nnarx_bf]=modEval(W1,W2,ed,vd,Nhidden+1,"NNARX");
easyPlot(nnarx_mdl,"nnarx mdl") %best tradeoff na=nb=2 nk=3
easyPlot(nnarx_rmse,"nnarx rmse") %best tradeoff na=nb=2 nk=3

%% Functions
function [m_rmse m_fpe m_aic m_mdl m_bf]=modEval(cacheW1,cacheW2,ed,vd,cost,name)
    for k=1:5
        for n=1:5
            yhatEST=predictionFromNN(cacheW1{n,k},cacheW2{n,k},ed,n,k);
            yhatVAL=predictionFromNN(cacheW1{n,k},cacheW2{n,k},vd,n,k);
            % REMEMBER TO TRANSPOSE THE ESTIMATED OUTPUT YHAT!
            
        % White test on estimation dataset
            [passed,nOut,nOutMax]=whiteTest(yhatEST',ed,30);
            nOutM(n,k)=nOut;
            passedM(n,k)=passed;

        % Cross validation on validation dataset
            [m_rmse(n,k) m_fpe(n,k) m_aic(n,k) m_mdl(n,k) m_bf(n,k)]=crossValidation(vd,yhatVAL',n*cost,10);
        end
    end
    % Print results
    fprintf("%s results:\nk\\n ->\n|\nv\n",name)
    nOut=nOutM',passed=passedM',threshold=nOutMax
end

function yhat=predictionFromNN(W1,W2,dataset,n,k)
    y=dataset(:,1);
    u=dataset(:,2);
    N=length(y);
    % Coefficient extraction
    alfa=W2(1:end-1);
    alfa0=W2(end);
    beta=W1(:,1:end-1);
    beta0=W1(:,end);
    % Output simulation
    % t_min should be at least max[na+1,nb+nk]
    t_min=n+k; % na=nb, k>0
    for t=t_min:N
        phi= [y(t-1:-1:t-n); u(t-k:-1:t-k-n+1)];
        yhat(t)=alfa*tanh(beta*phi+beta0)+alfa0;
    end % before t_min yhat is set to 0
end

function [rmse fpe aic mdl bf]=crossValidation(dataset,yhat,order,skip)
    y=dataset(:,1);
    N=length(yhat);
    N=length(y);
    err=y-yhat;
    err=err(skip+1:end);
    mse=mean(err.^2);
    rmse=sqrt(mse);
    fpe=fpeTest(mse,order,N-skip);
    aic=aicTest(mse,order,N-skip);
    mdl=mdlTest(mse,order,N-skip);
    bf=bfTest(mse,y,order,N-skip);
end

function bf=bfTest(mse,y,n,N)
    bf=1-sqrt(mse/mean( (y-mean(y)).^2 ));
end

function mdl=mdlTest(mse,n,N)
    mdl=n*log(N)/N+log(mse);
end

function aic=aicTest(mse,n,N)
    aic=n*2/N+log(mse);
end

function fpe=fpeTest(mse,n,N)
    fpe=(N+n)/(N-n)*mse;
end

function [passed,nOut,nOutMax]=whiteTest(yhat,dataset,samples)
    confidence=.99;
    % prediction error
    y=dataset(:,1);
    N=length(y);
    err=y-yhat;
    % error autocorrelation in absolute value
    [r,lag]=xcorr(err,'normalized');
    rPrime=abs(r(N+1:end)); %skip lag 0
    % limit due to the expected variance [1/sqrt(N)]
    limit=sqrt(2)*erfinv(confidence)/sqrt(N);
    % test
    nOut=sum(rPrime(1:samples) > limit);
    nOutMax=floor(samples*(1-confidence)); %definition
    nOutMax=3; %custom threshold
    passed= nOut <= nOutMax;
    if false % resid-like plot suppression
       figure,plot(1:samples+1,r(N:N+samples),"*")
       refline(0,limit),refline(0,-limit),nOut,nOutMax
    end
end

function net=netDef(n)
    a='H';b='L';
    for i=2:n
        a=[a 'H'];
        b=[b '-'];
    end
    net=[a; b];
end

function easyPlot(test,mytitle)
    figure,plot(1:5,test(:,1),1:5,test(:,2),1:5,test(:,3),1:5,test(:,4),1:5,test(:,5)) 
    title(mytitle),legend("k=1","k=2","k=3","k=4","k=5")
end

