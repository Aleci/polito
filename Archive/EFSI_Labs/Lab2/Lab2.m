%% Problem 2 (Linear regression)
clear all; close all; clc; format short e
load("SensorData.mat"); err=5e-4;

% Linear regression of the linear part only (data trim shrinked)
var=1;
while(z(var)<0.013)
    var=var+1;
end;  minIndex=var;
while(z(var)<0.035)
    var=var+1;
end;  maxIndex=var;
Vztrim=Vz(minIndex:maxIndex); ztrim=z(minIndex:maxIndex);
Ntrim=length(Vztrim); n=2;

% Least square estimator
fi=[Vztrim ones(size(Vztrim))];
A=pinv(fi);
thetaLS=A*ztrim;
KtLS=1/thetaLS(1);
V0LS=-thetaLS(2)/thetaLS(1);

% EUI calculation
for j=1:n
    thetaMin(j)=0;
    for k=1:Ntrim
        thetaMin(j)=thetaMin(j) + A(j,k) * ( ztrim(k) - (err)*sign(A(j,k)) );
    end
    thetaMax(j)=2*thetaLS(j) - thetaMin(j);
end
rangeKtSM=[1/thetaMax(1) 1/thetaMin(1)];
rangeV0SM=[-thetaMax(2)/thetaMin(1) -thetaMin(2)/thetaMax(1)];

% Confidence interval of the estimate, with a priori assumptions
std_devData=2.5e-4; % <= max error .5 mm with confidence .95
covData=diag(std_devData^2*ones(1,length(Vztrim)));
covTheta=std_devData^2*inv(fi' * fi); 
std_devTheta1=sqrt(covTheta(1,1));
std_devTheta2=sqrt(covTheta(2,2));
rangeTheta1=[thetaLS(1)-2*std_devTheta1 thetaLS(1)+2*std_devTheta1];
rangeTheta2=[thetaLS(2)-2*std_devTheta2 thetaLS(2)+2*std_devTheta2];
rangeKtSTA=[1/max(rangeTheta1) 1/min(rangeTheta1)];
rangeV0STA=[-max(rangeTheta2)/min(rangeTheta1) -min(rangeTheta2)/max(rangeTheta1)];

% PUI calculation
% min c'*x s.t. A*x<=b
A=[fi; -fi];
b=[ztrim+err; -ztrim+err];

c=[1,0]; % min theta1
PUI1(1)=[1,0]*linprog(c,A,b);
c=[-1, 0]; % max theta1
PUI1(2)=[1,0]*linprog(c,A,b);

c=[0,1]; % min theta2
PUI2(1)=[0,1]*linprog(c,A,b);
c=[0,-1]; % max theta2
PUI2(2)=[0,1]*linprog(c,A,b);

% Uncertainty Intervals
minRangeKtSM=[1/PUI1(2), 1/PUI1(1)];
minRangeV0SM=[-PUI2(2)/PUI1(1), -PUI2(1)/PUI1(2)];

% Central Estimator
thetaCE=[sum(PUI1)/2, sum(PUI2)/2];
KtCE=1/thetaCE(1);
V0CE=-thetaCE(2)/thetaCE(1);

% Method comparison and results presentation
clc
KtLS, intervalLS=abs(diff(rangeKtSTA))
KtCE, intervalSM=abs(diff(minRangeKtSM))

V0LS, intervalLS=abs(diff(rangeV0STA))
V0CE, intervalSM=abs(diff(minRangeV0SM))

plot(z,Vz,'k*'), hold on
plot(z,KtLS*z+V0LS,'r');
plot(z,KtCE*z+V0CE,'g');
legend("Data","STAT","SETMEMB")