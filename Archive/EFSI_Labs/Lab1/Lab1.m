% Problem 2 (Linear regression)
clear all; close all; clc
% Linear regression of all the data
load("SensorData.mat");
fiTot=[Vz ones(size(Vz))];
thetaTot=pinv(fiTot)*z;
KtTot=1/thetaTot(1);
V0Tot=-thetaTot(2)/thetaTot(1);
figure(2), plot(z,Vz,'k*')
hold on 
plot(z,KtTot*z+V0Tot,'b')
% Linear regression of the linear part only (data trim)
var=1;
while(z(var)<0.01)
    var=var+1;
end;  minIndex=var;
while(z(var)<0.035)
    var=var+1;
end;  maxIndex=var;
Vztrim=Vz(minIndex:maxIndex); ztrim=z(minIndex:maxIndex);

fi=[Vztrim ones(size(Vztrim))];
theta=pinv(fi)*ztrim;
Kt=1/theta(1);
V0=-theta(2)/theta(1);
plot(z,Kt*z+V0,'r')

% Problem 3 (Confidence interval of the estimate, with a priori assumptions)
std_devData=2.5e-4; % <= max error .5 mm with confidence .95
covData=diag(std_devData^2*ones(1,length(Vztrim)))
%covTheta=inv(fi'*covData*fi) %general formula, but badly approximated
covTheta=std_devData^2*inv(fi' * fi) %specific formula, more precise
std_devTheta1=sqrt(covTheta(1,1))
std_devTheta2=sqrt(covTheta(2,2))
rangeTheta1=[theta(1)-2*std_devTheta1 theta(1)+2*std_devTheta1]
rangeTheta2=[theta(2)-2*std_devTheta2 theta(2)+2*std_devTheta2]
rangeKt=[1/max(rangeTheta1) 1/min(rangeTheta1)]
rangeV0=[-max(rangeTheta2)/min(rangeTheta1) -min(rangeTheta2)/max(rangeTheta1)]
% OCCHIO IL GRAFICO HA UNA TOLLERANZA DIVERSA DA QUELLA DEL PROF!
plot(z,min(rangeKt(1)*z,rangeKt(2)*z)+rangeV0(1),'m.')
plot(z,max(rangeKt(1)*z,rangeKt(2)*z)+rangeV0(2),'m.')

% Problem 3bis (Confidence interval of the estimate, no clue)
varDataAVG=1/length(ztrim) * (ztrim-fi*theta)'*(ztrim-fi*theta)
covDataGauss=diag(varDataAVG*ones(1,length(Vztrim)))
covThetaGauss=varDataAVG*inv(fi' * fi) %specific formula, more precise
std_devTheta1Gauss=sqrt(covThetaGauss(1,1))
std_devTheta2Gauss=sqrt(covThetaGauss(2,2))
rangeTheta1Gauss=[theta(1)-2*std_devTheta1Gauss theta(1)+2*std_devTheta1Gauss]
rangeTheta2Gauss=[theta(2)-2*std_devTheta2Gauss theta(2)+2*std_devTheta2Gauss]
rangeKt=[1/max(rangeTheta1Gauss) 1/min(rangeTheta1Gauss)]
rangeV0=[-max(rangeTheta2Gauss)/min(rangeTheta1Gauss) -min(rangeTheta2Gauss)/max(rangeTheta1Gauss)]

% Problem 4
mypoly=polyfit(z,Vz,3)
for i=1:length(z)
    Vpoly(i)=polyval(mypoly,z(i))
end
plot(z,Vpoly,'g')