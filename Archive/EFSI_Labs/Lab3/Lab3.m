%% LAB 3 Dataset 1
clear all; close all; clc
load("ResistorData1.mat")
figure(1), title("Dataset 1"), hold on, plot(i,V,'k-o');
N1=20; Variance1=25; SigmaEE=Variance1*eye(20);

% Estimate using Sample quantities
RawR=V./i;
Rsm=mean(RawR)
Rsm=V' * (1./i) / N1            %explicit formula
RsmSampleVar=var(RawR)
RsmSampleVar=(RawR - Rsm)' * (RawR - Rsm) / (N1 -1) %explicit formula
plot(i,Rsm*i,'r')

% Estimate using Least squares method
Phi=i;
Rls=pinv(Phi)*V
Rls=Phi\V           % more reliable
RlsVar=Variance1 * inv(Phi' * Phi)
plot(i,Rls*i,'g')

% Estimate using Gauss Markov method (Optimal weighted least squares choice)
% With the current assumption is equivalent to the Least squares method
Qgm=inv(SigmaEE);
Rgm=inv(Phi'* Qgm * Phi) * Phi' * Qgm * V % WLS formula | Q=Qgm
RgmVar=pinv(Phi) * SigmaEE * pinv(Phi)'

% Estimate using Bayesian method
% Assumptions
SigmaRE=zeros(1,N1); % uncorrelation between innovation and the estimate
% The result are almost the same in the case of initial assumption equal to:
%     - random guess + high variance (closer to Rgm when variance gets higher)
%     - sample mean + sample variance
%     - gauss markov estimate + gauss markov variance => Rb=Rls=Rgm
Rprior=3;%Rsm;%Rgm;  % mean assumption       (prof: try to use Rsm or 3)
SigmaRR=10;%RsmSampleVar;%RgmVar; % variance assumption  (prof: try to use [10, 1, .1, .01])
% Derived quantities
Vbar=Rprior*i;
SigmaVV=SigmaRR*i*i' + SigmaEE; % SigmaVV=E[(V-Vbar)(V-Vbar)] rewritten in terms of SigmaRR and SigmaEE
SigmaRV=SigmaRR*i' + SigmaRE; % SigmaRV=E[(R-Rprior)(V-Vbar)] rewritten in terms of SigmaRR and SigmaRE
% Estimate
Rb=Rprior + SigmaRV*inv(SigmaVV) * (V-Vbar)
RbVar=SigmaRR - SigmaRV*inv(SigmaVV)*SigmaRV'
plot(i,Rb*i,'b--'), legend("Data","Sample Mean","Least Squares","Bayesian","location","northwest")

% Comments
% 1) If Rgm is used as Rprior assumption the estimate is equivalent to the WLS one,
% no matter the choice of the SigmaRR, since the estimator will converge to that value.
% 2) If Rsm is used as Rprior assumption the estimate is equivalent to the WLS one only if
% the choice of SigmaRR is worst or equal to the Sample variance.

%% LAB 3 Dataset 2
% clear all; close all; clc;
load("ResistorData2.mat")
figure(2), title("Dataset 2"), hold on, plot(i,V,'k-o');
N1=20; Variance2=25; SigmaEE=Variance2*eye(20); SigmaEE(7)=125;

% Estimate using Sample quantities
RawR=V./i;
Rsm=mean(RawR)
Rsm=V' * (1./i) / N1            %explicit formula
RsmSampleVar=var(RawR)
RsmSampleVar=(RawR - Rsm)' * (RawR - Rsm) / (N1 -1) %explicit formula
plot(i,Rsm*i,'g')

% Estimate using Least squares method
Phi=i;
Rls=pinv(Phi)*V
Rls=Phi\V           % more reliable
RlsVar=Variance2 * inv(Phi' * Phi)
plot(i,Rls*i,'b')

% Estimate using Gauss Markov method (Optimal weighted least squares choice)
Qgm=inv(SigmaEE);
Rgm=inv(Phi'* Qgm * Phi) * Phi' * Qgm * V % WLS formula | Q=Qgm
RgmVar=pinv(Phi) * SigmaEE * pinv(Phi)'
plot(i,Rgm*i,'c')

% Estimate using Bayesian method
% Assumptions
SigmaRE=zeros(1,N1); % uncorrelation between innovation and the estimate
Rprior=3;  % mean assumption       (prof: try to use Rgm or 3)
SigmaRR=1; % variance assumption  (prof: try to use [10, 1, .1, .01])
% Derived quantities
Vbar=Rprior*i;
SigmaVV=SigmaRR*i*i' + SigmaEE; % SigmaVV=E[(V-Vbar)(V-Vbar)] rewritten in terms of SigmaRR and SigmaEE
SigmaRV=SigmaRR*i' + SigmaRE; % SigmaRV=E[(R-Rprior)(V-Vbar)] rewritten in terms of SigmaRR and SigmaRE
% Estimate
Rb=Rprior + SigmaRV*inv(SigmaVV) * (V-Vbar)
RbVar=SigmaRR - SigmaRV*inv(SigmaVV)*SigmaRV'
plot(i,Rb*i,'r--'), legend("Data","Sample Mean","Least Squares","Gauss Markov","Bayesian","location","southeast")

% Comments
% 1) Even if the Rprior assumption is wrong a small mistake is made if the
% variance is kept high in the assumption phase